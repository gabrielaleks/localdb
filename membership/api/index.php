<?php

require_once __DIR__ . '/vendor/autoload.php';

use Atlas\Membership\Shared\Infrastructure\ConnectionFactory;
use Doctrine\DBAL\Driver\Connection;
use Atlas\Membership\Member\Infrastructure\Web\MemberRoutes;
use DI\ContainerBuilder;
use Slim\Exception\HttpNotFoundException;
use Slim\Factory\AppFactory;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Atlas\Membership\Member\Dependencies as MemberDependencies;

Dotenv\Dotenv::createUnsafeImmutable(__DIR__)->load();

$containerBuilder = new ContainerBuilder();

$baseDefinitions = [
    Connection::class => function () {
        return ConnectionFactory::getConnection();
    },
];

$containerBuilder->addDefinitions(
    array_merge(
        $baseDefinitions,
        MemberDependencies::definitions()
    )
);

$container = $containerBuilder->build();

$app = AppFactory::createFromContainer($container);

$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$errorMiddleware->setDefaultErrorHandler(function ($request, $exception) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    $code = $exception->getCode() ? $exception->getCode() : 400;
    $response->getBody()->write(json_encode([
        'status' => 'error',
        'code' => $code,
        'message' => $exception->getMessage(),
        'timestamp' => date('Y-m-d H:i:s'),
    ]));

    return $response->withHeader('Content-Type', 'application/json')->withStatus($code);
});

$app->setBasePath('/api');
MemberRoutes::addRoutes($app);

$app->any('/{routes:.+}', function (Request $request, Response $response) {
    throw new HttpNotFoundException($request, "Route not found.");
});

$app->run();
