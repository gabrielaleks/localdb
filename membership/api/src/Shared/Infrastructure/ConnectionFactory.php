<?php

declare(strict_types=1);

namespace Atlas\Membership\Shared\Infrastructure;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\OCI8\Driver as OCI8Driver;
use Doctrine\DBAL\DriverManager;

class ConnectionFactory
{
    private static function getParameters(): array
    {
        return [
            'driverClass' => OCI8Driver::class,
            'dbname'      => getenv("DB_DNS"),
            'user'        => getenv("DB_USERNAME"),
            'password'    => getenv("DB_PASSWORD")
        ];
    }

    private static function getDBALConfiguration(): Configuration
    {
        $configuration = new Configuration();
        $configuration->setSQLLogger(new QueryLogger());

        return $configuration;
    }

    public static function getConnection(): Connection
    {
        $parameters = self::getParameters();

        $configuration = self::getDBALConfiguration();

        $connection = DriverManager::getConnection(
            $parameters,
            $configuration
        );

        /**
         * A Doctrine\DBAL\Connection, by default, operates in auto-commit mode, which means it is non-transactional
         * unless a transaction is started explicitly via beginTransaction(). By disabling auto-commit, a new
         * transaction is opened on connect() and after commit() or rollBack().
         *  */
        $connection->setAutoCommit(false);

        return $connection;
    }
}
