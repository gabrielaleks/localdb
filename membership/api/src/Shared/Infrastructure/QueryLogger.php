<?php

declare(strict_types=1);

namespace Atlas\Membership\Shared\Infrastructure;

use Doctrine\DBAL\Logging\SQLLogger;

class QueryLogger implements SQLLogger
{
    private $startTime;

    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
        $logger = MonologLogger::logger();
        $this->startTime = microtime(true);

        $logger->info('[SQL] Query: ' . $sql . "\n");

        if ($params) {
            $logger->info('[SQL] Parameters: ' . json_encode($params) . "\n");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
        $logger = MonologLogger::logger();
        $executionTime = microtime(true) - $this->startTime;
        $logger->info('[SQL] Query execution time: ' . $executionTime . ' seconds.' . "\n");
    }
}
