<?php

declare(strict_types=1);

namespace Atlas\Membership\Shared\Infrastructure;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as Logger;

class MonologLogger
{
    public static function logger(): Logger
    {
        $logger = new Logger('logger');
        $lineFormatter = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n", "Y-m-d H:i:s.u");

        // File handler
        $streamHandler = new StreamHandler(getenv('LOG_PATH') . '/application.log', Logger::DEBUG);
        $streamHandler->setFormatter($lineFormatter);
        $logger->pushHandler($streamHandler);

        return $logger;
    }
}
