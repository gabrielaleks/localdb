<?php

namespace Atlas\Membership\Member\Application\DeleteStatus;

use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;

class DeleteStatusHandler
{
    private $statusReadRepository;
    private $statusWriteRepository;

    public function __construct(
        StatusReadRepositoryInterface $statusReadRepository,
        StatusWriteRepositoryInterface $statusWriteRepository
    ) {
        $this->statusWriteRepository = $statusWriteRepository;
        $this->statusReadRepository = $statusReadRepository;
    }

    public function handle(DeleteStatusCommand $command): void
    {
        $status = $this->statusReadRepository->findById($command->id());
        if (!$status) {
            throw new \Exception("No status with id " . $command->id() . " found.");
        }
        $this->statusWriteRepository->deleteStatus($status);
    }
}
