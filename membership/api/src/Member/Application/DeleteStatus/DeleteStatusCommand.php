<?php

namespace Atlas\Membership\Member\Application\DeleteStatus;

final class DeleteStatusCommand
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromHttpRequest(int $id): self
    {
        return new self($id);
    }

    public function id(): int
    {
        return $this->id;
    }
}
