<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\RegisterStatus;

final class RegisterStatusCommand
{
    private $name;
    private $agentId;

    public function __construct(
        string $name,
        int $agentId
    ) {
        $this->name = $name;
        $this->agentId = $agentId;
    }

    public static function fromHttpRequest(array $input): self
    {
        $command = new self(
            $input['name'],
            $input['agentId']
        );

        return $command;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function agentId(): int
    {
        return $this->agentId;
    }
}
