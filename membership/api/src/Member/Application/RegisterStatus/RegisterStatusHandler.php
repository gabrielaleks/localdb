<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\RegisterStatus;

use Atlas\Membership\Member\Domain\IntegerId;
use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;

class RegisterStatusHandler
{
    private $statusReadRepository;
    private $statusWriteRepository;

    public function __construct(
        StatusReadRepositoryInterface $statusReadRepository,
        StatusWriteRepositoryInterface $statusWriteRepository
    ) {
        $this->statusWriteRepository = $statusWriteRepository;
        $this->statusReadRepository = $statusReadRepository;
    }

    public function handle(RegisterStatusCommand $command): Status
    {
        $id = new IntegerId(
            $this->statusReadRepository->findNextStatusId()
        );

        $status = Status::create(
            $id,
            $command->name(),
            new IntegerId($command->agentId())
        );

        $this->statusWriteRepository->registerStatus($status);

        return $status;
    }
}
