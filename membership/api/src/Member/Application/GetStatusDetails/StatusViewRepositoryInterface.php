<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\GetStatusDetails;

interface StatusViewRepositoryInterface
{
    public function findDetailsById(int $id): ?Status;
    public function findAll(): ?StatusCollection;
}
