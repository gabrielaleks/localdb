<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\GetStatusDetails;

use DateTimeImmutable;

class Status implements \JsonSerializable
{
    private $id;
    private $name;
    private $modifiedOn;
    private $agentId;

    public function __construct(
        int $id,
        string $name,
        DateTimeImmutable $modifiedOn,
        int $agentId
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->modifiedOn = $modifiedOn;
        $this->agentId = $agentId;
    }

    public static function fromPrimitives(
        int $id,
        string $name,
        string $modifiedOn,
        int $agentId
    ): self {
        return new self(
            $id,
            $name,
            new DateTimeImmutable($modifiedOn),
            $agentId
        );
    }

    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "modifiedOn" => $this->modifiedOn->format("Y-m-d H:i:s"),
            "agentId" => $this->agentId
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
