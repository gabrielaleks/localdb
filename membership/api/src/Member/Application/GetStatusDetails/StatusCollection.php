<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\GetStatusDetails;

class StatusCollection implements \JsonSerializable
{
    /** @var StatusCollection[] */
    private $statusCollection = [];

    public function add(Status $status): void
    {
        $this->statusCollection[] = $status;
    }

    public function toArray(): array
    {
        return array_map(function (Status $status) {
            return $status->toArray();
        }, $this->statusCollection);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
