<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\UpdateStatus;

use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\IntegerId;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;

class UpdateStatusHandler
{
    private $statusReadRepository;
    private $statusWriteRepository;

    public function __construct(
        StatusReadRepositoryInterface $statusReadRepository,
        StatusWriteRepositoryInterface $statusWriteRepository
    ) {
        $this->statusReadRepository = $statusReadRepository;
        $this->statusWriteRepository = $statusWriteRepository;
    }

    public function handle(UpdateStatusCommand $command): Status
    {
        $status = $this->statusReadRepository->findById($command->id());

        if (!$status) {
            throw new \Exception("No status with id " . $command->id() . " found.");
        }

        if ($command->name()) {
            $status->updateName(
                $command->name(),
                new IntegerId($command->agentId())
            );
        }

        $this->statusWriteRepository->updateStatus($status);

        return $status;
    }
}
