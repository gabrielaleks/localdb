<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Application\UpdateStatus;

final class UpdateStatusCommand
{
    private $id;
    private $name;
    private $agentId;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromHttpRequest(int $id, array $input): self
    {
        $command = new self($id);

        if ($input['name']) {
            $command->name = $input['name'];
        }

        $command->agentId = $input['agentId'];

        return $command;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function agentId(): int
    {
        return $this->agentId;
    }
}
