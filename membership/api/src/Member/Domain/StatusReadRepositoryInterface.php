<?php

namespace Atlas\Membership\Member\Domain;

interface StatusReadRepositoryInterface
{
    public function findNextStatusId(): int;
    public function findById(int $id): ?Status;
}
