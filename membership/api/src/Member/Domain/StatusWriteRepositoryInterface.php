<?php

namespace Atlas\Membership\Member\Domain;

interface StatusWriteRepositoryInterface
{
    public function registerStatus(Status $status): void;
    public function updateStatus(Status $status): void;
    public function deleteStatus(Status $status): void;
}
