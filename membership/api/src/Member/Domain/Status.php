<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Domain;

class Status
{
    private $id;
    private $name;
    private $agentId;

    private function __construct(
        IntegerId $id,
        string $name,
        IntegerId $agentId
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->agentId = $agentId;
    }

    public static function fromPrimitives(
        int $id,
        string $name,
        int $agentId
    ): self {
        return new self(
            IntegerId::fromInteger($id),
            $name,
            IntegerId::fromInteger($agentId)
        );
    }

    public static function create(
        IntegerId $id,
        string $name,
        IntegerId $agentId
    ): self {
        return new self(
            $id,
            $name,
            $agentId
        );
    }

    public function id(): IntegerId
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function agentId(): IntegerId
    {
        return $this->agentId;
    }

    public function updateName(
        string $name,
        IntegerId $agentId
    ): void {
        $this->name = $name;
        $this->agentId = $agentId;
    }
}
