<?php

namespace Atlas\Membership\Member;

use Doctrine\DBAL\Driver\Connection;
use Atlas\Membership\Member\Application\GetStatusDetails\StatusViewRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;
use Atlas\Membership\Member\Infrastructure\Persistence\SqlStatusRepository;

final class Dependencies
{
    public static function definitions(): array
    {
        return [
            StatusViewRepositoryInterface::class => function (Connection $connection) {
                return new SqlStatusRepository($connection);
            },
            StatusReadRepositoryInterface::class => function (Connection $connection) {
                return new SqlStatusRepository($connection);
            },
            StatusWriteRepositoryInterface::class => function (Connection $connection) {
                return new SqlStatusRepository($connection);
            }
        ];
    }
}
