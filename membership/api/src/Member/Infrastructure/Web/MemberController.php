<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Infrastructure\Web;

use Exception;
use Atlas\Membership\Member\Application\GetStatusDetails\StatusViewRepositoryInterface;
use Atlas\Membership\Member\Application\RegisterStatus\RegisterStatusCommand;
use Atlas\Membership\Member\Application\RegisterStatus\RegisterStatusHandler;
use Atlas\Membership\Member\Application\DeleteStatus\DeleteStatusCommand;
use Atlas\Membership\Member\Application\DeleteStatus\DeleteStatusHandler;
use Atlas\Membership\Member\Application\UpdateStatus\UpdateStatusCommand;
use Atlas\Membership\Member\Application\UpdateStatus\UpdateStatusHandler;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class MemberController
{
    private $statusDetailsRepository;
    private $registerStatusHandler;
    private $deleteStatusHandler;
    private $updateStatusHandler;

    public function __construct(
        StatusViewRepositoryInterface $statusDetailsRepository,
        RegisterStatusHandler $registerStatusHandler,
        DeleteStatusHandler $deleteStatusHandler,
        UpdateStatusHandler $updateStatusHandler
    ) {
        $this->statusDetailsRepository = $statusDetailsRepository;
        $this->registerStatusHandler = $registerStatusHandler;
        $this->deleteStatusHandler = $deleteStatusHandler;
        $this->updateStatusHandler = $updateStatusHandler;
    }

    public function findStatusById(Request $request, Response $response, array $args): Response
    {
        $id = (int) $args['id'];

        $status = $this->statusDetailsRepository->findDetailsById($id);

        if (!$status) {
            throw new HttpNotFoundException($request, "Status not found with id: {$id}");
        }

        $response->getBody()->write(json_encode(["status" => $status]));
        return $response->withAddedHeader("Content-Type", "application/json");
    }

    public function findAllStatuses(Request $request, Response $response): Response
    {
        $statuses = $this->statusDetailsRepository->findAll();

        if (!$statuses) {
            throw new HttpNotFoundException($request, "There are no statuses registered!");
        }

        $response->getBody()->write(json_encode(["statuses" => $statuses]));
        return $response->withAddedHeader("Content-Type", "application/json");
    }

    public function registerStatus(Request $request, Response $response, array $args): Response
    {
        $input = json_decode($request->getBody()->getContents(), true);

        $command = RegisterStatusCommand::fromHttpRequest($input["status"]);

        try {
            $status = $this->registerStatusHandler->handle($command);
        } catch (Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage());
        }

        $response->getBody()->write(
            json_encode([
                "status" => $this->statusDetailsRepository->findDetailsById(
                    $status->id()->toInteger()
                )
            ])
        );

        return $response->withAddedHeader("Content-Type", "application/json");
    }

    public function updateStatus(Request $request, Response $response, array $args): Response
    {
        $id = (int) $args['id'];
        $input = json_decode($request->getBody()->getContents(), true);

        $command = UpdateStatusCommand::fromHttpRequest($id, $input["status"]);

        try {
            $status = $this->updateStatusHandler->handle($command);
        } catch (Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage());
        }

        $response->getBody()->write(
            json_encode([
                "status" => $this->statusDetailsRepository->findDetailsById(
                    $status->id()->toInteger()
                )
            ])
        );

        return $response->withAddedHeader("Content-Type", "application/json");
    }

    public function deleteStatus(Request $request, Response $response, array $args): Response
    {
        $id = (int) $args['id'];

        $command = DeleteStatusCommand::fromHttpRequest($id);

        try {
            $this->deleteStatusHandler->handle($command);
        } catch (Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage());
        }

        $response->getBody()->write(json_encode(["message" => "Status deleted successfully"]));
        return $response->withAddedHeader("Content-Type", "application/json");
    }
}
