<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Infrastructure\Web;

use Slim\App;

class MemberRoutes
{
    public static function addRoutes(App $app): void
    {
        // Member routes
        $app->get("/status/{id}", [MemberController::class, "findStatusById"]);
        $app->get("/status", [MemberController::class, "findAllStatuses"]);
        $app->post("/status", [MemberController::class, "registerStatus"]);
        $app->patch("/status/{id}", [MemberController::class, "updateStatus"]);
        $app->delete("/status/{id}", [MemberController::class, "deleteStatus"]);
    }
}
