<?php

declare(strict_types=1);

namespace Atlas\Membership\Member\Infrastructure\Persistence;

use Atlas\Membership\Member\Application\GetStatusDetails\StatusCollection;
use Atlas\Membership\Member\Application\GetStatusDetails\StatusViewRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;
use Atlas\Membership\Member\Application\GetStatusDetails\Status as StatusDetails;
use Atlas\Membership\Member\Domain\Status;
use Doctrine\DBAL\Driver\Connection;

class SqlStatusRepository implements
    StatusViewRepositoryInterface,
    StatusReadRepositoryInterface,
    StatusWriteRepositoryInterface
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findDetailsById(int $id): ?StatusDetails
    {
        $query = "SELECT
                ID,
                NAME,
                MODIFIED_ON,
                AGENT_ID
            FROM ME_STATUS
            WHERE ID = :id";

        $statement = $this->connection->prepare($query);
        $payload = [
            'id' => $id
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAllAssociative();

        if (!$rows) {
            return null;
        }

        $status = $rows[0];
        return StatusDetails::fromPrimitives(
            (int) $status['ID'],
            $status['NAME'],
            $status['MODIFIED_ON'],
            (int) $status['AGENT_ID']
        );
    }

    public function findAll(): ?StatusCollection
    {
        $query = "SELECT
                ID,
                NAME,
                MODIFIED_ON,
                AGENT_ID
            FROM ME_STATUS";

        $statement = $this->connection->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAllAssociative();

        if (!$rows) {
            return null;
        }

        $statusCollection = new StatusCollection();
        foreach ($rows as $status) {
            $status = StatusDetails::fromPrimitives(
                (int) $status['ID'],
                $status['NAME'],
                $status['MODIFIED_ON'],
                (int) $status['AGENT_ID']
            );
            $statusCollection->add($status);
        }
        return $statusCollection;
    }

    public function findNextStatusId(): int
    {
        $query = "SELECT SEQ_ME_STATUS.NEXTVAL FROM DUAL";
        $statement = $this->connection->prepare($query);
        $statement->execute();
        return (int) $statement->fetchOne();
    }

    public function findById(int $id): ?Status
    {
        $query = "SELECT
                ID,
                NAME,
                AGENT_ID
            FROM ME_STATUS
            WHERE ID = :id";

        $statement = $this->connection->prepare($query);
        $payload = [
            'id' => $id
        ];
        $statement->execute($payload);
        $rows = $statement->fetchAllAssociative();

        if (!$rows) {
            return null;
        }

        $status = $rows[0];
        return Status::fromPrimitives(
            (int) $status['ID'],
            $status['NAME'],
            (int) $status['AGENT_ID']
        );
    }

    public function registerStatus(Status $status): void
    {
        $query = "INSERT INTO ME_STATUS (ID, NAME, MODIFIED_ON, AGENT_ID)
            VALUES (:id, :name, SYSDATE, :agentId)";

        $statement = $this->connection->prepare($query);
        $payload = [
            'id' => $status->id()->toInteger(),
            'name' => $status->name(),
            'agentId' => $status->agentId()->toInteger(),
        ];
        $statement->execute($payload);
        $this->connection->commit();
    }

    public function updateStatus(Status $status): void
    {
        $query = "UPDATE ME_STATUS 
            SET NAME = :name, AGENT_ID = :agentId, MODIFIED_ON = SYSDATE
            WHERE ID = :id";

        $statement = $this->connection->prepare($query);
        $payload = [
            'id' => $status->id()->toInteger(),
            'name' => $status->name(),
            'agentId' => $status->agentId()->toInteger(),
        ];
        $statement->execute($payload);
        $this->connection->commit();
    }

    public function deleteStatus(Status $status): void
    {
        $query = "DELETE FROM ME_STATUS WHERE ID = :id";

        $statement = $this->connection->prepare($query);
        $payload = [
            'id' => $status->id()->toInteger()
        ];
        $statement->execute($payload);
        $this->connection->commit();
    }
}
