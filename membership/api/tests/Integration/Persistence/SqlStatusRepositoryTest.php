<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\Integration\Persistence;

use Atlas\Membership\Member\Application\GetStatusDetails\StatusViewRepositoryInterface;
use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;

/**
 * @group integration
 */
class SqlStatusRepositoryTest extends PersistenceBaseTest
{
    private $statusId;
    private const STATUS_NAME = "Test Status";
    private const STATUS_AGENT_ID = 1;

    public function setUp(): void
    {
        $this->statusId = $this->selectNextStatusId();
        $this->insertTestStatusToDatabaseWith($this->statusId);
    }

    /** @test */
    public function testFetchingStatus(): void
    {
        /** @var StatusViewRepositoryInterface $viewRepository */
        $viewRepository = self::$container->get(StatusViewRepositoryInterface::class);
        $status = $viewRepository->findDetailsById($this->statusId)->toArray();
        $this->assertEquals($this->statusId, $status["id"]);
        $this->assertEquals('Test Status', $status["name"]);
    }

    /** @test */
    public function testUpdatingStatus(): void
    {
        $newName = 'Dissociated';
        $status = Status::fromPrimitives(
            $this->statusId,
            $newName,
            self::STATUS_AGENT_ID
        );

        /** @var StatusWriteRepositoryInterface $writeRepository */
        $writeRepository = self::$container->get(StatusWriteRepositoryInterface::class);
        $writeRepository->updateStatus($status);

        $updatedStatus = $this->readTestStatusFromDatabaseWith($this->statusId);
        $this->assertEquals(
            $newName,
            $updatedStatus["name"]
        );
        $this->assertEquals(
            self::STATUS_AGENT_ID,
            $updatedStatus["agentId"]
        );
    }

    /** @test */
    public function testDeletingStatus(): void
    {
        $status = Status::fromPrimitives(
            $this->statusId,
            self::STATUS_NAME,
            self::STATUS_AGENT_ID
        );

        /** @var StatusWriteRepositoryInterface $writeRepository */
        $writeRepository = self::$container->get(StatusWriteRepositoryInterface::class);
        $writeRepository->deleteStatus($status);
        $status = $this->readTestStatusFromDatabaseWith($this->statusId);

        $this->assertNull($status);
    }

    /** @test */
    public function testRegisteringStatus(): void
    {
        $statusId = $this->selectNextStatusId();
        $statusName = 'New name';
        $status = Status::fromPrimitives(
            $statusId,
            $statusName,
            self::STATUS_AGENT_ID
        );

        /** @var StatusWriteRepositoryInterface $writeRepository */
        $writeRepository = self::$container->get(StatusWriteRepositoryInterface::class);
        $writeRepository->registerStatus($status);
        $registeredStatus = $this->readTestStatusFromDatabaseWith($statusId);

        $this->assertEquals(
            $statusId,
            $registeredStatus['id']
        );

        $this->assertEquals(
            $statusName,
            $registeredStatus['name']
        );

        $this->assertEquals(
            self::STATUS_AGENT_ID,
            $registeredStatus['agentId']
        );

        $this->deleteTestStatusFromDatabaseWith($statusId);
    }


    private function selectNextStatusId(): int
    {
        $query = "SELECT SEQ_ME_STATUS.NEXTVAL FROM DUAL";
        $statement = self::connection()->prepare($query);
        $statement->execute();
        $statusId = (int) $statement->fetchOne();
        return $statusId;
    }

    private function insertTestStatusToDatabaseWith(int $statusId): void
    {
        $query = "INSERT INTO ME_STATUS (ID, NAME, MODIFIED_ON, AGENT_ID)
            VALUES (:id, :name, SYSDATE, :agentId)";
        $statement = self::connection()->prepare($query);
        $payload = [
            'id' => $statusId,
            'name' => self::STATUS_NAME,
            'agentId' => self::STATUS_AGENT_ID
        ];
        $statement->execute($payload);
        self::connection()->commit();
    }

    private function readTestStatusFromDatabaseWith(int $statusId): ?array
    {
        $query = "SELECT ID, NAME, MODIFIED_ON, AGENT_ID FROM ME_STATUS WHERE ID = :id";
        $statement = self::connection()->prepare($query);
        $payload = ["id" => $statusId];
        $statement->execute($payload);
        $rows = $statement->fetchAllAssociative();

        if (!$rows) {
            return null;
        }

        $status = $rows[0];
        return [
            "id" => $status["ID"],
            "name" => $status["NAME"],
            "modifiedOn" => $status["MODIFIED_ON"],
            "agentId" => $status["AGENT_ID"]
        ];
    }

    private function deleteTestStatusFromDatabaseWith(int $statusId): void
    {
        $query = "DELETE FROM ME_STATUS WHERE ID = :id";
        $statement = self::connection()->prepare($query);
        $payload = ["id" => $statusId];
        $statement->execute($payload);
        self::connection()->commit();
    }

    public function tearDown(): void
    {
        $this->deleteTestStatusFromDatabaseWith($this->statusId);
    }
}
