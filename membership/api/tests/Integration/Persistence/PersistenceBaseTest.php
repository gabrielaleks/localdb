<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\Integration\Persistence;

use Atlas\Membership\Shared\Infrastructure\ConnectionFactory;
use Doctrine\DBAL\Driver\Connection;
use DI\ContainerBuilder;
use Atlas\Membership\Member\Dependencies as MemberDependencies;
use PHPUnit\Framework\TestCase;

class PersistenceBaseTest extends TestCase
{
    public static $container;

    public static function setUpBeforeClass(): void
    {
        // Loading variables
        \Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . "/../../..")->load();

        // Setting PHP-DI container
        $containerBuilder = new ContainerBuilder();

        $baseDefinitions = [
            Connection::class => function () {
                return ConnectionFactory::getConnection();
            },
        ];

        $containerBuilder->addDefinitions(
            array_merge(
                $baseDefinitions,
                MemberDependencies::definitions()
            )
        );

        self::$container = $containerBuilder->build();
    }

    public static function connection(): Connection
    {
        return self::$container->get(Connection::class);
    }
}
