<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\Integration\Web;

use Atlas\Membership\Member\Application\DeleteStatus\DeleteStatusHandler;
use Atlas\Membership\Member\Application\GetStatusDetails\Status as StatusDetails;
use Atlas\Membership\Member\Application\GetStatusDetails\StatusViewRepositoryInterface;
use Atlas\Membership\Member\Application\RegisterStatus\RegisterStatusHandler;
use Atlas\Membership\Member\Application\UpdateStatus\UpdateStatusHandler;
use Atlas\Membership\Member\Domain\Status;
use Slim\Exception\HttpNotFoundException;

/**
 * @group integration
 */
class HttpRequestTest extends WebBaseTest
{
    private const STATUS_ID = 1;
    private const STATUS_INVALID_ID = 99999;
    private const STATUS_NAME = 'This is a status';
    private const STATUS_MODIFIED_ON = '2023-01-01';
    private const STATUS_AGENT_ID = 8;

    /** @test */
    public function testFindDetailsByIdShouldReturn200WithValidId(): void
    {
        // Mock view repository
        $statusDetailsRepository = $this->createMock(StatusViewRepositoryInterface::class);
        $statusDetailsRepository->expects($this->once())
            ->method('findDetailsById')
            ->with(self::STATUS_ID)
            ->willReturn($this->mockStatusDetails());
        $this->container->set(StatusViewRepositoryInterface::class, $statusDetailsRepository);

        // Test
        $request = $this->createRequest('GET', '/api/status/' . self::STATUS_ID);
        $response = $this->slimApp->handle($request);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function testFindDetailsByIdShouldThrowExceptionIfIdDoesNotExist(): void
    {
        // Mock view repository
        $statusDetailsRepository = $this->createMock(StatusViewRepositoryInterface::class);
        $statusDetailsRepository->expects($this->once())
            ->method('findDetailsById')
            ->with(self::STATUS_INVALID_ID)
            ->willReturn(null);
        $this->container->set(StatusViewRepositoryInterface::class, $statusDetailsRepository);

        // Test
        $request = $this->createRequest('GET', '/api/status/' . self::STATUS_INVALID_ID);
        $this->expectException(HttpNotFoundException::class);
        $this->slimApp->handle($request);
    }

    /**  @test */
    public function testRegisterStatusShouldReturn200WhenPayloadIsValid(): void
    {
        // Mock Handler
        $registerStatusHandler = $this->createMock(RegisterStatusHandler::class);
        $registerStatusHandler->expects($this->once())
            ->method('handle')
            ->willReturn($this->mockStatusDomain());
        $this->container->set(RegisterStatusHandler::class, $registerStatusHandler);

        // Mock view repository
        $statusDetailsRepository = $this->createMock(StatusViewRepositoryInterface::class);
        $statusDetailsRepository->expects($this->once())
            ->method('findDetailsById')
            ->with(self::STATUS_ID)
            ->willReturn($this->mockStatusDetails());
        $this->container->set(StatusViewRepositoryInterface::class, $statusDetailsRepository);

        // Test
        $request = $this->createRequest('POST', '/api/status', json_encode($this->mockRequestBody()));
        $response = $this->slimApp->handle($request);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**  @test */
    public function testUpdateStatusShouldReturn200WhenPayloadIsValid(): void
    {
        // Mock Handler
        $updateStatusHandler = $this->createMock(UpdateStatusHandler::class);
        $updateStatusHandler->expects($this->once())
            ->method('handle')
            ->willReturn($this->mockStatusDomain());
        $this->container->set(UpdateStatusHandler::class, $updateStatusHandler);

        // Mock view repository
        $statusDetailsRepository = $this->createMock(StatusViewRepositoryInterface::class);
        $statusDetailsRepository->expects($this->once())
            ->method('findDetailsById')
            ->with(self::STATUS_ID)
            ->willReturn($this->mockStatusDetails());
        $this->container->set(StatusViewRepositoryInterface::class, $statusDetailsRepository);

        // Test
        $request = $this->createRequest('PATCH', '/api/status/' . self::STATUS_ID, json_encode($this->mockRequestBody()));
        $response = $this->slimApp->handle($request);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**  @test */
    public function testDeleteStatusShouldReturn200WhenPayloadIsValid(): void
    {
        // Mock Handler
        $deleteStatusHandler = $this->createMock(DeleteStatusHandler::class);
        $deleteStatusHandler->expects($this->once())
            ->method('handle');
        $this->container->set(DeleteStatusHandler::class, $deleteStatusHandler);

        // Test
        $request = $this->createRequest('DELETE', '/api/status/' . self::STATUS_ID, json_encode($this->mockRequestBody()));
        $response = $this->slimApp->handle($request);
        $this->assertEquals(200, $response->getStatusCode());
    }

    private function mockStatusDetails(): StatusDetails
    {
        return StatusDetails::fromPrimitives(
            self::STATUS_ID,
            self::STATUS_NAME,
            self::STATUS_MODIFIED_ON,
            self::STATUS_AGENT_ID
        );
    }

    private function mockStatusDomain(): Status
    {
        return Status::fromPrimitives(
            self::STATUS_ID,
            self::STATUS_NAME,
            self::STATUS_AGENT_ID
        );
    }

    private function mockRequestBody(): array
    {
        return [
            'status' => [
                'name' => self::STATUS_NAME,
                'agentId' => self::STATUS_AGENT_ID
            ]
        ];
    }
}
