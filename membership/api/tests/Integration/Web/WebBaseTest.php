<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\Integration\Web;

use Atlas\Membership\Member\Infrastructure\Web\MemberRoutes;
use Atlas\Membership\Shared\Infrastructure\ConnectionFactory;
use Doctrine\DBAL\Driver\Connection;
use DI\ContainerBuilder;
use Atlas\Membership\Member\Dependencies as MemberDependencies;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface as Request;
use Nyholm\Psr7\ServerRequest as SlimRequest;
use Slim\Factory\AppFactory;

class WebBaseTest extends TestCase
{
    public $container;
    public $slimApp;

    public function setUp(): void
    {
        // Loading variables
        \Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . "/../../..")->load();

        // Setting PHP-DI container
        $containerBuilder = new ContainerBuilder();

        $baseDefinitions = [
            Connection::class => function () {
                return ConnectionFactory::getConnection();
            },
        ];

        $containerBuilder->addDefinitions(
            array_merge(
                $baseDefinitions,
                MemberDependencies::definitions()
            )
        );

        $this->container = $containerBuilder->build();

        // Setting Slim App
        $this->slimApp = AppFactory::createFromContainer($this->container);
        $this->slimApp->setBasePath('/api');
        MemberRoutes::addRoutes($this->slimApp);
    }

    public function createRequest(
        string $method,
        string $uri,
        string $body = null
    ): Request {
        $headers = ['Content-Type' => 'application/json'];
        return new SlimRequest($method, $uri, $headers, $body);
    }
}
