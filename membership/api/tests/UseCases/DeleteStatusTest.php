<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\UseCases;

use Atlas\Membership\Member\Application\DeleteStatus\DeleteStatusCommand;
use Atlas\Membership\Member\Application\DeleteStatus\DeleteStatusHandler;
use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * @group use-cases
 */
class DeleteStatusTest extends TestCase
{
    private const STATUS_ID = 1;

    /** @test */
    public function shouldDeleteStatus(): void
    {
        $statusReadRepository = $this->createMock(StatusReadRepositoryInterface::class);
        $statusWriteRepository = $this->createMock(StatusWriteRepositoryInterface::class);

        $command = DeleteStatusCommand::fromHttpRequest(self::STATUS_ID);

        $statusReadRepository->expects($this->once())
            ->method('findById')
            ->with($command->id())
            ->willReturn($this->mockStatusWithExistingId());

        $statusWriteRepository->expects($this->once())
            ->method('deleteStatus')
            ->with($this->mockStatusWithExistingId());

        $handler = new DeleteStatusHandler(
            $statusReadRepository,
            $statusWriteRepository
        );

        $handler->handle($command);
    }

    /** @test */
    public function shouldThrowExceptionIfNoStatusWasFound(): void
    {
        $statusReadRepository = $this->createMock(StatusReadRepositoryInterface::class);
        $statusWriteRepository = $this->createMock(StatusWriteRepositoryInterface::class);

        $command = DeleteStatusCommand::fromHttpRequest(self::STATUS_ID);

        $statusReadRepository->expects($this->once())
            ->method('findById')
            ->with($command->id())
            ->willReturn(null);

        $statusWriteRepository->expects($this->never())
        ->method('deleteStatus');

        $handler = new DeleteStatusHandler(
            $statusReadRepository,
            $statusWriteRepository
        );

        $this->expectExceptionMessage("No status with id " . $command->id() . " found.");

        $handler->handle($command);
    }

    private function mockStatusWithExistingId(): Status
    {
        return Status::fromPrimitives(
            self::STATUS_ID,
            'fake status',
            1
        );
    }
}
