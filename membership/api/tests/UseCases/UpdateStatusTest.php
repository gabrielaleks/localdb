<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\UseCases;

use Atlas\Membership\Member\Application\UpdateStatus\UpdateStatusCommand;
use Atlas\Membership\Member\Application\UpdateStatus\UpdateStatusHandler;
use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * @group use-cases
 */
class UpdateStatusTest extends TestCase
{
    private const STATUS_ID = 1;
    private const STATUS_AGENT_ID = 9;
    private const STATUS_NAME = 'this is a fake status';

    /** @test */
    public function shouldUpdateStatus(): void
    {
        $statusReadRepository = $this->createMock(StatusReadRepositoryInterface::class);
        $statusWriteRepository = $this->createMock(StatusWriteRepositoryInterface::class);

        $command = UpdateStatusCommand::fromHttpRequest(
            self::STATUS_ID,
            $this->mockRequestBody()
        );

        $statusReadRepository->expects($this->once())
            ->method('findById')
            ->with($command->id())
            ->willReturn($this->mockStatusWithExistingId());

        $statusWriteRepository->expects($this->once())
            ->method('updateStatus');

        $handler = new UpdateStatusHandler(
            $statusReadRepository,
            $statusWriteRepository
        );

        $status = $handler->handle($command);

        $this->assertInstanceOf(Status::class, $status);
        $this->assertEquals(self::STATUS_ID, $status->id()->toInteger());
        $this->assertEquals(self::STATUS_NAME, $status->name());
        $this->assertEquals(self::STATUS_AGENT_ID, $status->agentId()->toInteger());
    }

    /** @test */
    public function shouldThrowExceptionIfNoStatusIsFound(): void
    {
        $statusReadRepository = $this->createMock(StatusReadRepositoryInterface::class);
        $statusWriteRepository = $this->createMock(StatusWriteRepositoryInterface::class);

        $command = UpdateStatusCommand::fromHttpRequest(
            self::STATUS_ID,
            $this->mockRequestBody()
        );

        $statusReadRepository->expects($this->once())
            ->method('findById')
            ->with($command->id())
            ->willReturn(null);

        $statusWriteRepository->expects($this->never())
            ->method('updateStatus');

        $this->expectExceptionMessage("No status with id " . $command->id() . " found.");

        $handler = new UpdateStatusHandler(
            $statusReadRepository,
            $statusWriteRepository
        );

        $handler->handle($command);
    }

    private function mockRequestBody(): array
    {
        return [
            'name' => self::STATUS_NAME,
            'agentId' => self::STATUS_AGENT_ID
        ];
    }

    private function mockStatusWithExistingId(): Status
    {
        return Status::fromPrimitives(
            self::STATUS_ID,
            'fake status',
            1
        );
    }
}
