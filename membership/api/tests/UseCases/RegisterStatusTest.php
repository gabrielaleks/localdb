<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\UseCases;

use Atlas\Membership\Member\Application\RegisterStatus\RegisterStatusCommand;
use Atlas\Membership\Member\Application\RegisterStatus\RegisterStatusHandler;
use Atlas\Membership\Member\Domain\Status;
use Atlas\Membership\Member\Domain\StatusReadRepositoryInterface;
use Atlas\Membership\Member\Domain\StatusWriteRepositoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * @group use-cases
 */
class RegisterStatusTest extends TestCase
{
    private const STATUS_ID = 1;
    private const STATUS_AGENT_ID = 9;
    private const STATUS_NAME = 'this is a fake status';

    /** @test */
    public function shouldRegisterStatus(): void
    {
        $statusReadRepository = $this->createMock(StatusReadRepositoryInterface::class);
        $statusWriteRepository = $this->createMock(StatusWriteRepositoryInterface::class);

        $command = RegisterStatusCommand::fromHttpRequest(
            $this->mockRequestBody()
        );

        $statusReadRepository->expects($this->once())
            ->method('findNextStatusId')
            ->willReturn(self::STATUS_ID);

        $statusWriteRepository->expects($this->once())
            ->method('registerStatus');

        $handler = new RegisterStatusHandler(
            $statusReadRepository,
            $statusWriteRepository
        );

        $status = $handler->handle($command);

        $this->assertInstanceOf(Status::class, $status);
        $this->assertEquals(self::STATUS_ID, $status->id()->toInteger());
        $this->assertEquals(self::STATUS_NAME, $status->name());
        $this->assertEquals(self::STATUS_AGENT_ID, $status->agentId()->toInteger());
    }

    private function mockRequestBody(): array
    {
        return [
            'name' => self::STATUS_NAME,
            'agentId' => self::STATUS_AGENT_ID
        ];
    }
}
