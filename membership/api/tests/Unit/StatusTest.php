<?php

declare(strict_types=1);

namespace Atlas\Membership\Tests\Unit;

use Atlas\Membership\Member\Domain\IntegerId;
use Atlas\Membership\Member\Domain\Status;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class StatusTest extends TestCase
{
    /** @test */
    public function shouldCreateStatusInstance(): void
    {
        $id = new IntegerId(1);
        $name = 'Status name';
        $agentId = new IntegerId(5);

        $status = Status::create(
            $id,
            $name,
            $agentId
        );

        $this->assertEquals(
            $status->id(),
            $id
        );

        $this->assertEquals(
            $status->name(),
            $name
        );

        $this->assertEquals(
            $status->agentId(),
            $agentId
        );
    }

    /** @test */
    public function shouldUpdateName(): void
    {
        $id = new IntegerId(1);
        $name = 'Status name';
        $agentId = new IntegerId(5);

        $status = Status::create(
            $id,
            $name,
            $agentId
        );

        $newName = 'this is a new name';
        $status->updateName($newName, $agentId);

        $this->assertEquals($newName, $status->name());
    }
}
