--liquibase formatted sql
--changeset Gabriel:7 endDelimiter:/ rollbackEndDelimiter:/

INSERT INTO
    ATLAS_AUTHDB.USERGROUP (
        GROUP_ID,
        DESCRIPTION,
        SYSTEM,
        SYSTEMS_ID,
        NAME,
        CONTACT
    )
VALUES
    (
        'EXPERT',
        'The Database Expert is responsible for maintaining the ATLAS AUTHDB database structure and its systems.',
        'MEMBERSHIP',
        4,
        'Expert',
        'atlas.glance@cern.ch'
    )

/

INSERT INTO
    ATLAS_AUTHDB.USERS_USERGROUP (
        GROUP_ID,
        NCP_FUNDA_ID,
        IR_INSTITUTE_ID,
        IR_INSTITUTION_ID,
        CERN_CCID
    )
VALUES
    ('EXPERT', null, null, null, 851404)


/

--rollback DELETE FROM ATLAS_AUTHDB.USERS_USERGROUP
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.USERGROUP
--rollback /