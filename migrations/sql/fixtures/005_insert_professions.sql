--liquibase formatted sql
--changeset Gabriel:5 endDelimiter:/ rollbackEndDelimiter:/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (1, 'Physicist')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (2, 'Physics PhD student')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (3, 'Physics masters/diploma student')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (4, 'Undergraduate/summer student')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (5, 'Engineer with PhD')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (6, 'Engineer without PhD')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (7, 'Engineering student')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (8, 'Technician or equivalent')

/

INSERT INTO ATLAS_AUTHDB.PTYPE (ID, DESCRIPTION)
VALUES (9, 'Administrator/other')

/

--rollback DELETE FROM ATLAS_AUTHDB.PTYPE
--rollback /