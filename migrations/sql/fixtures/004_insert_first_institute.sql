--liquibase formatted sql
--changeset Gabriel:4 endDelimiter:/ rollbackEndDelimiter:/

INSERT INTO ATLAS_AUTHDB.FUNDA (ID, NAME)
VALUES (1, 'Brazil')

/

INSERT INTO ATLAS_AUTHDB.GENERAL_COUNTRY (ID, NAME, SHORT_NAME, AGENT_ID, MODIFIED_ON)
VALUES (SEQ_GENERAL_COUNTRY.NEXTVAL, 'Brazil', 'Brazil', SEQ_MEMB.CURRVAL, SYSDATE)

/

INSERT INTO ATLAS_AUTHDB.COUNTRY (ID, NAME, CODE, CODE_2)
VALUES (SEQ_GENERAL_COUNTRY.CURRVAL, 'Brazil', 'BRA', 'BR')

/

INSERT INTO
    ATLAS_AUTHDB.IN_INSTITUTE (
        ID,
        NAME,
        SHORT_NAME,
        COUNTRY_ID,
        PHONE,
        FAX,
        WEBPAGE,
        TIME_ZONE,
        FUNDING_AGENCY_ID,
        MODIFIED_ON,
        DOMAIN,
        SPIRES_ICN,
        AGENT_ID,
        ROR,
        GRID
    )
VALUES
    (
        SEQ_INST.NEXTVAL,
        'Universidade Federal do Rio De Janeiro, COPPE/EE/IF ',
        'Rio de Janeiro  UF',
        SEQ_GENERAL_COUNTRY.CURRVAL,
        '+55 21 3938-7923',
        '+55 21 2562.73.68',
        'http://www.coppe.ufrj.br/',
        null,
        1,
        SYSDATE,
        'ufrj.br',
        'Rio de Janeiro Federal U.',
        SEQ_MEMB.CURRVAL,
        null,
        null
    )

/

--rollback DELETE FROM ATLAS_AUTHDB.IN_INSTITUTE
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.COUNTRY
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.GENERAL_COUNTRY
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.FUNDA