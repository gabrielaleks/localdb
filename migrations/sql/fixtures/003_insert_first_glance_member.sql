--liquibase formatted sql
--changeset Gabriel:3 endDelimiter:/ rollbackEndDelimiter:/

ALTER TABLE ATLAS_AUTHDB.ME_MEMBER
DISABLE CONSTRAINT MEMB_AGENT_FK

/

BEGIN
    DBMS_MVIEW.REFRESH('MEMBER_INFORMATION', 'C');
END;

/

INSERT INTO
    ATLAS_AUTHDB.ME_MEMBER (
        ID,
        INITIALS,
        CERN_CCID,
        COMMENTS,
        LAST_NAME_LATEX,
        FIRST_NAME_LATEX,
        MODIFIED_ON,
        INSPIRE,
        ORCID,
        INSPIRE_HEPNAME,
        STATUS_ID,
        AGENT_ID,
        OTP_ID
    )
VALUES
    (
        SEQ_MEMB.CURRVAL,
        'G.',
        851404,
        null,
        'Aleksandravicius',
        'Gabriel',
        SYSDATE,
        null,
        null,
        null,
        SEQ_ME_STATUS.CURRVAL,
        SEQ_MEMB.CURRVAL,
        SEQ_MEMB.CURRVAL
    )

/


ALTER TABLE ATLAS_AUTHDB.ME_MEMBER
ENABLE CONSTRAINT MEMB_AGENT_FK

/

ALTER TABLE ATLAS_AUTHDB.ME_STATUS
ENABLE CONSTRAINT ME_STATUS_FK1

/

--rollback ALTER TABLE ATLAS_AUTHDB.ME_MEMBER
--rollback DISABLE CONSTRAINT MEMB_AGENT_FK
--rollback /
--rollback ALTER TABLE ATLAS_AUTHDB.ME_STATUS
--rollback DISABLE CONSTRAINT ME_STATUS_FK1
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.ME_MEMBER