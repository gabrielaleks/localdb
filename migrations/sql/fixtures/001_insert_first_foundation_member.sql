--liquibase formatted sql
--changeset Gabriel:1 endDelimiter:/ rollbackEndDelimiter:/

INSERT INTO
    ATLAS_AUTHDB.CERNDB_PARTICIPANT_ATLAS (
        PERSON_ID,
        CERN_ID,
        LAST_NAME,
        FIRST_NAME,
        QUALITY,
        STATUS,
        AT_CERN,
        PERCENTAGE_PRESENCE,
        GENERIC_EMAIL,
        PREFERRED_EMAIL,
        SEX,
        TELEPHONE1,
        TELEPHONE2,
        PORTABLE_PHONE,
        CONTRACT_END_DATE,
        EXPERIMENT,
        BUILDING,
        FLOOR,
        ROOM,
        ORG_DEPARTMENT,
        ORG_GROUP,
        ORG_SECTION,
        ORGANIC_UNIT,
        ORCID
    )
VALUES
    (
        851404,
        169990,
        'Aleksandravicius',
        'Gabriel',
        'Mr.',
        'USER',
        'Y',
        100,
        'gabriel.aleks@cern.ch',
        'gabriel.aleks@cern.ch',
        'M',
        '67716',
        null,
        null,
        DATE '2024-12-31',
        'ATLAS',
        '3150',
        'R',
        '016',
        null,
        null,
        null,
        null,
        null
    )

/

INSERT INTO
    ATLAS_AUTHDB.CERNDB_PARTICIPATION_ATLAS (
        PERSON_ID,
        EXPERIMENT_NAME,
        INSTITUTE_CODE,
        IS_AUTHOR,
        IS_PRIMARY,
        START_DATE,
        END_DATE
    )
VALUES
    (
        851404,
        'ATLAS',
        '006159',
        'N',
        'Y',
        DATE '2021-01-01',
        null
    )

/

BEGIN
    DBMS_MVIEW.REFRESH('MV_FOUNDATION_MEMBER', 'C');
END;

--rollback DELETE FROM ATLAS_AUTHDB.CERNDB_PARTICIPATION_ATLAS
--rollback /
--rollback DELETE FROM ATLAS_AUTHDB.CERNDB_PARTICIPANT_ATLAS
--rollback /