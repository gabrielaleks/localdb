--liquibase formatted sql
--changeset Gabriel:6 endDelimiter:/ rollbackEndDelimiter:/

INSERT INTO
    ATLAS_AUTHDB.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE
    )
VALUES
    (
        SEQ_EMPLOY.NEXTVAL,
        SEQ_MEMB.CURRVAL,
        SEQ_INST.CURRVAL,
        DATE '2020-01-18',
        DATE '2026-12-31',
        null,
        null,
        'gabriel.aleks@cern.ch',
        0,
        null,
        4,
        0,
        0,
        null,
        0,
        0,
        'first employment',
        SYSDATE,
        851404,
        null,
        null,
        null,
        null,
        1,
        851404,
        null,
        null,
        null,
        null
    )

/

--rollback DELETE FROM ATLAS_AUTHDB.ME_EMPLOYMENT
--rollback /