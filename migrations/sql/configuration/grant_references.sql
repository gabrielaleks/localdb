DECLARE
   schema1 VARCHAR2(30) := 'ATLAS_AUTHDB';
   schema2 VARCHAR2(30) := 'ATLAS_PUB_TRACK';

   CURSOR schema1_table_cursor IS
      SELECT table_name
      FROM all_tables
      WHERE owner = schema1;

   CURSOR schema2_table_cursor IS
      SELECT table_name
      FROM all_tables
      WHERE owner = schema2;

BEGIN
   FOR table_rec IN schema1_table_cursor LOOP
      EXECUTE IMMEDIATE 'GRANT REFERENCES ON ' || schema1 || '.' || table_rec.table_name || ' TO ' || schema2;
   END LOOP;
   FOR table_rec IN schema2_table_cursor LOOP
      EXECUTE IMMEDIATE 'GRANT REFERENCES ON ' || schema2 || '.' || table_rec.table_name || ' TO ' || schema1;
   END LOOP;
END;