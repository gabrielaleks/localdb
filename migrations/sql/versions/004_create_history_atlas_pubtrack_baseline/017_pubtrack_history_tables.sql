--liquibase formatted sql
--changeset Gabriel:17 endDelimiter:/ rollbackEndDelimiter:/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.DATABASECHANGELOGLOCK
(
    ID          NUMBER    NOT NULL,
    LOCKED      NUMBER(1) NOT NULL,
    LOCKGRANTED TIMESTAMP(6),
    LOCKEDBY    VARCHAR2(255)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.DATABASECHANGELOG
(
    ID            VARCHAR2(255) NOT NULL,
    AUTHOR        VARCHAR2(255) NOT NULL,
    FILENAME      VARCHAR2(255) NOT NULL,
    DATEEXECUTED  TIMESTAMP(6)  NOT NULL,
    ORDEREXECUTED NUMBER        NOT NULL,
    EXECTYPE      VARCHAR2(10)  NOT NULL,
    MD5SUM        VARCHAR2(35),
    DESCRIPTION   VARCHAR2(255),
    COMMENTS      VARCHAR2(255),
    TAG           VARCHAR2(255),
    LIQUIBASE     VARCHAR2(20),
    CONTEXTS      VARCHAR2(255),
    LABELS        VARCHAR2(255),
    DEPLOYMENT_ID VARCHAR2(10)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_ECM_UNIT
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_ECM_VALUE
(
    ID          NUMBER                     NOT NULL,
    NAME        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_LUMINOSITY_UNIT
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_RUN
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_TYPE
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.COL_YEAR
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.GRP_GROUP
(
    ID          NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.GRP_SUBGROUP
(
    ID          NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(60)               NOT NULL,
    PARENT_ID   NUMBER,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY_GROUP
(
    ID          NUMBER                     NOT NULL,
    CATEGORY_ID NUMBER                     NOT NULL,
    GROUP_ID    NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(70)               NOT NULL,
    CATEGORY_ID NUMBER                     NOT NULL,
    RANK        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(80)               NOT NULL,
    RANK        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(80)               NOT NULL,
    RANK        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(4)                NOT NULL,
    NAME        VARCHAR2(80)               NOT NULL,
    RANK        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK
(
    ID                 NUMBER                     NOT NULL,
    SHORT_TITLE        VARCHAR2(255),
    PUBLIC_SHORT_TITLE VARCHAR2(1000),
    FULL_TITLE         VARCHAR2(2000),
    COMMENTS           VARCHAR2(1000),
    MODIFIED_ON        DATE                       NOT NULL,
    AGENT_ID           NUMBER                     NOT NULL,
    HISTORY_ID         RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION             CHAR                       NOT NULL,
    DIFF_COLUMN        VARCHAR2(30),
    OLD_VALUE          VARCHAR2(4000),
    NEW_VALUE          VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_ADD_MEMBER_IN_EGROUP
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_ANALYSIS_TEAM_MEMBER
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION
(
    ID                 NUMBER                     NOT NULL,
    TASK_ID            NUMBER                     NOT NULL,
    YEAR_ID            NUMBER,
    RUN_ID             NUMBER,
    TYPE_ID            NUMBER,
    ECM_VALUE_ID       NUMBER,
    ECM_UNIT_ID        NUMBER,
    LUMINOSITY_VALUE   NUMBER,
    LUMINOSITY_UNIT_ID NUMBER,
    MODIFIED_ON        DATE                       NOT NULL,
    AGENT_ID           NUMBER                     NOT NULL,
    HISTORY_ID         RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION             CHAR                       NOT NULL,
    DIFF_COLUMN        VARCHAR2(30),
    OLD_VALUE          VARCHAR2(4000),
    NEW_VALUE          VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_CONTACT_EDITOR
(
    ID          NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_KEYWORD
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    KEYWORD_ID  NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_GROUP
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    GROUP_ID    NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_SUBGROUP
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    SUBGROUP_ID NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION
(
    ID                    NUMBER                     NOT NULL,
    SUPPORTING_INT_DOC_ID NUMBER                     NOT NULL,
    CONTRIBUTION          VARCHAR2(510)              NOT NULL,
    MEMBER_ID             NUMBER                     NOT NULL,
    MODIFIED_ON           DATE                       NOT NULL,
    AGENT_ID              NUMBER                     NOT NULL,
    HISTORY_ID            RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                CHAR                       NOT NULL,
    DIFF_COLUMN           VARCHAR2(30),
    OLD_VALUE             VARCHAR2(4000),
    NEW_VALUE             VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_MVA_ML_TOOL
(
    ID             NUMBER                     NOT NULL,
    TASK_ID        NUMBER                     NOT NULL,
    MVA_ML_TOOL_ID NUMBER                     NOT NULL,
    MODIFIED_ON    DATE                       NOT NULL,
    AGENT_ID       NUMBER                     NOT NULL,
    HISTORY_ID     RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION         CHAR                       NOT NULL,
    DIFF_COLUMN    VARCHAR2(30),
    OLD_VALUE      VARCHAR2(4000),
    NEW_VALUE      VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_OTHER_GROUP
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    GROUP_ID    NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_STAT_INTERPRETATION
(
    ID                     NUMBER                     NOT NULL,
    TASK_ID                NUMBER                     NOT NULL,
    STAT_INTERPRETATION_ID NUMBER                     NOT NULL,
    MODIFIED_ON            DATE                       NOT NULL,
    AGENT_ID               NUMBER                     NOT NULL,
    HISTORY_ID             RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                 CHAR                       NOT NULL,
    DIFF_COLUMN            VARCHAR2(30),
    OLD_VALUE              VARCHAR2(4000),
    NEW_VALUE              VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_STAT_TOOL
(
    ID           NUMBER                     NOT NULL,
    TASK_ID      NUMBER                     NOT NULL,
    STAT_TOOL_ID NUMBER                     NOT NULL,
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_STA_IN_EGROUP
(
    ID            NUMBER                     NOT NULL,
    TASK_ID       NUMBER                     NOT NULL,
    STA_MEMBER_ID NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_SUBGROUP
(
    TASK_ID     NUMBER                     NOT NULL,
    SUBGROUP_ID NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_PUB_TRACK_HISTORY.TASK_SUPPORTING_INT_DOC
(
    ID          NUMBER                     NOT NULL,
    TASK_ID     NUMBER                     NOT NULL,
    LINK_ID     NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

--rollback DECLARE
--rollback   V_TABLE_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR TABLE_REC IN (SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER = 'ATLAS_PUB_TRACK_HISTORY') LOOP
--rollback     V_TABLE_NAME := TABLE_REC.TABLE_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP TABLE ATLAS_PUB_TRACK_HISTORY.' || V_TABLE_NAME || ' PURGE';
--rollback   END LOOP;
--rollback END;
--rollback /