--liquibase formatted sql
--changeset Gabriel:13 endDelimiter:/ rollbackEndDelimiter:/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION
(
    ID            NUMBER                     NOT NULL,
    ACTIVITY_ID   NUMBER                     NOT NULL,
    NOMINATION_ID NUMBER                     NOT NULL,
    SYSTEM_ID     NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_WATCHER
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    WATCHER_ID       NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_MEMBER
(
    ID               NUMBER                     NOT NULL,
    CERN_CCID        NUMBER(9),
    STATUS_ID        NUMBER                     NOT NULL,
    INITIALS         VARCHAR2(10)               NOT NULL,
    COMMENTS         VARCHAR2(255),
    FIRST_NAME_LATEX VARCHAR2(255)              NOT NULL,
    LAST_NAME_LATEX  VARCHAR2(255)              NOT NULL,
    INSPIRE          VARCHAR2(25 CHAR),
    ORCID            VARCHAR2(19),
    INSPIRE_HEPNAME  VARCHAR2(100 CHAR),
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    START_DATE  DATE                       NOT NULL,
    END_DATE    DATE                       NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS
(
    ID          NUMBER NOT NULL,
    ACTIVITY_ID NUMBER NOT NULL,
    STATUS_ID   NUMBER NOT NULL,
    MODIFIED_ON DATE   NOT NULL,
    AGENT_ID    NUMBER NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT
(
    ID                NUMBER                     NOT NULL,
    QUALIFICATION_ID  NUMBER                     NOT NULL,
    FINAL_REPORT_DATE DATE                       NOT NULL,
    COMMENTS          VARCHAR2(1100 CHAR)        NOT NULL,
    MODIFIED_ON       TIMESTAMP(6)               NOT NULL,
    AGENT_ID          NUMBER                     NOT NULL,
    HISTORY_ID        RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION            CHAR                       NOT NULL,
    DIFF_COLUMN       VARCHAR2(30),
    OLD_VALUE         VARCHAR2(4000),
    NEW_VALUE         VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_FUTURE
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    FUTURE_STATUS_ID NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER
(
    ID          NUMBER NOT NULL,
    ACTIVITY_ID NUMBER NOT NULL,
    MEMBER_ID   NUMBER NOT NULL,
    MODIFIED_ON DATE   NOT NULL,
    AGENT_ID    NUMBER NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_APPLYING_FOR_A_JOB
(
    MEMBER_ID   NUMBER                     NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE
(
    ID          NUMBER                     NOT NULL,
    CODE        VARCHAR2(20)               NOT NULL,
    NAME        VARCHAR2(255)              NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_DELETED
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_CONTACT
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    NAME         VARCHAR2(255)              NOT NULL,
    EMAIL        VARCHAR2(255),
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS
(
    ID          NUMBER       NOT NULL,
    NAME        VARCHAR2(64) NOT NULL,
    MODIFIED_ON DATE         NOT NULL,
    AGENT_ID    NUMBER       NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_STATE
(
    ID          NUMBER                     NOT NULL,
    NAME        VARCHAR2(100)              NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.NOM_DISCARDED
(
    ID            NUMBER                     NOT NULL,
    NOMINATION_ID NUMBER                     NOT NULL,
    MEMBER_ID     NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_OTP_TASK
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    TASK_ID          NUMBER                     NOT NULL,
    REQUIREMENT_ID   NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_HIERARCHY
(
    ID                  NUMBER                     NOT NULL,
    INSTITUTE_ID        NUMBER(4)                  NOT NULL,
    PARENT_INSTITUTE_ID NUMBER(4)                  NOT NULL,
    MODIFIED_ON         DATE                       NOT NULL,
    AGENT_ID            NUMBER                     NOT NULL,
    HISTORY_ID          RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION              CHAR                       NOT NULL,
    DIFF_COLUMN         VARCHAR2(30),
    OLD_VALUE           VARCHAR2(4000),
    NEW_VALUE           VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_REMOVAL
(
    ID          NUMBER(38) NOT NULL,
    MEMBER_ID   NUMBER(38) NOT NULL,
    AGENT_ID    NUMBER(38) NOT NULL,
    MODIFIED_ON DATE       NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_AFFILIATION
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    START_DATE   DATE,
    END_DATE     DATE,
    TYPE_ID      NUMBER                     NOT NULL,
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC
(
    ID                      NUMBER                     NOT NULL,
    MEMBER_ID               NUMBER                     NOT NULL,
    PREFERRED_CONF_ID       NUMBER                     NOT NULL,
    TALK_PREFERRED_TOPIC_ID NUMBER                     NOT NULL,
    RANK                    NUMBER                     NOT NULL,
    MODIFIED_ON             DATE                       NOT NULL,
    AGENT_ID                NUMBER                     NOT NULL,
    HISTORY_ID              RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                  CHAR                       NOT NULL,
    DIFF_COLUMN             VARCHAR2(30),
    OLD_VALUE               VARCHAR2(4000),
    NEW_VALUE               VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE
(
    ID           NUMBER                     NOT NULL,
    MEMBER_ID    NUMBER                     NOT NULL,
    TALK_TYPE_ID NUMBER                     NOT NULL,
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.AT_ACTION
(
    ID          NUMBER        NOT NULL,
    NAME        VARCHAR2(255) NOT NULL,
    CODE        VARCHAR2(30),
    MODIFIED_ON DATE          NOT NULL,
    AGENT_ID    NUMBER        NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR
(
    ID                      NUMBER                     NOT NULL,
    QUALIFICATION_ID        NUMBER                     NOT NULL,
    TECHNICAL_SUPERVISOR_ID NUMBER                     NOT NULL,
    MODIFIED_ON             TIMESTAMP(6)               NOT NULL,
    AGENT_ID                NUMBER                     NOT NULL,
    HISTORY_ID              RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                  CHAR                       NOT NULL,
    DIFF_COLUMN             VARCHAR2(30),
    OLD_VALUE               VARCHAR2(4000),
    NEW_VALUE               VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT
(
    MEMBER_ID   NUMBER                     NOT NULL,
    COMMENTS    VARCHAR2(1000)             NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW
(
    ID             NUMBER NOT NULL,
    ACTIVITY_ID    NUMBER NOT NULL,
    APPOINTMENT_ID NUMBER NOT NULL,
    MODIFIED_ON    DATE   NOT NULL,
    AGENT_ID       NUMBER NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID     NUMBER GENERATED AS IDENTITY,
    ACTION         CHAR,
    DIFF_COLUMN    VARCHAR2(30),
    OLD_VALUE      VARCHAR2(100),
    NEW_VALUE      VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY
(
    ID          NUMBER         NOT NULL,
    CODE        VARCHAR2(4)    NOT NULL,
    NAME        VARCHAR2(256)  NOT NULL,
    DESCRIPTION VARCHAR2(1064) NOT NULL,
    TYPE_ID     NUMBER         NOT NULL,
    STATUS_ID   NUMBER         NOT NULL,
    NATURAL_ID  VARCHAR2(4)    NOT NULL,
    MODIFIED_ON DATE           NOT NULL,
    AGENT_ID    NUMBER         NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT
(
    ID            NUMBER                     NOT NULL,
    ACTIVITY_ID   NUMBER                     NOT NULL,
    NOMINATION_ID NUMBER                     NOT NULL,
    MEMBER_ID     NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
(
    ID          NUMBER(38) NOT NULL,
    PAPER_ID    NUMBER(38) NOT NULL,
    REMOVAL_ID  NUMBER(38) NOT NULL,
    AGENT_ID    NUMBER(38) NOT NULL,
    MODIFIED_ON DATE       NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION
(
    ID               NUMBER                     NOT NULL,
    MEMBER_ID        NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION
(
    ID          NUMBER NOT NULL,
    ACTIVITY_ID NUMBER NOT NULL,
    ACTION_ID   NUMBER NOT NULL,
    MODIFIED_ON DATE   NOT NULL,
    AGENT_ID    NUMBER NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE
(
    ID          NUMBER       NOT NULL,
    NAME        VARCHAR2(64) NOT NULL,
    MODIFIED_ON DATE         NOT NULL,
    AGENT_ID    NUMBER       NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID  NUMBER GENERATED AS IDENTITY,
    ACTION      CHAR,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(100),
    NEW_VALUE   VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_LOCATION
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    LATITUDE     VARCHAR2(20),
    LONGITUDE    VARCHAR2(20),
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_ADDRESS
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    LINE_1       VARCHAR2(255),
    LINE_2       VARCHAR2(255),
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    ACTIVITY_ID NUMBER                     NOT NULL,
    RANK        NUMBER                     NOT NULL,
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.NOM_NOMINATION
(
    ID                          NUMBER                     NOT NULL,
    MEMBER_ID                   NUMBER                     NOT NULL,
    PRIORITY                    NUMBER,
    COMMENTS                    VARCHAR2(4000),
    ATLAS_FRACTION              NUMBER,
    CATEGORY_ID                 NUMBER                     NOT NULL,
    TYPE_ID                     NUMBER                     NOT NULL,
    PROFESSIONAL_ADVANCEMENT_ID NUMBER,
    SUBMITTER_ID                NUMBER                     NOT NULL,
    SUBMISSION_DATE             DATE                       NOT NULL,
    MODIFIED_ON                 DATE                       NOT NULL,
    AGENT_ID                    NUMBER                     NOT NULL,
    HISTORY_ID                  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                      CHAR                       NOT NULL,
    DIFF_COLUMN                 VARCHAR2(30),
    OLD_VALUE                   VARCHAR2(4000),
    NEW_VALUE                   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    CONF_ID     NUMBER                     NOT NULL,
    RANK        NUMBER                     NOT NULL,
    COMMENTS    VARCHAR2(1000),
    MODIFIED_ON DATE                       NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN
(
    ID               NUMBER                     NOT NULL,
    MEMBER_ID        NUMBER                     NOT NULL,
    CREDIT           NUMBER(6)                  NOT NULL,
    REMAINING_CREDIT NUMBER(6)                  NOT NULL,
    COMMENTS         VARCHAR2(255),
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_INSTITUTE
(
    ID                NUMBER(4)                  NOT NULL,
    NAME              VARCHAR2(255)              NOT NULL,
    SHORT_NAME        VARCHAR2(255)              NOT NULL,
    COUNTRY_ID        NUMBER(4)                  NOT NULL,
    PHONE             VARCHAR2(255)              NOT NULL,
    FAX               VARCHAR2(255),
    WEBPAGE           VARCHAR2(255),
    DOMAIN            VARCHAR2(255),
    TIME_ZONE         VARCHAR2(8),
    FUNDING_AGENCY_ID NUMBER(4)                  NOT NULL,
    SPIRES_ICN        VARCHAR2(255),
    MODIFIED_ON       DATE                       NOT NULL,
    AGENT_ID          NUMBER                     NOT NULL,
    HISTORY_ID        RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION            CHAR                       NOT NULL,
    DIFF_COLUMN       VARCHAR2(30),
    OLD_VALUE         VARCHAR2(4000),
    NEW_VALUE         VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    PRE_ADDRESS  VARCHAR2(255),
    ADDRESS      VARCHAR2(255)              NOT NULL,
    INFN_ADDRESS VARCHAR2(255),
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS
(
    ID          NUMBER                     NOT NULL,
    NAME        VARCHAR2(255)              NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR
(
    ID                  NUMBER                     NOT NULL,
    QUALIFICATION_ID    NUMBER                     NOT NULL,
    LOCAL_SUPERVISOR_ID NUMBER                     NOT NULL,
    MODIFIED_ON         TIMESTAMP(6)               NOT NULL,
    AGENT_ID            NUMBER                     NOT NULL,
    HISTORY_ID          RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION              CHAR                       NOT NULL,
    DIFF_COLUMN         VARCHAR2(30),
    OLD_VALUE           VARCHAR2(4000),
    NEW_VALUE           VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP
(
    ID             NUMBER                     NOT NULL,
    NOM_UPGRADE_ID NUMBER                     NOT NULL,
    MODIFIED_ON    DATE                       NOT NULL,
    AGENT_ID       NUMBER                     NOT NULL,
    HISTORY_ID     RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION         CHAR                       NOT NULL,
    DIFF_COLUMN    VARCHAR2(30),
    OLD_VALUE      VARCHAR2(4000),
    NEW_VALUE      VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION
(
    ID            NUMBER                     NOT NULL,
    INSTITUTE_ID  NUMBER                     NOT NULL,
    NOMINATION_ID NUMBER                     NOT NULL,
    MEMBER_ID     NUMBER                     NOT NULL,
    SYSTEM_ID     NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY
(
    ACTIVITY_ID        NUMBER NOT NULL,
    PARENT_ACTIVITY_ID NUMBER,
    MODIFIED_ON        DATE   NOT NULL,
    AGENT_ID           NUMBER NOT NULL,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID         NUMBER GENERATED AS IDENTITY,
    ACTION             CHAR,
    DIFF_COLUMN        VARCHAR2(30),
    OLD_VALUE          VARCHAR2(100),
    NEW_VALUE          VARCHAR2(100)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_PROJECT
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    PROJECT_ID       NUMBER(4)                  NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION
(
    ID                    NUMBER                     NOT NULL,
    DESCRIPTION           VARCHAR2(4000),
    QUALIFICATION_DATE    DATE,
    PROPOSED_START_DATE   DATE,
    START_DATE            DATE,
    END_DATE              DATE,
    STATE_ID              NUMBER,
    FTE                   NUMBER,
    INSTITUTE_INTEGRATION VARCHAR2(612 CHAR),
    MODIFIED_ON           TIMESTAMP(6)               NOT NULL,
    AGENT_ID              NUMBER                     NOT NULL,
    HISTORY_ID            RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION                CHAR                       NOT NULL,
    DIFF_COLUMN           VARCHAR2(30),
    OLD_VALUE             VARCHAR2(4000),
    NEW_VALUE             VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_ACTIVITY
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    ACTIVITY_ID      NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.NOM_UPGRADE
(
    ID            NUMBER                     NOT NULL,
    NOMINATION_ID NUMBER                     NOT NULL,
    MEMBER_ID     NUMBER                     NOT NULL,
    MODIFIED_ON   DATE                       NOT NULL,
    AGENT_ID      NUMBER                     NOT NULL,
    HISTORY_ID    RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION        CHAR                       NOT NULL,
    DIFF_COLUMN   VARCHAR2(30),
    OLD_VALUE     VARCHAR2(4000),
    NEW_VALUE     VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    CHECKPOINT_DATE  DATE                       NOT NULL,
    COMMENTS         VARCHAR2(400 CHAR)         NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_SECRETARY
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    NAME         VARCHAR2(255),
    PHONE        VARCHAR2(255),
    EMAIL        VARCHAR2(255),
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED
(
    ID           NUMBER                     NOT NULL,
    INSTITUTE_ID NUMBER(4)                  NOT NULL,
    MODIFIED_ON  DATE                       NOT NULL,
    AGENT_ID     NUMBER                     NOT NULL,
    HISTORY_ID   RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION       CHAR                       NOT NULL,
    DIFF_COLUMN  VARCHAR2(30),
    OLD_VALUE    VARCHAR2(4000),
    NEW_VALUE    VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_STATUS
(
    ID          NUMBER                     NOT NULL,
    NAME        VARCHAR2(20)               NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY
(
    ID               NUMBER                     NOT NULL,
    MEMBER_ID        NUMBER                     NOT NULL,
    CONF_PRIORITY_ID NUMBER                     NOT NULL,
    MODIFIED_ON      DATE                       NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER
(
    ID          NUMBER                     NOT NULL,
    MEMBER_ID   NUMBER                     NOT NULL,
    CERN_ID     NUMBER(8),
    FIRST_NAME  VARCHAR2(255)              NOT NULL,
    LAST_NAME   VARCHAR2(255)              NOT NULL,
    MODIFIED_ON TIMESTAMP(6)               NOT NULL,
    AGENT_ID    NUMBER                     NOT NULL,
    HISTORY_ID  RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION      CHAR                       NOT NULL,
    DIFF_COLUMN VARCHAR2(30),
    OLD_VALUE   VARCHAR2(4000),
    NEW_VALUE   VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY
(
    ID               NUMBER                     NOT NULL,
    QUALIFICATION_ID NUMBER                     NOT NULL,
    START_DATE       DATE                       NOT NULL,
    END_DATE         DATE                       NOT NULL,
    MODIFIED_ON      TIMESTAMP(6)               NOT NULL,
    AGENT_ID         NUMBER                     NOT NULL,
    HISTORY_ID       RAW(16) DEFAULT SYS_GUID() NOT NULL,
    ACTION           CHAR                       NOT NULL,
    DIFF_COLUMN      VARCHAR2(30),
    OLD_VALUE        VARCHAR2(4000),
    NEW_VALUE        VARCHAR2(4000)
)
/

CREATE TABLE ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT
(
    ID                  NUMBER    NOT NULL,
    MEMB_ID             NUMBER(6) NOT NULL,
    INST_ID             NUMBER(4) NOT NULL,
    START_DATE          DATE      NOT NULL,
    END_DATE            DATE      NOT NULL,
    DEATH_DATE          DATE,
    INST_PHONE          VARCHAR2(30),
    EMAIL               VARCHAR2(255),
    STUDENT             NUMBER(1) NOT NULL,
    ACTIVITY            NUMBER(16),
    PTYPE               NUMBER(4) NOT NULL,
    MOFREE              NUMBER(1),
    SUBDET              NUMBER(8),
    FOOTNOTE            VARCHAR2(255),
    AFLFLAG             NUMBER(1) NOT NULL,
    MAILFLAG            NUMBER(4),
    COMMENTS            VARCHAR2(255),
    MODIFIED_ON         DATE,
    MODIFIED_BY         NUMBER(8),
    INST_FOOTNOTE_ATLAS NUMBER(4),
    INST_FOOTNOTE_EXT   NUMBER(4),
    MAILFLAG_STARTDATE  DATE,
    MAILFLAG_ENDDATE    DATE,
    FUNDA_ID            NUMBER(4),
    AGENT               NUMBER(6),
    ONLEAVE_STARTDATE   DATE,
    ONLEAVE_ENDDATE     DATE,
    FOOTNOTE_ENDDATE    DATE,
    FOOTNOTE_STARTDATE  DATE,
    -- IDENTITY COLUMNS ARE ONLY SUPPORTED SINCE 12
    HISTORY_ID          NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY,
    ACTION              CHAR      NOT NULL,
    DIFF_COLUMN         VARCHAR2(30),
    OLD_VALUE           VARCHAR2(100),
    NEW_VALUE           VARCHAR2(100)
)
/

--rollback DECLARE
--rollback   V_TABLE_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR TABLE_REC IN (SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER = 'ATLAS_AUTHDB_HISTORY') LOOP
--rollback     V_TABLE_NAME := TABLE_REC.TABLE_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP TABLE ATLAS_AUTHDB_HISTORY.' || V_TABLE_NAME || ' PURGE';
--rollback   END LOOP;
--rollback END;
--rollback /