--liquibase formatted sql
--changeset Gabriel:11 endDelimiter:/ rollbackEndDelimiter:/

-- MV_FOUNDATION_MEMBER should be the first MV to be created since other MVs depend on it indirectly
create materialized view ATLAS_AUTHDB.MV_FOUNDATION_MEMBER
    refresh complete on demand
as
SELECT
    PERSON_ID,
    CERN_ID,
    LAST_NAME,
    FIRST_NAME,
    QUALITY,
    STATUS,
    CONTRACT_END_DATE,
    PERCENTAGE_PRESENCE,
    GENERIC_EMAIL,
    PREFERRED_EMAIL,
    ORCID,
    SEX,
    TELEPHONE1,
    TELEPHONE2,
    PORTABLE_PHONE,
    BUILDING,
    FLOOR,
    ROOM
FROM CERNDB_PARTICIPANT_ATLAS
/

create materialized view ATLAS_AUTHDB.MEMBER_INFORMATION
    refresh complete
as
SELECT
    ME_MEMBER.ID                                                  AS MEMBER_ID,
    ME_QUALIFICATION.QUALIFICATION_ID                             AS QUALIFICATION_ID,
    ME_MEMBER.ROWID                                               AS ME_MEMBER_ROWID,
    ME_STATUS.ROWID                                               AS ME_STATUS_ROWID,
    ME_ALLOW_PUBLIC_APPOINTMENT.ROWID                             AS ME_ALLOW_PUBLIC_APPOINTMENT_ROWID,
    ME_HR_IGNORE.ROWID                                            AS ME_HR_IGNORE_ROWID,
    ME_PRE_DATA_TAKEN.ROWID                                       AS ME_PRE_DATE_TAKEN_ROWID,
    ME_HIDE_ONLEAVE_DATE.ROWID                                    AS ME_HIDE_ONLEAVE_DATE_ROWID,
    ME_DELETED.ROWID                                              AS ME_DELETED_ROWID,
    ME_DECEASED_MEMBER.ROWID                                      AS ME_DECEASED_MEMBER_ROWID,
    ME_QUALIFICATION.ROWID                                        AS ME_QUALIFICATION_ROWID,
    MODIFIER.ROWID                                                AS MODIFIER_ROWID,
    Cast(NULL AS NUMBER(1))                                       AS STATUS_ATLAS_TEMP,
    0                                                             AS UPDATE_EMAIL,
    0                                                             AS TDAQ_AUTHOR,
    ME_MEMBER.INSPIRE                                             AS INSPIRE,
    ME_MEMBER.ORCID                                               AS ORCID,
    ME_MEMBER.INSPIRE_HEPNAME                                     AS INSPIRE_HEPNAME,
    Cast(ME_MEMBER.INITIALS AS VARCHAR(10))                       AS INITIALS,
    Cast(ME_MEMBER.CERN_CCID AS NUMBER(6))                        AS CERN_CCID,
    ME_MEMBER.COMMENTS                                            AS COMMENTS,
    ME_MEMBER.LAST_NAME_LATEX                                     AS LAST_NAME_LTX,
    ME_MEMBER.FIRST_NAME_LATEX                                    AS FIRST_NAME_LTX,
    ME_DECEASED_MEMBER.DEATH_DATE                                 AS DEATH_DATE,
    CASE
    WHEN ME_DECEASED_MEMBER.MEMBER_ID IS NOT NULL THEN 1
    ELSE 0
    END                                                           AS IS_DEAD,
    ME_MEMBER.OTP_ID                                              AS OTP_ID,
    ME_MEMBER.MODIFIED_ON                                         AS MODIFIED_ON,
    CASE
    WHEN ME_STATUS.NAME = 'Retired' THEN 'retired'
    WHEN ME_STATUS.NAME = 'Deceased' THEN 'deceased'
    ELSE 'none'
    END                                                           AS STATUS_ATLAS,
    CASE
    WHEN ME_HIDE_ONLEAVE_DATE.MEMBER_ID IS NOT NULL THEN 1
    ELSE 0
    END                                                           AS HIDE_ONLEAVE_DATE,
    CASE
    WHEN ME_ALLOW_PUBLIC_APPOINTMENT.MEMBER_ID IS NOT NULL THEN 1
    ELSE 0
    END                                                           AS ALLOW_PUBLIC_APPOINTMENT,
    CASE
    WHEN ME_HR_IGNORE.MEMBER_ID IS NOT NULL THEN 1
    ELSE 0
    END                                                           AS HR_IGNORE,
    Coalesce(ME_PRE_DATA_TAKEN.REMAINING_CREDIT, 0)               AS RPDTC,
    Coalesce(ME_PRE_DATA_TAKEN.CREDIT, 0)                         AS PRE_CREDIT,
    ME_PRE_DATA_TAKEN.COMMENTS                                    AS COMMENTS_PREDT,
    CASE
    WHEN ME_DELETED.MEMBER_ID IS NOT NULL THEN 1
    ELSE 0
    END                                                           AS DELETED,
    MODIFIER.CERN_CCID                                            AS AGENT,
    MODIFIER.CERN_CCID                                            AS MODIFIED_BY
FROM   ME_MEMBER,
    ME_STATUS,
    ME_HIDE_ONLEAVE_DATE,
    ME_ALLOW_PUBLIC_APPOINTMENT,
    ME_HR_IGNORE,
    ME_PRE_DATA_TAKEN,
    ME_DELETED,
    ME_DECEASED_MEMBER,
    ME_MEMBER MODIFIER,
    ME_QUALIFICATION
WHERE  ( ME_MEMBER.STATUS_ID = ME_STATUS.ID (+) )
    AND ( ME_MEMBER.ID = ME_HIDE_ONLEAVE_DATE.MEMBER_ID (+) )
    AND ( ME_MEMBER.ID = ME_ALLOW_PUBLIC_APPOINTMENT.MEMBER_ID (+) )
    AND ( ME_MEMBER.ID = ME_HR_IGNORE.MEMBER_ID (+) )
    AND ( ME_MEMBER.ID = ME_PRE_DATA_TAKEN.MEMBER_ID (+) )
    AND ( ME_MEMBER.ID = ME_DELETED.MEMBER_ID (+) )
    AND ( ME_MEMBER.ID = ME_DECEASED_MEMBER.MEMBER_ID (+) )
    AND ( ME_MEMBER.AGENT_ID = MODIFIER.ID )
    AND ( ME_MEMBER.ID = ME_QUALIFICATION.MEMBER_ID (+) )
/

create materialized view ATLAS_AUTHDB.QUALIFICATION_INFORMATION
    refresh force on commit
as
SELECT QU_QUALIFICATION.ID                                           AS QU_QUALIFICATION_ID,
          QU_QUAL_FLAG.ID                                               AS QU_QUAL_FLAG_ID,
          QU_PROJECT.ID                                                 AS QU_PROJECT_ID,
          QU_ACTIVITY.ID                                                AS QU_ACTIVITY_ID,
          QU_SIGNING_ONLY.ID                                            AS QU_SIGNING_ONLY_ID,
          QU_LOCAL_SUPERVISOR.ID                                        AS QU_LOCAL_SUP_ID,
          QU_TECHNICAL_SUPERVISOR.ID                                    AS QU_TECHNICAL_SUP_ID,
          QU_CHECKPOINT.ID                                              AS QU_CHECKPOINT_ID,
          QU_FUTURE.ID                                                  AS QU_FUTURE_ID,
          QU_FINAL_REPORT.ID                                            AS QU_FINAL_REPORT_ID,
          QU_EXCLUDE_PRE_DATA_TAKEN.ID                                  AS QU_EXCLUDE_PRE_DATA_TAKEN_ID,
          QU_QUALIFICATION.FTE                                          AS QUAL_FTE,
          QU_QUALIFICATION.IS_PRE_TDR                                   AS IS_PRE_TDR,
          Cast(QU_QUALIFICATION.INSTITUTE_INTEGRATION AS VARCHAR2(512)) AS QUAL_INTEGRATION,
          Cast(QU_QUALIFICATION.PROPOSED_START_DATE AS DATE)            AS PROPOSED_QUAL_START,
          Cast(QU_QUALIFICATION.QUALIFICATION_DATE AS DATE)             AS QUAL_DATE,
          Cast(QU_QUALIFICATION.DESCRIPTION AS VARCHAR2(4000))          AS QUAL_STRING,
          Cast(QU_QUALIFICATION.END_DATE AS DATE)                       AS QUAL_END,
          Cast(QU_QUALIFICATION.START_DATE AS DATE)                     AS QUAL_START,
          CASE
            WHEN QU_QUALIFICATION.STATE_ID = 9 THEN 7
            WHEN QU_QUALIFICATION.STATE_ID = 8 THEN 6
            WHEN QU_QUALIFICATION.STATE_ID = 7 THEN 5
            WHEN QU_QUALIFICATION.STATE_ID = 6 THEN 4
            WHEN QU_QUALIFICATION.STATE_ID = 5 THEN 3
            WHEN QU_QUALIFICATION.STATE_ID = 4 THEN 2
            WHEN QU_QUALIFICATION.STATE_ID = 3 THEN 1
            WHEN QU_QUALIFICATION.STATE_ID = 2 THEN 0
            WHEN QU_QUALIFICATION.STATE_ID = 1 THEN NULL
          END                                                           AS QUAL_STATE,
          CASE
            WHEN QU_QUAL_FLAG.QUALIFICATION_ID IS NOT NULL THEN 1
            ELSE 0
          END                                                           AS QUAL_FLAG,
          QU_PROJECT.PROJECT_ID                                         AS QUAL_RESP,
          Cast(ACTIVITIES.ID AS NUMBER(4))                              AS QUAL_ACTIVITY,
          QU_SIGNING_ONLY.START_DATE                                    AS SSA,
          QU_SIGNING_ONLY.END_DATE                                      AS SO_END_DATE,
          QU_LOCAL_SUPERVISOR.LOCAL_SUPERVISOR_ID                       AS LOCAL_SUPERVISOR,
          QU_TECHNICAL_SUPERVISOR.TECHNICAL_SUPERVISOR_ID               AS TECH_SUPERVISOR,
          QU_CHECKPOINT.CHECKPOINT_DATE                                 AS CHECKPOINT_DATE,
          QU_CHECKPOINT.COMMENTS                                        AS CHECKPOINT_COMMENTS,
          QU_FUTURE.FUTURE_STATUS_ID                                    AS QUAL_FUTURE,
          QU_FINAL_REPORT.FINAL_REPORT_DATE                             AS FINAL_REPORT_DATE,
          QU_FINAL_REPORT.COMMENTS                                      AS FINAL_REPORT_COMMENTS,
          QU_FINAL_REPORT.DOCUMENTATION_URL                             AS FINAL_REPORT_DOCUMENTATION_URL,
          CASE
            WHEN QU_EXCLUDE_PRE_DATA_TAKEN.QUALIFICATION_ID IS NOT NULL THEN 1
            ELSE 0
          END                                                           AS EXCLUDE_PREDT
   FROM   QU_QUALIFICATION,
          QU_QUAL_FLAG,
          QU_PROJECT,
          QU_ACTIVITY,
          ACTIVITIES,
          QU_SIGNING_ONLY,
          QU_LOCAL_SUPERVISOR,
          QU_TECHNICAL_SUPERVISOR,
          QU_CHECKPOINT,
          QU_FUTURE,
          QU_FINAL_REPORT,
          QU_EXCLUDE_PRE_DATA_TAKEN
   WHERE  ( QU_QUALIFICATION.ID = QU_QUAL_FLAG.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_PROJECT.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_ACTIVITY.QUALIFICATION_ID (+) )
          AND ( QU_ACTIVITY.ACTIVITY_ID = ACTIVITIES.NEW_ACTIVITY_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_SIGNING_ONLY.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_LOCAL_SUPERVISOR.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_TECHNICAL_SUPERVISOR.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_CHECKPOINT.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_FUTURE.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_FINAL_REPORT.QUALIFICATION_ID (+) )
          AND ( QU_QUALIFICATION.ID = QU_EXCLUDE_PRE_DATA_TAKEN.QUALIFICATION_ID (+) )
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_MV
    refresh force on demand
as
SELECT * FROM otp_contribution
/

create materialized view ATLAS_AUTHDB.MV_ME_SEGREGATED_TDAQ_NOMS
    refresh force on demand
as
SELECT n.member_id
     , COUNT(n.id)                                                             AS count
     , MIN(s.RANK)                                                             AS score
     , MIN(n.priority)                                                         AS max_priority
     , LISTAGG(n.id, '|') WITHIN GROUP (ORDER BY n.id)                         AS ids
     , LISTAGG(it_nom.comments, '|') WITHIN GROUP (ORDER BY N.ID)              AS it_nom_comments
     , LISTAGG(it_nom.nomination_id, '|') WITHIN GROUP (ORDER BY N.ID)         AS it_nom_ids
     , LISTAGG(it_nom.priorities, '|') WITHIN GROUP (ORDER BY N.ID)            AS it_nom_priorities
     , LISTAGG(it_nom.atlas_fractions, '|') WITHIN GROUP (ORDER BY N.ID)       AS it_nom_atlas_fractions
     , LISTAGG(it_nom.advancements_ids, '|') WITHIN GROUP (ORDER BY N.ID)      AS it_nom_advancements_ids
     , LISTAGG(it_nom.advancements, '|') WITHIN GROUP (ORDER BY N.ID)          AS it_nom_advancements
     , LISTAGG(it_nom.categories, '|') WITHIN GROUP (ORDER BY N.ID)            AS it_nom_categories
     , LISTAGG(it_nom.types, '|') WITHIN GROUP (ORDER BY N.ID)                 AS it_nom_types
     , LISTAGG(it_nom.institutes_ids, '|') WITHIN GROUP (ORDER BY N.ID)        AS it_nom_institutes_ids
     , LISTAGG(it_nom.institutes_onames, '|') WITHIN GROUP (ORDER BY N.ID)     AS it_nom_institutes
     , LISTAGG(it_nom.submission_dates, '|') WITHIN GROUP (ORDER BY N.ID)      AS it_nom_submission_dates
     , LISTAGG(it_nom.submitters_ids, '|') WITHIN GROUP (ORDER BY n.id)        AS it_nom_submitters_ids
     , LISTAGG(it_nom.submitters_ccids, '|') WITHIN GROUP (ORDER BY n.id)      AS it_nom_submitters_ccids
     , LISTAGG(it_nom.submitters_lastnames, '|') WITHIN GROUP (ORDER BY n.id)  AS it_nom_submitters_lastnames
     , LISTAGG(it_nom.submitters_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_submitters_firstnames
     , LISTAGG(it_nom.modified_on_dates, '|') WITHIN GROUP (ORDER BY N.ID)     AS it_nom_modified_on_dates
     , LISTAGG(it_nom.modifiers_ids, '|') WITHIN GROUP (ORDER BY n.id)         AS it_nom_modifiers_ids
     , LISTAGG(it_nom.modifiers_ccids, '|') WITHIN GROUP (ORDER BY n.id)       AS it_nom_modifiers_ccids
     , LISTAGG(it_nom.modifiers_lastnames, '|') WITHIN GROUP (ORDER BY n.id)   AS it_nom_modifiers_lastnames
     , LISTAGG(it_nom.modifiers_firstnames, '|') WITHIN GROUP (ORDER BY n.id)  AS it_nom_modifiers_firstnames
     , LISTAGG(it_nom.sub_ids, '|') WITHIN GROUP (ORDER BY n.id)               AS it_nom_sub_ids
     , LISTAGG(it_nom.sub_names, '|') WITHIN GROUP (ORDER BY n.id)             AS it_nom_sub_names
     , LISTAGG(it_nom.parents_ids, '|') WITHIN GROUP (ORDER BY n.id)           AS it_nom_sub_parents_ids
     , LISTAGG(it_nom.parents_names, '|') WITHIN GROUP (ORDER BY n.id)         AS it_nom_sub_parents_names
     , LISTAGG(at_nom.nomination_id, '|') WITHIN GROUP (ORDER BY N.ID)         AS at_nom_ids
     , LISTAGG(at_nom.comments, '|') WITHIN GROUP (ORDER BY N.ID)              AS at_nom_comments
     , LISTAGG(at_nom.priorities, '|') WITHIN GROUP (ORDER BY N.ID)            AS at_nom_priorities
     , LISTAGG(at_nom.upgrades, '|') WITHIN GROUP (ORDER BY N.ID)              AS at_nom_upgrades
     , LISTAGG(at_nom.ignore_otp_upgrades, '|') WITHIN GROUP (ORDER BY N.ID)   AS at_nom_ignore_otp_upgrades
     , LISTAGG(at_nom.categories, '|') WITHIN GROUP (ORDER BY N.ID)            AS at_nom_categories
     , LISTAGG(at_nom.types, '|') WITHIN GROUP (ORDER BY N.ID)                 AS at_nom_types
     , LISTAGG(at_nom.activities_ids, '|') WITHIN GROUP (ORDER BY N.ID)        AS at_nom_activities_ids
     , LISTAGG(at_nom.activities_names, '|') WITHIN GROUP (ORDER BY N.ID)      AS at_nom_activities
     , LISTAGG(at_nom.submission_dates, '|') WITHIN GROUP (ORDER BY N.ID)      AS at_nom_submission_dates
     , LISTAGG(at_nom.submitters_ids, '|') WITHIN GROUP (ORDER BY n.id)        AS at_nom_submitters_ids
     , LISTAGG(at_nom.submitters_ccids, '|') WITHIN GROUP (ORDER BY n.id)      AS at_nom_submitters_ccids
     , LISTAGG(at_nom.submitters_lastnames, '|') WITHIN GROUP (ORDER BY n.id)  AS at_nom_submitters_lastnames
     , LISTAGG(at_nom.submitters_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_submitters_firstnames
     , LISTAGG(at_nom.modified_on_dates, '|') WITHIN GROUP (ORDER BY N.ID)     AS at_nom_modified_on_dates
     , LISTAGG(at_nom.modifiers_ids, '|') WITHIN GROUP (ORDER BY n.id)         AS at_nom_modifiers_ids
     , LISTAGG(at_nom.modifiers_ccids, '|') WITHIN GROUP (ORDER BY n.id)       AS at_nom_modifiers_ccids
     , LISTAGG(at_nom.modifiers_lastnames, '|') WITHIN GROUP (ORDER BY n.id)   AS at_nom_modifiers_lastnames
     , LISTAGG(at_nom.modifiers_firstnames, '|') WITHIN GROUP (ORDER BY n.id)  AS at_nom_modifiers_firstnames
     , LISTAGG(at_nom.sub_ids, '|') WITHIN GROUP (ORDER BY n.id)               AS at_nom_sub_ids
     , LISTAGG(at_nom.sub_names, '|') WITHIN GROUP (ORDER BY n.id)             AS at_nom_sub_names
     , LISTAGG(at_nom.parents_ids, '|') WITHIN GROUP (ORDER BY n.id)           AS at_nom_sub_parents_ids
     , LISTAGG(at_nom.parents_names, '|') WITHIN GROUP (ORDER BY n.id)         AS at_nom_sub_parents_names
FROM V_MEMBER_NOMINATIONS n
     LEFT JOIN V_ACTIVITY_NOMINATIONS at_nom ON at_nom.nomination_id = n.id
     LEFT JOIN V_INSTITUTE_NOMINATIONS it_nom ON it_nom.nomination_id = n.id
     LEFT JOIN SCABBER s ON n.MEMBER_ID = s.MEMB_ID
WHERE n.system_id = 3
GROUP BY n.member_id
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_TODAY_MV
    refresh force on demand
as
SELECT * FROM otp_contribution_today
/

create materialized view ATLAS_AUTHDB.MV_MEMBER_SEGREGATED_SCAB_NOMS
    refresh complete on demand
as
SELECT n.member_id
        , COUNT(n.id) AS count
        , MIN(s.RANK) as score
        , LISTAGG(n.id, '|') WITHIN GROUP (ORDER BY n.id) AS ids
        , LISTAGG(it_nom.comments, '|') WITHIN GROUP (ORDER BY N.ID) AS it_nom_comments
        , LISTAGG(it_nom.nomination_id, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_ids
        , LISTAGG(it_nom.priorities, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_priorities
        , LISTAGG(it_nom.atlas_fractions, '|') WITHIN GROUP (ORDER BY N.ID) AS it_nom_atlas_fractions
        , LISTAGG(it_nom.advancements_ids, '|') WITHIN GROUP (ORDER BY N.ID) AS it_nom_advancements_ids
        , LISTAGG(it_nom.advancements, '|') WITHIN GROUP (ORDER BY N.ID) AS it_nom_advancements
        , LISTAGG(it_nom.categories, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_categories
        , LISTAGG(it_nom.types, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_types
        , LISTAGG(it_nom.institutes_ids, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_institutes_ids
        , LISTAGG(it_nom.institutes_onames, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_institutes
        , LISTAGG(it_nom.submission_dates, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_submission_dates
        , LISTAGG(it_nom.submitters_ids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_submitters_ids
        , LISTAGG(it_nom.submitters_ccids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_submitters_ccids
        , LISTAGG(it_nom.submitters_lastnames, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_submitters_lastnames
        , LISTAGG(it_nom.submitters_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_submitters_firstnames
        , LISTAGG(it_nom.modified_on_dates, '|' ) WITHIN GROUP (ORDER BY N.ID) AS it_nom_modified_on_dates
        , LISTAGG(it_nom.modifiers_ids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_modifiers_ids
        , LISTAGG(it_nom.modifiers_ccids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_modifiers_ccids
        , LISTAGG(it_nom.modifiers_lastnames, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_modifiers_lastnames
        , LISTAGG(it_nom.modifiers_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_modifiers_firstnames
        , LISTAGG(it_nom.sub_ids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_sub_ids
        , LISTAGG(it_nom.sub_names, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_sub_names
        , LISTAGG(it_nom.parents_ids, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_sub_parents_ids
        , LISTAGG(it_nom.parents_names, '|') WITHIN GROUP (ORDER BY n.id) AS it_nom_sub_parents_names
        , LISTAGG(at_nom.nomination_id, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_ids
        , LISTAGG(at_nom.comments, '|') WITHIN GROUP (ORDER BY N.ID) AS at_nom_comments
        , LISTAGG(at_nom.priorities, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_priorities
        , LISTAGG(at_nom.upgrades, '|') WITHIN GROUP (ORDER BY N.ID) AS at_nom_upgrades
        , LISTAGG(at_nom.ignore_otp_upgrades, '|') WITHIN GROUP (ORDER BY N.ID) AS at_nom_ignore_otp_upgrades
        , LISTAGG(at_nom.categories, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_categories
        , LISTAGG(at_nom.types, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_types
        , LISTAGG(at_nom.activities_ids, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_activities_ids
        , LISTAGG(at_nom.activities_names, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_activities
        , LISTAGG(at_nom.submission_dates, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_submission_dates
        , LISTAGG(at_nom.submitters_ids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_submitters_ids
        , LISTAGG(at_nom.submitters_ccids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_submitters_ccids
        , LISTAGG(at_nom.submitters_lastnames, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_submitters_lastnames
        , LISTAGG(at_nom.submitters_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_submitters_firstnames
        , LISTAGG(at_nom.modified_on_dates, '|' ) WITHIN GROUP (ORDER BY N.ID) AS at_nom_modified_on_dates
        , LISTAGG(at_nom.modifiers_ids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_modifiers_ids
        , LISTAGG(at_nom.modifiers_ccids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_modifiers_ccids
        , LISTAGG(at_nom.modifiers_lastnames, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_modifiers_lastnames
        , LISTAGG(at_nom.modifiers_firstnames, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_modifiers_firstnames
        , LISTAGG(at_nom.sub_ids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_sub_ids
        , LISTAGG(at_nom.sub_names, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_sub_names
        , LISTAGG(at_nom.parents_ids, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_sub_parents_ids
        , LISTAGG(at_nom.parents_names, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_sub_parents_names
        , LISTAGG(at_nom.seed_activities, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_seed_activities
        , LISTAGG(at_nom.seed_priorities, '|') WITHIN GROUP (ORDER BY n.id) AS at_nom_seed_priorities
    FROM V_MEMBER_SCAB_NOMINATIONS n
    LEFT JOIN V_ACTIVITY_NOMINATIONS at_nom ON at_nom.nomination_id = n.id AND at_nom.SYSTEM_ID = 1
    LEFT JOIN V_INSTITUTE_NOMINATIONS it_nom ON it_nom.nomination_id = n.id AND it_nom.SYSTEM_ID = 1
    LEFT JOIN SCABBER s ON n.MEMBER_ID = s.MEMB_ID
    GROUP BY n.member_id
/

create materialized view ATLAS_AUTHDB.OTP_UPGRADE_2018
            (ALLOCATED_PERSON_ID)
    refresh force on demand
as
SELECT UNIQUE(allocated_person_id) FROM otp_activities_upgrade otp_upgrade WHERE EXTRACT(YEAR FROM otp_upgrade."DATE") = 2018
/

create materialized view ATLAS_AUTHDB.OTP_UPGRADE_2019
            (ALLOCATED_PERSON_ID)
    refresh force on demand
as
SELECT UNIQUE(allocated_person_id) FROM otp_activities_upgrade WHERE EXTRACT(YEAR FROM OTP_ACTIVITIES_UPGRADE."DATE") = 2019
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_UPGRADE_MV
    refresh force on demand
as
SELECT 
    *
FROM 
    OTP_CONTRIBUTION_UPGRADE
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_TODAY_NEW_MV
    refresh force on demand
as
SELECT * FROM otp_contribution_today_onleave
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_HGTD_MV
    refresh force on demand
as
SELECT 
    *
FROM 
    OTP_CONTRIBUTION_HGTD
/

create materialized view ATLAS_AUTHDB.OTP_CONTRIBUTION_HGTD_2020_MV
    refresh force on demand
as
SELECT * FROM OTP_CONTRIBUTION_HGTD_2020
/

create materialized view ATLAS_AUTHDB.OTP_ACTIVITIES_UPGRADE_MV
    refresh force on demand
as
SELECT 
    *
FROM 
    OTP_ACTIVITIES_UPGRADE
/

create materialized view ATLAS_AUTHDB.MEMB_ATLAS_ORGANISATION
    refresh complete on demand
as
SELECT  MEMB_APPOINTMENT.APPOINTMENT_ID, 
        stragg(MEMB.FIRST_NAME || ' ' || MEMB.LAST_NAME) AS NAME,
        APPOINTMENT.NAME AS APP_NAME,
        PREVIOUS_CONVENERS.NAMES,
        APPOINTMENT_CATEGORY.CATEGORY_ID AS CATEGORY,
        APPOINTMENT_CATEGORIES.NAME AS CATEGORY_NAME,
        APPOINTMENT_CATEGORY_TREE.PARENT_CATEGORY_ID AS PARENT
FROM MEMB_APPOINTMENT 
      JOIN MEMB ON MEMB_APPOINTMENT.MEMB_ID = MEMB.ID
      JOIN APPOINTMENT ON MEMB_APPOINTMENT.APPOINTMENT_ID = APPOINTMENT.ID
      JOIN APPOINTMENT_CATEGORY ON APPOINTMENT.ID = APPOINTMENT_CATEGORY.APPOINTMENT_ID
      JOIN APPOINTMENT_CATEGORIES ON APPOINTMENT_CATEGORY.CATEGORY_ID = APPOINTMENT_CATEGORIES.ID
      LEFT OUTER JOIN APPOINTMENT_CATEGORY_TREE ON APPOINTMENT_CATEGORY.CATEGORY_ID = APPOINTMENT_CATEGORY_TREE.CATEGORY_ID
      LEFT OUTER JOIN (
            SELECT  stragg(distinct MEMB.FIRST_NAME || ' ' || MEMB.LAST_NAME || ' (' || to_char(M_APPOINTMENT.START_DATE,'MM/YYYY') || '-' || to_char(M_APPOINTMENT.END_DATE,'MM/YYYY') || ')') AS NAMES,
                    M_APPOINTMENT.APPOINTMENT_ID AS APPOINTMENT_ID
            FROM
                    MEMB JOIN (
                      SELECT  MEMB_APPOINTMENT.MEMB_ID AS MEMB_ID,
                              MEMB_APPOINTMENT.START_DATE AS START_DATE,
                              MEMB_APPOINTMENT.END_DATE AS END_DATE,
                              MEMB_APPOINTMENT.APPOINTMENT_ID AS APPOINTMENT_ID
                      FROM MEMB_APPOINTMENT
                      ORDER BY MEMB_APPOINTMENT.END_DATE) M_APPOINTMENT ON MEMB.ID = M_APPOINTMENT.MEMB_ID
            WHERE
                    M_APPOINTMENT.END_DATE < SYSDATE AND add_months(SYSDATE,-60) < M_APPOINTMENT.START_DATE
            GROUP BY 
                    M_APPOINTMENT.APPOINTMENT_ID
      ) PREVIOUS_CONVENERS ON PREVIOUS_CONVENERS.APPOINTMENT_ID = APPOINTMENT.ID
WHERE 
      (SYSDATE BETWEEN MEMB_APPOINTMENT.START_DATE AND MEMB_APPOINTMENT.END_DATE) AND (APPOINTMENT_CATEGORY_TREE.PARENT_CATEGORY_ID = 'AC-PR' OR APPOINTMENT_CATEGORY.CATEGORY_ID = 'GENERAL')
GROUP BY
      MEMB_APPOINTMENT.APPOINTMENT_ID, APPOINTMENT.NAME, APPOINTMENT_CATEGORY.CATEGORY_ID,
      APPOINTMENT_CATEGORIES.NAME, APPOINTMENT_CATEGORY_TREE.PARENT_CATEGORY_ID, PREVIOUS_CONVENERS.NAMES
/

--rollback DECLARE
--rollback   V_MVIEW_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR MVIEW_REC IN (SELECT MVIEW_NAME FROM ALL_MVIEWS WHERE OWNER = 'ATLAS_AUTHDB') LOOP
--rollback     V_MVIEW_NAME := MVIEW_REC.MVIEW_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP MATERIALIZED VIEW ATLAS_AUTHDB.' || V_MVIEW_NAME;
--rollback   END LOOP;
--rollback END;
--rollback /