--liquibase formatted sql
--changeset Gabriel:9 endDelimiter:/ rollbackEndDelimiter:/

CREATE FUNCTION atlas_authdb.OTP_ROOT_WBS(WBS_ID IN NUMBER) RETURN VARCHAR2 IS RESULT VARCHAR2(255);
BEGIN
    SELECT TITLE
    INTO RESULT
    FROM OTP_PUB_WBS_NODE
    WHERE CONNECT_BY_ISLEAF = 1
    START WITH ID = WBS_ID
    CONNECT BY ID = PRIOR PARENT_ID;
    RETURN RESULT;
END;
/

CREATE FUNCTION atlas_authdb.OTP_DATE_ID_YEAR(DATE_ID IN NUMBER) RETURN NUMBER IS RESULT NUMBER;
BEGIN
    RETURN TRUNC(DATE_ID/100000);
END;
/

create FUNCTION atlas_authdb.is_number (p_string IN VARCHAR2)
  RETURN INT
IS
  v_num NUMBER;
BEGIN
  v_num := TO_NUMBER(p_string);
  RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
  RETURN 0;
END is_number;
/

create FUNCTION atlas_authdb.can_add_new_activity_id (act_id IN INT)
    RETURN INT
AS
    qu_activity_table_cnt INT;
    activities_table_cnt INT;
BEGIN
    IF act_id IS NULL THEN RETURN 1;
    END IF;

    SELECT count(*) INTO qu_activity_table_cnt FROM qu_activity WHERE activity_id = act_id;
    SELECT count(*) INTO activities_table_cnt FROM activities WHERE new_activity_id = act_id;

    IF (qu_activity_table_cnt > 0 AND activities_table_cnt > 0) THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;

/

create FUNCTION atlas_authdb.defineConfNoteInternalCode
  (new_lead_group IN VARCHAR2,
   new_creation_date IN DATE,
   new_sequence_number OUT NUMBER)
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  code VARCHAR2(30);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the INTERNAL_CODE like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  --DECODE(max(sequence_number), NULL, 0, max(sequence_number))
  select max(confnote_publication.internal_sequence_number) into max_sequence_number from confnote_publication
    where extract(year from confnote_publication.creation_date) = extract(year from new_creation_date)
    and confnote_publication.lead_group = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

  new_sequence_number := max_sequence_number + 1;
  code := 'CONF-' || aux_lead_group || '-' || extract(year from new_creation_date) || '-' || trim(to_char(new_sequence_number, '09'));

  RETURN code;

END;
/

create FUNCTION atlas_authdb.dec2bin (N in number) RETURN varchar2 IS
  binval varchar2(64);
  N2     number := N;
BEGIN
  while ( N2 > 0 ) loop
     binval := mod(N2, 2) || binval;
     N2 := trunc( N2 / 2 );
  end loop;
  return binval;
END dec2bin;
/

create FUNCTION atlas_authdb.defineConfNoteReferenceCode
  (new_lead_group IN VARCHAR2,
   new_year IN NUMBER
  -- ,new_sequence_number OUT NUMBER
   )
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  temp_ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  select max(confnote_publication.sequence_number) into max_sequence_number from confnote_publication
    where substr(temp_ref_code,11,4) = new_year
    and substr(temp_ref_code,6,4) = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

  temp_ref_code := aux_lead_group || '-' || new_year || '-' || trim(to_char(max_sequence_number + 1, '09'));

  RETURN temp_ref_code;

END;
/

create FUNCTION atlas_authdb."DEFINEANALYSISSYSREFERENCECODE"
  (new_lead_group IN VARCHAR2,
   new_year IN NUMBER
   )
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the REF_CODE (reference code) like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  select max(analysis_sys.sequence_number) into max_sequence_number from analysis_sys
    where substr(ref_code,10,4) = new_year
    and substr(ref_code,5,4) = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

  ref_code := aux_lead_group || '-' || new_year || '-' || trim(to_char(max_sequence_number + 1, '09'));

  RETURN ref_code;

END;
/

create function atlas_authdb.photo_url_function(atlas_authdb.cern_ccid number) return varchar2

as

begin

    return  HRPUB.ATLAS_PICS.GET_URL@cerndb1(cern_ccid);

    exception
        when others then
        return '';


end;
/

create FUNCTION atlas_authdb.bitor(x IN NUMBER, y IN NUMBER) RETURN NUMBER AS
BEGIN
    RETURN x + y - bitand(x,y);
END;
/

create FUNCTION atlas_authdb.stragg(input varchar2 )
  RETURN varchar2
  PARALLEL_ENABLE AGGREGATE USING string_agg_type;
/

create function atlas_authdb.clagg( input clob )
return clob
deterministic
parallel_enable aggregate using clagg_type;
/

create FUNCTION atlas_authdb.definePubnoteReferenceCode
  (new_lead_group IN VARCHAR2,
   new_year IN NUMBER
  -- ,new_sequence_number OUT NUMBER
   )
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  temp_ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the REF_CODE (reference code) like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  --DECODE(max(sequence_number), NULL, 0, max(sequence_number))
  select max(pubnote_publication.sequence_number) into max_sequence_number from pubnote_publication
    where substr(temp_ref_code,10,4) = CAST(new_year AS varchar(4))
    and substr(temp_ref_code,5,4) = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

 -- new_sequence_number := max_sequence_number + 1;
  temp_ref_code := aux_lead_group || '-' || new_year || '-' || trim(to_char(max_sequence_number + 1, '09'));

  RETURN temp_ref_code;

END;
/

create FUNCTION atlas_authdb.bin2dec (binval in char) RETURN number IS
  i                 number;
  digits            number;
  result            number := 0;
  current_digit     char(1);
  current_digit_dec number;
BEGIN
  digits := length(binval);
  for i in 1..digits loop
     current_digit := SUBSTR(binval, i, 1);
     current_digit_dec := to_number(current_digit);
     result := (result * 2) + current_digit_dec;
  end loop;
  return result;
END bin2dec;
/

create FUNCTION atlas_authdb.ANALYSIS_DATA_FUNC (client_name VARCHAR2, asys_id VARCHAR2)
RETURN analysis_tab
AS
    -- The AUTONOMOUS_TRANSACTION and the rollback at the end of the proc are neccessary to close
    -- the transaction open by Oracle because of the used DB link.
    PRAGMA AUTONOMOUS_TRANSACTION;

    sql_tab analysis_tab := analysis_tab();

    time_now DATE := SYSDATE;

    rel_module_template VARCHAR2(250);
    v_code              NUMBER;
    v_errm              VARCHAR2(512);
BEGIN
    -- Acess log information
    INSERT INTO ORDS_LOG (date_time, client_name)
    VALUES (time_now, client_name);
    COMMIT;

    IF UPPER(client_name) = 'CAP'
    THEN
        IF asys_id IS NULL
            THEN
            SELECT analysis_obj(id, short_title, full_title, pub_short_title,
                                creation_date, refcode, status) BULK COLLECT INTO sql_tab
            FROM (
                    SELECT asys.id,
                    short_title,
                    full_title,
                    pub_short_title,
                    creation_date,
                    ref_code AS refcode,
                    status
                    FROM ANALYSIS_SYS asys
                );
        ELSIF IS_NUMBER(asys_id) = 1
        THEN
            SELECT analysis_obj(id, short_title, full_title, pub_short_title,
                                creation_date, refcode, status) BULK COLLECT INTO sql_tab
            FROM (
                    SELECT asys.id,
                    short_title,
                    full_title,
                    pub_short_title,
                    creation_date,
                    ref_code AS refcode,
                    status
                    FROM ANALYSIS_SYS asys
                    where asys.id = asys_id
                );
        END IF;
    END IF;

    -- Sometimes the following query returns more than 1 rows
    SELECT  'https://oraweb.cern.ch/ords/atlr/atlas_authdb' ||
            (SELECT URI_PREFIX FROM USER_ORDS_MODULES WHERE id = module_id) || URI_TEMPLATE
            || '?client_name="'||client_name||'"&id="'||asys_id||'"' INTO rel_module_template
    FROM USER_ORDS_TEMPLATES WHERE id IN
    (
        SELECT template_id from USER_ORDS_HANDLERS WHERE UPPER(source) LIKE upper('%/*version3.0*/%')
    );

    UPDATE ORDS_LOG
    SET accessed_url = rel_module_template
    WHERE date_time = time_now;
    COMMIT;


RETURN sql_tab;
EXCEPTION
        WHEN OTHERS THEN

        v_code := SQLCODE;
        v_errm := SQLERRM || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
        DBMS_OUTPUT.PUT_LINE('Error code ' || v_code || ': ' || v_errm);

        UPDATE ORDS_LOG
        SET error_msg = 'Error code ' || v_code || ': ' || v_errm
        WHERE date_time = time_now;
        COMMIT;

    RAISE;

END ANALYSIS_DATA_FUNC;
/

create FUNCTION atlas_authdb.defineAnalysisReferenceCode_v2
  (new_lead_group IN VARCHAR2,
   new_year IN NUMBER
  -- ,new_sequence_number OUT NUMBER
   )
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the REF_CODE (reference code) like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  --DECODE(max(sequence_number), NULL, 0, max(sequence_number))
  select max(publication.sequence_number) into max_sequence_number from publication
    where substr(ref_code,6,4) = new_year
    and substr(ref_code,1,4) = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

 -- new_sequence_number := max_sequence_number + 1;
  ref_code := aux_lead_group || '-' || new_year || '-' || trim(to_char(max_sequence_number + 1, '09'));

  RETURN ref_code;

END;
/

create FUNCTION atlas_authdb."DEFINEANALYSISREFERENCECODE"
  (new_lead_group IN VARCHAR2,
   new_creation_date IN DATE,
   new_sequence_number OUT NUMBER)
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the REF_CODE (reference code) like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  --DECODE(max(sequence_number), NULL, 0, max(sequence_number))
  select max(publication.sequence_number) into max_sequence_number from publication
    where extract(year from publication.creation_date) = extract(year from new_creation_date)
    and publication.lead_group = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

  new_sequence_number := max_sequence_number + 1;
  ref_code := aux_lead_group || '-' || extract(year from new_creation_date) || '-' || trim(to_char(new_sequence_number, '09'));

  RETURN ref_code;

END;
/

create FUNCTION atlas_authdb."DEFINEPLOTREFERENCECODE"
  (new_lead_group IN VARCHAR2,
   new_year IN NUMBER
   )
  RETURN VARCHAR2
AS
  max_sequence_number NUMBER;
  ref_code VARCHAR2(16);
  aux_lead_group VARCHAR2(8);
BEGIN
  -- Determines the REF_CODE (reference code) like <LEAD_GROUP>-<YEAR>-<SEQUENCE_NUMBER>
  -- Makes NONE-<YEAR>-<SEQUENCE_NUMBER> when lead_group is not provided

  IF (new_lead_group IS NULL OR new_lead_group = '') THEN
    aux_lead_group := 'NONE';
  ELSE
    aux_lead_group := new_lead_group;
  END IF;

  select max(plot_publication.sequence_number) into max_sequence_number from plot_publication
    where substr(ref_code,11,4) = new_year
    and substr(ref_code,6,4) = aux_lead_group;

  IF max_sequence_number IS NULL THEN
    max_sequence_number := 0;
  END IF;

  ref_code := aux_lead_group || '-' || new_year || '-' || trim(to_char(max_sequence_number + 1, '09'));

  RETURN ref_code;

END;
/

create PROCEDURE atlas_authdb.assert (
     condition_in IN BOOLEAN,
     raise_exception_in IN BOOLEAN := TRUE,
     exception_in IN VARCHAR2 := 'VALUE_ERROR'
  )
  IS
  BEGIN
     IF    NOT condition_in
        OR condition_in IS NULL

     THEN
        DBMS_OUTPUT.PUT_LINE ('Assertion Failure!');

        IF raise_exception_in
        THEN
           EXECUTE IMMEDIATE

             'BEGIN RAISE ' || exception_in || '; END;';
        END IF;
     ELSE
        DBMS_OUTPUT.PUT_LINE ('Assertion Successful!');
     END IF;
END assert;
/

create PROCEDURE atlas_authdb.SET_EXPERT(
  p_cern_ccid IN users_usergroup.cern_ccid%TYPE)
IS
BEGIN

    INSERT INTO users_usergroup (group_id, ncp_funda_id, ir_institute_id, ir_institution_id, cern_ccid)
    VALUES ('EXPERT',NULL,NULL,NULL,p_cern_ccid);

END;
/

create PROCEDURE atlas_authdb.SYNC_INST_IR
IS
  CURSOR INST_GROUP IS
    select inst.id,
      stragg(QUALITY||' '||  MEMB.INITIALS ||' '|| MEMBERS_LASTEST_EMPLOY_RECORD.LAST_NAME) as institute_rep,
      stragg(MEMBERS_LASTEST_EMPLOY_RECORD.EMAIL) as INSTITUTE_REP_EMAIL
  from
    MEMB,
    MEMBERS_LASTEST_EMPLOY_RECORD,
    users_usergroup,
    ATLAS_MEMBERS_CERNDB,
    inst
  where users_usergroup.cern_ccid = members_lastest_employ_record.cern_ccid
        and MEMB.CERN_CCID = members_lastest_employ_record.cern_ccid
        and PERSON_ID = members_lastest_employ_record.cern_ccid
        and users_usergroup.ir_institute_id = inst.id
  group by inst.id;
  inst_record INST_GROUP%rowtype;
BEGIN
dbms_output.enable(9999999);
FOR inst_record IN INST_GROUP
	LOOP
   IF (inst_record.INSTITUTE_REP_EMAIL is not null) and (inst_record.id <> 34)
    THEN
       UPDATE INST
       SET CONTACT = inst_record.INSTITUTE_REP,
       CONTACT_EMAIL = inst_record.INSTITUTE_REP_EMAIL
       WHERE ID = inst_record.id;
       dbms_output.put_line('ID: '|| inst_record.id || ' IR: ' || inst_record.institute_rep || ' EMAIL: '||INST_RECORD.institute_rep_email);
   END IF;
  END LOOP;
END;
/

create PROCEDURE atlas_authdb.REMOVE_EXPERT(
  p_cern_ccid IN users_usergroup.cern_ccid%TYPE)
IS
BEGIN

    DELETE FROM users_usergroup WHERE group_id = 'EXPERT' AND cern_ccid = p_cern_ccid;

END;
/

--rollback DROP FUNCTION ATLAS_AUTHDB.OTP_ROOT_WBS
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.OTP_DATE_ID_YEAR
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.IS_NUMBER
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.CAN_ADD_NEW_ACTIVITY_ID
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINECONFNOTEINTERNALCODE
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEC2BIN
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINECONFNOTEREFERENCECODE
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINEANALYSISSYSREFERENCECODE
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.PHOTO_URL_FUNCTION
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.BITOR
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.STRAGG
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.CLAGG
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINEPUBNOTEREFERENCECODE
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.BIN2DEC
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.ANALYSIS_DATA_FUNC
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINEANALYSISREFERENCECODE_V2
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINEANALYSISREFERENCECODE
--rollback /
--rollback DROP FUNCTION ATLAS_AUTHDB.DEFINEPLOTREFERENCECODE
--rollback /
--rollback DROP PROCEDURE ATLAS_AUTHDB.ASSERT
--rollback /
--rollback DROP PROCEDURE ATLAS_AUTHDB.SET_EXPERT
--rollback /
--rollback DROP PROCEDURE ATLAS_AUTHDB.SYNC_INST_IR
--rollback /
--rollback DROP PROCEDURE ATLAS_AUTHDB.REMOVE_EXPERT
--rollback /