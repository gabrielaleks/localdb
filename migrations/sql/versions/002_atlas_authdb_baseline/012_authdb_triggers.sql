--liquibase formatted sql
--changeset Gabriel:12 endDelimiter:/ rollbackEndDelimiter:/

create trigger ATLAS_AUTHDB.HTR_QU_STATE
    after insert or update or delete
    on ATLAS_AUTHDB.QU_STATE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_STATE (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_STATE (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_STATE (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_STATE (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_STATE (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_ADDRESS
    after insert or update or delete
    on ATLAS_AUTHDB.IN_ADDRESS
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
          ID,
          INSTITUTE_ID,
          LINE_1,
          LINE_2,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LINE_1,
          :NEW.LINE_2,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
          ID,
          INSTITUTE_ID,
          LINE_1,
          LINE_2,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LINE_1,
          :NEW.LINE_2,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LINE_1), 'null') <> COALESCE(TO_CHAR(:NEW.LINE_1), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
          ID,
          INSTITUTE_ID,
          LINE_1,
          LINE_2,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LINE_1,
          :NEW.LINE_2,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LINE_1',
          TO_CHAR(:OLD.LINE_1),
          TO_CHAR(:NEW.LINE_1)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LINE_2), 'null') <> COALESCE(TO_CHAR(:NEW.LINE_2), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
          ID,
          INSTITUTE_ID,
          LINE_1,
          LINE_2,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LINE_1,
          :NEW.LINE_2,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LINE_2',
          TO_CHAR(:OLD.LINE_2),
          TO_CHAR(:NEW.LINE_2)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
          ID,
          INSTITUTE_ID,
          LINE_1,
          LINE_2,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LINE_1,
          :NEW.LINE_2,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
      ID,
      INSTITUTE_ID,
      LINE_1,
      LINE_2,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.LINE_1,
      :OLD.LINE_2,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_ADDRESS (
      ID,
      INSTITUTE_ID,
      LINE_1,
      LINE_2,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.LINE_1,
      :NEW.LINE_2,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.CONFNOTE_PHASE_1_DELETE
    after delete
    on ATLAS_AUTHDB.CONFNOTE_PHASE_1
    for each row
begin
	delete from CONFNOTE_review_cycle
	where CONFNOTE_review_cycle.phase_id = :old.phase_id;
		--and CONFNOTE_review_cycle.phase_loop = :old.loop_sequence;
end;

/

create trigger ATLAS_AUTHDB.THIST_CONFNOTE_PHASE_1
    after insert or update or delete
    on ATLAS_AUTHDB.CONFNOTE_PHASE_1
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_CONFNOTE_PHASE_1
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PHASE_ID = :OLD.PHASE_ID
    ;

    IF (:NEW.PHASE_ID IS NOT NULL) THEN
        INSERT INTO HIST_CONFNOTE_PHASE_1
        (PHASE_ID, REVIEWERS_SIGN_OFF, PRESENTATION_PG, INDICO_URL, LPG_APPROVAL, DRAFT_CDS_URL, SIGN_OFF_DRAFT, PRESENTATION_ATLAS_GEN, STATE, ATLAS_RELEASE, REVIEWERS_APPROVAL, PUBCOMM_SIGN_OFF, PHYSCOORD_SIGN_OFF, ATLAS_APPROVAL, SIGN_OFF_1, SIGN_OFF_1_MEMBER, SIGN_OFF_2, SIGN_OFF_2_MEMBER, SIGN_OFF_1_MEMBER_ROLE, SIGN_OFF_2_MEMBER_ROLE, SIGN_OFF_1_MEMBER_ID, SIGN_OFF_2_MEMBER_ID, PGC_SIGN_OFF_DRAFT, MAIL_TO_EDBOARD, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.PHASE_ID, :NEW.REVIEWERS_SIGN_OFF, :NEW.PRESENTATION_PG, :NEW.INDICO_URL, :NEW.LPG_APPROVAL, :NEW.DRAFT_CDS_URL, :NEW.SIGN_OFF_DRAFT, :NEW.PRESENTATION_ATLAS_GEN, :NEW.STATE, :NEW.ATLAS_RELEASE, :NEW.REVIEWERS_APPROVAL, :NEW.PUBCOMM_SIGN_OFF, :NEW.PHYSCOORD_SIGN_OFF, :NEW.ATLAS_APPROVAL, :NEW.SIGN_OFF_1, :NEW.SIGN_OFF_1_MEMBER, :NEW.SIGN_OFF_2, :NEW.SIGN_OFF_2_MEMBER, :NEW.SIGN_OFF_1_MEMBER_ROLE, :NEW.SIGN_OFF_2_MEMBER_ROLE, :NEW.SIGN_OFF_1_MEMBER_ID, :NEW.SIGN_OFF_2_MEMBER_ID, :NEW.PGC_SIGN_OFF_DRAFT, :NEW.MAIL_TO_EDBOARD, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_PUBLICATION
    after insert or update or delete
    on ATLAS_AUTHDB.PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBLICATION
        (ID, SHORT_TITLE, FULL_TITLE, PLANNED_JOURNAL, TARGET_DATE_CONF, TARGET_DATE_JOURNAL, REF_CODE, SUB_CERN_APPROVAL, CERN_APPROVAL, COMMMENTS, JOURNAL_SUB, JOURNAL, CDS_URL, LEAD_GROUP, SEQUENCE_NUMBER, CREATION_DATE, STATUS, JOURNAL_PUBLICATION_URL, PAPER_REFERENCE, LUMINOSITY, LUMINOSITY_UNIT, LUMINOSITY_UNCERTAINTY, DATA_USED, JOURNAL_APPROVAL_DATE, ARXIV_URL, ROW_START_DATE, ROW_END_DATE,TRACKING_REF, REFEREE_REPORT_ON, PROOFS_ON
         ,final_title_tex
            ,sub_group, DELETION_REASON, DELETION_REQUEST, DELETION_MEMBERID, PUBLICATION_RUN, CURR_DRAFT_CDS_URL
        )
        VALUES
        (:NEW.ID, :NEW.SHORT_TITLE, :NEW.FULL_TITLE, :NEW.PLANNED_JOURNAL, :NEW.TARGET_DATE_CONF, :NEW.TARGET_DATE_JOURNAL, :NEW.REF_CODE, :NEW.SUB_CERN_APPROVAL, :NEW.CERN_APPROVAL, :NEW.COMMMENTS, :NEW.JOURNAL_SUB, :NEW.JOURNAL, :NEW.CDS_URL, :NEW.LEAD_GROUP, :NEW.SEQUENCE_NUMBER, :NEW.CREATION_DATE, :NEW.STATUS, :NEW.JOURNAL_PUBLICATION_URL, :NEW.PAPER_REFERENCE, :NEW.LUMINOSITY, :NEW.LUMINOSITY_UNIT, :NEW.LUMINOSITY_UNCERTAINTY, :NEW.DATA_USED, :NEW.JOURNAL_APPROVAL_DATE, :NEW.ARXIV_URL, Now, NULL,:NEW.TRACKING_REF, :NEW.REFEREE_REPORT_ON, :NEW.PROOFS_ON
        ,:NEW.final_title_tex
            ,:NEW.sub_group, :NEW.DELETION_REASON, :NEW.DELETION_REQUEST, :NEW.DELETION_MEMBERID, :NEW.PUBLICATION_RUN, :NEW.CURR_DRAFT_CDS_URL
        )
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.FULL_TITLE_UPDATE
    before update of FINAL_TITLE_TEX
    on ATLAS_AUTHDB.PUBLICATION
    for each row
BEGIN
  :NEW.FULL_TITLE := :NEW.FINAL_TITLE_TEX;
END;
/

create trigger ATLAS_AUTHDB.PUBLICATION_INSERT_UPDATE
    before insert or update
    on ATLAS_AUTHDB.PUBLICATION
    for each row
begin

  -- When inserting, sets the creation date and the autoincrement ID.
  IF INSERTING and (:new.id IS NULL) THEN
    select seq_publication.nextval into :new.id from dual;
    IF :new.creation_date is null THEN
	    :new.creation_date := sysdate;
    END IF;
  END IF;

  -- Whenever LEAD_GROUP is not changing, do NOT change REF_CODE.
  IF UPDATING
  and (:new.ref_code IS NULL
  or :new.ref_code = '')
  --  and (:new.lead_group = :old.lead_group
   -- or (:new.lead_group IS NULL and :old.lead_group IS NULL)
  --  or (:new.lead_group = '' and :old.lead_group = '')
  --  or (:new.lead_group IS NULL and :old.lead_group = '')
  --  or (:new.lead_group = '' and :old.lead_group IS NULL))
  THEN
    :new.ref_code := :old.ref_code;
    :new.sequence_number := :old.sequence_number;
  END IF;
END;

/

create trigger ATLAS_AUTHDB.TALK_INSERT_UPDATE
    before insert or update
    on ATLAS_AUTHDB.TALK
    for each row
BEGIN
  IF INSERTING THEN
    IF :new.id IS NULL THEN
      select seq_talk.nextval into :new.id from dual;
    END IF;
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_HIERARCHY
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_HIERARCHY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY (
            ACTIVITY_ID,
            PARENT_ACTIVITY_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ACTIVITY_ID,
            :NEW.PARENT_ACTIVITY_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTIVITY_ID',
            TO_CHAR(:OLD.ACTIVITY_ID),
            TO_CHAR(:NEW.ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PARENT_ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.PARENT_ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY (
            ACTIVITY_ID,
            PARENT_ACTIVITY_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ACTIVITY_ID,
            :NEW.PARENT_ACTIVITY_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'PARENT_ACTIVITY_ID',
            TO_CHAR(:OLD.PARENT_ACTIVITY_ID),
            TO_CHAR(:NEW.PARENT_ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY (
            ACTIVITY_ID,
            PARENT_ACTIVITY_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ACTIVITY_ID,
            :NEW.PARENT_ACTIVITY_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY (
            ACTIVITY_ID,
            PARENT_ACTIVITY_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ACTIVITY_ID,
            :OLD.PARENT_ACTIVITY_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_HIERARCHY (
            ACTIVITY_ID,
            PARENT_ACTIVITY_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ACTIVITY_ID,
            :NEW.PARENT_ACTIVITY_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_FUCTION_INSERT
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_FUNCTION
    for each row
BEGIN
  if (:new.ID is null) then
      select SEQ_ANALYSIS_SYS_FUNCTION.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.THIST_PHASE
    after insert or update or delete
    on ATLAS_AUTHDB.PHASE
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PHASE
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PHASE
        (ID, PUBLICATION_ID, PHASE, START_DATE, INDICO_URL, SIGN_OFF, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PUBLICATION_ID, :NEW.PHASE, :NEW.START_DATE, :NEW.INDICO_URL, :NEW.SIGN_OFF, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.PHASE
    before insert
    on ATLAS_AUTHDB.PHASE
    for each row
begin
select seq_phase.nextval into :new.id from dual;
if :new.start_date is null or :new.start_date = '' then
	:new.start_date := SYSDATE;
end if;
end;
/

create trigger ATLAS_AUTHDB.PLOT_PHASE
    before insert or update
    on ATLAS_AUTHDB.PLOT_PHASE
    for each row
begin
  IF INSERTING THEN
    select seq_plot_phase.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.ACADEMIC_RECORD_HIST_UPDATE
    before update
    on ATLAS_AUTHDB.ACADEMIC_RECORD
    for each row
DECLARE

	MEMB_ID_AUX           NUMBER(8) 	      DEFAULT NULL;
	THESIS_TITLE_AUX      VARCHAR2(255) 	  DEFAULT NULL;
	TYPE_AUX              VARCHAR2(15) 	    DEFAULT NULL;
  INST_ID_AUX           NUMBER(4)       	DEFAULT NULL;
  THESIS_DATE_AUX       DATE 	            DEFAULT NULL;
  LINK_AUX              VARCHAR2(255) 	  DEFAULT NULL;
  COMMENTS_AUX          VARCHAR2(255) 	  DEFAULT NULL;
	STATUS_AUX            NUMBER(1) 		    DEFAULT NULL;
  MEMB_NAME_AUX         VARCHAR2(255)		  DEFAULT NULL;
  REP_NUMBER_AUX        VARCHAR2(25)		  DEFAULT NULL;

	update_trigger		boolean;

BEGIN

	update_trigger:=false;

   IF ((:NEW.MEMB_ID  <> :OLD.MEMB_ID)
       OR (:NEW.MEMB_ID IS NULL AND :OLD.MEMB_ID IS NOT NULL)
       OR (:NEW.MEMB_ID IS NOT NULL AND :OLD.MEMB_ID IS NULL))
     THEN MEMB_ID_AUX := :OLD.MEMB_ID;
        update_trigger:=true;
   END IF;
   
   IF ((:NEW.THESIS_TITLE  <> :OLD.THESIS_TITLE)
       OR (:NEW.THESIS_TITLE IS NULL AND :OLD.THESIS_TITLE IS NOT NULL)
       OR (:NEW.THESIS_TITLE IS NOT NULL AND :OLD.THESIS_TITLE IS NULL))
     THEN THESIS_TITLE_AUX := :OLD.THESIS_TITLE;
     update_trigger:=true;
   END IF;
 
   IF ((:NEW.TYPE  <> :OLD.TYPE)
       OR (:NEW.TYPE IS NULL AND :OLD.TYPE IS NOT NULL)
       OR (:NEW.TYPE IS NOT NULL AND :OLD.TYPE IS NULL))
     THEN TYPE_AUX := :OLD.TYPE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.INST_ID  <> :OLD.INST_ID)
       OR (:NEW.INST_ID IS NULL AND :OLD.INST_ID IS NOT NULL)
       OR (:NEW.INST_ID IS NOT NULL AND :OLD.INST_ID IS NULL))
     THEN INST_ID_AUX := :OLD.INST_ID;
     update_trigger:=true;
   END IF;
   
    IF ((:NEW.THESIS_DATE  <> :OLD.THESIS_DATE)
       OR (:NEW.THESIS_DATE IS NULL AND :OLD.THESIS_DATE IS NOT NULL)
       OR (:NEW.THESIS_DATE IS NOT NULL AND :OLD.THESIS_DATE IS NULL))
     THEN THESIS_DATE_AUX := :OLD.THESIS_DATE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.LINK  <> :OLD.LINK)
       OR (:NEW.LINK IS NULL AND :OLD.LINK IS NOT NULL)
       OR (:NEW.LINK IS NOT NULL AND :OLD.LINK IS NULL))
     THEN LINK_AUX := :OLD.LINK;
     update_trigger:=true;
   END IF;

   IF ((:NEW.COMMENTS  <> :OLD.COMMENTS)
       OR (:NEW.COMMENTS IS NULL AND :OLD.COMMENTS IS NOT NULL)
       OR (:NEW.COMMENTS IS NOT NULL AND :OLD.COMMENTS IS NULL))
     THEN COMMENTS_AUX := :OLD.COMMENTS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.STATUS  <> :OLD.STATUS)
       OR (:NEW.STATUS IS NULL AND :OLD.STATUS IS NOT NULL)
       OR (:NEW.STATUS IS NOT NULL AND :OLD.STATUS IS NULL))
     THEN STATUS_AUX := :OLD.STATUS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.MEMB_NAME  <> :OLD.MEMB_NAME)
       OR (:NEW.MEMB_NAME IS NULL AND :OLD.MEMB_NAME IS NOT NULL)
       OR (:NEW.MEMB_NAME IS NOT NULL AND :OLD.MEMB_NAME IS NULL))
     THEN MEMB_NAME_AUX := :OLD.MEMB_NAME;
     update_trigger:=true;
   END IF;

   IF ((:NEW.REP_NUMBER  <> :OLD.REP_NUMBER)
       OR (:NEW.REP_NUMBER IS NULL AND :OLD.REP_NUMBER IS NOT NULL)
       OR (:NEW.REP_NUMBER IS NOT NULL AND :OLD.REP_NUMBER IS NULL))
     THEN REP_NUMBER_AUX := :OLD.REP_NUMBER;
     update_trigger:=true;
   END IF;

  if (update_trigger)
	  then
		  INSERT INTO ACADEMIC_RECORD_HIST (ID, MEMB_ID, THESIS_TITLE, TYPE, INST_ID, THESIS_DATE, LINK, COMMENTS, STATUS, MEMB_NAME, 
      REP_NUMBER, MODIFIED_BY, MODIFIED_ON) VALUES (:NEW.ID, MEMB_ID_AUX, THESIS_TITLE_AUX,
		  TYPE_AUX, INST_ID_AUX, THESIS_DATE_AUX, LINK_AUX, COMMENTS_AUX, STATUS_AUX,
		  MEMB_NAME_AUX ,REP_NUMBER_AUX, :NEW.MODIFIED_BY, SYSDATE);
      :new.modified_on := sysdate;
  end if;

END;
/

create trigger ATLAS_AUTHDB.ACADEMIC_RECORD_HIST_DELETE
    before delete
    on ATLAS_AUTHDB.ACADEMIC_RECORD
    for each row
DECLARE
BEGIN
 INSERT INTO ACADEMIC_RECORD_HIST (ID, MEMB_ID, THESIS_TITLE, TYPE, INST_ID, THESIS_DATE, LINK, COMMENTS, STATUS, MEMB_NAME, 
      REP_NUMBER, MODIFIED_BY, MODIFIED_ON) VALUES (:OLD.ID, :old.memb_ID, :old.THESIS_TITLE, :old.TYPE,
  :old.INST_ID, :old.THESIS_DATE, :old.LINK, :old.COMMENTS, :old.STATUS, :OLD.MEMB_NAME, :old.REP_NUMBER, :NEW.MODIFIED_BY, SYSDATE);
END;
/

create trigger ATLAS_AUTHDB.T_ACADEMIC_RECORD
    after insert or update or delete
    on ATLAS_AUTHDB.ACADEMIC_RECORD
    for each row
BEGIN
    IF INSERTING AND :NEW.AWARD_DATE is not null THEN
        INSERT INTO MEMB_AWARD
        VALUES (
            :NEW.MEMB_ID,
            'thesis',
            :NEW.AWARD_DATE,
            :NEW.ID,
            null,
            null
        );
    END IF;

    IF DELETING AND :OLD.AWARD_DATE is not null THEN
        DELETE FROM MEMB_AWARD
        WHERE 
            MEMB_ID = :OLD.MEMB_ID AND
            TYPE = 'thesis' AND
            YEAR = :OLD.AWARD_DATE AND
            THESIS_ID = :OLD.ID
        ;
    END IF;

    IF UPDATING AND :NEW.AWARD_DATE is not null AND :OLD.AWARD_DATE is null THEN
        INSERT INTO MEMB_AWARD
        VALUES (
            :NEW.MEMB_ID,
            'thesis',
            :NEW.AWARD_DATE,
            :NEW.ID,
            null,
            null
        );
    END IF;

    IF UPDATING AND :NEW.AWARD_DATE is not null AND :OLD.AWARD_DATE is not null THEN
        UPDATE MEMB_AWARD
        SET
            YEAR = :NEW.AWARD_DATE
        WHERE
            MEMB_ID = :NEW.MEMB_ID AND
            TYPE = 'thesis' AND
            THESIS_ID = :OLD.ID AND
            YEAR = :OLD.AWARD_DATE
        ;
    END IF;

    IF UPDATING AND :NEW.AWARD_DATE is null AND :OLD.AWARD_DATE is not null THEN
        DELETE FROM MEMB_AWARD
        WHERE
            MEMB_ID = :NEW.MEMB_ID AND
            TYPE = 'thesis' AND
            THESIS_ID = :OLD.ID AND
            YEAR = :OLD.AWARD_DATE
        ;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_ACADEMIC_RECORD
    before insert
    on ATLAS_AUTHDB.ACADEMIC_RECORD
    for each row
begin
   if ( :new.ID is null ) then
      select seq_ACADEMIC_RECORD.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.HTR_ME_PREFERRED_CONF
    after insert or update or delete
    on ATLAS_AUTHDB.ME_PREFERRED_CONF
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CONF_ID), 'null') <> COALESCE(TO_CHAR(:NEW.CONF_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CONF_ID',
          TO_CHAR(:OLD.CONF_ID),
          TO_CHAR(:NEW.CONF_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'null') <> COALESCE(TO_CHAR(:NEW.RANK), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
          ID,
          MEMBER_ID,
          CONF_ID,
          RANK,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_ID,
          :NEW.RANK,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
      ID,
      MEMBER_ID,
      CONF_ID,
      RANK,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.CONF_ID,
      :OLD.RANK,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF (
      ID,
      MEMBER_ID,
      CONF_ID,
      RANK,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.CONF_ID,
      :NEW.RANK,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_VETOED_TALK_TYPE
    after insert or update or delete
    on ATLAS_AUTHDB.ME_VETOED_TALK_TYPE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
          ID,
          MEMBER_ID,
          TALK_TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.TALK_TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
          ID,
          MEMBER_ID,
          TALK_TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.TALK_TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TALK_TYPE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TALK_TYPE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
          ID,
          MEMBER_ID,
          TALK_TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.TALK_TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TALK_TYPE_ID',
          TO_CHAR(:OLD.TALK_TYPE_ID),
          TO_CHAR(:NEW.TALK_TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
          ID,
          MEMBER_ID,
          TALK_TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.TALK_TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
      ID,
      MEMBER_ID,
      TALK_TYPE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.TALK_TYPE_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_TALK_TYPE (
      ID,
      MEMBER_ID,
      TALK_TYPE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.TALK_TYPE_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_LINKS_INSERT
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_LINKS
    for each row
BEGIN
   if ( :new.ID is null ) then
      select seq_ANALYSIS_SYS_LINKS.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.THIST_PHASE_3
    after insert or update or delete
    on ATLAS_AUTHDB.PHASE_3
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PHASE_3
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PHASE_ID = :OLD.PHASE_ID
    ;

    IF (:NEW.PHASE_ID IS NOT NULL) THEN
        INSERT INTO HIST_PHASE_3
        (PHASE_ID, START_DATE, LPG_SIGN_OFF, OPEN_DISCUSSION_DATE, OPEN_DISCUSSION_INDICO_URL, RELEASE_ATLAS_COMMENTS, END_ATLAS_COMMENTS, FINAL_APPROVAL, FINAL_CDS_LINK, STATE, LPG_FINAL_SIGN_OFF, DRAFT_CDS_URL, LPG_APPROVAL, LOOP_SEQUENCE, PUBLIC_READING_INDICO_URL, PUBLIC_READING_DATE, PUBCOMM_SIGN_OFF, PHYSCOORD_SIGN_OFF, ROW_START_DATE, ROW_END_DATE
        ,cern_reference_number
        ,edboard_signoff_date
        ,pubcomm_name
        ,spokesperson_name
        ,spokesperson_sign_off
        ,cern_sign_off
        )
        VALUES
        (:NEW.PHASE_ID, :NEW.START_DATE, :NEW.LPG_SIGN_OFF, :NEW.OPEN_DISCUSSION_DATE, :NEW.OPEN_DISCUSSION_INDICO_URL, :NEW.RELEASE_ATLAS_COMMENTS, :NEW.END_ATLAS_COMMENTS, :NEW.FINAL_APPROVAL, :NEW.FINAL_CDS_LINK, :NEW.STATE, :NEW.LPG_FINAL_SIGN_OFF, :NEW.DRAFT_CDS_URL, :NEW.LPG_APPROVAL, :NEW.LOOP_SEQUENCE, :NEW.PUBLIC_READING_INDICO_URL, :NEW.PUBLIC_READING_DATE, :NEW.PUBCOMM_SIGN_OFF, :NEW.PHYSCOORD_SIGN_OFF, Now, NULL
        ,:NEW.cern_reference_number
        ,:NEW.edboard_signoff_date
        ,:NEW.pubcomm_name
        ,:NEW.spokesperson_name
        ,:NEW.spokesperson_sign_off
        ,:NEW.cern_sign_off
        );
   END IF;
END;
/

create trigger ATLAS_AUTHDB.PHASE_3_DELETE
    after delete
    on ATLAS_AUTHDB.PHASE_3
    for each row
begin
	delete from review_cycle
	where review_cycle.phase_id = :old.phase_id
		and review_cycle.phase_loop = :old.loop_sequence;
  delete from phase_links
	where phase_links.phase_id = :old.phase_id;
end;
/

create trigger ATLAS_AUTHDB.HTR_IN_AFFILIATION
    after insert or update or delete
    on ATLAS_AUTHDB.IN_AFFILIATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'START_DATE',
          TO_CHAR(:OLD.START_DATE),
          TO_CHAR(:NEW.START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.END_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.END_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'END_DATE',
          TO_CHAR(:OLD.END_DATE),
          TO_CHAR(:NEW.END_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TYPE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TYPE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TYPE_ID',
          TO_CHAR(:OLD.TYPE_ID),
          TO_CHAR(:NEW.TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
          ID,
          INSTITUTE_ID,
          START_DATE,
          END_DATE,
          TYPE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.TYPE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
      ID,
      INSTITUTE_ID,
      START_DATE,
      END_DATE,
      TYPE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.START_DATE,
      :OLD.END_DATE,
      :OLD.TYPE_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION (
      ID,
      INSTITUTE_ID,
      START_DATE,
      END_DATE,
      TYPE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.START_DATE,
      :NEW.END_DATE,
      :NEW.TYPE_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_MEMB_PUBLICATION
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_MEMB_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND MEMB_ID = :OLD.MEMB_ID
    AND PUBLICATION_ID = :OLD.PUBLICATION_ID
    ;

    IF (:NEW.MEMB_ID IS NOT NULL AND :NEW.PUBLICATION_ID IS NOT NULL) THEN
        INSERT INTO HIST_MEMB_PUBLICATION
        (MEMB_ID, PUBLICATION_ID, GROUP_ID, MEMB_FUNCTION, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.MEMB_ID, :NEW.PUBLICATION_ID, :NEW.GROUP_ID, :NEW.MEMB_FUNCTION, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.TALK_MAT_INSERT_UPDATE
    before insert
    on ATLAS_AUTHDB.TALK_MAT
    for each row
BEGIN IF
  (   :new.ID IS NULL
  ) THEN 
  SELECT seq_talkmat.nextval INTO :new.ID FROM dual;
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_LEGACY_MEMBER
    after insert or update or delete
    on ATLAS_AUTHDB.LEGACY_MEMBER
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CERN_ID), 'null') <> COALESCE(TO_CHAR(:NEW.CERN_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CERN_ID',
          TO_CHAR(:OLD.CERN_ID),
          TO_CHAR(:NEW.CERN_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FIRST_NAME), 'null') <> COALESCE(TO_CHAR(:NEW.FIRST_NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FIRST_NAME',
          TO_CHAR(:OLD.FIRST_NAME),
          TO_CHAR(:NEW.FIRST_NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LAST_NAME), 'null') <> COALESCE(TO_CHAR(:NEW.LAST_NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LAST_NAME',
          TO_CHAR(:OLD.LAST_NAME),
          TO_CHAR(:NEW.LAST_NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
          ID,
          MEMBER_ID,
          CERN_ID,
          FIRST_NAME,
          LAST_NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CERN_ID,
          :NEW.FIRST_NAME,
          :NEW.LAST_NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
      ID,
      MEMBER_ID,
      CERN_ID,
      FIRST_NAME,
      LAST_NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.CERN_ID,
      :OLD.FIRST_NAME,
      :OLD.LAST_NAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.LEGACY_MEMBER (
      ID,
      MEMBER_ID,
      CERN_ID,
      FIRST_NAME,
      LAST_NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.CERN_ID,
      :NEW.FIRST_NAME,
      :NEW.LAST_NAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.T_SCABBER
    after delete
    on ATLAS_AUTHDB.SCABBER
    for each row
BEGIN
    IF DELETING THEN
        INSERT INTO SCABBER_HIST
        VALUES (
            :OLD.MEMB_ID,
            :OLD.RANK,
            :OLD.score,
            :OLD.sum_prios,
            :OLD.otp_score,
            :OLD.upgrade_otp_score,
            :OLD.last_talk_weight,
            :OLD.prof_ad_weight,
            :OLD.otp_rate,
            :OLD.otp_value,
            :OLD.calculation_date,
            :OLD.otp_upgrade,
            :OLD.otp_total,
            :OLD.conf_weight,
            :OLD.years_since_lt,
            :OLD.prof_ad_id,
            :OLD.sum_noms,
            :OLD.upgrade_noms
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_NOMINATION
    after insert or update or delete
    on ATLAS_AUTHDB.NOMINATIONS_BACKUP
    for each row
DECLARE
    change_composition  number;
    delete_date         date;
    MEMB_ID_AUX         NUMBER(8)      DEFAULT NULL;
    COMMENTS_AUX        VARCHAR2(2048)  DEFAULT NULL;
    PRIORITY_AUX        NUMBER(1)      DEFAULT NULL;
    ATLAS_FRACTION_AUX  NUMBER(3)      DEFAULT NULL;
    INTEREST_AUX        VARCHAR2(255)  DEFAULT NULL;
    SUBMITTED_BY_AUX   NUMBER(8)      DEFAULT NULL;
    SUBMISSION_DATE_AUX DATE           DEFAULT NULL;
    MODIFIED_BY_AUX     NUMBER(8)      DEFAULT NULL;
    APP_RESP_AUX        NUMBER(4)      DEFAULT NULL;
    IR_RESP_AUX         NUMBER(4)      DEFAULT NULL;
    SCAB_PRIORITY_AUX   NUMBER(2)      DEFAULT NULL;
    SYSTEMS_ID_AUX      NUMBER(4)      DEFAULT NULL;
    PROF_ADVANCE_ID_AUX NUMBER(2)      DEFAULT NULL;
    UPGRADE_AUX         VARCHAR2(1)    DEFAULT NULL;

BEGIN

    change_composition:=0;
   IF ((:NEW.MEMB_ID  <> :OLD.MEMB_ID)
       OR (:NEW.MEMB_ID IS NULL AND :OLD.MEMB_ID IS NOT NULL)
       OR (:NEW.MEMB_ID IS NOT NULL AND :OLD.MEMB_ID IS NULL))
     THEN MEMB_ID_AUX := :OLD.MEMB_ID;
        change_composition:=change_composition+power(2,0);
        
   END IF;

   IF ((:NEW.COMMENTS  <> :OLD.COMMENTS)
       OR (:NEW.COMMENTS IS NULL AND :OLD.COMMENTS IS NOT NULL)
       OR (:NEW.COMMENTS IS NOT NULL AND :OLD.COMMENTS IS NULL))
     THEN COMMENTS_AUX := :OLD.COMMENTS;
     change_composition:=change_composition+power(2,1);
   END IF;

   IF ((:NEW.PRIORITY  <> :OLD.PRIORITY)
       OR (:NEW.PRIORITY IS NULL AND :OLD.PRIORITY IS NOT NULL)
       OR (:NEW.PRIORITY IS NOT NULL AND :OLD.PRIORITY IS NULL))
     THEN PRIORITY_AUX := :OLD.PRIORITY;
     change_composition:=change_composition+power(2,2);
   END IF;

   IF ((:NEW.ATLAS_FRACTION  <> :OLD.ATLAS_FRACTION)
       OR (:NEW.ATLAS_FRACTION IS NULL AND :OLD.ATLAS_FRACTION IS NOT NULL)
       OR (:NEW.ATLAS_FRACTION IS NOT NULL AND :OLD.ATLAS_FRACTION IS NULL))
     THEN ATLAS_FRACTION_AUX := :OLD.ATLAS_FRACTION;
     change_composition:=change_composition+power(2,3);
   END IF;

   IF ((:NEW.INTEREST  <> :OLD.INTEREST)
       OR (:NEW.INTEREST IS NULL AND :OLD.INTEREST IS NOT NULL)
       OR (:NEW.INTEREST IS NOT NULL AND :OLD.INTEREST IS NULL))
     THEN INTEREST_AUX := :OLD.INTEREST;
     change_composition:=change_composition+power(2,4);
   END IF;

   IF ((:NEW.SUBMITTED_BY  <> :OLD.SUBMITTED_BY)
       OR (:NEW.SUBMITTED_BY IS NULL AND :OLD.SUBMITTED_BY IS NOT NULL)
       OR (:NEW.SUBMITTED_BY IS NOT NULL AND :OLD.SUBMITTED_BY IS NULL))
     THEN SUBMITTED_BY_AUX := :OLD.SUBMITTED_BY;
     change_composition:=change_composition+power(2,5);
   END IF;

   IF ((:NEW.SUBMISSION_DATE  <> :OLD.SUBMISSION_DATE)
       OR (:NEW.SUBMISSION_DATE IS NULL AND :OLD.SUBMISSION_DATE IS NOT NULL)
       OR (:NEW.SUBMISSION_DATE IS NOT NULL AND :OLD.SUBMISSION_DATE IS NULL))
     THEN
        SUBMISSION_DATE_AUX := :OLD.SUBMISSION_DATE;
        change_composition:=change_composition+power(2,6);
   END IF;

   IF ((:NEW.MODIFIED_BY  <> :OLD.MODIFIED_BY)
       OR (:NEW.MODIFIED_BY IS NULL AND :OLD.MODIFIED_BY IS NOT NULL)
       OR (:NEW.MODIFIED_BY IS NOT NULL AND :OLD.MODIFIED_BY IS NULL))
     THEN MODIFIED_BY_AUX := :OLD.MODIFIED_BY;
     change_composition:=change_composition+power(2,7);
   END IF;

   IF ((:NEW.APP_RESP  <> :OLD.APP_RESP)
       OR (:NEW.APP_RESP IS NULL AND :OLD.APP_RESP IS NOT NULL)
       OR (:NEW.APP_RESP IS NOT NULL AND :OLD.APP_RESP IS NULL))
     THEN APP_RESP_AUX := :OLD.APP_RESP;
     change_composition:=change_composition+power(2,8);
   END IF;

   IF ((:NEW.IR_RESP  <> :OLD.IR_RESP)
       OR (:NEW.IR_RESP IS NULL AND :OLD.IR_RESP IS NOT NULL)
       OR (:NEW.IR_RESP IS NOT NULL AND :OLD.IR_RESP IS NULL))
     THEN IR_RESP_AUX := :OLD.IR_RESP;
     change_composition:=change_composition+power(2,9);
   END IF;

   IF ((:NEW.SCAB_PRIORITY  <> :OLD.SCAB_PRIORITY)
       OR (:NEW.SCAB_PRIORITY IS NULL AND :OLD.SCAB_PRIORITY IS NOT NULL)
       OR (:NEW.SCAB_PRIORITY IS NOT NULL AND :OLD.SCAB_PRIORITY IS NULL))
     THEN SCAB_PRIORITY_AUX := :OLD.SCAB_PRIORITY;
     change_composition:=change_composition+power(2,10);
   END IF;

   IF ((:NEW.SYSTEMS_ID  <> :OLD.SYSTEMS_ID)
       OR (:NEW.SYSTEMS_ID IS NULL AND :OLD.SYSTEMS_ID IS NOT NULL)
       OR (:NEW.SYSTEMS_ID IS NOT NULL AND :OLD.SYSTEMS_ID IS NULL))
     THEN SYSTEMS_ID_AUX := :OLD.SYSTEMS_ID;
     change_composition:=change_composition+power(2,11);
   END IF;
   
   IF ((:NEW.PROF_ADVANCE_ID  <> :OLD.PROF_ADVANCE_ID)
       OR (:NEW.PROF_ADVANCE_ID IS NULL AND :OLD.PROF_ADVANCE_ID IS NOT NULL)
       OR (:NEW.PROF_ADVANCE_ID IS NOT NULL AND :OLD.PROF_ADVANCE_ID IS NULL))
     THEN PROF_ADVANCE_ID_AUX := :OLD.PROF_ADVANCE_ID;
     change_composition:=change_composition+power(2,12);
   END IF;
   
   IF ((:NEW.UPGRADE <> :OLD.UPGRADE)
       OR (:NEW.UPGRADE IS NULL AND :OLD.UPGRADE IS NOT NULL)
       OR (:NEW.UPGRADE IS NOT NULL AND :OLD.UPGRADE IS NULL))
     THEN UPGRADE_AUX := :OLD.UPGRADE;
     change_composition:=change_composition+power(2,13);
   END IF;   
   if :new.id is null then --deleted
        delete_date:=sysdate;
        update nominations_hist_new set up_to = SYSDATE where up_to is null and id = :old.id;
        INSERT INTO NOMINATIONS_HIST_NEW 
        VALUES
        (delete_date, delete_date, change_composition, :OLD.ID,
		:OLD.MEMB_ID, :OLD.COMMENTS, :OLD.PRIORITY, :OLD.ATLAS_FRACTION, :OLD.INTEREST, :OLD.SUBMITTED_BY,
		:OLD.SUBMISSION_DATE, :OLD.MODIFIED_BY, :OLD.APP_RESP, :OLD.IR_RESP, :OLD.SCAB_PRIORITY, :OLD.SYSTEMS_ID,
		:OLD.PROF_ADVANCE_ID, :OLD.UPGRADE);
    end if;
    if :new.id is not null and change_composition!=0 then 
        update nominations_hist_new set up_to = SYSDATE where up_to is null and id = :old.id;
        INSERT INTO NOMINATIONS_HIST_NEW 
        VALUES
        (sysdate, null, change_composition, :NEW.ID,
		:NEW.MEMB_ID, :NEW.COMMENTS, :NEW.PRIORITY, :NEW.ATLAS_FRACTION, :NEW.INTEREST, :NEW.SUBMITTED_BY,
		:NEW.SUBMISSION_DATE, :NEW.MODIFIED_BY, :NEW.APP_RESP, :NEW.IR_RESP, :NEW.SCAB_PRIORITY, :NEW.SYSTEMS_ID,
		:NEW.PROF_ADVANCE_ID, :NEW.UPGRADE);
    end if;
END;
/

create trigger ATLAS_AUTHDB.T_IR_NOMINATIONS
    before insert
    on ATLAS_AUTHDB.NOMINATIONS_BACKUP
    for each row
BEGIN
    IF INSERTING THEN
        DELETE FROM NOMINATIONS
        WHERE 
        :NEW.IR_RESP IS NOT NULL
        AND :NEW.SYSTEMS_ID = 1 -- scab rule only
        AND MEMB_ID = :NEW.MEMB_ID
        AND IR_RESP IS NOT NULL
        AND SYSTEMS_ID = 1; -- scab rule only
    END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_NOMINATIONS
    before insert
    on ATLAS_AUTHDB.NOMINATIONS_BACKUP
    for each row
begin
   if ( :new.ID is null ) then
      select seq_NOMINATIONS.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.TRACK_STA_AFFILIATION
    after insert or update or delete
    on ATLAS_AUTHDB.STA_AFFILIATION
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.inst_atlas  <> :OLD.inst_atlas)
        OR (:NEW.inst_atlas IS NULL AND :OLD.inst_atlas IS NOT NULL)
        OR (:NEW.inst_atlas IS NOT NULL AND :OLD.inst_atlas IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.inst_ghost <> :OLD.inst_ghost)
        OR (:NEW.inst_ghost IS NULL AND :OLD.inst_ghost IS NOT NULL)
        OR (:NEW.inst_ghost IS NOT NULL AND :OLD.inst_ghost IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.start_date <> :OLD.start_date)
        OR (:NEW.start_date IS NULL AND :OLD.start_date IS NOT NULL)
        OR (:NEW.start_date IS NOT NULL AND :OLD.start_date IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.end_date  <> :OLD.end_date)
        OR (:NEW.end_date IS NULL AND :OLD.end_date IS NOT NULL)
        OR (:NEW.end_date IS NOT NULL AND :OLD.end_date IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE sta_affiliation_hist SET up_to = operation_date WHERE up_to IS NULL AND sta_id = :OLD.sta_id;
        IF :NEW.sta_id IS NOT NULL THEN
            INSERT INTO sta_affiliation_hist
            VALUES (
            :NEW.sta_id,
            operation_date, NULL, :NEW.agent, change_composition,
            :NEW.inst_atlas, :NEW.inst_ghost, :NEW.start_date, :NEW.end_date
            );
        ELSE
            -- SHORT_TERM deleted!!
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO sta_affiliation_hist
            VALUES (
            :OLD.sta_id,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.inst_atlas, :OLD.inst_ghost, :OLD.start_date, :OLD.end_date
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_CONFNOTE_MEMB_PUBLICATIO
    after insert or update or delete
    on ATLAS_AUTHDB.CONFNOTE_MEMB_PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_CONFNOTE_MEMB_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND MEMB_ID = :OLD.MEMB_ID
    AND PUBLICATION_ID = :OLD.PUBLICATION_ID
    ;

    IF (:NEW.MEMB_ID IS NOT NULL AND :NEW.PUBLICATION_ID IS NOT NULL) THEN
        INSERT INTO HIST_CONFNOTE_MEMB_PUBLICATION
        (MEMB_ID, PUBLICATION_ID, GROUP_ID, MEMB_FUNCTION, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.MEMB_ID, :NEW.PUBLICATION_ID, :NEW.GROUP_ID, :NEW.MEMB_FUNCTION, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_SUPP_NOTES
    before insert or update
    on ATLAS_AUTHDB.PLOT_SUPP_NOTES
    for each row
begin
  IF INSERTING THEN
    select seq_plot_supp_notes.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.PHASE_1_DELETE
    after delete
    on ATLAS_AUTHDB.PHASE_1
    for each row
begin
	delete from review_cycle
	where review_cycle.phase_id = :old.phase_id;
		--and review_cycle.phase_loop = :old.loop_sequence;
  delete from phase_links
	where phase_links.phase_id = :old.phase_id;
end;
/

create trigger ATLAS_AUTHDB.THIST_PHASE_1
    after insert or update or delete
    on ATLAS_AUTHDB.PHASE_1
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PHASE_1
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PHASE_ID = :OLD.PHASE_ID
    ;

    IF (:NEW.PHASE_ID IS NOT NULL) THEN
        INSERT INTO HIST_PHASE_1
        (PHASE_ID, REVIEWERS_SIGN_OFF, PRESENTATION_PG, LPG_APPROVAL, DRAFT_CDS_URL, SIGN_OFF_DRAFT, PRESENTATION_ATLAS_GEN, STATE, ATLAS_RELEASE, PRESENTATION_ATLAS_URL, REVIEWERS_APPROVAL, PUBCOMM_SIGN_OFF, PHYSCOORD_SIGN_OFF, SUPPORT_DOCUMENTS_URL, MAIL_TO_EDBOARD, PGC_SIGN_OFF_DRAFT, ROW_START_DATE, ROW_END_DATE
        ,draftcdsurl
        ,authorlist_id
        )
        VALUES
        (:NEW.PHASE_ID, :NEW.REVIEWERS_SIGN_OFF, :NEW.PRESENTATION_PG, :NEW.LPG_APPROVAL, :NEW.DRAFT_CDS_URL, :NEW.SIGN_OFF_DRAFT, :NEW.PRESENTATION_ATLAS_GEN, :NEW.STATE, :NEW.ATLAS_RELEASE, :NEW.PRESENTATION_ATLAS_URL, :NEW.REVIEWERS_APPROVAL, :NEW.PUBCOMM_SIGN_OFF, :NEW.PHYSCOORD_SIGN_OFF, :NEW.SUPPORT_DOCUMENTS_URL, :NEW.MAIL_TO_EDBOARD, :NEW.PGC_SIGN_OFF_DRAFT, Now, NULL
        ,:new.draftcdsurl
        ,:new.authorlist_id
        );
   END IF;
END;
/

create trigger ATLAS_AUTHDB.SUBMISSION_DELETE
    after delete
    on ATLAS_AUTHDB.SUBMISSION
    for each row
begin
	delete from review_cycle
	where review_cycle.phase_id = :old.phase_id;
		--and review_cycle.phase_loop = :old.loop_sequence;
    delete from phase_links
	where phase_links.phase_id = :old.phase_id;
end;
/

create trigger ATLAS_AUTHDB.PLOT_MEMB_CONTRIBUTION
    before insert or update
    on ATLAS_AUTHDB.PLOT_MEMB_CONTRIBUTION
    for each row
begin
  IF INSERTING THEN
    select seq_plot_memb_contribution.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.PLOT_PHASE_1
    before insert or update
    on ATLAS_AUTHDB.PLOT_PHASE_1
    for each row
begin
  IF INSERTING THEN
    select seq_plot_phase_1.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_TYPE
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_TYPE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'NAME',
            TO_CHAR(:OLD.NAME),
            TO_CHAR(:NEW.NAME)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.NAME,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_TYPE (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_PHASE_LINKS
    before insert or update
    on ATLAS_AUTHDB.PLOT_PHASE_LINKS
    for each row
begin
  IF INSERTING THEN
    select seq_plot_phase_links.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.ID_PUBNOTE_MEMB_CONTRIBUTION
    before insert
    on ATLAS_AUTHDB.PUBNOTE_MEMB_CONTRIBUTION
    for each row
begin
   if ( :new.ID is null ) then
      select SEQ_PUBNOTE_MEMB_CONTRIBUTION.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    before insert or update
    on ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    for each row
begin

  -- When inserting, sets the creation date and the autoincrement ID.
  IF INSERTING and (:new.id IS NULL) THEN
    select seq_confnote_publication.nextval into :new.id from dual;
    IF :new.creation_date is null THEN
	    :new.creation_date := sysdate;
    END IF;
  END IF;

  -- Whenever LEAD_GROUP is not changing, do NOT change INTERNAL_CODE.
  IF UPDATING
    and (:new.lead_group = :old.lead_group
    or (:new.lead_group IS NULL and :old.lead_group IS NULL)
    or (:new.lead_group = '' and :old.lead_group = '')
    or (:new.lead_group IS NULL and :old.lead_group = '')
    or (:new.lead_group = '' and :old.lead_group IS NULL))
  THEN
    :new.internal_code := :old.internal_code;
    :new.internal_sequence_number := :old.internal_sequence_number;
  END IF;
END;

/

create trigger ATLAS_AUTHDB.THIST_CONFNOTE_PUBLICATION
    after insert or update or delete
    on ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_CONFNOTE_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_CONFNOTE_PUBLICATION
        (ID, SHORT_TITLE, FULL_TITLE, PLANNED_JOURNAL, TARGET_DATE_CONF, TARGET_DATE_JOURNAL, REF_CODE, SUB_CERN_APPROVAL, CERN_APPROVAL, COMMMENTS, JOURNAL_SUB, JOURNAL, CDS_URL, LEAD_GROUP, SEQUENCE_NUMBER, CREATION_DATE, STATUS, JOURNAL_PUBLICATION_URL, PAPER_REFERENCE, LUMINOSITY, LUMINOSITY_UNIT, LUMINOSITY_UNCERTAINTY, DATA_USED, SUPPORT_DOCUMENTS_URL, INTERNAL_CODE, INTERNAL_SEQUENCE_NUMBER, ADDITIONAL_NOTES, TEMP_REF_CODE, DELETION_REASON, DELETION_REQUEST, DELETION_MEMBERID, ROW_START_DATE, ROW_END_DATE, CONFNOTE_RUN)
        VALUES
        (:NEW.ID, :NEW.SHORT_TITLE, :NEW.FULL_TITLE, :NEW.PLANNED_JOURNAL, :NEW.TARGET_DATE_CONF, :NEW.TARGET_DATE_JOURNAL, :NEW.REF_CODE, :NEW.SUB_CERN_APPROVAL, :NEW.CERN_APPROVAL, :NEW.COMMMENTS, :NEW.JOURNAL_SUB, :NEW.JOURNAL, :NEW.CDS_URL, :NEW.LEAD_GROUP, :NEW.SEQUENCE_NUMBER, :NEW.CREATION_DATE, :NEW.STATUS, :NEW.JOURNAL_PUBLICATION_URL, :NEW.PAPER_REFERENCE, :NEW.LUMINOSITY, :NEW.LUMINOSITY_UNIT, :NEW.LUMINOSITY_UNCERTAINTY, :NEW.DATA_USED, :NEW.SUPPORT_DOCUMENTS_URL, :NEW.INTERNAL_CODE, :NEW.INTERNAL_SEQUENCE_NUMBER, :NEW.ADDITIONAL_NOTES, :NEW.TEMP_REF_CODE, :NEW.DELETION_REASON, :NEW.DELETION_REQUEST, :NEW.DELETION_MEMBERID, Now, NULL, :NEW.CONFNOTE_RUN)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_STA_AUTHORSHIP
    after insert or delete
    on ATLAS_AUTHDB.STA_AUTHORSHIP
    for each row
DECLARE
  operation_date  DATE;
BEGIN
  operation_date := SYSDATE;
  IF :NEW.sta_id IS NOT NULL THEN
    INSERT INTO sta_authorship_hist VALUES (
    :NEW.sta_id
    ,operation_date, NULL, :NEW.agent
    ,:NEW.paper_id
    );
  ELSE
    UPDATE sta_authorship_hist
    SET up_to = operation_date
    WHERE sta_id = :OLD.sta_id
      AND paper_id = :OLD.paper_id;  
    INSERT into sta_authorship_hist VALUES (
    :OLD.sta_id
    ,operation_date, operation_date, :OLD.agent
    ,:OLD.paper_id
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_SUPP_NOTES_INSERT
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_SUPP_NOTES
    for each row
BEGIN
  if (:new.ID is null) then
      select SEQ_ANALYSIS_SYS_SUPP_NOTES.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_SECRETARY
    after insert or update or delete
    on ATLAS_AUTHDB.IN_SECRETARY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PHONE), 'null') <> COALESCE(TO_CHAR(:NEW.PHONE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PHONE',
          TO_CHAR(:OLD.PHONE),
          TO_CHAR(:NEW.PHONE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.EMAIL), 'null') <> COALESCE(TO_CHAR(:NEW.EMAIL), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'EMAIL',
          TO_CHAR(:OLD.EMAIL),
          TO_CHAR(:NEW.EMAIL)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
          ID,
          INSTITUTE_ID,
          NAME,
          PHONE,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.PHONE,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
      ID,
      INSTITUTE_ID,
      NAME,
      PHONE,
      EMAIL,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.NAME,
      :OLD.PHONE,
      :OLD.EMAIL,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_SECRETARY (
      ID,
      INSTITUTE_ID,
      NAME,
      PHONE,
      EMAIL,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.NAME,
      :NEW.PHONE,
      :NEW.EMAIL,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB_TALK_PREFERENCES
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.applying_for_job  <> :OLD.applying_for_job)
        OR (:NEW.applying_for_job IS NULL AND :OLD.applying_for_job IS NOT NULL)
        OR (:NEW.applying_for_job IS NOT NULL AND :OLD.applying_for_job IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.other_constraints <> :OLD.other_constraints)
        OR (:NEW.other_constraints IS NULL AND :OLD.other_constraints IS NOT NULL)
        OR (:NEW.other_constraints IS NOT NULL AND :OLD.other_constraints IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE memb_talk_preferences_hist SET up_to = operation_date WHERE up_to IS NULL AND memb_id = :OLD.memb_id;
        IF :NEW.memb_id IS NOT NULL THEN
            INSERT INTO memb_talk_preferences_hist
            VALUES (
            :NEW.memb_id,
            operation_date, null, :NEW.agent, change_composition,
            :NEW.applying_for_job, :NEW.other_constraints
            );
        ELSE
            -- SHORT_TERM deleted!!
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO memb_talk_preferences_hist
            VALUES (
            :OLD.memb_id,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.applying_for_job, :OLD.other_constraints
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_SHORT_TERM
    after insert or update or delete
    on ATLAS_AUTHDB.SHORT_TERM
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.inspire  <> :OLD.inspire)
        OR (:NEW.inspire IS NULL AND :OLD.inspire IS NOT NULL)
        OR (:NEW.inspire IS NOT NULL AND :OLD.inspire IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.last_name_ltx <> :OLD.last_name_ltx)
        OR (:NEW.last_name_ltx IS NULL AND :OLD.last_name_ltx IS NOT NULL)
        OR (:NEW.last_name_ltx IS NOT NULL AND :OLD.last_name_ltx IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.first_name_ltx <> :OLD.first_name_ltx)
        OR (:NEW.first_name_ltx IS NULL AND :OLD.first_name_ltx IS NOT NULL)
        OR (:NEW.first_name_ltx IS NOT NULL AND :OLD.first_name_ltx IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.initials  <> :OLD.initials)
        OR (:NEW.initials IS NULL AND :OLD.initials IS NOT NULL)
        OR (:NEW.initials IS NOT NULL AND :OLD.initials IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE short_term_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO short_term_hist
            VALUES (
            :NEW.id, :NEW.cern_ccid, 
            operation_date, null, :NEW.agent, change_composition,
            :NEW.inspire, :NEW.last_name_ltx, :NEW.first_name_ltx, :NEW.initials
            );
        ELSE
            -- SHORT_TERM deleted!!
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO short_term_hist
            VALUES (
            :OLD.id, :OLD.cern_ccid,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.inspire, :OLD.last_name_ltx, :OLD.first_name_ltx, :OLD.initials
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEETING_ATTENDEE
    after insert or update or delete
    on ATLAS_AUTHDB.MEETING_ATTENDEE
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition := 0;
    
    IF ((:NEW.memb_id <> :OLD.memb_id)
        OR (:NEW.memb_id IS NULL AND :OLD.memb_id IS NOT NULL)
        OR (:NEW.memb_id IS NOT NULL AND :OLD.memb_id IS NULL))
    THEN change_composition := change_composition + POWER(2,0);
    END IF;
    
    IF ((:NEW.proxy_id <> :OLD.proxy_id)
        OR (:NEW.proxy_id IS NULL AND :OLD.proxy_id IS NOT NULL)
        OR (:NEW.proxy_id IS NOT NULL AND :OLD.proxy_id IS NULL))
    THEN change_composition := change_composition + POWER(2,1);
    END IF;
    
    IF :NEW.institution_id IS NULL THEN
        operation_date := SYSDATE;
        INSERT INTO meeting_attendee_hist (
        meeting_id, institution_id, since, up_to, agent, what_changed, memb_id, proxy_id
        ) VALUES (
        :OLD.meeting_id, :OLD.institution_id, operation_date, operation_date,
        :OLD.agent, change_composition, :OLD.memb_id, :OLD.proxy_id
        );
    END IF;
    
    IF :NEW.institution_id IS NOT NULL AND change_composition > 0 THEN
        operation_date := SYSDATE;
        UPDATE meeting_attendee_hist SET up_to = operation_date
        WHERE meeting_id = :OLD.meeting_id AND institution_id = :OLD.institution_id AND up_to IS NULL;
        
        INSERT INTO meeting_attendee_hist (
        meeting_id, institution_id, since, up_to, agent, what_changed, memb_id, proxy_id
        ) VALUES (
        :NEW.meeting_id, :NEW.institution_id, operation_date, NULL,
        :NEW.agent, change_composition, :NEW.memb_id, :NEW.proxy_id
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.T_REPRESENTATIVES_GROUP
    after insert or update or delete
    on ATLAS_AUTHDB.INSTITUTE_REP
    for each row
BEGIN
    IF DELETING THEN
        DELETE FROM USERS_USERGROUP
        WHERE USERS_USERGROUP.CERN_CCID = (SELECT CERN_CCID FROM MEMB WHERE ID=:OLD.MEMB_ID)
        AND USERS_USERGROUP.GROUP_ID = CASE
            WHEN :OLD.TYPE = 'SEC' THEN 'INSTITUTE_SEC'
            WHEN :OLD.TYPE = 'DEP' or :OLD.TYPE = 'ATLAS' THEN 'INSTITUTE_REP'
            WHEN :OLD.TYPE = 'TDAQ' THEN 'TDAQ_INSTITUTE_REP'
            ELSE NULL
        END;
    END IF;

    IF INSERTING THEN
        INSERT INTO USERS_USERGROUP
        VALUES (
            CASE
                WHEN :NEW.TYPE = 'SEC' THEN 'INSTITUTE_SEC'
                WHEN :NEW.TYPE = 'DEP' or :NEW.TYPE = 'ATLAS' THEN 'INSTITUTE_REP'
                WHEN :NEW.TYPE = 'TDAQ' THEN 'TDAQ_INSTITUTE_REP'
                ELSE NULL
            END,
            null,
            :NEW.INST_ID,
            null,
            (SELECT CERN_CCID FROM MEMB WHERE ID=:NEW.MEMB_ID)
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.CONF_DELETE
    before delete
    on ATLAS_AUTHDB.CONF
    for each row
begin
	delete from TALK
	where TALK.CONF_ID= :old.ID;
end;
/

create trigger ATLAS_AUTHDB.CONF_INSERT
    before insert
    on ATLAS_AUTHDB.CONF
    for each row
BEGIN
  IF INSERTING THEN
    IF :new.id IS NULL THEN 
      select seq_conf.nextval into :new.id from dual;
    END IF;
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
            ID,
            CODE,
            NAME,
            DESCRIPTION,
            TYPE_ID,
            STATUS_ID,
            NATURAL_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.CODE,
            :NEW.NAME,
            :NEW.DESCRIPTION,
            :NEW.TYPE_ID,
            :NEW.STATUS_ID,
            :NEW.NATURAL_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CODE), 'null') <> COALESCE(TO_CHAR(:NEW.CODE), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
            ID,
            CODE,
            NAME,
            DESCRIPTION,
            TYPE_ID,
            STATUS_ID,
            NATURAL_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.DESCRIPTION), 'null') <> COALESCE(TO_CHAR(:NEW.DESCRIPTION), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'DESCRIPTION',
          TO_CHAR(:OLD.DESCRIPTION),
          TO_CHAR(:NEW.DESCRIPTION)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TYPE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TYPE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TYPE_ID',
          TO_CHAR(:OLD.TYPE_ID),
          TO_CHAR(:NEW.TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STATUS_ID), 'null') <> COALESCE(TO_CHAR(:NEW.STATUS_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'STATUS_ID',
          TO_CHAR(:OLD.STATUS_ID),
          TO_CHAR(:NEW.STATUS_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NATURAL_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NATURAL_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NATURAL_ID',
          TO_CHAR(:OLD.NATURAL_ID),
          TO_CHAR(:NEW.NATURAL_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
          ID,
          CODE,
          NAME,
          DESCRIPTION,
          TYPE_ID,
          STATUS_ID,
          NATURAL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.DESCRIPTION,
          :NEW.TYPE_ID,
          :NEW.STATUS_ID,
          :NEW.NATURAL_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
            ID,
            CODE,
            NAME,
            DESCRIPTION,
            TYPE_ID,
            STATUS_ID,
            NATURAL_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.CODE,
            :OLD.NAME,
            :OLD.DESCRIPTION,
            :OLD.TYPE_ID,
            :OLD.STATUS_ID,
            :OLD.NATURAL_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY (
            ID,
            CODE,
            NAME,
            DESCRIPTION,
            TYPE_ID,
            STATUS_ID,
            NATURAL_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.CODE,
            :NEW.NAME,
            :NEW.DESCRIPTION,
            :NEW.TYPE_ID,
            :NEW.STATUS_ID,
            :NEW.NATURAL_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_FUTURE_STATUS
    after insert or update or delete
    on ATLAS_AUTHDB.QU_FUTURE_STATUS
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE_STATUS (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_MEMB_PUBLICATION
    before insert or update
    on ATLAS_AUTHDB.PLOT_MEMB_PUBLICATION
    for each row
begin
  IF INSERTING THEN
    select seq_plot_memb_publication.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_RESPONSIBLE_MEMBER
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.MEMBER_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.MEMBER_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTIVITY_ID',
            TO_CHAR(:OLD.ACTIVITY_ID),
            TO_CHAR(:NEW.ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.MEMBER_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'MEMBER_ID',
            TO_CHAR(:OLD.MEMBER_ID),
            TO_CHAR(:NEW.MEMBER_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.MEMBER_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.ACTIVITY_ID,
            :OLD.MEMBER_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_RESPONSIBLE_MEMBER (
            ID,
            ACTIVITY_ID,
            MEMBER_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.MEMBER_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_NOM_DISCARDED
    after insert or update or delete
    on ATLAS_AUTHDB.NOM_DISCARDED
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOMINATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOMINATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOMINATION_ID',
          TO_CHAR(:OLD.NOMINATION_ID),
          TO_CHAR(:NEW.NOMINATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
      ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NOMINATION_ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_DISCARDED (
      ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NOMINATION_ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_NOMINATION
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_NOMINATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ACTIVITY_ID',
          TO_CHAR(:OLD.ACTIVITY_ID),
          TO_CHAR(:NEW.ACTIVITY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOMINATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOMINATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOMINATION_ID',
          TO_CHAR(:OLD.NOMINATION_ID),
          TO_CHAR(:NEW.NOMINATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SYSTEM_ID), 'null') <> COALESCE(TO_CHAR(:NEW.SYSTEM_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SYSTEM_ID',
          TO_CHAR(:OLD.SYSTEM_ID),
          TO_CHAR(:NEW.SYSTEM_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
      ID,
      ACTIVITY_ID,
      NOMINATION_ID,
      SYSTEM_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.ACTIVITY_ID,
      :OLD.NOMINATION_ID,
      :OLD.SYSTEM_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_NOMINATION (
      ID,
      ACTIVITY_ID,
      NOMINATION_ID,
      SYSTEM_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.ACTIVITY_ID,
      :NEW.NOMINATION_ID,
      :NEW.SYSTEM_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_PUBLICATION_GROUP
    before insert or update
    on ATLAS_AUTHDB.PLOT_PUBLICATION_GROUP
    for each row
begin
  IF INSERTING THEN
    select seq_plot_publication_group.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.HTR_IN_INCLUDE_DECEASED
    after insert or update or delete
    on ATLAS_AUTHDB.IN_INCLUDE_DECEASED
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED (
          ID,
          INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED (
          ID,
          INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED (
          ID,
          INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED (
      ID,
      INSTITUTE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INCLUDE_DECEASED (
      ID,
      INSTITUTE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_PUBLICATION
    before insert or update
    on ATLAS_AUTHDB.PLOT_PUBLICATION
    for each row
begin
  -- When inserting, sets the creation date and the autoincrement ID.
  IF INSERTING THEN
    select seq_plot_publication.nextval into :new.id from dual;
    IF :new.creation_date is null THEN
	    :new.creation_date := sysdate;
    END IF;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.ID_PROF_ADVANCE
    before insert
    on ATLAS_AUTHDB.PROF_ADVANCE
    for each row
begin
select seq_prof_advance.nextval into :new.id from dual;
end;
/

create trigger ATLAS_AUTHDB.HTR_NOM_NOMINATION
    after insert or update or delete
    on ATLAS_AUTHDB.NOM_NOMINATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PRIORITY), 'null') <> COALESCE(TO_CHAR(:NEW.PRIORITY), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PRIORITY',
          TO_CHAR(:OLD.PRIORITY),
          TO_CHAR(:NEW.PRIORITY)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ATLAS_FRACTION), 'null') <> COALESCE(TO_CHAR(:NEW.ATLAS_FRACTION), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ATLAS_FRACTION',
          TO_CHAR(:OLD.ATLAS_FRACTION),
          TO_CHAR(:NEW.ATLAS_FRACTION)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CATEGORY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.CATEGORY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CATEGORY_ID',
          TO_CHAR(:OLD.CATEGORY_ID),
          TO_CHAR(:NEW.CATEGORY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TYPE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TYPE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TYPE_ID',
          TO_CHAR(:OLD.TYPE_ID),
          TO_CHAR(:NEW.TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PROFESSIONAL_ADVANCEMENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.PROFESSIONAL_ADVANCEMENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PROFESSIONAL_ADVANCEMENT_ID',
          TO_CHAR(:OLD.PROFESSIONAL_ADVANCEMENT_ID),
          TO_CHAR(:NEW.PROFESSIONAL_ADVANCEMENT_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SUBMITTER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.SUBMITTER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SUBMITTER_ID',
          TO_CHAR(:OLD.SUBMITTER_ID),
          TO_CHAR(:NEW.SUBMITTER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SUBMISSION_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.SUBMISSION_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SUBMISSION_DATE',
          TO_CHAR(:OLD.SUBMISSION_DATE),
          TO_CHAR(:NEW.SUBMISSION_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
          ID,
          MEMBER_ID,
          PRIORITY,
          COMMENTS,
          ATLAS_FRACTION,
          CATEGORY_ID,
          TYPE_ID,
          PROFESSIONAL_ADVANCEMENT_ID,
          SUBMITTER_ID,
          SUBMISSION_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PRIORITY,
          :NEW.COMMENTS,
          :NEW.ATLAS_FRACTION,
          :NEW.CATEGORY_ID,
          :NEW.TYPE_ID,
          :NEW.PROFESSIONAL_ADVANCEMENT_ID,
          :NEW.SUBMITTER_ID,
          :NEW.SUBMISSION_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
      ID,
      MEMBER_ID,
      PRIORITY,
      COMMENTS,
      ATLAS_FRACTION,
      CATEGORY_ID,
      TYPE_ID,
      PROFESSIONAL_ADVANCEMENT_ID,
      SUBMITTER_ID,
      SUBMISSION_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.PRIORITY,
      :OLD.COMMENTS,
      :OLD.ATLAS_FRACTION,
      :OLD.CATEGORY_ID,
      :OLD.TYPE_ID,
      :OLD.PROFESSIONAL_ADVANCEMENT_ID,
      :OLD.SUBMITTER_ID,
      :OLD.SUBMISSION_DATE,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_NOMINATION (
      ID,
      MEMBER_ID,
      PRIORITY,
      COMMENTS,
      ATLAS_FRACTION,
      CATEGORY_ID,
      TYPE_ID,
      PROFESSIONAL_ADVANCEMENT_ID,
      SUBMITTER_ID,
      SUBMISSION_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.PRIORITY,
      :NEW.COMMENTS,
      :NEW.ATLAS_FRACTION,
      :NEW.CATEGORY_ID,
      :NEW.TYPE_ID,
      :NEW.PROFESSIONAL_ADVANCEMENT_ID,
      :NEW.SUBMITTER_ID,
      :NEW.SUBMISSION_DATE,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_MEMB_CONTRIBUTION
    before insert
    on ATLAS_AUTHDB.MEMB_CONTRIBUTION
    for each row
begin
   if ( :new.ID is null ) then
      select SEQ_MEMB_CONTRIBUTION.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.PUBNOTE_PHASE
    before insert
    on ATLAS_AUTHDB.PUBNOTE_PHASE
    for each row
begin
select seq_PUBNOTE_phase.nextval into :new.id from dual;
end;
/

create trigger ATLAS_AUTHDB.THIST_PUBNOTE_PHASE
    after insert or update or delete
    on ATLAS_AUTHDB.PUBNOTE_PHASE
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBNOTE_PHASE
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBNOTE_PHASE
        (ID, PUBLICATION_ID, PHASE, START_DATE, SIGN_OFF, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PUBLICATION_ID, :NEW.PHASE, :NEW.START_DATE, :NEW.SIGN_OFF, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_HIERARCHY
    after insert or update or delete
    on ATLAS_AUTHDB.IN_HIERARCHY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
          ID,
          INSTITUTE_ID,
          PARENT_INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PARENT_INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
          ID,
          INSTITUTE_ID,
          PARENT_INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PARENT_INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PARENT_INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.PARENT_INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
          ID,
          INSTITUTE_ID,
          PARENT_INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PARENT_INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PARENT_INSTITUTE_ID',
          TO_CHAR(:OLD.PARENT_INSTITUTE_ID),
          TO_CHAR(:NEW.PARENT_INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
          ID,
          INSTITUTE_ID,
          PARENT_INSTITUTE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PARENT_INSTITUTE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
      ID,
      INSTITUTE_ID,
      PARENT_INSTITUTE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.PARENT_INSTITUTE_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_HIERARCHY (
      ID,
      INSTITUTE_ID,
      PARENT_INSTITUTE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.PARENT_INSTITUTE_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_VETOED_CONF_PRIORITY
    after insert or update or delete
    on ATLAS_AUTHDB.ME_VETOED_CONF_PRIORITY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
          ID,
          MEMBER_ID,
          CONF_PRIORITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_PRIORITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
          ID,
          MEMBER_ID,
          CONF_PRIORITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_PRIORITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CONF_PRIORITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.CONF_PRIORITY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
          ID,
          MEMBER_ID,
          CONF_PRIORITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_PRIORITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CONF_PRIORITY_ID',
          TO_CHAR(:OLD.CONF_PRIORITY_ID),
          TO_CHAR(:NEW.CONF_PRIORITY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
          ID,
          MEMBER_ID,
          CONF_PRIORITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CONF_PRIORITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
      ID,
      MEMBER_ID,
      CONF_PRIORITY_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.CONF_PRIORITY_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_VETOED_CONF_PRIORITY (
      ID,
      MEMBER_ID,
      CONF_PRIORITY_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.CONF_PRIORITY_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_APPOINTMENT_NEW
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.APPOINTMENT_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.APPOINTMENT_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTIVITY_ID',
            TO_CHAR(:OLD.ACTIVITY_ID),
            TO_CHAR(:NEW.ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.APPOINTMENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.APPOINTMENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.APPOINTMENT_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'APPOINTMENT_ID',
            TO_CHAR(:OLD.APPOINTMENT_ID),
            TO_CHAR(:NEW.APPOINTMENT_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.APPOINTMENT_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
    
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.ACTIVITY_ID,
            :OLD.APPOINTMENT_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_APPOINTMENT_NEW (
            ID,
            ACTIVITY_ID,
            APPOINTMENT_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.APPOINTMENT_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_AFFILIATION_TYPE
    after insert or update or delete
    on ATLAS_AUTHDB.IN_AFFILIATION_TYPE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CODE), 'null') <> COALESCE(TO_CHAR(:NEW.CODE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AFFILIATION_TYPE (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ACTIVITY_STATUS
    after insert or update or delete
    on ATLAS_AUTHDB.ACTIVITY_STATUS
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'NAME',
            TO_CHAR(:OLD.NAME),
            TO_CHAR(:NEW.NAME)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.NAME,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.ACTIVITY_STATUS (
            ID,
            NAME,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_CONT_INSERT
    before insert or update
    on ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_CONTRIBUTION
    for each row
BEGIN
   if ( :new.ID is null ) then
      select SEQ_ANALYSIS_SYS_MEMB_CONT.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.ID_SUPP_NOTES
    before insert
    on ATLAS_AUTHDB.SUPP_NOTES
    for each row
BEGIN
   IF ( :new.ID IS NULL ) THEN
      SELECT seq_SUPP_NOTES.nextval INTO :new.ID FROM DUAL;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_ALLOW_PHOTO
    after insert or update or delete
    on ATLAS_AUTHDB.ME_ALLOW_PHOTO
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_ALLOW_PHOTO (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_TALK_BLACKDATE
    after insert or update or delete
    on ATLAS_AUTHDB.ME_TALK_BLACKDATE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
          ID,
          MEMBER_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
          ID,
          MEMBER_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
          ID,
          MEMBER_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'START_DATE',
          TO_CHAR(:OLD.START_DATE),
          TO_CHAR(:NEW.START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.END_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.END_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
          ID,
          MEMBER_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'END_DATE',
          TO_CHAR(:OLD.END_DATE),
          TO_CHAR(:NEW.END_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
          ID,
          MEMBER_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
      ID,
      MEMBER_ID,
      START_DATE,
      END_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.START_DATE,
      :OLD.END_DATE,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_BLACKDATE (
      ID,
      MEMBER_ID,
      START_DATE,
      END_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.START_DATE,
      :NEW.END_DATE,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.PHYSICS_TRIGGER_INSERT
    before insert
    on ATLAS_AUTHDB.PHYSICS_TRIGGER
    for each row
BEGIN
  if (:new.ID is null) then
      select SEQ_PHYSICS_TRIGGER.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.THIST_PUBNOTE_PHASE_1
    after insert or update or delete
    on ATLAS_AUTHDB.PUBNOTE_PHASE_1
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBNOTE_PHASE_1
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PHASE_ID = :OLD.PHASE_ID
    ;

    IF (:NEW.PHASE_ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBNOTE_PHASE_1
        (PHASE_ID, REVIEWERS_SIGN_OFF, DRAFT_CDS_URL, SIGN_OFF_DRAFT, STATE, ATLAS_RELEASE,  SIGN_OFF_1, SIGN_OFF_2, SIGN_OFF_1_MEMBER_ID, SIGN_OFF_2_MEMBER_ID, MAIL_TO_READERS, CIRCULATION_PUBNOTE, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.PHASE_ID, :NEW.REVIEWERS_SIGN_OFF, :NEW.DRAFT_CDS_URL, :NEW.SIGN_OFF_DRAFT, :NEW.STATE, :NEW.ATLAS_RELEASE, :NEW.SIGN_OFF_1, :NEW.SIGN_OFF_2, :NEW.SIGN_OFF_1_MEMBER_ID, :NEW.SIGN_OFF_2_MEMBER_ID, :NEW.MAIL_TO_READERS, :NEW.CIRCULATION_PUBNOTE, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.PHASE_2_DELETE
    after delete
    on ATLAS_AUTHDB.PHASE_2
    for each row
begin
	delete from review_cycle
	where review_cycle.phase_id = :old.phase_id
		and review_cycle.phase_loop = :old.loop_sequence;
  delete from phase_links
	where phase_links.phase_id = :old.phase_id;
end;
/

create trigger ATLAS_AUTHDB.THIST_PHASE_2
    after insert or update or delete
    on ATLAS_AUTHDB.PHASE_2
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PHASE_2
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PHASE_ID = :OLD.PHASE_ID
    ;

    IF (:NEW.PHASE_ID IS NOT NULL) THEN
        INSERT INTO HIST_PHASE_2
        (PHASE_ID, START_DATE, LPG_SIGN_OFF, PRESENTATION_PG, INDICO_URL, LPG_APPROVAL, DRAFT_CDS_URL, SIGN_OFF_DRAFT, RELEASE_ATLAS_COMMENTS, END_ATLAS_COMMENTS, STATE, LOOP_SEQUENCE, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.PHASE_ID, :NEW.START_DATE, :NEW.LPG_SIGN_OFF, :NEW.PRESENTATION_PG, :NEW.INDICO_URL, :NEW.LPG_APPROVAL, :NEW.DRAFT_CDS_URL, :NEW.SIGN_OFF_DRAFT, :NEW.RELEASE_ATLAS_COMMENTS, :NEW.END_ATLAS_COMMENTS, :NEW.STATE, :NEW.LOOP_SEQUENCE, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB_TALK_TOPICS
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_TALK_TOPICS
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.topic  <> :OLD.topic)
        OR (:NEW.topic IS NULL AND :OLD.topic IS NOT NULL)
        OR (:NEW.topic IS NOT NULL AND :OLD.topic IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.rank <> :OLD.rank)
        OR (:NEW.rank IS NULL AND :OLD.rank IS NOT NULL)
        OR (:NEW.rank IS NOT NULL AND :OLD.rank IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE memb_talk_topics_hist SET up_to = operation_date WHERE up_to IS NULL AND memb_id = :OLD.memb_id;
        IF :NEW.memb_id IS NOT NULL THEN
            INSERT INTO memb_talk_topics_hist
            VALUES (
            :NEW.memb_id,
            operation_date, null, :NEW.agent, change_composition,
            :NEW.rank, :NEW.topic
            );
        ELSE
            -- SHORT_TERM deleted!!
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO memb_talk_topics_hist
            VALUES (
            :OLD.memb_id,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.rank, :OLD.topic
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_AUTHORLIST_ID
    after insert or update or delete
    on ATLAS_AUTHDB.IN_AUTHORLIST_IDENTIFICATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PRE_ADDRESS), 'null') <> COALESCE(TO_CHAR(:NEW.PRE_ADDRESS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PRE_ADDRESS',
          TO_CHAR(:OLD.PRE_ADDRESS),
          TO_CHAR(:NEW.PRE_ADDRESS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ADDRESS), 'null') <> COALESCE(TO_CHAR(:NEW.ADDRESS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ADDRESS',
          TO_CHAR(:OLD.ADDRESS),
          TO_CHAR(:NEW.ADDRESS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INFN_ADDRESS), 'null') <> COALESCE(TO_CHAR(:NEW.INFN_ADDRESS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INFN_ADDRESS',
          TO_CHAR(:OLD.INFN_ADDRESS),
          TO_CHAR(:NEW.INFN_ADDRESS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
          ID,
          INSTITUTE_ID,
          PRE_ADDRESS,
          ADDRESS,
          INFN_ADDRESS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.PRE_ADDRESS,
          :NEW.ADDRESS,
          :NEW.INFN_ADDRESS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
      ID,
      INSTITUTE_ID,
      PRE_ADDRESS,
      ADDRESS,
      INFN_ADDRESS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.PRE_ADDRESS,
      :OLD.ADDRESS,
      :OLD.INFN_ADDRESS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_AUTHORLIST_IDENTIFICATION (
      ID,
      INSTITUTE_ID,
      PRE_ADDRESS,
      ADDRESS,
      INFN_ADDRESS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.PRE_ADDRESS,
      :NEW.ADDRESS,
      :NEW.INFN_ADDRESS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.T_EMPLOY_DECEASED
    after update
    on ATLAS_AUTHDB.ME_EMPLOYMENT
    for each row
BEGIN
    IF (:NEW.DEATH_DATE IS NOT NULL) THEN
        UPDATE me_member SET status_id = (SELECT id FROM me_status WHERE name = 'Deceased')
        WHERE id=:OLD.MEMB_ID;
    END IF;

        IF (:OLD.DEATH_DATE IS NOT NULL AND :NEW.DEATH_DATE IS NULL) THEN
        UPDATE me_member SET status_id = (SELECT id FROM me_status WHERE name = 'None')
        WHERE id=:OLD.MEMB_ID;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_EMPLOYMENT
    after insert or update or delete
    on ATLAS_AUTHDB.ME_EMPLOYMENT
    for each row
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'ID',
        TO_CHAR(:OLD.ID),
        TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMB_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMB_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'MEMB_ID',
        TO_CHAR(:OLD.MEMB_ID),
        TO_CHAR(:NEW.MEMB_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INST_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INST_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'INST_ID',
        TO_CHAR(:OLD.INST_ID),
        TO_CHAR(:NEW.INST_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'START_DATE',
        TO_CHAR(:OLD.START_DATE),
        TO_CHAR(:NEW.START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.END_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.END_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'END_DATE',
        TO_CHAR(:OLD.END_DATE),
        TO_CHAR(:NEW.END_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.DEATH_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.DEATH_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'DEATH_DATE',
        TO_CHAR(:OLD.DEATH_DATE),
        TO_CHAR(:NEW.DEATH_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INST_PHONE), 'null') <> COALESCE(TO_CHAR(:NEW.INST_PHONE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'INST_PHONE',
        TO_CHAR(:OLD.INST_PHONE),
        TO_CHAR(:NEW.INST_PHONE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.EMAIL), 'null') <> COALESCE(TO_CHAR(:NEW.EMAIL), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'EMAIL',
        TO_CHAR(:OLD.EMAIL),
        TO_CHAR(:NEW.EMAIL)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STUDENT), 'null') <> COALESCE(TO_CHAR(:NEW.STUDENT), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'STUDENT',
        TO_CHAR(:OLD.STUDENT),
        TO_CHAR(:NEW.STUDENT)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'ACTIVITY',
        TO_CHAR(:OLD.ACTIVITY),
        TO_CHAR(:NEW.ACTIVITY)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PTYPE), 'null') <> COALESCE(TO_CHAR(:NEW.PTYPE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'PTYPE',
        TO_CHAR(:OLD.PTYPE),
        TO_CHAR(:NEW.PTYPE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MOFREE), 'null') <> COALESCE(TO_CHAR(:NEW.MOFREE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'MOFREE',
        TO_CHAR(:OLD.MOFREE),
        TO_CHAR(:NEW.MOFREE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SUBDET), 'null') <> COALESCE(TO_CHAR(:NEW.SUBDET), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'SUBDET',
        TO_CHAR(:OLD.SUBDET),
        TO_CHAR(:NEW.SUBDET)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FOOTNOTE), 'null') <> COALESCE(TO_CHAR(:NEW.FOOTNOTE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'FOOTNOTE',
        TO_CHAR(:OLD.FOOTNOTE),
        TO_CHAR(:NEW.FOOTNOTE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AFLFLAG), 'null') <> COALESCE(TO_CHAR(:NEW.AFLFLAG), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'AFLFLAG',
        TO_CHAR(:OLD.AFLFLAG),
        TO_CHAR(:NEW.AFLFLAG)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MAILFLAG), 'null') <> COALESCE(TO_CHAR(:NEW.MAILFLAG), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'MAILFLAG',
        TO_CHAR(:OLD.MAILFLAG),
        TO_CHAR(:NEW.MAILFLAG)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'COMMENTS',
        TO_CHAR(:OLD.COMMENTS),
        TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INST_FOOTNOTE_ATLAS), 'null') <> COALESCE(TO_CHAR(:NEW.INST_FOOTNOTE_ATLAS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'INST_FOOTNOTE_ATLAS',
        TO_CHAR(:OLD.INST_FOOTNOTE_ATLAS),
        TO_CHAR(:NEW.INST_FOOTNOTE_ATLAS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INST_FOOTNOTE_EXT), 'null') <> COALESCE(TO_CHAR(:NEW.INST_FOOTNOTE_EXT), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'INST_FOOTNOTE_EXT',
        TO_CHAR(:OLD.INST_FOOTNOTE_EXT),
        TO_CHAR(:NEW.INST_FOOTNOTE_EXT)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MAILFLAG_STARTDATE), 'null') <> COALESCE(TO_CHAR(:NEW.MAILFLAG_STARTDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'MAILFLAG_STARTDATE',
        TO_CHAR(:OLD.MAILFLAG_STARTDATE),
        TO_CHAR(:NEW.MAILFLAG_STARTDATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MAILFLAG_ENDDATE), 'null') <> COALESCE(TO_CHAR(:NEW.MAILFLAG_ENDDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'MAILFLAG_ENDDATE',
        TO_CHAR(:OLD.MAILFLAG_ENDDATE),
        TO_CHAR(:NEW.MAILFLAG_ENDDATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FUNDA_ID), 'null') <> COALESCE(TO_CHAR(:NEW.FUNDA_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'FUNDA_ID',
        TO_CHAR(:OLD.FUNDA_ID),
        TO_CHAR(:NEW.FUNDA_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ONLEAVE_STARTDATE), 'null') <> COALESCE(TO_CHAR(:NEW.ONLEAVE_STARTDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'ONLEAVE_STARTDATE',
        TO_CHAR(:OLD.ONLEAVE_STARTDATE),
        TO_CHAR(:NEW.ONLEAVE_STARTDATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ONLEAVE_ENDDATE), 'null') <> COALESCE(TO_CHAR(:NEW.ONLEAVE_ENDDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'ONLEAVE_ENDDATE',
        TO_CHAR(:OLD.ONLEAVE_ENDDATE),
        TO_CHAR(:NEW.ONLEAVE_ENDDATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FOOTNOTE_ENDDATE), 'null') <> COALESCE(TO_CHAR(:NEW.FOOTNOTE_ENDDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'FOOTNOTE_ENDDATE',
        TO_CHAR(:OLD.FOOTNOTE_ENDDATE),
        TO_CHAR(:NEW.FOOTNOTE_ENDDATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FOOTNOTE_STARTDATE), 'null') <> COALESCE(TO_CHAR(:NEW.FOOTNOTE_STARTDATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
      VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'U',
        'FOOTNOTE_STARTDATE',
        TO_CHAR(:OLD.FOOTNOTE_STARTDATE),
        TO_CHAR(:NEW.FOOTNOTE_STARTDATE)
      );
    END IF;
  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
    )
    VALUES (
        :OLD.ID,
        :OLD.MEMB_ID,
        :OLD.INST_ID,
        :OLD.START_DATE,
        :OLD.END_DATE,
        :OLD.DEATH_DATE,
        :OLD.INST_PHONE,
        :OLD.EMAIL,
        :OLD.STUDENT,
        :OLD.ACTIVITY,
        :OLD.PTYPE,
        :OLD.MOFREE,
        :OLD.SUBDET,
        :OLD.FOOTNOTE,
        :OLD.AFLFLAG,
        :OLD.MAILFLAG,
        :OLD.COMMENTS,
        SYSDATE,
        :OLD.MODIFIED_BY,
        :OLD.INST_FOOTNOTE_ATLAS,
        :OLD.INST_FOOTNOTE_EXT,
        :OLD.MAILFLAG_STARTDATE,
        :OLD.MAILFLAG_ENDDATE,
        :OLD.FUNDA_ID,
        :OLD.AGENT,
        :OLD.ONLEAVE_STARTDATE,
        :OLD.ONLEAVE_ENDDATE,
        :OLD.FOOTNOTE_ENDDATE,
        :OLD.FOOTNOTE_STARTDATE,
        'D',
        NULL,
        NULL,
        NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_EMPLOYMENT (
        ID,
        MEMB_ID,
        INST_ID,
        START_DATE,
        END_DATE,
        DEATH_DATE,
        INST_PHONE,
        EMAIL,
        STUDENT,
        ACTIVITY,
        PTYPE,
        MOFREE,
        SUBDET,
        FOOTNOTE,
        AFLFLAG,
        MAILFLAG,
        COMMENTS,
        MODIFIED_ON,
        MODIFIED_BY,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        FUNDA_ID,
        AGENT,
        ONLEAVE_STARTDATE,
        ONLEAVE_ENDDATE,
        FOOTNOTE_ENDDATE,
        FOOTNOTE_STARTDATE,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
    )
    VALUES (
        :NEW.ID,
        :NEW.MEMB_ID,
        :NEW.INST_ID,
        :NEW.START_DATE,
        :NEW.END_DATE,
        :NEW.DEATH_DATE,
        :NEW.INST_PHONE,
        :NEW.EMAIL,
        :NEW.STUDENT,
        :NEW.ACTIVITY,
        :NEW.PTYPE,
        :NEW.MOFREE,
        :NEW.SUBDET,
        :NEW.FOOTNOTE,
        :NEW.AFLFLAG,
        :NEW.MAILFLAG,
        :NEW.COMMENTS,
        SYSDATE,
        :NEW.MODIFIED_BY,
        :NEW.INST_FOOTNOTE_ATLAS,
        :NEW.INST_FOOTNOTE_EXT,
        :NEW.MAILFLAG_STARTDATE,
        :NEW.MAILFLAG_ENDDATE,
        :NEW.FUNDA_ID,
        :NEW.AGENT,
        :NEW.ONLEAVE_STARTDATE,
        :NEW.ONLEAVE_ENDDATE,
        :NEW.FOOTNOTE_ENDDATE,
        :NEW.FOOTNOTE_STARTDATE,
        'I',
        NULL,
        NULL,
        NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_PUBNOTE_SUPP_NOTES
    before insert
    on ATLAS_AUTHDB.PUBNOTE_SUPP_NOTES
    for each row
begin
   if ( :new.ID is null ) then
      select SEQ_PUBNOTE_SUPP_NOTES.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.HTR_ME_TALK_PREFERRED_TOPIC
    after insert or update or delete
    on ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
          ID,
          MEMBER_ID,
          ACTIVITY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.ACTIVITY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
          ID,
          MEMBER_ID,
          ACTIVITY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.ACTIVITY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
          ID,
          MEMBER_ID,
          ACTIVITY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.ACTIVITY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ACTIVITY_ID',
          TO_CHAR(:OLD.ACTIVITY_ID),
          TO_CHAR(:NEW.ACTIVITY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'null') <> COALESCE(TO_CHAR(:NEW.RANK), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
          ID,
          MEMBER_ID,
          ACTIVITY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.ACTIVITY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
          ID,
          MEMBER_ID,
          ACTIVITY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.ACTIVITY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
      ID,
      MEMBER_ID,
      ACTIVITY_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.ACTIVITY_ID,
      :OLD.RANK,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_TALK_PREFERRED_TOPIC (
      ID,
      MEMBER_ID,
      ACTIVITY_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.ACTIVITY_ID,
      :NEW.RANK,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_LOCATION
    after insert or update or delete
    on ATLAS_AUTHDB.IN_LOCATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
          ID,
          INSTITUTE_ID,
          LATITUDE,
          LONGITUDE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LATITUDE,
          :NEW.LONGITUDE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
          ID,
          INSTITUTE_ID,
          LATITUDE,
          LONGITUDE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LATITUDE,
          :NEW.LONGITUDE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LATITUDE), 'null') <> COALESCE(TO_CHAR(:NEW.LATITUDE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
          ID,
          INSTITUTE_ID,
          LATITUDE,
          LONGITUDE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LATITUDE,
          :NEW.LONGITUDE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LATITUDE',
          TO_CHAR(:OLD.LATITUDE),
          TO_CHAR(:NEW.LATITUDE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LONGITUDE), 'null') <> COALESCE(TO_CHAR(:NEW.LONGITUDE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
          ID,
          INSTITUTE_ID,
          LATITUDE,
          LONGITUDE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LATITUDE,
          :NEW.LONGITUDE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LONGITUDE',
          TO_CHAR(:OLD.LONGITUDE),
          TO_CHAR(:NEW.LONGITUDE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
          ID,
          INSTITUTE_ID,
          LATITUDE,
          LONGITUDE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.LATITUDE,
          :NEW.LONGITUDE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
      ID,
      INSTITUTE_ID,
      LATITUDE,
      LONGITUDE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.LATITUDE,
      :OLD.LONGITUDE,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_LOCATION (
      ID,
      INSTITUTE_ID,
      LATITUDE,
      LONGITUDE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.LATITUDE,
      :NEW.LONGITUDE,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_CONFNOTE_PHASE_LINKS
    before insert
    on ATLAS_AUTHDB.CONFNOTE_PHASE_LINKS
    for each row
BEGIN IF
    ( :new.ID IS NULL
    ) THEN
  SELECT seq_CONFNOTE_PHASE_LINKS.nextval INTO :new.ID FROM dual;
END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_CONFNOTE_PHASE_LINKS
    after insert or update or delete
    on ATLAS_AUTHDB.CONFNOTE_PHASE_LINKS
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_CONFNOTE_PHASE_LINKS
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_CONFNOTE_PHASE_LINKS
        (ID, PHASE_ID, ALIAS, HREF, TYPE, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PHASE_ID, :NEW.ALIAS, :NEW.HREF, :NEW.TYPE, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    after insert or update or delete
    on ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    for each row
BEGIN
  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION (
      ID,
      PAPER_ID,
      REMOVAL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.PAPER_ID,
      :OLD.REMOVAL_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION (
      ID,
      PAPER_ID,
      REMOVAL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.PAPER_ID,
      :NEW.REMOVAL_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;


/

create trigger ATLAS_AUTHDB.HTR_ME_APPLYING_FOR_A_JOB
    after insert or update or delete
    on ATLAS_AUTHDB.ME_APPLYING_FOR_A_JOB
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_APPLYING_FOR_A_JOB (
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_APPLYING_FOR_A_JOB (
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_APPLYING_FOR_A_JOB (
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_APPLYING_FOR_A_JOB (
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_SIGNOFF_INS
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_SIGNOFF
    for each row
BEGIN
  select SEQ_ANALYSIS_SYS_PHASE_SIGNOFF.nextval into :new.id from dual;
END;
/

create trigger ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    before insert or update
    on ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    for each row
begin

  -- When inserting, sets the creation date and the autoincrement ID.
  IF INSERTING and (:new.id IS NULL) THEN
    select seq_pubnote_publication.nextval into :new.id from dual;
    IF :new.creation_date is null THEN
	    :new.creation_date := sysdate;
    END IF;
  END IF;

  IF UPDATING
  and (:new.temp_ref_code IS NULL
  or :new.temp_ref_code = '')
  --  and (:new.lead_group = :old.lead_group
   -- or (:new.lead_group IS NULL and :old.lead_group IS NULL)
  --  or (:new.lead_group = '' and :old.lead_group = '')
  --  or (:new.lead_group IS NULL and :old.lead_group = '')
  --  or (:new.lead_group = '' and :old.lead_group IS NULL))
  THEN
    :new.temp_ref_code := :old.temp_ref_code;
  END IF;
END;

/

create trigger ATLAS_AUTHDB.THIST_PUBNOTE_PUBLICATION
    after insert or update or delete
    on ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBNOTE_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBNOTE_PUBLICATION
        (ID, SHORT_TITLE, FULL_TITLE, TEMP_REF_CODE, FINAL_REF_CODE, CDS_URL, LEAD_GROUP, CREATION_DATE, STATUS, ADDITIONAL_NOTES, DELETION_REASON, DELETION_REQUEST, DELETION_MEMBERID, ROW_START_DATE, ROW_END_DATE, PUBNOTE_RUN)
        VALUES
        (:NEW.ID, :NEW.SHORT_TITLE, :NEW.FULL_TITLE, :NEW.TEMP_REF_CODE, :NEW.FINAL_REF_CODE, :NEW.CDS_URL, :NEW.LEAD_GROUP, :NEW.CREATION_DATE, :NEW.STATUS, :NEW.ADDITIONAL_NOTES, :NEW.DELETION_REASON, :NEW.DELETION_REQUEST, :NEW.DELETION_MEMBERID, Now, NULL, :NEW.PUBNOTE_RUN)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_PROPOSAL_C_AUTHORLIST_REMOVAL
    after insert or update or delete
    on ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
    for each row
BEGIN 
  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_REMOVAL (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.PROPOSAL_C_AUTHORLIST_REMOVAL (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;


/

create trigger ATLAS_AUTHDB.HTR_NOM_ADDITIONAL_SUBJECT
    after insert or update or delete
    on ATLAS_AUTHDB.NOM_ADDITIONAL_SUBJECT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ACTIVITY_ID',
          TO_CHAR(:OLD.ACTIVITY_ID),
          TO_CHAR(:NEW.ACTIVITY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOMINATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOMINATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOMINATION_ID',
          TO_CHAR(:OLD.NOMINATION_ID),
          TO_CHAR(:NEW.NOMINATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
          ID,
          ACTIVITY_ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.ACTIVITY_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
      ID,
      ACTIVITY_ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.ACTIVITY_ID,
      :OLD.NOMINATION_ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_ADDITIONAL_SUBJECT (
      ID,
      ACTIVITY_ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.ACTIVITY_ID,
      :NEW.NOMINATION_ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_PUBNOTE_PHASE_LINKS
    after insert or update or delete
    on ATLAS_AUTHDB.PUBNOTE_PHASE_LINKS
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBNOTE_PHASE_LINKS
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBNOTE_PHASE_LINKS
        (ID, PHASE_ID, ALIAS, HREF, TYPE, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PHASE_ID, :NEW.ALIAS, :NEW.HREF, :NEW.TYPE, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_PUBNOTE_PHASE_LINKS
    before insert
    on ATLAS_AUTHDB.PUBNOTE_PHASE_LINKS
    for each row
BEGIN IF
    ( :new.ID IS NULL
    ) THEN
  SELECT seq_PUBNOTE_PHASE_LINKS.nextval INTO :new.ID FROM dual;
END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_NOM_UPGRADE
    after insert or update or delete
    on ATLAS_AUTHDB.NOM_UPGRADE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOMINATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOMINATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOMINATION_ID',
          TO_CHAR(:OLD.NOMINATION_ID),
          TO_CHAR(:NEW.NOMINATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
          ID,
          NOMINATION_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
      ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NOMINATION_ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_UPGRADE (
      ID,
      NOMINATION_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NOMINATION_ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_NOM_IGNORE_UPGRADE_OTP
    after insert or update or delete
    on ATLAS_AUTHDB.NOM_IGNORE_UPGRADE_OTP
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP (
          ID,
          NOM_UPGRADE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOM_UPGRADE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOM_UPGRADE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOM_UPGRADE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP (
          ID,
          NOM_UPGRADE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOM_UPGRADE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOM_UPGRADE_ID',
          TO_CHAR(:OLD.NOM_UPGRADE_ID),
          TO_CHAR(:NEW.NOM_UPGRADE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP (
          ID,
          NOM_UPGRADE_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NOM_UPGRADE_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP (
      ID,
      NOM_UPGRADE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NOM_UPGRADE_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.NOM_IGNORE_UPGRADE_OTP (
      ID,
      NOM_UPGRADE_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NOM_UPGRADE_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.THIST_PUBNOTE_MEMB_PUBLICATION
    after insert or update or delete
    on ATLAS_AUTHDB.PUBNOTE_MEMB_PUBLICATION
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PUBNOTE_MEMB_PUBLICATION
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND MEMB_ID = :OLD.MEMB_ID
    AND PUBLICATION_ID = :OLD.PUBLICATION_ID
    ;

    IF (:NEW.MEMB_ID IS NOT NULL AND :NEW.PUBLICATION_ID IS NOT NULL) THEN
        INSERT INTO HIST_PUBNOTE_MEMB_PUBLICATION
        (MEMB_ID, PUBLICATION_ID, GROUP_ID, MEMB_FUNCTION, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.MEMB_ID, :NEW.PUBLICATION_ID, :NEW.GROUP_ID, :NEW.MEMB_FUNCTION, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB_TALK_BLACKDATES
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_TALK_BLACKDATES_BACKUP
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.start_date  <> :OLD.start_date)
        OR (:NEW.start_date IS NULL AND :OLD.start_date IS NOT NULL)
        OR (:NEW.start_date IS NOT NULL AND :OLD.start_date IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.end_date <> :OLD.end_date)
        OR (:NEW.end_date IS NULL AND :OLD.end_date IS NOT NULL)
        OR (:NEW.end_date IS NOT NULL AND :OLD.end_date IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE memb_talk_blackdates_hist SET up_to = operation_date WHERE up_to IS NULL AND memb_id = :OLD.memb_id;
        IF :NEW.memb_id IS NOT NULL THEN
            INSERT INTO memb_talk_blackdates_hist
            VALUES (
            :NEW.memb_id,
            operation_date, null, :NEW.agent, change_composition,
            :NEW.start_date, :NEW.end_date
            );
        ELSE
            -- SHORT_TERM deleted!!
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO memb_talk_blackdates_hist
            VALUES (
            :OLD.memb_id,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.start_date, :OLD.end_date
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB_APPOINTMENT
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_APPOINTMENT
    for each row
DECLARE 
	change_composition number;
	operation_date date;
BEGIN
	change_composition := 0;
	IF ((:NEW.MEMB_ID  <> :OLD.MEMB_ID)
		OR (:NEW.MEMB_ID IS NULL AND :OLD.MEMB_ID IS NOT NULL)
		OR (:NEW.MEMB_ID IS NOT NULL AND :OLD.MEMB_ID IS NULL)) THEN
			change_composition:=change_composition+power(2,0);
	END IF;

	IF ((:NEW.APPOINTMENT_ID <> :OLD.APPOINTMENT_ID)
		OR (:NEW.APPOINTMENT_ID IS NULL AND :OLD.APPOINTMENT_ID IS NOT NULL)
		OR (:NEW.APPOINTMENT_ID IS NOT NULL AND :OLD.APPOINTMENT_ID IS NULL)) THEN
			change_composition:=change_composition+power(2,1);
	END IF;

	IF ((:NEW.START_DATE <> :OLD.START_DATE)
		OR (:NEW.START_DATE IS NULL AND :OLD.START_DATE IS NOT NULL)
		OR (:NEW.START_DATE IS NOT NULL AND :OLD.START_DATE IS NULL)) THEN
			change_composition:=change_composition+power(2,2);
	END IF;

	IF ((:NEW.END_DATE  <> :OLD.END_DATE)
		OR (:NEW.END_DATE IS NULL AND :OLD.END_DATE IS NOT NULL)
		OR (:NEW.END_DATE IS NOT NULL AND :OLD.END_DATE IS NULL)) THEN
			change_composition:=change_composition+power(2,3);
	END IF;

	IF change_composition!=0 THEN
		operation_date:=SYSDATE;
		UPDATE MEMB_APPOINTMENT_HIST SET up_to = operation_date WHERE up_to IS NULL AND ID = :OLD.ID;

		IF :NEW.ID IS NOT NULL THEN
			INSERT INTO MEMB_APPOINTMENT_HIST 
			VALUES (
			operation_date, NULL, change_composition,
			:NEW.ID, :NEW.MEMB_ID, :NEW.APPOINTMENT_ID, :NEW.START_DATE, :NEW.END_DATE,
			:NEW.AGENT
			);
		ELSE
			-- make sure every CGI sets the agent straight
			-- prior to deletion !!!!!
			INSERT INTO MEMB_APPOINTMENT_HIST 
			VALUES (
			operation_date, operation_date, change_composition,
			:OLD.ID, :OLD.MEMB_ID, :OLD.APPOINTMENT_ID, :OLD.START_DATE, :OLD.END_DATE,
			:OLD.AGENT
			);
		END IF;
	END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_CONF_SUPP_NOTES
    before insert
    on ATLAS_AUTHDB.CONF_SUPP_NOTES
    for each row
begin
   if ( :new.ID is null ) then
      select SEQ_CONF_SUPP_NOTES.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.HTR_AT_NOMINATION_DRAFT_STATUS
    after insert or update or delete
    on ATLAS_AUTHDB.AT_NOMINATION_DRAFT_STATUS
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.STATUS_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.STATUS_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTIVITY_ID',
            TO_CHAR(:OLD.ACTIVITY_ID),
            TO_CHAR(:NEW.ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STATUS_ID), 'null') <> COALESCE(TO_CHAR(:NEW.STATUS_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.STATUS_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'STATUS_ID',
            TO_CHAR(:OLD.STATUS_ID),
            TO_CHAR(:NEW.STATUS_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.STATUS_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.ACTIVITY_ID,
            :OLD.STATUS_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_NOMINATION_DRAFT_STATUS (
            ID,
            ACTIVITY_ID,
            STATUS_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.STATUS_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_OTP_TASK
    after insert or update or delete
    on ATLAS_AUTHDB.QU_OTP_TASK
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
          ID,
          QUALIFICATION_ID,
          TASK_ID,
          REQUIREMENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TASK_ID,
          :NEW.REQUIREMENT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
          ID,
          QUALIFICATION_ID,
          TASK_ID,
          REQUIREMENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TASK_ID,
          :NEW.REQUIREMENT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
          ID,
          QUALIFICATION_ID,
          TASK_ID,
          REQUIREMENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TASK_ID,
          :NEW.REQUIREMENT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.REQUIREMENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.REQUIREMENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
          ID,
          QUALIFICATION_ID,
          TASK_ID,
          REQUIREMENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TASK_ID,
          :NEW.REQUIREMENT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'REQUIREMENT_ID',
          TO_CHAR(:OLD.REQUIREMENT_ID),
          TO_CHAR(:NEW.REQUIREMENT_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
          ID,
          QUALIFICATION_ID,
          TASK_ID,
          REQUIREMENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TASK_ID,
          :NEW.REQUIREMENT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
      ID,
      QUALIFICATION_ID,
      TASK_ID,
      REQUIREMENT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.TASK_ID,
      :OLD.REQUIREMENT_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_OTP_TASK (
      ID,
      QUALIFICATION_ID,
      TASK_ID,
      REQUIREMENT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.TASK_ID,
      :NEW.REQUIREMENT_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_APPOINTMENT
    after insert or update or delete
    on ATLAS_AUTHDB.APPOINTMENT
    for each row
DECLARE
  change_composition number;
  operation_date date;

BEGIN

  change_composition := 0;

  --NO TRACK TO ID.
  IF ((:NEW.NAME  <> :OLD.NAME)
    OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
    OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
      change_composition:=change_composition+power(2,0);
  END IF;

  IF ((:NEW.WARNING_DATE <> :OLD.WARNING_DATE)
    OR (:NEW.WARNING_DATE IS NULL AND :OLD.WARNING_DATE IS NOT NULL)
    OR (:NEW.WARNING_DATE IS NOT NULL AND :OLD.WARNING_DATE IS NULL)) THEN
      change_composition:=change_composition+power(2,1);
  END IF;

  IF ((:NEW.SELECTION_BOARD <> :OLD.SELECTION_BOARD)
    OR (:NEW.SELECTION_BOARD IS NULL AND :OLD.SELECTION_BOARD IS NOT NULL)
    OR (:NEW.SELECTION_BOARD IS NOT NULL AND :OLD.SELECTION_BOARD IS NULL)) THEN
      change_composition:=change_composition+power(2,2);
  END IF;

  IF ((:NEW.CB_VOTE  <> :OLD.CB_VOTE)
    OR (:NEW.CB_VOTE IS NULL AND :OLD.CB_VOTE IS NOT NULL)
    OR (:NEW.CB_VOTE IS NOT NULL AND :OLD.CB_VOTE IS NULL)) THEN
      change_composition:=change_composition+power(2,3);
  END IF;

  IF ((:NEW.IB_VOTE  <> :OLD.IB_VOTE)
    OR (:NEW.IB_VOTE IS NULL AND :OLD.IB_VOTE IS NOT NULL)
    OR (:NEW.IB_VOTE IS NOT NULL AND :OLD.IB_VOTE IS NULL)) THEN
      change_composition:=change_composition+power(2,4);
  END IF;

  IF ((:NEW.EB_MEMBER  <> :OLD.EB_MEMBER)
    OR (:NEW.EB_MEMBER IS NULL AND :OLD.EB_MEMBER IS NOT NULL)
    OR (:NEW.EB_MEMBER IS NOT NULL AND :OLD.EB_MEMBER IS NULL)) THEN
      change_composition:=change_composition+power(2,5);
  END IF;

  IF ((:NEW.CB_MEMBER  <> :OLD.CB_MEMBER)
    OR (:NEW.CB_MEMBER IS NULL AND :OLD.CB_MEMBER IS NOT NULL)
    OR (:NEW.CB_MEMBER IS NOT NULL AND :OLD.CB_MEMBER IS NULL)) THEN
      change_composition:=change_composition+power(2,6);
  END IF;

  IF ((:NEW.OT_VALUE  <> :OLD.OT_VALUE)
    OR (:NEW.OT_VALUE IS NULL AND :OLD.OT_VALUE IS NOT NULL)
    OR (:NEW.OT_VALUE IS NOT NULL AND :OLD.OT_VALUE IS NULL)) THEN
      change_composition:=change_composition+power(2,7);
  END IF;

  IF ((:NEW.COMMENTS  <> :OLD.COMMENTS)
    OR (:NEW.COMMENTS IS NULL AND :OLD.COMMENTS IS NOT NULL)
    OR (:NEW.COMMENTS IS NOT NULL AND :OLD.COMMENTS IS NULL)) THEN
      change_composition:=change_composition+power(2,8);
  END IF;

  IF ((:NEW.ORDER_BY  <> :OLD.ORDER_BY)
    OR (:NEW.ORDER_BY IS NULL AND :OLD.ORDER_BY IS NOT NULL)
    OR (:NEW.ORDER_BY IS NOT NULL AND :OLD.ORDER_BY IS NULL)) THEN
      change_composition:=change_composition+power(2,9);
  END IF;

  IF ((:NEW.MANDATE  <> :OLD.MANDATE)
    OR (:NEW.MANDATE IS NULL AND :OLD.MANDATE IS NOT NULL)
    OR (:NEW.MANDATE IS NOT NULL AND :OLD.MANDATE IS NULL)) THEN
      change_composition:=change_composition+power(2,10);
  END IF;

  IF change_composition!=0 THEN
    operation_date:=SYSDATE;
    UPDATE APPOINTMENT_HIST SET up_to = operation_date WHERE up_to IS NULL AND ID = :OLD.ID;

    IF :NEW.ID IS NOT NULL THEN
        INSERT INTO APPOINTMENT_HIST
        VALUES (
            :NEW.ID, :NEW.agent,
            operation_date, NULL, change_composition,
            :NEW.NAME, :NEW.WARNING_DATE, :NEW.SELECTION_BOARD, :NEW.CB_VOTE,
            :NEW.IB_VOTE, :NEW.EB_MEMBER, :NEW.CB_MEMBER, :NEW.OT_VALUE, :NEW.COMMENTS,
            :NEW.ORDER_BY, :NEW.MANDATE
        );
    ELSE
      -- make sure every CGI sets the agent straight
      -- prior to deletion !!!!!
      INSERT INTO APPOINTMENT_HIST
      VALUES (
        :OLD.ID, :OLD.agent,
        operation_date, operation_date, change_composition,
        :OLD.NAME, :OLD.WARNING_DATE, :OLD.SELECTION_BOARD, :OLD.CB_VOTE,
        :OLD.IB_VOTE, :OLD.EB_MEMBER, :OLD.CB_MEMBER, :OLD.OT_VALUE, :OLD.COMMENTS,
        :OLD.ORDER_BY, :OLD.MANDATE
      );
    END IF;
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_CONFNOTE_MEMB_CONTRIBUTION
    before insert
    on ATLAS_AUTHDB.CONFNOTE_MEMB_CONTRIBUTION
    for each row
begin
   if ( :new.ID is null ) then
      select SEQ_CONFNOTE_MEMB_CONTRIBUTION.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.HTR_AT_ACTIVITY_ACTION
    after insert or update or delete
    on ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.ACTION_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.ACTION_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTIVITY_ID',
            TO_CHAR(:OLD.ACTIVITY_ID),
            TO_CHAR(:NEW.ACTIVITY_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTION_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.ACTION_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ACTION_ID',
            TO_CHAR(:OLD.ACTION_ID),
            TO_CHAR(:NEW.ACTION_ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.ACTION_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.ACTIVITY_ID,
            :OLD.ACTION_ID,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTIVITY_ACTION (
            ID,
            ACTIVITY_ID,
            ACTION_ID,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.ACTIVITY_ID,
            :NEW.ACTION_ID,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.PLOT_COLLISION
    before insert or update
    on ATLAS_AUTHDB.PLOT_COLLISION
    for each row
begin
  IF INSERTING THEN
    select seq_plot_collision.nextval into :new.id from dual;
  END IF;
end;
/

create trigger ATLAS_AUTHDB.HTR_INSTITUTE_NOMINATION
    after insert or update or delete
    on ATLAS_AUTHDB.INSTITUTE_NOMINATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NOMINATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.NOMINATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NOMINATION_ID',
          TO_CHAR(:OLD.NOMINATION_ID),
          TO_CHAR(:NEW.NOMINATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SYSTEM_ID), 'null') <> COALESCE(TO_CHAR(:NEW.SYSTEM_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SYSTEM_ID',
          TO_CHAR(:OLD.SYSTEM_ID),
          TO_CHAR(:NEW.SYSTEM_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
          ID,
          INSTITUTE_ID,
          NOMINATION_ID,
          MEMBER_ID,
          SYSTEM_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NOMINATION_ID,
          :NEW.MEMBER_ID,
          :NEW.SYSTEM_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
      ID,
      INSTITUTE_ID,
      NOMINATION_ID,
      MEMBER_ID,
      SYSTEM_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.NOMINATION_ID,
      :OLD.MEMBER_ID,
      :OLD.SYSTEM_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.INSTITUTE_NOMINATION (
      ID,
      INSTITUTE_ID,
      NOMINATION_ID,
      MEMBER_ID,
      SYSTEM_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.NOMINATION_ID,
      :NEW.MEMBER_ID,
      :NEW.SYSTEM_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.T_INST_GHOST
    after insert or update or delete
    on ATLAS_AUTHDB.INST_GHOST
    for each row
BEGIN
    IF UPDATING THEN
        INSERT INTO INST_GHOST_HIST
        VALUES (
            'U',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.COUNTRY,
            :OLD.SPIRES_ICN,
            :OLD.DOMAIN
        );
    END IF;

    IF DELETING THEN
        INSERT INTO INST_GHOST_HIST
        VALUES (
            'D',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.COUNTRY,
            :OLD.SPIRES_ICN,
            :OLD.DOMAIN
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO INST_GHOST_HIST
        VALUES (
            'I',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.COUNTRY,
            :OLD.SPIRES_ICN,
            :OLD.DOMAIN
        );
    END IF;
END;

/

create trigger ATLAS_AUTHDB.THIST_PHASE_LINKS
    after insert or update or delete
    on ATLAS_AUTHDB.PHASE_LINKS
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_PHASE_LINKS
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_PHASE_LINKS
        (ID, PHASE_ID, ALIAS, HREF, TYPE, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PHASE_ID, :NEW.ALIAS, :NEW.HREF, :NEW.TYPE, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.ID_PHASE_LINKS
    before insert
    on ATLAS_AUTHDB.PHASE_LINKS
    for each row
begin
   if ( :new.ID is null ) then
      select seq_PHASE_LINKS.nextval into :new.ID from dual;
   end if;
end;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_MEETING_INSERT
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_MEETING
    for each row
BEGIN
  if (:new.ID is null) then
      select SEQ_ANALYSIS_SYS_MEETING.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.ID_CATEGORY
    before insert
    on ATLAS_AUTHDB.CATEGORY
    for each row
BEGIN
  SELECT seq_category.nextval INTO :NEW.ID FROM dual;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_INSERT
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
    for each row
BEGIN
  select seq_analysis_sys_phase.nextval into :new.id from dual;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_OTHER_CONSTRAINT
    after insert or update or delete
    on ATLAS_AUTHDB.ME_OTHER_CONSTRAINT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT (
          MEMBER_ID,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.MEMBER_ID,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT (
          MEMBER_ID,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.MEMBER_ID,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT (
          MEMBER_ID,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.MEMBER_ID,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT (
      MEMBER_ID,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.MEMBER_ID,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_OTHER_CONSTRAINT (
      MEMBER_ID,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.MEMBER_ID,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB_TALK_OTHER_TOPICS
    after insert or update or delete
    on ATLAS_AUTHDB.MEMB_TALK_OTHER_TOPICS_BACKUP
    for each row
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.other_topic  <> :OLD.other_topic)
        OR (:NEW.other_topic IS NULL AND :OLD.other_topic IS NOT NULL)
        OR (:NEW.other_topic IS NOT NULL AND :OLD.other_topic IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE memb_talk_other_topics_hist SET up_to = operation_date WHERE up_to IS NULL AND memb_id = :OLD.memb_id;
        IF :NEW.memb_id IS NOT NULL THEN
            INSERT INTO memb_talk_other_topics_hist
            VALUES (
            :NEW.memb_id,
            operation_date, null, :NEW.agent, change_composition,
            :NEW.other_topic
            );
        ELSE
            -- make sure every CGI sets the agent straight
            -- prior to deletion !!!!!
            INSERT INTO memb_talk_other_topics_hist
            VALUES (
            :OLD.memb_id,
            operation_date, operation_date, :OLD.agent, change_composition,
            :OLD.other_topic
            );
        END IF;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_PREFERRED_CONF_TOPIC
    after insert or update or delete
    on ATLAS_AUTHDB.ME_PREFERRED_CONF_TOPIC
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PREFERRED_CONF_ID), 'null') <> COALESCE(TO_CHAR(:NEW.PREFERRED_CONF_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PREFERRED_CONF_ID',
          TO_CHAR(:OLD.PREFERRED_CONF_ID),
          TO_CHAR(:NEW.PREFERRED_CONF_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TALK_PREFERRED_TOPIC_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TALK_PREFERRED_TOPIC_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TALK_PREFERRED_TOPIC_ID',
          TO_CHAR(:OLD.TALK_PREFERRED_TOPIC_ID),
          TO_CHAR(:NEW.TALK_PREFERRED_TOPIC_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'null') <> COALESCE(TO_CHAR(:NEW.RANK), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
          ID,
          MEMBER_ID,
          PREFERRED_CONF_ID,
          TALK_PREFERRED_TOPIC_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.PREFERRED_CONF_ID,
          :NEW.TALK_PREFERRED_TOPIC_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
      ID,
      MEMBER_ID,
      PREFERRED_CONF_ID,
      TALK_PREFERRED_TOPIC_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.PREFERRED_CONF_ID,
      :OLD.TALK_PREFERRED_TOPIC_ID,
      :OLD.RANK,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PREFERRED_CONF_TOPIC (
      ID,
      MEMBER_ID,
      PREFERRED_CONF_ID,
      TALK_PREFERRED_TOPIC_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.PREFERRED_CONF_ID,
      :NEW.TALK_PREFERRED_TOPIC_ID,
      :NEW.RANK,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_LINKS_INS
    before insert
    on ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_LINKS
    for each row
BEGIN
   if ( :new.ID is null ) then
      select seq_ANALYSIS_SYS_PHASE_LINKS.nextval into :new.ID from dual;
   end if;
END;
/

create trigger ATLAS_AUTHDB.INST_HIST_DELETE
    before delete
    on ATLAS_AUTHDB.IN_INSTITUTE
    for each row
DECLARE
BEGIN
 INSERT INTO INST_HIST (ID, NAME, ONAME, ADDRESS, COUNTRY_ID, PHONE, FAX, CONTACT, CONTACT_EMAIL, WEBPAGE, TIME_ZONE, INCL_DEAD, CONTACT_ID, 
 SECRETARY, SEC_PHONE, SEC_EMAIL, CLUSTER_ID, IS_CLUSTER, CLUSTER_COM, FUNDA_ID, CONTACT_CERN_ID, CTRLFLAG, PREADDRESS, INFNADDRESS, MODIFIED_ON, 
 MODIFIED_BY, FAKE_CLUSTER, SPIRES_ICN, DOMAIN ) VALUES (:old.ID, :old.NAME, :old.ONAME,
  :old.ADDRESS, :old.COUNTRY_ID, :old.PHONE, :old.FAX, :old.CONTACT,
  :old.CONTACT_EMAIL, :old.WEBPAGE, :old.TIME_ZONE, :old.INCL_DEAD, :old.CONTACT_ID,
  :old.SECRETARY,:old.SEC_PHONE,:old.SEC_EMAIL,:old.CLUSTER_ID,:old.IS_CLUSTER,:old.CLUSTER_COM,
  :old.FUNDA_ID,:old.CONTACT_CERN_ID,:old.CTRLFLAG,:old.PREADDRESS,:old.INFNADDRESS,sysdate, :old.MODIFIED_BY, :old.FAKE_CLUSTER, :old.SPIRES_ICN,
  :old.DOMAIN);
END;
/

create trigger ATLAS_AUTHDB.INST_HIST_UPDATE
    before update
    on ATLAS_AUTHDB.IN_INSTITUTE
    for each row
DECLARE

	NAME_AUX       		VARCHAR2(255) 	DEFAULT NULL;
	ONAME_AUX      		VARCHAR2(255) 	DEFAULT NULL;
	ADDRESS_AUX        	VARCHAR2(255) 	DEFAULT NULL;
	COUNTRY_ID_AUX      NUMBER(4) 		DEFAULT NULL;
	PHONE_AUX       	VARCHAR2(255) 	DEFAULT NULL;
	FAX_AUX       		VARCHAR2(255)	DEFAULT NULL;
	CONTACT_AUX     	VARCHAR2(255) 	DEFAULT NULL;
	CONTACT_EMAIL_AUX   VARCHAR2(255) 	DEFAULT NULL;
	WEBPAGE_AUX        	VARCHAR2(255) 	DEFAULT NULL;
	TIME_ZONE_AUX   	VARCHAR2(8) 	DEFAULT NULL;
	INCL_DEAD_AUX  		NUMBER(1) 		DEFAULT NULL;
	CONTACT_ID_AUX      NUMBER(6) 		DEFAULT NULL;
	SECRETARY_AUX       VARCHAR2(255) 	DEFAULT NULL;
	SEC_PHONE_AUX       VARCHAR2(255) 	DEFAULT NULL;
	SEC_EMAIL_AUX      	VARCHAR2(255)	DEFAULT NULL;
	CLUSTER_ID_AUX  	NUMBER(4) 		DEFAULT NULL;
	IS_CLUSTER_AUX      NUMBER(1) 		DEFAULT NULL;
	CLUSTER_COM_AUX     VARCHAR2(255) 	DEFAULT NULL;
	FUNDA_ID_AUX        NUMBER(4) 		DEFAULT NULL;
	CONTACT_CERN_ID_AUX NUMBER(6)		DEFAULT NULL;
	CTRLFLAG_AUX        NUMBER(4) 		DEFAULT NULL;
	PREADDRESS_AUX      VARCHAR2(255) 	DEFAULT NULL;
	INFNADDRESS_AUX     VARCHAR2(255)	DEFAULT NULL;
	MODIFIED_BY_AUX   	NUMBER(8)		DEFAULT NULL;
  FAKE_CLUSTER_AUX    NUMBER(1) DEFAULT NULL;
  SPIRES_ICN_AUX          VARCHAR2(255)	DEFAULT NULL;
  DOMAIN_AUX          VARCHAR2(255)	DEFAULT NULL;
	update_trigger		boolean;

BEGIN

	update_trigger:=false;

   IF ((:NEW.NAME  <> :OLD.NAME)
       OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
       OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL))
     THEN NAME_AUX := :OLD.NAME;
        update_trigger:=true;
   END IF;

   IF ((:NEW.ONAME  <> :OLD.ONAME)
       OR (:NEW.ONAME IS NULL AND :OLD.ONAME IS NOT NULL)
       OR (:NEW.ONAME IS NOT NULL AND :OLD.ONAME IS NULL))
     THEN ONAME_AUX := :OLD.ONAME;
     update_trigger:=true;
   END IF;

   IF ((:NEW.ADDRESS  <> :OLD.ADDRESS)
       OR (:NEW.ADDRESS IS NULL AND :OLD.ADDRESS IS NOT NULL)
       OR (:NEW.ADDRESS IS NOT NULL AND :OLD.ADDRESS IS NULL))
     THEN ADDRESS_AUX := :OLD.ADDRESS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.COUNTRY_ID  <> :OLD.COUNTRY_ID)
       OR (:NEW.COUNTRY_ID IS NULL AND :OLD.COUNTRY_ID IS NOT NULL)
       OR (:NEW.COUNTRY_ID IS NOT NULL AND :OLD.COUNTRY_ID IS NULL))
     THEN COUNTRY_ID_AUX := :OLD.COUNTRY_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.PHONE  <> :OLD.PHONE)
       OR (:NEW.PHONE IS NULL AND :OLD.PHONE IS NOT NULL)
       OR (:NEW.PHONE IS NOT NULL AND :OLD.PHONE IS NULL))
     THEN PHONE_AUX := :OLD.PHONE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.FAX  <> :OLD.FAX)
       OR (:NEW.FAX IS NULL AND :OLD.FAX IS NOT NULL)
       OR (:NEW.FAX IS NOT NULL AND :OLD.FAX IS NULL))
     THEN FAX_AUX := :OLD.FAX;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CONTACT  <> :OLD.CONTACT)
       OR (:NEW.CONTACT IS NULL AND :OLD.CONTACT IS NOT NULL)
       OR (:NEW.CONTACT IS NOT NULL AND :OLD.CONTACT IS NULL))
     THEN
        CONTACT_AUX := :OLD.CONTACT;
        update_trigger:=true;
   END IF;

   IF ((:NEW.CONTACT_EMAIL  <> :OLD.CONTACT_EMAIL)
       OR (:NEW.CONTACT_EMAIL IS NULL AND :OLD.CONTACT_EMAIL IS NOT NULL)
       OR (:NEW.CONTACT_EMAIL IS NOT NULL AND :OLD.CONTACT_EMAIL IS NULL))
     THEN CONTACT_EMAIL_AUX := :OLD.CONTACT_EMAIL;
     update_trigger:=true;
   END IF;

   IF ((:NEW.WEBPAGE  <> :OLD.WEBPAGE)
       OR (:NEW.WEBPAGE IS NULL AND :OLD.WEBPAGE IS NOT NULL)
       OR (:NEW.WEBPAGE IS NOT NULL AND :OLD.WEBPAGE IS NULL))
     THEN WEBPAGE_AUX := :OLD.WEBPAGE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.TIME_ZONE  <> :OLD.TIME_ZONE)
       OR (:NEW.TIME_ZONE IS NULL AND :OLD.TIME_ZONE IS NOT NULL)
       OR (:NEW.TIME_ZONE IS NOT NULL AND :OLD.TIME_ZONE IS NULL))
     THEN TIME_ZONE_AUX := :OLD.TIME_ZONE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.INCL_DEAD  <> :OLD.INCL_DEAD)
       OR (:NEW.INCL_DEAD IS NULL AND :OLD.INCL_DEAD IS NOT NULL)
       OR (:NEW.INCL_DEAD IS NOT NULL AND :OLD.INCL_DEAD IS NULL))
     THEN INCL_DEAD_AUX := :OLD.INCL_DEAD;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CONTACT_ID  <> :OLD.CONTACT_ID)
       OR (:NEW.CONTACT_ID IS NULL AND :OLD.CONTACT_ID IS NOT NULL)
       OR (:NEW.CONTACT_ID IS NOT NULL AND :OLD.CONTACT_ID IS NULL))
     THEN CONTACT_ID_AUX := :OLD.CONTACT_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.SECRETARY  <> :OLD.SECRETARY)
       OR (:NEW.SECRETARY IS NULL AND :OLD.SECRETARY IS NOT NULL)
       OR (:NEW.SECRETARY IS NOT NULL AND :OLD.SECRETARY IS NULL))
     THEN SECRETARY_AUX := :OLD.SECRETARY;
     update_trigger:=true;
   END IF;

   IF ((:NEW.SEC_PHONE  <> :OLD.SEC_PHONE)
       OR (:NEW.SEC_PHONE IS NULL AND :OLD.SEC_PHONE IS NOT NULL)
       OR (:NEW.SEC_PHONE IS NOT NULL AND :OLD.SEC_PHONE IS NULL))
     THEN SEC_PHONE_AUX := :OLD.SEC_PHONE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.SEC_EMAIL  <> :OLD.SEC_EMAIL)
       OR (:NEW.SEC_EMAIL IS NULL AND :OLD.SEC_EMAIL IS NOT NULL)
       OR (:NEW.SEC_EMAIL IS NOT NULL AND :OLD.SEC_EMAIL IS NULL))
     THEN SEC_EMAIL_AUX := :OLD.SEC_EMAIL;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CLUSTER_ID  <> :OLD.CLUSTER_ID)
       OR (:NEW.CLUSTER_ID IS NULL AND :OLD.CLUSTER_ID IS NOT NULL)
       OR (:NEW.CLUSTER_ID IS NOT NULL AND :OLD.CLUSTER_ID IS NULL))
     THEN CLUSTER_ID_AUX := :OLD.CLUSTER_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.IS_CLUSTER  <> :OLD.IS_CLUSTER)
       OR (:NEW.IS_CLUSTER IS NULL AND :OLD.IS_CLUSTER IS NOT NULL)
       OR (:NEW.IS_CLUSTER IS NOT NULL AND :OLD.IS_CLUSTER IS NULL))
     THEN IS_CLUSTER_AUX := :OLD.IS_CLUSTER;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CLUSTER_COM  <> :OLD.CLUSTER_COM)
       OR (:NEW.CLUSTER_COM IS NULL AND :OLD.CLUSTER_COM IS NOT NULL)
       OR (:NEW.CLUSTER_COM IS NOT NULL AND :OLD.CLUSTER_COM IS NULL))
     THEN CLUSTER_COM_AUX := :OLD.CLUSTER_COM;
     update_trigger:=true;
   END IF;

   IF ((:NEW.FUNDA_ID  <> :OLD.FUNDA_ID)
       OR (:NEW.FUNDA_ID IS NULL AND :OLD.FUNDA_ID IS NOT NULL)
       OR (:NEW.FUNDA_ID IS NOT NULL AND :OLD.FUNDA_ID IS NULL))
     THEN FUNDA_ID_AUX := :OLD.FUNDA_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CONTACT_CERN_ID  <> :OLD.CONTACT_CERN_ID)
       OR (:NEW.CONTACT_CERN_ID IS NULL AND :OLD.CONTACT_CERN_ID IS NOT NULL)
       OR (:NEW.CONTACT_CERN_ID IS NOT NULL AND :OLD.CONTACT_CERN_ID IS NULL))
     THEN CONTACT_CERN_ID_AUX := :OLD.CONTACT_CERN_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CTRLFLAG  <> :OLD.CTRLFLAG)
       OR (:NEW.CTRLFLAG IS NULL AND :OLD.CTRLFLAG IS NOT NULL)
       OR (:NEW.CTRLFLAG IS NOT NULL AND :OLD.CTRLFLAG IS NULL))
     THEN CTRLFLAG_AUX := :OLD.CTRLFLAG;
     update_trigger:=true;
   END IF;

   IF ((:NEW.PREADDRESS  <> :OLD.PREADDRESS)
       OR (:NEW.PREADDRESS IS NULL AND :OLD.PREADDRESS IS NOT NULL)
       OR (:NEW.PREADDRESS IS NOT NULL AND :OLD.PREADDRESS IS NULL))
     THEN PREADDRESS_AUX := :OLD.PREADDRESS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.INFNADDRESS  <> :OLD.INFNADDRESS)
       OR (:NEW.INFNADDRESS IS NULL AND :OLD.INFNADDRESS IS NOT NULL)
       OR (:NEW.INFNADDRESS IS NOT NULL AND :OLD.INFNADDRESS IS NULL))
     THEN INFNADDRESS_AUX := :OLD.INFNADDRESS;
     update_trigger:=true;
   END IF;

   --IF ((:NEW.MODIFIED_BY  <> :OLD.MODIFIED_BY)
     --  OR (:NEW.MODIFIED_BY IS NULL AND :OLD.MODIFIED_BY IS NOT NULL)
      -- OR (:NEW.MODIFIED_BY IS NOT NULL AND :OLD.MODIFIED_BY IS NULL))
     --THEN MODIFIED_BY_AUX := :OLD.MODIFIED_BY;
     --update_trigger:=true;
   --END IF;
   
    IF ((:NEW.FAKE_CLUSTER  <> :OLD.FAKE_CLUSTER)
       OR (:NEW.FAKE_CLUSTER IS NULL AND :OLD.FAKE_CLUSTER IS NOT NULL)
       OR (:NEW.FAKE_CLUSTER IS NOT NULL AND :OLD.FAKE_CLUSTER IS NULL))
     THEN FAKE_CLUSTER_AUX := :OLD.FAKE_CLUSTER;
     update_trigger:=true;
   END IF;
   
   IF ((:NEW.SPIRES_ICN  <> :OLD.SPIRES_ICN)
       OR (:NEW.SPIRES_ICN IS NULL AND :OLD.SPIRES_ICN IS NOT NULL)
       OR (:NEW.SPIRES_ICN IS NOT NULL AND :OLD.SPIRES_ICN IS NULL))
     THEN SPIRES_ICN_AUX := :OLD.SPIRES_ICN;
     update_trigger:=true;
   END IF;
   
  IF ((:NEW.DOMAIN  <> :OLD.DOMAIN)
       OR (:NEW.DOMAIN IS NULL AND :OLD.DOMAIN IS NOT NULL)
       OR (:NEW.DOMAIN IS NOT NULL AND :OLD.DOMAIN IS NULL))
     THEN DOMAIN_AUX := :OLD.DOMAIN;
     update_trigger:=true;
   END IF;

  if (update_trigger)
	  then
		  INSERT INTO INST_HIST (ID, NAME, ONAME, ADDRESS, COUNTRY_ID, PHONE, FAX, CONTACT, CONTACT_EMAIL, WEBPAGE, TIME_ZONE, INCL_DEAD, CONTACT_ID, 
 SECRETARY, SEC_PHONE, SEC_EMAIL, CLUSTER_ID, IS_CLUSTER, CLUSTER_COM, FUNDA_ID, CONTACT_CERN_ID, CTRLFLAG, PREADDRESS, INFNADDRESS, MODIFIED_ON, 
 MODIFIED_BY, FAKE_CLUSTER, SPIRES_ICN, DOMAIN ) VALUES (:NEW.ID, NAME_AUX, ONAME_AUX,
		  ADDRESS_AUX, COUNTRY_ID_AUX, PHONE_AUX, FAX_AUX, CONTACT_AUX, CONTACT_EMAIL_AUX,
		  WEBPAGE_AUX, TIME_ZONE_AUX, INCL_DEAD_AUX,CONTACT_ID_AUX,SECRETARY_AUX,SEC_PHONE_AUX,SEC_EMAIL_AUX,
		  CLUSTER_ID_AUX, IS_CLUSTER_AUX, CLUSTER_COM_AUX,FUNDA_ID_AUX,CONTACT_CERN_ID_AUX,CTRLFLAG_AUX,PREADDRESS_AUX,
		  INFNADDRESS_AUX, sysdate,:old.MODIFIED_BY, FAKE_CLUSTER_AUX, SPIRES_ICN_AUX, DOMAIN_AUX);
      :new.modified_on := sysdate;
  end if;

END;
/

create trigger ATLAS_AUTHDB.T_INST_AFFILIATION
    before update
    on ATLAS_AUTHDB.IN_INSTITUTE
    for each row
BEGIN
    IF (:NEW.AFF_TYPE <> :OLD.AFF_TYPE AND (:NEW.AFF_TYPE != 'ASSOCIATED' AND :NEW.AFF_TYPE != 'CLUSTER_MEMBER')) THEN
        :NEW.cluster_id := :OLD.ID;
    END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_INSTITUTE
    after insert or update or delete
    on ATLAS_AUTHDB.IN_INSTITUTE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SHORT_NAME), 'null') <> COALESCE(TO_CHAR(:NEW.SHORT_NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SHORT_NAME',
          TO_CHAR(:OLD.SHORT_NAME),
          TO_CHAR(:NEW.SHORT_NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COUNTRY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.COUNTRY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COUNTRY_ID',
          TO_CHAR(:OLD.COUNTRY_ID),
          TO_CHAR(:NEW.COUNTRY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PHONE), 'null') <> COALESCE(TO_CHAR(:NEW.PHONE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PHONE',
          TO_CHAR(:OLD.PHONE),
          TO_CHAR(:NEW.PHONE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FAX), 'null') <> COALESCE(TO_CHAR(:NEW.FAX), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FAX',
          TO_CHAR(:OLD.FAX),
          TO_CHAR(:NEW.FAX)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.WEBPAGE), 'null') <> COALESCE(TO_CHAR(:NEW.WEBPAGE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'WEBPAGE',
          TO_CHAR(:OLD.WEBPAGE),
          TO_CHAR(:NEW.WEBPAGE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.DOMAIN), 'null') <> COALESCE(TO_CHAR(:NEW.DOMAIN), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'DOMAIN',
          TO_CHAR(:OLD.DOMAIN),
          TO_CHAR(:NEW.DOMAIN)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TIME_ZONE), 'null') <> COALESCE(TO_CHAR(:NEW.TIME_ZONE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TIME_ZONE',
          TO_CHAR(:OLD.TIME_ZONE),
          TO_CHAR(:NEW.TIME_ZONE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FUNDING_AGENCY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.FUNDING_AGENCY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FUNDING_AGENCY_ID',
          TO_CHAR(:OLD.FUNDING_AGENCY_ID),
          TO_CHAR(:NEW.FUNDING_AGENCY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SPIRES_ICN), 'null') <> COALESCE(TO_CHAR(:NEW.SPIRES_ICN), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'SPIRES_ICN',
          TO_CHAR(:OLD.SPIRES_ICN),
          TO_CHAR(:NEW.SPIRES_ICN)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
          ID,
          NAME,
          SHORT_NAME,
          COUNTRY_ID,
          PHONE,
          FAX,
          WEBPAGE,
          DOMAIN,
          TIME_ZONE,
          FUNDING_AGENCY_ID,
          SPIRES_ICN,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.SHORT_NAME,
          :NEW.COUNTRY_ID,
          :NEW.PHONE,
          :NEW.FAX,
          :NEW.WEBPAGE,
          :NEW.DOMAIN,
          :NEW.TIME_ZONE,
          :NEW.FUNDING_AGENCY_ID,
          :NEW.SPIRES_ICN,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
      ID,
      NAME,
      SHORT_NAME,
      COUNTRY_ID,
      PHONE,
      FAX,
      WEBPAGE,
      DOMAIN,
      TIME_ZONE,
      FUNDING_AGENCY_ID,
      SPIRES_ICN,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      :OLD.SHORT_NAME,
      :OLD.COUNTRY_ID,
      :OLD.PHONE,
      :OLD.FAX,
      :OLD.WEBPAGE,
      :OLD.DOMAIN,
      :OLD.TIME_ZONE,
      :OLD.FUNDING_AGENCY_ID,
      :OLD.SPIRES_ICN,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_INSTITUTE (
      ID,
      NAME,
      SHORT_NAME,
      COUNTRY_ID,
      PHONE,
      FAX,
      WEBPAGE,
      DOMAIN,
      TIME_ZONE,
      FUNDING_AGENCY_ID,
      SPIRES_ICN,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      :NEW.SHORT_NAME,
      :NEW.COUNTRY_ID,
      :NEW.PHONE,
      :NEW.FAX,
      :NEW.WEBPAGE,
      :NEW.DOMAIN,
      :NEW.TIME_ZONE,
      :NEW.FUNDING_AGENCY_ID,
      :NEW.SPIRES_ICN,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.T_INST_HIST
    after insert or update or delete
    on ATLAS_AUTHDB.IN_INSTITUTE
    for each row
BEGIN
    IF UPDATING THEN
        INSERT INTO INST_HIST
        VALUES (
            'U',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.ONAME,
            :OLD.ADDRESS,
            :OLD.COUNTRY_ID,
            :OLD.PHONE,
            :OLD.FAX,
            :OLD.CONTACT,
            :OLD.CONTACT_EMAIL,
            :OLD.WEBPAGE,
            :OLD.TIME_ZONE,
            :OLD.INCL_DEAD,
            :OLD.CONTACT_ID,
            :OLD.SECRETARY,
            :OLD.SEC_PHONE,
            :OLD.SEC_EMAIL,
            :OLD.CLUSTER_ID,
            :OLD.IS_CLUSTER,
            :OLD.CLUSTER_COM,
            :OLD.FUNDA_ID,
            :OLD.CONTACT_CERN_ID,
            :OLD.CTRLFLAG,
            :OLD.PREADDRESS,
            :OLD.INFNADDRESS,
            :OLD.MODIFIED_ON,
            :OLD.MODIFIED_BY,
            :OLD.FAKE_CLUSTER,
            :OLD.DOMAIN,
            :OLD.SPIRES_ICN,
            :OLD.AFF_START,
            :OLD.AFF_END,
            :OLD.AFF_TYPE_ID,
            :OLD.ADDRESS_2,
            :OLD.ADDRESS_3,
            :OLD.AFF_TYPE,
            :OLD.LATITUDE,
            :OLD.LONGITUDE
        );
    END IF;

    IF DELETING THEN
        INSERT INTO INST_HIST
        VALUES (
            'D',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.ONAME,
            :OLD.ADDRESS,
            :OLD.COUNTRY_ID,
            :OLD.PHONE,
            :OLD.FAX,
            :OLD.CONTACT,
            :OLD.CONTACT_EMAIL,
            :OLD.WEBPAGE,
            :OLD.TIME_ZONE,
            :OLD.INCL_DEAD,
            :OLD.CONTACT_ID,
            :OLD.SECRETARY,
            :OLD.SEC_PHONE,
            :OLD.SEC_EMAIL,
            :OLD.CLUSTER_ID,
            :OLD.IS_CLUSTER,
            :OLD.CLUSTER_COM,
            :OLD.FUNDA_ID,
            :OLD.CONTACT_CERN_ID,
            :OLD.CTRLFLAG,
            :OLD.PREADDRESS,
            :OLD.INFNADDRESS,
            :OLD.MODIFIED_ON,
            :OLD.MODIFIED_BY,
            :OLD.FAKE_CLUSTER,
            :OLD.DOMAIN,
            :OLD.SPIRES_ICN,
            :OLD.AFF_START,
            :OLD.AFF_END,
            :OLD.AFF_TYPE_ID,
            :OLD.ADDRESS_2,
            :OLD.ADDRESS_3,
            :OLD.AFF_TYPE,
            :OLD.LATITUDE,
            :OLD.LONGITUDE
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO INST_HIST
        VALUES (
            'I',
            SYSTIMESTAMP(0),
            :NEW.MODIFIED_BY,
            :OLD.ID,
            :OLD.NAME,
            :OLD.ONAME,
            :OLD.ADDRESS,
            :OLD.COUNTRY_ID,
            :OLD.PHONE,
            :OLD.FAX,
            :OLD.CONTACT,
            :OLD.CONTACT_EMAIL,
            :OLD.WEBPAGE,
            :OLD.TIME_ZONE,
            :OLD.INCL_DEAD,
            :OLD.CONTACT_ID,
            :OLD.SECRETARY,
            :OLD.SEC_PHONE,
            :OLD.SEC_EMAIL,
            :OLD.CLUSTER_ID,
            :OLD.IS_CLUSTER,
            :OLD.CLUSTER_COM,
            :OLD.FUNDA_ID,
            :OLD.CONTACT_CERN_ID,
            :OLD.CTRLFLAG,
            :OLD.PREADDRESS,
            :OLD.INFNADDRESS,
            :OLD.MODIFIED_ON,
            :OLD.MODIFIED_BY,
            :OLD.FAKE_CLUSTER,
            :OLD.DOMAIN,
            :OLD.SPIRES_ICN,
            :OLD.AFF_START,
            :OLD.AFF_END,
            :OLD.AFF_TYPE_ID,
            :OLD.ADDRESS_2,
            :OLD.ADDRESS_3,
            :OLD.AFF_TYPE,
            :OLD.LATITUDE,
            :OLD.LONGITUDE
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.CONFNOTE_PHASE
    before insert
    on ATLAS_AUTHDB.CONFNOTE_PHASE
    for each row
begin
select seq_CONFNOTE_phase.nextval into :new.id from dual;
end;

/

create trigger ATLAS_AUTHDB.THIST_CONFNOTE_PHASE
    after insert or update or delete
    on ATLAS_AUTHDB.CONFNOTE_PHASE
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_CONFNOTE_PHASE
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND ID = :OLD.ID
    ;

    IF (:NEW.ID IS NOT NULL) THEN
        INSERT INTO HIST_CONFNOTE_PHASE
        (ID, PUBLICATION_ID, PHASE, START_DATE, INDICO_URL, SIGN_OFF, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.ID, :NEW.PUBLICATION_ID, :NEW.PHASE, :NEW.START_DATE, :NEW.INDICO_URL, :NEW.SIGN_OFF, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_IN_CONTACT
    after insert or update or delete
    on ATLAS_AUTHDB.IN_CONTACT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
          ID,
          INSTITUTE_ID,
          NAME,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
          ID,
          INSTITUTE_ID,
          NAME,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_ID',
          TO_CHAR(:OLD.INSTITUTE_ID),
          TO_CHAR(:NEW.INSTITUTE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
          ID,
          INSTITUTE_ID,
          NAME,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.EMAIL), 'null') <> COALESCE(TO_CHAR(:NEW.EMAIL), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
          ID,
          INSTITUTE_ID,
          NAME,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'EMAIL',
          TO_CHAR(:OLD.EMAIL),
          TO_CHAR(:NEW.EMAIL)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
          ID,
          INSTITUTE_ID,
          NAME,
          EMAIL,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.INSTITUTE_ID,
          :NEW.NAME,
          :NEW.EMAIL,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
      ID,
      INSTITUTE_ID,
      NAME,
      EMAIL,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.INSTITUTE_ID,
      :OLD.NAME,
      :OLD.EMAIL,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.IN_CONTACT (
      ID,
      INSTITUTE_ID,
      NAME,
      EMAIL,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.INSTITUTE_ID,
      :NEW.NAME,
      :NEW.EMAIL,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_WATCHER
    after insert or update or delete
    on ATLAS_AUTHDB.QU_WATCHER
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
          ID,
          QUALIFICATION_ID,
          WATCHER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.WATCHER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
          ID,
          QUALIFICATION_ID,
          WATCHER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.WATCHER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.WATCHER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.WATCHER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
          ID,
          QUALIFICATION_ID,
          WATCHER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.WATCHER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'WATCHER_ID',
          TO_CHAR(:OLD.WATCHER_ID),
          TO_CHAR(:NEW.WATCHER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
          ID,
          QUALIFICATION_ID,
          WATCHER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.WATCHER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
      ID,
      QUALIFICATION_ID,
      WATCHER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.WATCHER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_WATCHER (
      ID,
      QUALIFICATION_ID,
      WATCHER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.WATCHER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_AT_ACTION
    after insert or update or delete
    on ATLAS_AUTHDB.AT_ACTION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.CODE,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'ID',
            TO_CHAR(:OLD.ID),
            TO_CHAR(:NEW.ID)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.CODE,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'NAME',
            TO_CHAR(:OLD.NAME),
            TO_CHAR(:NEW.NAME)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CODE), 'null') <> COALESCE(TO_CHAR(:NEW.CODE), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.CODE,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'CODE',
            TO_CHAR(:OLD.CODE),
            TO_CHAR(:NEW.CODE)
        );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.CODE,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'U',
            'AGENT_ID',
            TO_CHAR(:OLD.AGENT_ID),
            TO_CHAR(:NEW.AGENT_ID)
        );
    END IF;
   
  END IF;

    IF DELETING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :OLD.ID,
            :OLD.NAME,
            :OLD.CODE,
            :OLD.AGENT_ID,
            OPERATION_DATE,
            'D',
            NULL,
            NULL,
            NULL
        );
    END IF;

    IF INSERTING THEN
        INSERT INTO ATLAS_AUTHDB_HISTORY.AT_ACTION (
            ID,
            NAME,
            CODE,
            AGENT_ID,
            MODIFIED_ON,
            ACTION,
            DIFF_COLUMN,
            OLD_VALUE,
            NEW_VALUE
        )
        VALUES (
            :NEW.ID,
            :NEW.NAME,
            :NEW.CODE,
            :NEW.AGENT_ID,
            OPERATION_DATE,
            'I',
            NULL,
            NULL,
            NULL
        );
    END IF;
END;
/

create trigger ATLAS_AUTHDB.T_TRIM_INITIALS
    before insert or update
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
BEGIN
    :new.initials := REPLACE(:NEW.initials, ' ', '');
END;
/

create trigger ATLAS_AUTHDB.MEMB_FULL_REQUALIFICATION
    before update
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
declare
begin
      if( :new.QUAL_STATE = 0 and :old.SSA is not null ) then
          :new.QUAL_FLAG := 0;
          :new.EXCLUDE_PREDT := 0;
          :new.QUAL_STRING := null;
          :new.QUAL_END := null; -- SSA should follow
          :new.PROPOSED_QUAL_START := null;
          :new.QUAL_START := null;
          :new.QUAL_RESP := null;
          :new.PRE_CREDIT := :old.RPDTC - round((sysdate-add_months(:old.SSA,12))/30);
          :new.RPDTC := 0;
          if( :new.PRE_CREDIT < 0 ) then
              :new.PRE_CREDIT := 0;
          end if;
          if( round((sysdate-add_months(:old.SSA,12))/30) < 0  ) then
              :new.PRE_CREDIT := :old.RPDTC;
          end if;
      end if;
end;
/

create trigger ATLAS_AUTHDB.TRACK_MEMB
    after insert or update or delete
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
DECLARE
    change_composition  NUMBER;
    delete_date         DATE;
BEGIN

    change_composition:=0;

   IF ((:NEW.LAST_NAME  <> :OLD.LAST_NAME)
       OR (:NEW.LAST_NAME IS NULL AND :OLD.LAST_NAME IS NOT NULL)
       OR (:NEW.LAST_NAME IS NOT NULL AND :OLD.LAST_NAME IS NULL))
     THEN change_composition:=change_composition+power(2,0);
   END IF;

   IF ((:NEW.FIRST_NAME  <> :OLD.FIRST_NAME)
       OR (:NEW.FIRST_NAME IS NULL AND :OLD.FIRST_NAME IS NOT NULL)
       OR (:NEW.FIRST_NAME IS NOT NULL AND :OLD.FIRST_NAME IS NULL))
     THEN change_composition:=change_composition+power(2,1);
   END IF;

   IF ((:NEW.INITIALS  <> :OLD.INITIALS)
       OR (:NEW.INITIALS IS NULL AND :OLD.INITIALS IS NOT NULL)
       OR (:NEW.INITIALS IS NOT NULL AND :OLD.INITIALS IS NULL))
     THEN change_composition:=change_composition+power(2,2);
   END IF;

   IF ((:NEW.CERN_ID  <> :OLD.CERN_ID)
       OR (:NEW.CERN_ID IS NULL AND :OLD.CERN_ID IS NOT NULL)
       OR (:NEW.CERN_ID IS NOT NULL AND :OLD.CERN_ID IS NULL))
     THEN change_composition:=change_composition+power(2,3);
   END IF;

   IF ((:NEW.CERN_CCID  <> :OLD.CERN_CCID)
       OR (:NEW.CERN_CCID IS NULL AND :OLD.CERN_CCID IS NOT NULL)
       OR (:NEW.CERN_CCID IS NOT NULL AND :OLD.CERN_CCID IS NULL))
     THEN change_composition:=change_composition+power(2,4);
   END IF;

   IF ((:NEW.QUAL_DATE  <> :OLD.QUAL_DATE)
       OR (:NEW.QUAL_DATE IS NULL AND :OLD.QUAL_DATE IS NOT NULL)
       OR (:NEW.QUAL_DATE IS NOT NULL AND :OLD.QUAL_DATE IS NULL))
     THEN change_composition:=change_composition+power(2,5);
   END IF;

   IF ((:NEW.QUAL_STRING  <> :OLD.QUAL_STRING)
       OR (:NEW.QUAL_STRING IS NULL AND :OLD.QUAL_STRING IS NOT NULL)
       OR (:NEW.QUAL_STRING IS NOT NULL AND :OLD.QUAL_STRING IS NULL))
     THEN change_composition:=change_composition+power(2,6);
   END IF;

   IF ((:NEW.PRE_CREDIT  <> :OLD.PRE_CREDIT)
       OR (:NEW.PRE_CREDIT IS NULL AND :OLD.PRE_CREDIT IS NOT NULL)
       OR (:NEW.PRE_CREDIT IS NOT NULL AND :OLD.PRE_CREDIT IS NULL))
     THEN change_composition:=change_composition+power(2,7);
   END IF;

   IF ((:NEW.COMMENTS  <> :OLD.COMMENTS)
       OR (:NEW.COMMENTS IS NULL AND :OLD.COMMENTS IS NOT NULL)
       OR (:NEW.COMMENTS IS NOT NULL AND :OLD.COMMENTS IS NULL))
     THEN change_composition:=change_composition+power(2,8);
   END IF;

   IF ((:NEW.LAST_NAME_LTX  <> :OLD.LAST_NAME_LTX)
       OR (:NEW.LAST_NAME_LTX IS NULL AND :OLD.LAST_NAME_LTX IS NOT NULL)
       OR (:NEW.LAST_NAME_LTX IS NOT NULL AND :OLD.LAST_NAME_LTX IS NULL))
     THEN change_composition:=change_composition+power(2,9);
   END IF;

   IF ((:NEW.FIRST_NAME_LTX  <> :OLD.FIRST_NAME_LTX)
       OR (:NEW.FIRST_NAME_LTX IS NULL AND :OLD.FIRST_NAME_LTX IS NOT NULL)
       OR (:NEW.FIRST_NAME_LTX IS NOT NULL AND :OLD.FIRST_NAME_LTX IS NULL))
     THEN change_composition:=change_composition+power(2,10);
   END IF;

   IF ((:NEW.PHOTO  <> :OLD.PHOTO)
       OR (:NEW.PHOTO IS NULL AND :OLD.PHOTO IS NOT NULL)
       OR (:NEW.PHOTO IS NOT NULL AND :OLD.PHOTO IS NULL))
     THEN change_composition:=change_composition+power(2,11);
   END IF;

   IF ((:NEW.HR_IGNORE  <> :OLD.HR_IGNORE)
       OR (:NEW.HR_IGNORE IS NULL AND :OLD.HR_IGNORE IS NOT NULL)
       OR (:NEW.HR_IGNORE IS NOT NULL AND :OLD.HR_IGNORE IS NULL))
     THEN change_composition:=change_composition+power(2,12);
   END IF;

   IF ((:NEW.QUAL_END  <> :OLD.QUAL_END)
       OR (:NEW.QUAL_END IS NULL AND :OLD.QUAL_END IS NOT NULL)
       OR (:NEW.QUAL_END IS NOT NULL AND :OLD.QUAL_END IS NULL))
     THEN change_composition:=change_composition+power(2,13);
   END IF;

   IF ((:NEW.QUAL_START  <> :OLD.QUAL_START)
       OR (:NEW.QUAL_START IS NULL AND :OLD.QUAL_START IS NOT NULL)
       OR (:NEW.QUAL_START IS NOT NULL AND :OLD.QUAL_START IS NULL))
     THEN change_composition:=change_composition+power(2,14);
   END IF;

   IF ((:NEW.QUAL_STATE  <> :OLD.QUAL_STATE)
       OR (:NEW.QUAL_STATE IS NULL AND :OLD.QUAL_STATE IS NOT NULL)
       OR (:NEW.QUAL_STATE IS NOT NULL AND :OLD.QUAL_STATE IS NULL))
     THEN change_composition:=change_composition+power(2,15);
   END IF;

   IF ((:NEW.QUAL_RESP  <> :OLD.QUAL_RESP)
       OR (:NEW.QUAL_RESP IS NULL AND :OLD.QUAL_RESP IS NOT NULL)
       OR (:NEW.QUAL_RESP IS NOT NULL AND :OLD.QUAL_RESP IS NULL))
     THEN change_composition:=change_composition+power(2,16);
   END IF;

   IF ((:NEW.PROPOSED_QUAL_START  <> :OLD.PROPOSED_QUAL_START)
       OR (:NEW.PROPOSED_QUAL_START IS NULL AND :OLD.PROPOSED_QUAL_START IS NOT NULL)
       OR (:NEW.PROPOSED_QUAL_START IS NOT NULL AND :OLD.PROPOSED_QUAL_START IS NULL))
     THEN change_composition:=change_composition+power(2,17);
   END IF;

   IF ((:NEW.UPDATE_EMAIL  <> :OLD.UPDATE_EMAIL)
       OR (:NEW.UPDATE_EMAIL IS NULL AND :OLD.UPDATE_EMAIL IS NOT NULL)
       OR (:NEW.UPDATE_EMAIL IS NOT NULL AND :OLD.UPDATE_EMAIL IS NULL))
     THEN change_composition:=change_composition+power(2,18);
   END IF;

   IF ((:NEW.STATUS_ATLAS_TEMP  <> :OLD.STATUS_ATLAS_TEMP)
       OR (:NEW.STATUS_ATLAS_TEMP IS NULL AND :OLD.STATUS_ATLAS_TEMP IS NOT NULL)
       OR (:NEW.STATUS_ATLAS_TEMP IS NOT NULL AND :OLD.STATUS_ATLAS_TEMP IS NULL))
     THEN change_composition:=change_composition+power(2,19);
   END IF;

   IF ((:NEW.EXCLUDE_PREDT  <> :OLD.EXCLUDE_PREDT)
       OR (:NEW.EXCLUDE_PREDT IS NULL AND :OLD.EXCLUDE_PREDT IS NOT NULL)
       OR (:NEW.EXCLUDE_PREDT IS NOT NULL AND :OLD.EXCLUDE_PREDT IS NULL))
     THEN change_composition:=change_composition+power(2,20);
   END IF;

   IF ((:NEW.COMMENTS_PREDT  <> :OLD.COMMENTS_PREDT)
       OR (:NEW.COMMENTS_PREDT IS NULL AND :OLD.COMMENTS_PREDT IS NOT NULL)
       OR (:NEW.COMMENTS_PREDT IS NOT NULL AND :OLD.COMMENTS_PREDT IS NULL))
     THEN change_composition:=change_composition+power(2,21);
   END IF;

    IF ((:NEW.INSPIRE  <> :OLD.INSPIRE)
       OR (:NEW.INSPIRE IS NULL AND :OLD.INSPIRE IS NOT NULL)
       OR (:NEW.INSPIRE IS NOT NULL AND :OLD.INSPIRE IS NULL))
     THEN change_composition:=change_composition+power(2,22);
   END IF;

   IF ((:NEW.STATUS_ATLAS  <> :OLD.STATUS_ATLAS)
       OR (:NEW.STATUS_ATLAS IS NULL AND :OLD.STATUS_ATLAS IS NOT NULL)
       OR (:NEW.STATUS_ATLAS IS NOT NULL AND :OLD.STATUS_ATLAS IS NULL))
     THEN change_composition:=change_composition+power(2,23);
   END IF;

  IF ((:NEW.SSA  <> :OLD.SSA)
       OR (:NEW.SSA IS NULL AND :OLD.SSA IS NOT NULL)
       OR (:NEW.SSA IS NOT NULL AND :OLD.SSA IS NULL))
     THEN change_composition:=change_composition+power(2,24);
   END IF;

   IF ((:NEW.QUAL_FLAG  <> :OLD.QUAL_FLAG)
       OR (:NEW.QUAL_FLAG IS NULL AND :OLD.QUAL_FLAG IS NOT NULL)
       OR (:NEW.QUAL_FLAG IS NOT NULL AND :OLD.QUAL_FLAG IS NULL))
     THEN change_composition:=change_composition+power(2,25);
   END IF;

   IF ((:NEW.RPDTC  <> :OLD.RPDTC)
       OR (:NEW.RPDTC IS NULL AND :OLD.RPDTC IS NOT NULL)
       OR (:NEW.RPDTC IS NOT NULL AND :OLD.RPDTC IS NULL))
     THEN change_composition:=change_composition+power(2,26);
   END IF;

  IF ((:NEW.TDAQ_AUTHOR  <> :OLD.TDAQ_AUTHOR)
       OR (:NEW.TDAQ_AUTHOR IS NULL AND :OLD.TDAQ_AUTHOR IS NOT NULL)
       OR (:NEW.TDAQ_AUTHOR IS NOT NULL AND :OLD.TDAQ_AUTHOR IS NULL))
     THEN change_composition:=change_composition+power(2,27);
  END IF;

  IF ((:NEW.QUAL_INTEGRATION  <> :OLD.QUAL_INTEGRATION)
       OR (:NEW.QUAL_INTEGRATION IS NULL AND :OLD.QUAL_INTEGRATION IS NOT NULL)
       OR (:NEW.QUAL_INTEGRATION IS NOT NULL AND :OLD.QUAL_INTEGRATION IS NULL))
     THEN change_composition:=change_composition+power(2,28);
  END IF;

  IF ((:NEW.LOCAL_SUPERVISOR  <> :OLD.LOCAL_SUPERVISOR)
       OR (:NEW.LOCAL_SUPERVISOR IS NULL AND :OLD.LOCAL_SUPERVISOR IS NOT NULL)
       OR (:NEW.LOCAL_SUPERVISOR IS NOT NULL AND :OLD.LOCAL_SUPERVISOR IS NULL))
     THEN change_composition:=change_composition+power(2,29);
  END IF;

  IF ((:NEW.TECH_SUPERVISOR  <> :OLD.TECH_SUPERVISOR)
       OR (:NEW.TECH_SUPERVISOR IS NULL AND :OLD.TECH_SUPERVISOR IS NOT NULL)
       OR (:NEW.TECH_SUPERVISOR IS NOT NULL AND :OLD.TECH_SUPERVISOR IS NULL))
     THEN change_composition:=change_composition+power(2,30);
  END IF;

  IF ((:NEW.CHECKPOINT_DATE  <> :OLD.CHECKPOINT_DATE)
       OR (:NEW.CHECKPOINT_DATE IS NULL AND :OLD.CHECKPOINT_DATE IS NOT NULL)
       OR (:NEW.CHECKPOINT_DATE IS NOT NULL AND :OLD.CHECKPOINT_DATE IS NULL))
     THEN change_composition:=change_composition+power(2,31);
  END IF;

  IF ((:NEW.CHECKPOINT_COMMENTS  <> :OLD.CHECKPOINT_COMMENTS)
       OR (:NEW.CHECKPOINT_COMMENTS IS NULL AND :OLD.CHECKPOINT_COMMENTS IS NOT NULL)
       OR (:NEW.CHECKPOINT_COMMENTS IS NOT NULL AND :OLD.CHECKPOINT_COMMENTS IS NULL))
     THEN change_composition:=change_composition+power(2,32);
  END IF;

  IF ((:NEW.FINAL_REPORT_DATE  <> :OLD.FINAL_REPORT_DATE)
       OR (:NEW.FINAL_REPORT_DATE IS NULL AND :OLD.FINAL_REPORT_DATE IS NOT NULL)
       OR (:NEW.FINAL_REPORT_DATE IS NOT NULL AND :OLD.FINAL_REPORT_DATE IS NULL))
     THEN change_composition:=change_composition+power(2,33);
  END IF;

  IF ((:NEW.FINAL_REPORT_COMMENTS  <> :OLD.FINAL_REPORT_COMMENTS)
       OR (:NEW.FINAL_REPORT_COMMENTS IS NULL AND :OLD.FINAL_REPORT_COMMENTS IS NOT NULL)
       OR (:NEW.FINAL_REPORT_COMMENTS IS NOT NULL AND :OLD.FINAL_REPORT_COMMENTS IS NULL))
     THEN change_composition:=change_composition+power(2,34);
  END IF;

    if :new.id is null then --deleted
        delete_date:=SYSDATE;
        INSERT INTO memb_hist_new
        VALUES
        (delete_date, delete_date, change_composition,
        :OLD.ID, :OLD.LAST_NAME, :OLD.FIRST_NAME, :OLD.INITIALS, :OLD.CERN_ID, :OLD.CERN_CCID, :OLD.QUAL_DATE,
        :OLD.QUAL_STRING, :OLD.PRE_CREDIT, :OLD.COMMENTS, :OLD.LAST_NAME_LTX, :OLD.FIRST_NAME_LTX, :OLD.PHOTO,
        :OLD.HR_IGNORE, :OLD.QUAL_END, :OLD.QUAL_START, :OLD.QUAL_STATE, :OLD.QUAL_RESP, :OLD.PROPOSED_QUAL_START,
        :OLD.MODIFIED_BY, :OLD.UPDATE_EMAIL, :OLD.STATUS_ATLAS_TEMP, :OLD.EXCLUDE_PREDT, :OLD.COMMENTS_PREDT,
        :OLD.INSPIRE, :OLD.STATUS_ATLAS, :OLD.SSA, :OLD.QUAL_FLAG, :OLD.RPDTC, :OLD.TDAQ_AUTHOR,
        :OLD.QUAL_INTEGRATION, :OLD.LOCAL_SUPERVISOR, :OLD.TECH_SUPERVISOR, :OLD.CHECKPOINT_DATE,
        :OLD.CHECKPOINT_COMMENTS, :OLD.FINAL_REPORT_DATE, :OLD.FINAL_REPORT_COMMENTS
        );
    end if;
    if :new.id is not null and change_composition!=0 then
        UPDATE memb_hist_new SET up_to = SYSDATE WHERE up_to IS NULL AND id = :old.id;
        INSERT INTO memb_hist_new
        VALUES
        (SYSDATE, NULL, change_composition,
        :NEW.ID, :NEW.LAST_NAME, :NEW.FIRST_NAME, :NEW.INITIALS, :NEW.CERN_ID, :NEW.CERN_CCID, :NEW.QUAL_DATE,
        :NEW.QUAL_STRING, :NEW.PRE_CREDIT, :NEW.COMMENTS, :NEW.LAST_NAME_LTX, :NEW.FIRST_NAME_LTX, :NEW.PHOTO,
        :NEW.HR_IGNORE, :NEW.QUAL_END, :NEW.QUAL_START, :NEW.QUAL_STATE, :NEW.QUAL_RESP, :NEW.PROPOSED_QUAL_START,
        :NEW.MODIFIED_BY, :NEW.UPDATE_EMAIL, :NEW.STATUS_ATLAS_TEMP, :NEW.EXCLUDE_PREDT, :NEW.COMMENTS_PREDT,
        :NEW.INSPIRE, :NEW.STATUS_ATLAS, :NEW.SSA, :NEW.QUAL_FLAG, :NEW.RPDTC, :NEW.TDAQ_AUTHOR,
        :NEW.QUAL_INTEGRATION, :NEW.LOCAL_SUPERVISOR, :NEW.TECH_SUPERVISOR, :NEW.CHECKPOINT_DATE,
        :NEW.CHECKPOINT_COMMENTS, :NEW.FINAL_REPORT_DATE, :NEW.FINAL_REPORT_COMMENTS
        );
    end if;
END;
/

create trigger ATLAS_AUTHDB.ARC_MEMB_DELETIONS
    after delete
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
BEGIN
    INSERT INTO  memb_arch_deleted (
    	ID,
	LAST_NAME,
	FIRST_NAME,
	INITIALS,
	CERN_ID,
	CERN_CCID,
	QUAL_DATE,
	QUAL_STRING,
	PRE_CREDIT,
	COMMENTS,
	LAST_NAME_LTX,
	FIRST_NAME_LTX,
	PHOTO,
	HR_IGNORE,
	QUAL_END ,
  EXCLUDE_PREDT,
  COMMENTS_PREDT,
	DELETED_ON )
    VALUES (
 	:old.ID,
	:old.LAST_NAME,
	:old.FIRST_NAME,
	:old.INITIALS,
	:old.CERN_ID,
	:old.CERN_CCID,
	:old.QUAL_DATE,
	:old.QUAL_STRING,
	:old.PRE_CREDIT,
	:old.COMMENTS,
	:old.LAST_NAME_LTX,
	:old.FIRST_NAME_LTX,
	:old.PHOTO,
	:old.HR_IGNORE,
	:old.QUAL_END,
  :old.EXCLUDE_PREDT,
  :old.COMMENTS_PREDT,
    	sysdate );
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_MEMBER
    after insert or update or delete
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CERN_CCID), 'null') <> COALESCE(TO_CHAR(:NEW.CERN_CCID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CERN_CCID',
          TO_CHAR(:OLD.CERN_CCID),
          TO_CHAR(:NEW.CERN_CCID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STATUS_ID), 'null') <> COALESCE(TO_CHAR(:NEW.STATUS_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'STATUS_ID',
          TO_CHAR(:OLD.STATUS_ID),
          TO_CHAR(:NEW.STATUS_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INITIALS), 'null') <> COALESCE(TO_CHAR(:NEW.INITIALS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INITIALS',
          TO_CHAR(:OLD.INITIALS),
          TO_CHAR(:NEW.INITIALS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FIRST_NAME_LATEX), 'null') <> COALESCE(TO_CHAR(:NEW.FIRST_NAME_LATEX), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FIRST_NAME_LATEX',
          TO_CHAR(:OLD.FIRST_NAME_LATEX),
          TO_CHAR(:NEW.FIRST_NAME_LATEX)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LAST_NAME_LATEX), 'null') <> COALESCE(TO_CHAR(:NEW.LAST_NAME_LATEX), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LAST_NAME_LATEX',
          TO_CHAR(:OLD.LAST_NAME_LATEX),
          TO_CHAR(:NEW.LAST_NAME_LATEX)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSPIRE), 'null') <> COALESCE(TO_CHAR(:NEW.INSPIRE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSPIRE',
          TO_CHAR(:OLD.INSPIRE),
          TO_CHAR(:NEW.INSPIRE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ORCID), 'null') <> COALESCE(TO_CHAR(:NEW.ORCID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ORCID',
          TO_CHAR(:OLD.ORCID),
          TO_CHAR(:NEW.ORCID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSPIRE_HEPNAME), 'null') <> COALESCE(TO_CHAR(:NEW.INSPIRE_HEPNAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSPIRE_HEPNAME',
          TO_CHAR(:OLD.INSPIRE_HEPNAME),
          TO_CHAR(:NEW.INSPIRE_HEPNAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
          ID,
          CERN_CCID,
          STATUS_ID,
          INITIALS,
          COMMENTS,
          FIRST_NAME_LATEX,
          LAST_NAME_LATEX,
          INSPIRE,
          ORCID,
          INSPIRE_HEPNAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CERN_CCID,
          :NEW.STATUS_ID,
          :NEW.INITIALS,
          :NEW.COMMENTS,
          :NEW.FIRST_NAME_LATEX,
          :NEW.LAST_NAME_LATEX,
          :NEW.INSPIRE,
          :NEW.ORCID,
          :NEW.INSPIRE_HEPNAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
      ID,
      CERN_CCID,
      STATUS_ID,
      INITIALS,
      COMMENTS,
      FIRST_NAME_LATEX,
      LAST_NAME_LATEX,
      INSPIRE,
      ORCID,
      INSPIRE_HEPNAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CERN_CCID,
      :OLD.STATUS_ID,
      :OLD.INITIALS,
      :OLD.COMMENTS,
      :OLD.FIRST_NAME_LATEX,
      :OLD.LAST_NAME_LATEX,
      :OLD.INSPIRE,
      :OLD.ORCID,
      :OLD.INSPIRE_HEPNAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_MEMBER (
      ID,
      CERN_CCID,
      STATUS_ID,
      INITIALS,
      COMMENTS,
      FIRST_NAME_LATEX,
      LAST_NAME_LATEX,
      INSPIRE,
      ORCID,
      INSPIRE_HEPNAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CERN_CCID,
      :NEW.STATUS_ID,
      :NEW.INITIALS,
      :NEW.COMMENTS,
      :NEW.FIRST_NAME_LATEX,
      :NEW.LAST_NAME_LATEX,
      :NEW.INSPIRE,
      :NEW.ORCID,
      :NEW.INSPIRE_HEPNAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.MEMB_QUAL_END_SET
    before update
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
declare
begin
      -- here we verify the conditions that are gonna overwrite SSA 
      -- and therefore recalculate the Remaining Pre-Data Credits
      if(    :new.QUAL_END is null  -- if QUAL_END is cleared, so should SSA.
          or :new.QUAL_END is not null and :new.QUAL_END != to_date('31/12/2049','dd/mm/yyyy') and (
                                            :new.SSA is null or 
                                            :new.SSA < :new.QUAL_END 
                                            )
      ) then
          -- Set SSA equal to QUAL_END
          :new.SSA := :new.QUAL_END;
      end if;
      if( :new.SSA is not null and :new.PRE_CREDIT is not null ) then
          :new.RPDTC := :new.PRE_CREDIT - ((add_months(:new.QUAL_END,12)-to_date('01/01/2012','dd/mm/yyyy'))/2)/30;
          -- 0 <= RPDTC <= PDTC
          if( :new.RPDTC < 0 ) then
              :new.RPDTC := 0;
          end if;
          if( :new.RPDTC > :new.PRE_CREDIT ) then
              :new.RPDTC := :new.PRE_CREDIT;
          end if;
      end if;
end;
/

create trigger ATLAS_AUTHDB.MEMB_HIST_DELETE
    before delete
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
DECLARE
BEGIN
 INSERT INTO MEMB_HIST (ID, LAST_NAME, FIRST_NAME, INITIALS, CERN_ID, CERN_CCID, QUAL_DATE, QUAL_STRING, PRE_CREDIT, COMMENTS,
      LAST_NAME_LTX, FIRST_NAME_LTX, PHOTO, HR_IGNORE, QUAL_END, QUAL_START, QUAL_STATE, QUAL_RESP, PROPOSED_QUAL_START, MODIFIED_ON,
      MODIFIED_BY, UPDATE_EMAIL, STATUS_ATLAS, EXCLUDE_PREDT, COMMENTS_PREDT, INSPIRE, QUAL_FLAG, RPDTC, SSA) VALUES (:old.ID, :old.LAST_NAME, :old.FIRST_NAME,
  :old.INITIALS, :old.CERN_ID, :old.CERN_CCID, :old.QUAL_DATE, :old.QUAL_STRING,
  :old.PRE_CREDIT, :old.COMMENTS, :old.LAST_NAME_LTX, :old.FIRST_NAME_LTX, :old.PHOTO,
  :old.HR_IGNORE,:old.QUAL_END,:old.QUAL_START, :old.QUAL_STATE,:old.QUAL_RESP,:old.PROPOSED_QUAL_START, sysdate,:old.MODIFIED_BY,
  :old.UPDATE_EMAIL, :old.STATUS_ATLAS, :old.EXCLUDE_PREDT, :old.COMMENTS_PREDT, :old.INSPIRE, :old.QUAL_FLAG, :old.RPDTC, :old.SSA);
END;
/

create trigger ATLAS_AUTHDB.MEMB_HIST_UPDATE
    before update
    on ATLAS_AUTHDB.ME_MEMBER
    for each row
DECLARE

	LAST_NAME_AUX       VARCHAR2(255) 	DEFAULT NULL;
	FIRST_NAME_AUX      VARCHAR2(255) 	DEFAULT NULL;
	INITIALS_AUX        VARCHAR2(10) 	DEFAULT NULL;
	CERN_ID_AUX         NUMBER(8) 		DEFAULT NULL;
	CERN_CCID_AUX       NUMBER(8) 		DEFAULT NULL;
	QUAL_DATE_AUX       DATE	 		DEFAULT NULL;
	QUAL_STRING_AUX     VARCHAR2(255) 	DEFAULT NULL;
	PRE_CREDIT_AUX      NUMBER(6) 		DEFAULT NULL;
	COMMENTS_AUX        VARCHAR2(255) 	DEFAULT NULL;
	LAST_NAME_LTX_AUX   VARCHAR2(255) 	DEFAULT NULL;
	FIRST_NAME_LTX_AUX  VARCHAR2(255) 	DEFAULT NULL;
	PHOTO_AUX           NUMBER(1) 		DEFAULT NULL;
	HR_IGNORE_AUX       NUMBER(1) 		DEFAULT NULL;
	QUAL_END_AUX        DATE 			DEFAULT NULL;
	QUAL_START_AUX      DATE			DEFAULT NULL;
	QUAL_STATE_AUX   	NUMBER(1)		DEFAULT NULL;
	QUAL_RESP_AUX   	NUMBER(6)		DEFAULT NULL;
	PROPOSED_QUAL_START_AUX   	varchar2(32)	DEFAULT NULL;
  UPDATE_EMAIL_AUX           NUMBER(1) 		DEFAULT NULL;
  STATUS_ATLAS_AUX           VARCHAR2(8 CHAR) 		DEFAULT NULL;
  EXCLUDE_PREDT_AUX NUMBER(1) DEFAULT NULL;
  COMMENTS_PREDT_AUX VARCHAR2(255) DEFAULT NULL;
  INSPIRE_AUX VARCHAR2(25 CHAR) DEFAULT NULL;
  QUAL_FLAG_AUX   NUMBER(1) DEFAULT NULL;
  RPDTC_AUX       NUMBER(6) DEFAULT NULL;
  SSA_AUX         DATE      DEFAULT NULL;
	update_trigger		boolean;

BEGIN

	update_trigger:=false;

   IF ((:NEW.LAST_NAME  <> :OLD.LAST_NAME)
       OR (:NEW.LAST_NAME IS NULL AND :OLD.LAST_NAME IS NOT NULL)
       OR (:NEW.LAST_NAME IS NOT NULL AND :OLD.LAST_NAME IS NULL))
     THEN LAST_NAME_AUX := :OLD.LAST_NAME;
        update_trigger:=true;
   END IF;

   IF ((:NEW.FIRST_NAME  <> :OLD.FIRST_NAME)
       OR (:NEW.FIRST_NAME IS NULL AND :OLD.FIRST_NAME IS NOT NULL)
       OR (:NEW.FIRST_NAME IS NOT NULL AND :OLD.FIRST_NAME IS NULL))
     THEN FIRST_NAME_AUX := :OLD.FIRST_NAME;
     update_trigger:=true;
   END IF;

   IF ((:NEW.INITIALS  <> :OLD.INITIALS)
       OR (:NEW.INITIALS IS NULL AND :OLD.INITIALS IS NOT NULL)
       OR (:NEW.INITIALS IS NOT NULL AND :OLD.INITIALS IS NULL))
     THEN INITIALS_AUX := :OLD.INITIALS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CERN_ID  <> :OLD.CERN_ID)
       OR (:NEW.CERN_ID IS NULL AND :OLD.CERN_ID IS NOT NULL)
       OR (:NEW.CERN_ID IS NOT NULL AND :OLD.CERN_ID IS NULL))
     THEN CERN_ID_AUX := :OLD.CERN_ID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.CERN_CCID  <> :OLD.CERN_CCID)
       OR (:NEW.CERN_CCID IS NULL AND :OLD.CERN_CCID IS NOT NULL)
       OR (:NEW.CERN_CCID IS NOT NULL AND :OLD.CERN_CCID IS NULL))
     THEN CERN_CCID_AUX := :OLD.CERN_CCID;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_DATE  <> :OLD.QUAL_DATE)
       OR (:NEW.QUAL_DATE IS NULL AND :OLD.QUAL_DATE IS NOT NULL)
       OR (:NEW.QUAL_DATE IS NOT NULL AND :OLD.QUAL_DATE IS NULL))
     THEN QUAL_DATE_AUX := :OLD.QUAL_DATE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_STRING  <> :OLD.QUAL_STRING)
       OR (:NEW.QUAL_STRING IS NULL AND :OLD.QUAL_STRING IS NOT NULL)
       OR (:NEW.QUAL_STRING IS NOT NULL AND :OLD.QUAL_STRING IS NULL))
     THEN
        QUAL_STRING_AUX := :OLD.QUAL_STRING;
        update_trigger:=true;
   END IF;

   IF ((:NEW.PRE_CREDIT  <> :OLD.PRE_CREDIT)
       OR (:NEW.PRE_CREDIT IS NULL AND :OLD.PRE_CREDIT IS NOT NULL)
       OR (:NEW.PRE_CREDIT IS NOT NULL AND :OLD.PRE_CREDIT IS NULL))
     THEN PRE_CREDIT_AUX := :OLD.PRE_CREDIT;
     update_trigger:=true;
   END IF;

   IF ((:NEW.COMMENTS  <> :OLD.COMMENTS)
       OR (:NEW.COMMENTS IS NULL AND :OLD.COMMENTS IS NOT NULL)
       OR (:NEW.COMMENTS IS NOT NULL AND :OLD.COMMENTS IS NULL))
     THEN COMMENTS_AUX := :OLD.COMMENTS;
     update_trigger:=true;
   END IF;

   IF ((:NEW.LAST_NAME_LTX  <> :OLD.LAST_NAME_LTX)
       OR (:NEW.LAST_NAME_LTX IS NULL AND :OLD.LAST_NAME_LTX IS NOT NULL)
       OR (:NEW.LAST_NAME_LTX IS NOT NULL AND :OLD.LAST_NAME_LTX IS NULL))
     THEN LAST_NAME_LTX_AUX := :OLD.LAST_NAME_LTX;
     update_trigger:=true;
   END IF;

   IF ((:NEW.FIRST_NAME_LTX  <> :OLD.FIRST_NAME_LTX)
       OR (:NEW.FIRST_NAME_LTX IS NULL AND :OLD.FIRST_NAME_LTX IS NOT NULL)
       OR (:NEW.FIRST_NAME_LTX IS NOT NULL AND :OLD.FIRST_NAME_LTX IS NULL))
     THEN FIRST_NAME_LTX_AUX := :OLD.FIRST_NAME_LTX;
     update_trigger:=true;
   END IF;

   IF ((:NEW.PHOTO  <> :OLD.PHOTO)
       OR (:NEW.PHOTO IS NULL AND :OLD.PHOTO IS NOT NULL)
       OR (:NEW.PHOTO IS NOT NULL AND :OLD.PHOTO IS NULL))
     THEN PHOTO_AUX := :OLD.PHOTO;
     update_trigger:=true;
   END IF;

   IF ((:NEW.HR_IGNORE  <> :OLD.HR_IGNORE)
       OR (:NEW.HR_IGNORE IS NULL AND :OLD.HR_IGNORE IS NOT NULL)
       OR (:NEW.HR_IGNORE IS NOT NULL AND :OLD.HR_IGNORE IS NULL))
     THEN HR_IGNORE_AUX := :OLD.HR_IGNORE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_END  <> :OLD.QUAL_END)
       OR (:NEW.QUAL_END IS NULL AND :OLD.QUAL_END IS NOT NULL)
       OR (:NEW.QUAL_END IS NOT NULL AND :OLD.QUAL_END IS NULL))
     THEN QUAL_END_AUX := :OLD.QUAL_END;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_START  <> :OLD.QUAL_START)
       OR (:NEW.QUAL_START IS NULL AND :OLD.QUAL_START IS NOT NULL)
       OR (:NEW.QUAL_START IS NOT NULL AND :OLD.QUAL_START IS NULL))
     THEN QUAL_START_AUX := :OLD.QUAL_START;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_STATE  <> :OLD.QUAL_STATE)
       OR (:NEW.QUAL_STATE IS NULL AND :OLD.QUAL_STATE IS NOT NULL)
       OR (:NEW.QUAL_STATE IS NOT NULL AND :OLD.QUAL_STATE IS NULL))
     THEN QUAL_STATE_AUX := :OLD.QUAL_STATE;
     update_trigger:=true;
   END IF;

   IF ((:NEW.QUAL_RESP  <> :OLD.QUAL_RESP)
       OR (:NEW.QUAL_RESP IS NULL AND :OLD.QUAL_RESP IS NOT NULL)
       OR (:NEW.QUAL_RESP IS NOT NULL AND :OLD.QUAL_RESP IS NULL))
     THEN QUAL_RESP_AUX := :OLD.QUAL_RESP;
     update_trigger:=true;
   END IF;

   IF ((:NEW.PROPOSED_QUAL_START  <> :OLD.PROPOSED_QUAL_START)
       OR (:NEW.PROPOSED_QUAL_START IS NULL AND :OLD.PROPOSED_QUAL_START IS NOT NULL)
       OR (:NEW.PROPOSED_QUAL_START IS NOT NULL AND :OLD.PROPOSED_QUAL_START IS NULL))
     THEN PROPOSED_QUAL_START_AUX := :OLD.PROPOSED_QUAL_START;
     update_trigger:=true;
   END IF;
   
   IF ((:NEW.UPDATE_EMAIL  <> :OLD.UPDATE_EMAIL)
       OR (:NEW.UPDATE_EMAIL IS NULL AND :OLD.UPDATE_EMAIL IS NOT NULL)
       OR (:NEW.UPDATE_EMAIL IS NOT NULL AND :OLD.UPDATE_EMAIL IS NULL))
     THEN UPDATE_EMAIL_AUX := :OLD.UPDATE_EMAIL;
        update_trigger:=true;
   END IF;
   
   IF ((:NEW.STATUS_ATLAS  <> :OLD.STATUS_ATLAS)
       OR (:NEW.STATUS_ATLAS IS NULL AND :OLD.STATUS_ATLAS IS NOT NULL)
       OR (:NEW.STATUS_ATLAS IS NOT NULL AND :OLD.STATUS_ATLAS IS NULL))
     THEN STATUS_ATLAS_AUX := :OLD.STATUS_ATLAS;
        update_trigger:=true;
   END IF;
   
   IF ((:NEW.EXCLUDE_PREDT  <> :OLD.EXCLUDE_PREDT)
       OR (:NEW.EXCLUDE_PREDT IS NULL AND :OLD.EXCLUDE_PREDT IS NOT NULL)
       OR (:NEW.EXCLUDE_PREDT IS NOT NULL AND :OLD.EXCLUDE_PREDT IS NULL))
     THEN EXCLUDE_PREDT_AUX := :OLD.EXCLUDE_PREDT;
     update_trigger:=true;
   END IF;
   
   IF ((:NEW.COMMENTS_PREDT  <> :OLD.COMMENTS_PREDT)
       OR (:NEW.COMMENTS_PREDT IS NULL AND :OLD.COMMENTS_PREDT IS NOT NULL)
       OR (:NEW.COMMENTS_PREDT IS NOT NULL AND :OLD.COMMENTS_PREDT IS NULL))
     THEN COMMENTS_PREDT_AUX := :OLD.COMMENTS_PREDT;
     update_trigger:=true;
   END IF;
   
    IF ((:NEW.INSPIRE  <> :OLD.INSPIRE)
       OR (:NEW.INSPIRE IS NULL AND :OLD.INSPIRE IS NOT NULL)
       OR (:NEW.INSPIRE IS NOT NULL AND :OLD.INSPIRE IS NULL))
     THEN INSPIRE_AUX := :OLD.INSPIRE;
     update_trigger:=true;
   END IF;
   
   IF ((:NEW.QUAL_FLAG  <> :OLD.QUAL_FLAG)
       OR (:NEW.QUAL_FLAG IS NULL AND :OLD.QUAL_FLAG IS NOT NULL)
       OR (:NEW.QUAL_FLAG IS NOT NULL AND :OLD.QUAL_FLAG IS NULL))
     THEN QUAL_FLAG_AUX := :OLD.QUAL_FLAG;
     update_trigger:=true;
   END IF;
   
   IF ((:NEW.RPDTC  <> :OLD.RPDTC)
       OR (:NEW.RPDTC IS NULL AND :OLD.RPDTC IS NOT NULL)
       OR (:NEW.RPDTC IS NOT NULL AND :OLD.RPDTC IS NULL))
     THEN RPDTC_AUX := :OLD.RPDTC;
     update_trigger:=true;
   END IF;
   
  IF ((:NEW.SSA  <> :OLD.SSA)
       OR (:NEW.SSA IS NULL AND :OLD.SSA IS NOT NULL)
       OR (:NEW.SSA IS NOT NULL AND :OLD.SSA IS NULL))
     THEN SSA_AUX := :OLD.SSA;
     update_trigger:=true;
  END IF;
  
  if (update_trigger)
	  then
		  INSERT INTO MEMB_HIST (ID, LAST_NAME, FIRST_NAME, INITIALS, CERN_ID, CERN_CCID, QUAL_DATE, QUAL_STRING, PRE_CREDIT, COMMENTS,
      LAST_NAME_LTX, FIRST_NAME_LTX, PHOTO, HR_IGNORE, QUAL_END, QUAL_START, QUAL_STATE, QUAL_RESP, PROPOSED_QUAL_START, MODIFIED_ON,
      MODIFIED_BY, UPDATE_EMAIL, STATUS_ATLAS, EXCLUDE_PREDT, COMMENTS_PREDT, INSPIRE, QUAL_FLAG, RPDTC, SSA) VALUES (:NEW.ID, LAST_NAME_AUX, FIRST_NAME_AUX,
		  INITIALS_AUX, CERN_ID_AUX, CERN_CCID_AUX, QUAL_DATE_AUX, QUAL_STRING_AUX, PRE_CREDIT_AUX,
		  COMMENTS_AUX, LAST_NAME_LTX_AUX, FIRST_NAME_LTX_AUX,PHOTO_AUX,HR_IGNORE_AUX,QUAL_END_AUX,
		  QUAL_START_AUX, QUAL_STATE_AUX,QUAL_RESP_AUX,PROPOSED_QUAL_START_AUX, sysdate,:NEW.MODIFIED_BY, UPDATE_EMAIL_AUX, 
      STATUS_ATLAS_AUX, EXCLUDE_PREDT_AUX,COMMENTS_PREDT_AUX, INSPIRE_AUX, QUAL_FLAG_AUX, RPDTC_AUX, SSA_AUX);
      :new.modified_on := sysdate;
  end if;

END;
/

create trigger ATLAS_AUTHDB.CONFNOTE_REVIEW_CYCLE
    before insert
    on ATLAS_AUTHDB.CONFNOTE_REVIEW_CYCLE
    for each row
begin
select seq_CONFNOTE_review_cycle.nextval into :new.id from dual;
end;
/

create trigger ATLAS_AUTHDB.REVIEW_CYCLE
    before insert
    on ATLAS_AUTHDB.REVIEW_CYCLE
    for each row
begin
select seq_review_cycle.nextval into :new.id from dual;
end;
/

create trigger ATLAS_AUTHDB.ID_ACTIVITIES
    before insert
    on ATLAS_AUTHDB.ACTIVITIES
    for each row
BEGIN IF
    ( :new.ID IS NULL
    ) THEN
  SELECT seq_activities.nextval INTO :new.ID FROM dual;
END IF;
END;
/

create trigger ATLAS_AUTHDB.ACTIVITIES_INSERT_OR_UPDATE
    before insert or update
    on ATLAS_AUTHDB.ACTIVITIES
    for each row
DECLARE
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    IF can_add_new_activity_id(:new.new_activity_id) = 0
    THEN raise_application_error(-20999,'new_activity_id not allowed. It will duplicate members in MEMB view.');
    END IF;
END;
/

create trigger ATLAS_AUTHDB.TALK_ABSTRACT
    before insert
    on ATLAS_AUTHDB.TALK_ABSTRACT
    for each row
BEGIN
  select SEQ_TALK_ABSTRACT.nextval into :new.id from dual;
END;
/

create trigger ATLAS_AUTHDB.THIST_FENCE_JSON
    after insert or update or delete
    on ATLAS_AUTHDB.FENCE_JSON
    for each row
DECLARE Now TIMESTAMP;
BEGIN
    SELECT CURRENT_TIMESTAMP INTO Now FROM Dual;

    UPDATE HIST_FENCE_JSON
    SET row_end_date = Now
    WHERE row_end_date IS NULL
    AND PATH = :OLD.PATH
    ;

    IF (:NEW.PATH IS NOT NULL) THEN
        INSERT INTO HIST_FENCE_JSON
        (PATH, CONTENT, RESPONSIBLE_CCID, COMMENTS, ROW_START_DATE, ROW_END_DATE)
        VALUES
        (:NEW.PATH, :NEW.CONTENT, :NEW.RESPONSIBLE_CCID, :NEW.COMMENTS, Now, NULL)
        ;
   END IF;
END;
/

create trigger ATLAS_AUTHDB.ANALYSIS_SYS_INSERT_UPDATE
    before insert or update
    on ATLAS_AUTHDB.ANALYSIS_SYS
    for each row
BEGIN
    -- When inserting, sets the creation date and the autoincrement ID.
  IF INSERTING and (:new.id IS NULL) THEN
    select seq_analysis_sys.nextval into :new.id from dual;
    IF :new.creation_date is null THEN
        :new.creation_date := sysdate;
    END IF;
  END IF;

  -- Whenever LEAD_GROUP is not changing, do NOT change REF_CODE.
  IF UPDATING
  and (:new.ref_code IS NULL
  or :new.ref_code = '')
  --  and (:new.lead_group = :old.lead_group
   -- or (:new.lead_group IS NULL and :old.lead_group IS NULL)
  --  or (:new.lead_group = '' and :old.lead_group = '')
  --  or (:new.lead_group IS NULL and :old.lead_group = '')
  --  or (:new.lead_group = '' and :old.lead_group IS NULL))
  THEN
    :new.ref_code := :old.ref_code;
    :new.sequence_number := :old.sequence_number;
  END IF;
END;

/

create trigger ATLAS_AUTHDB.HTR_ME_DELETED
    after insert or update or delete
    on ATLAS_AUTHDB.ME_DELETED
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_DELETED (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_DELETED (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_DELETED (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_DELETED (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_DELETED (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_STATUS
    after insert or update or delete
    on ATLAS_AUTHDB.ME_STATUS
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_STATUS (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_STATUS (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_STATUS (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_HIDE_ONLEAVE_DATE
    after insert or update or delete
    on ATLAS_AUTHDB.ME_HIDE_ONLEAVE_DATE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
      IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HIDE_ONLEAVE_DATE (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_HR_IGNORE
    after insert or update or delete
    on ATLAS_AUTHDB.ME_HR_IGNORE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE (
          ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_HR_IGNORE (
      ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_PRE_DATA_TAKEN
    after insert or update or delete
    on ATLAS_AUTHDB.ME_PRE_DATA_TAKEN
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CREDIT), 'null') <> COALESCE(TO_CHAR(:NEW.CREDIT), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CREDIT',
          TO_CHAR(:OLD.CREDIT),
          TO_CHAR(:NEW.CREDIT)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.REMAINING_CREDIT), 'null') <> COALESCE(TO_CHAR(:NEW.REMAINING_CREDIT), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'REMAINING_CREDIT',
          TO_CHAR(:OLD.REMAINING_CREDIT),
          TO_CHAR(:NEW.REMAINING_CREDIT)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
          ID,
          MEMBER_ID,
          CREDIT,
          REMAINING_CREDIT,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.CREDIT,
          :NEW.REMAINING_CREDIT,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
      ID,
      MEMBER_ID,
      CREDIT,
      REMAINING_CREDIT,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.CREDIT,
      :OLD.REMAINING_CREDIT,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_PRE_DATA_TAKEN (
      ID,
      MEMBER_ID,
      CREDIT,
      REMAINING_CREDIT,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.CREDIT,
      :NEW.REMAINING_CREDIT,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_QUALIFICATION
    after insert or update or delete
    on ATLAS_AUTHDB.QU_QUALIFICATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.DESCRIPTION), 'null') <> COALESCE(TO_CHAR(:NEW.DESCRIPTION), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'DESCRIPTION',
          TO_CHAR(:OLD.DESCRIPTION),
          TO_CHAR(:NEW.DESCRIPTION)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_DATE',
          TO_CHAR(:OLD.QUALIFICATION_DATE),
          TO_CHAR(:NEW.QUALIFICATION_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PROPOSED_START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.PROPOSED_START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PROPOSED_START_DATE',
          TO_CHAR(:OLD.PROPOSED_START_DATE),
          TO_CHAR(:NEW.PROPOSED_START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'START_DATE',
          TO_CHAR(:OLD.START_DATE),
          TO_CHAR(:NEW.START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.END_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.END_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'END_DATE',
          TO_CHAR(:OLD.END_DATE),
          TO_CHAR(:NEW.END_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STATE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.STATE_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'STATE_ID',
          TO_CHAR(:OLD.STATE_ID),
          TO_CHAR(:NEW.STATE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FTE), 'null') <> COALESCE(TO_CHAR(:NEW.FTE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FTE',
          TO_CHAR(:OLD.FTE),
          TO_CHAR(:NEW.FTE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.INSTITUTE_INTEGRATION), 'null') <> COALESCE(TO_CHAR(:NEW.INSTITUTE_INTEGRATION), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'INSTITUTE_INTEGRATION',
          TO_CHAR(:OLD.INSTITUTE_INTEGRATION),
          TO_CHAR(:NEW.INSTITUTE_INTEGRATION)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
          ID,
          DESCRIPTION,
          QUALIFICATION_DATE,
          PROPOSED_START_DATE,
          START_DATE,
          END_DATE,
          STATE_ID,
          FTE,
          INSTITUTE_INTEGRATION,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.DESCRIPTION,
          :NEW.QUALIFICATION_DATE,
          :NEW.PROPOSED_START_DATE,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.STATE_ID,
          :NEW.FTE,
          :NEW.INSTITUTE_INTEGRATION,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
      ID,
      DESCRIPTION,
      QUALIFICATION_DATE,
      PROPOSED_START_DATE,
      START_DATE,
      END_DATE,
      STATE_ID,
      FTE,
      INSTITUTE_INTEGRATION,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.DESCRIPTION,
      :OLD.QUALIFICATION_DATE,
      :OLD.PROPOSED_START_DATE,
      :OLD.START_DATE,
      :OLD.END_DATE,
      :OLD.STATE_ID,
      :OLD.FTE,
      :OLD.INSTITUTE_INTEGRATION,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUALIFICATION (
      ID,
      DESCRIPTION,
      QUALIFICATION_DATE,
      PROPOSED_START_DATE,
      START_DATE,
      END_DATE,
      STATE_ID,
      FTE,
      INSTITUTE_INTEGRATION,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.DESCRIPTION,
      :NEW.QUALIFICATION_DATE,
      :NEW.PROPOSED_START_DATE,
      :NEW.START_DATE,
      :NEW.END_DATE,
      :NEW.STATE_ID,
      :NEW.FTE,
      :NEW.INSTITUTE_INTEGRATION,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_ME_QUALIFICATION
    after insert or update or delete
    on ATLAS_AUTHDB.ME_QUALIFICATION
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
          ID,
          MEMBER_ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
          ID,
          MEMBER_ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
          ID,
          MEMBER_ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
          ID,
          MEMBER_ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
      ID,
      MEMBER_ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.QUALIFICATION_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.ME_QUALIFICATION (
      ID,
      MEMBER_ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.QUALIFICATION_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_SIGNING_ONLY
    after insert or update or delete
    on ATLAS_AUTHDB.QU_SIGNING_ONLY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
        IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
          ID,
          QUALIFICATION_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
          ID,
          QUALIFICATION_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.START_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.START_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
          ID,
          QUALIFICATION_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'START_DATE',
          TO_CHAR(:OLD.START_DATE),
          TO_CHAR(:NEW.START_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.END_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.END_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
          ID,
          QUALIFICATION_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'END_DATE',
          TO_CHAR(:OLD.END_DATE),
          TO_CHAR(:NEW.END_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
          ID,
          QUALIFICATION_ID,
          START_DATE,
          END_DATE,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.START_DATE,
          :NEW.END_DATE,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
      ID,
      QUALIFICATION_ID,
      START_DATE,
      END_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.START_DATE,
      :OLD.END_DATE,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_SIGNING_ONLY (
      ID,
      QUALIFICATION_ID,
      START_DATE,
      END_DATE,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.START_DATE,
      :NEW.END_DATE,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_CHECKPOINT
    after insert or update or delete
    on ATLAS_AUTHDB.QU_CHECKPOINT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
          ID,
          QUALIFICATION_ID,
          CHECKPOINT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.CHECKPOINT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
          ID,
          QUALIFICATION_ID,
          CHECKPOINT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.CHECKPOINT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CHECKPOINT_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.CHECKPOINT_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
          ID,
          QUALIFICATION_ID,
          CHECKPOINT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.CHECKPOINT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'CHECKPOINT_DATE',
          TO_CHAR(:OLD.CHECKPOINT_DATE),
          TO_CHAR(:NEW.CHECKPOINT_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
          ID,
          QUALIFICATION_ID,
          CHECKPOINT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.CHECKPOINT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
          ID,
          QUALIFICATION_ID,
          CHECKPOINT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.CHECKPOINT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
      ID,
      QUALIFICATION_ID,
      CHECKPOINT_DATE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.CHECKPOINT_DATE,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_CHECKPOINT (
      ID,
      QUALIFICATION_ID,
      CHECKPOINT_DATE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.CHECKPOINT_DATE,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_PROJECT
    after insert or update or delete
    on ATLAS_AUTHDB.QU_PROJECT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
          ID,
          QUALIFICATION_ID,
          PROJECT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.PROJECT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
          ID,
          QUALIFICATION_ID,
          PROJECT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.PROJECT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PROJECT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.PROJECT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
          ID,
          QUALIFICATION_ID,
          PROJECT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.PROJECT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'PROJECT_ID',
          TO_CHAR(:OLD.PROJECT_ID),
          TO_CHAR(:NEW.PROJECT_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
          ID,
          QUALIFICATION_ID,
          PROJECT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.PROJECT_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
      ID,
      QUALIFICATION_ID,
      PROJECT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.PROJECT_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_PROJECT (
      ID,
      QUALIFICATION_ID,
      PROJECT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.PROJECT_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_FINAL_REPORT
    after insert or update or delete
    on ATLAS_AUTHDB.QU_FINAL_REPORT
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
          ID,
          QUALIFICATION_ID,
          FINAL_REPORT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FINAL_REPORT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
          ID,
          QUALIFICATION_ID,
          FINAL_REPORT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FINAL_REPORT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FINAL_REPORT_DATE), 'null') <> COALESCE(TO_CHAR(:NEW.FINAL_REPORT_DATE), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
          ID,
          QUALIFICATION_ID,
          FINAL_REPORT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FINAL_REPORT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FINAL_REPORT_DATE',
          TO_CHAR(:OLD.FINAL_REPORT_DATE),
          TO_CHAR(:NEW.FINAL_REPORT_DATE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'null') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
          ID,
          QUALIFICATION_ID,
          FINAL_REPORT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FINAL_REPORT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
          ID,
          QUALIFICATION_ID,
          FINAL_REPORT_DATE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FINAL_REPORT_DATE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
      ID,
      QUALIFICATION_ID,
      FINAL_REPORT_DATE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.FINAL_REPORT_DATE,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FINAL_REPORT (
      ID,
      QUALIFICATION_ID,
      FINAL_REPORT_DATE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.FINAL_REPORT_DATE,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_EXCLUDE_PRE_DATA_TAKEN
    after insert or update or delete
    on ATLAS_AUTHDB.QU_EXCLUDE_PRE_DATA_TAKEN
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN (
      ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_EXCLUDE_PRE_DATA_TAKEN (
      ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_TECHNICAL_SUPERVISOR
    after insert or update or delete
    on ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          TECHNICAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TECHNICAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          TECHNICAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TECHNICAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TECHNICAL_SUPERVISOR_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TECHNICAL_SUPERVISOR_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          TECHNICAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TECHNICAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'TECHNICAL_SUPERVISOR_ID',
          TO_CHAR(:OLD.TECHNICAL_SUPERVISOR_ID),
          TO_CHAR(:NEW.TECHNICAL_SUPERVISOR_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          TECHNICAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.TECHNICAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
      ID,
      QUALIFICATION_ID,
      TECHNICAL_SUPERVISOR_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.TECHNICAL_SUPERVISOR_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_TECHNICAL_SUPERVISOR (
      ID,
      QUALIFICATION_ID,
      TECHNICAL_SUPERVISOR_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.TECHNICAL_SUPERVISOR_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_LOCAL_SUPERVISOR
    after insert or update or delete
    on ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          LOCAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.LOCAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          LOCAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.LOCAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LOCAL_SUPERVISOR_ID), 'null') <> COALESCE(TO_CHAR(:NEW.LOCAL_SUPERVISOR_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          LOCAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.LOCAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'LOCAL_SUPERVISOR_ID',
          TO_CHAR(:OLD.LOCAL_SUPERVISOR_ID),
          TO_CHAR(:NEW.LOCAL_SUPERVISOR_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
          ID,
          QUALIFICATION_ID,
          LOCAL_SUPERVISOR_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.LOCAL_SUPERVISOR_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
      ID,
      QUALIFICATION_ID,
      LOCAL_SUPERVISOR_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.LOCAL_SUPERVISOR_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_LOCAL_SUPERVISOR (
      ID,
      QUALIFICATION_ID,
      LOCAL_SUPERVISOR_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.LOCAL_SUPERVISOR_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_QUAL_FLAG
    after insert or update or delete
    on ATLAS_AUTHDB.QU_QUAL_FLAG
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG (
          ID,
          QUALIFICATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG (
      ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_QUAL_FLAG (
      ID,
      QUALIFICATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_ACTIVITY
    after insert or update or delete
    on ATLAS_AUTHDB.QU_ACTIVITY
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
          ID,
          QUALIFICATION_ID,
          ACTIVITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.ACTIVITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
          ID,
          QUALIFICATION_ID,
          ACTIVITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.ACTIVITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ACTIVITY_ID), 'null') <> COALESCE(TO_CHAR(:NEW.ACTIVITY_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
          ID,
          QUALIFICATION_ID,
          ACTIVITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.ACTIVITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ACTIVITY_ID',
          TO_CHAR(:OLD.ACTIVITY_ID),
          TO_CHAR(:NEW.ACTIVITY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
          ID,
          QUALIFICATION_ID,
          ACTIVITY_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.ACTIVITY_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
      ID,
      QUALIFICATION_ID,
      ACTIVITY_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.ACTIVITY_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_ACTIVITY (
      ID,
      QUALIFICATION_ID,
      ACTIVITY_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.ACTIVITY_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

create trigger ATLAS_AUTHDB.HTR_QU_FUTURE
    after insert or update or delete
    on ATLAS_AUTHDB.QU_FUTURE
    for each row
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'null') <> COALESCE(TO_CHAR(:NEW.ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
          ID,
          QUALIFICATION_ID,
          FUTURE_STATUS_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FUTURE_STATUS_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.QUALIFICATION_ID), 'null') <> COALESCE(TO_CHAR(:NEW.QUALIFICATION_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
          ID,
          QUALIFICATION_ID,
          FUTURE_STATUS_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FUTURE_STATUS_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'QUALIFICATION_ID',
          TO_CHAR(:OLD.QUALIFICATION_ID),
          TO_CHAR(:NEW.QUALIFICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FUTURE_STATUS_ID), 'null') <> COALESCE(TO_CHAR(:NEW.FUTURE_STATUS_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
          ID,
          QUALIFICATION_ID,
          FUTURE_STATUS_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FUTURE_STATUS_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'FUTURE_STATUS_ID',
          TO_CHAR(:OLD.FUTURE_STATUS_ID),
          TO_CHAR(:NEW.FUTURE_STATUS_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_ID), 'null') <> COALESCE(TO_CHAR(:NEW.AGENT_ID), 'null') THEN
      INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
          ID,
          QUALIFICATION_ID,
          FUTURE_STATUS_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.QUALIFICATION_ID,
          :NEW.FUTURE_STATUS_ID,
          :NEW.AGENT_ID,
          OPERATION_DATE,
          'U',
          'AGENT_ID',
          TO_CHAR(:OLD.AGENT_ID),
          TO_CHAR(:NEW.AGENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
      ID,
      QUALIFICATION_ID,
      FUTURE_STATUS_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.QUALIFICATION_ID,
      :OLD.FUTURE_STATUS_ID,
      :OLD.AGENT_ID,
      OPERATION_DATE,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_AUTHDB_HISTORY.QU_FUTURE (
      ID,
      QUALIFICATION_ID,
      FUTURE_STATUS_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.QUALIFICATION_ID,
      :NEW.FUTURE_STATUS_ID,
      :NEW.AGENT_ID,
      OPERATION_DATE,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;
/

-- Disable triggers that are also currently disabled in production
ALTER TRIGGER PHASE_1_DELETE DISABLE
/

ALTER TRIGGER CONF_DELETE DISABLE
/

ALTER TRIGGER MEMB_HIST_UPDATE DISABLE
/

ALTER TRIGGER MEMB_HIST_DELETE DISABLE
/

ALTER TRIGGER MEMB_QUAL_END_SET DISABLE
/

ALTER TRIGGER ARC_MEMB_DELETIONS DISABLE
/

ALTER TRIGGER MEMB_FULL_REQUALIFICATION DISABLE
/

ALTER TRIGGER TRACK_MEMB DISABLE
/

ALTER TRIGGER T_TRIM_INITIALS DISABLE
/

ALTER TRIGGER T_INST_HIST DISABLE
/

ALTER TRIGGER T_INST_AFFILIATION DISABLE
/

ALTER TRIGGER INST_HIST_UPDATE DISABLE
/

ALTER TRIGGER INST_HIST_DELETE DISABLE
/

--rollback DECLARE
--rollback   V_TRIGGER_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR TRIGGER_REC IN (SELECT TRIGGER_NAME FROM ALL_TRIGGERS WHERE OWNER = 'ATLAS_AUTHDB') LOOP
--rollback     V_TRIGGER_NAME := TRIGGER_REC.TRIGGER_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP TRIGGER ATLAS_AUTHDB.' || V_TRIGGER_NAME;
--rollback   END LOOP;
--rollback END;
--rollback /