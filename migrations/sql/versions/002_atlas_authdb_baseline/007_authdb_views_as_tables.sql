--liquibase formatted sql
--changeset Gabriel:7 endDelimiter:/ rollbackEndDelimiter:/

--- FOUNDATION VIEWS ---
CREATE TABLE ATLAS_AUTHDB.CERNDB_PARTICIPANT_ATLAS
(
    PERSON_ID           NUMBER(9)    NOT NULL,
    CERN_ID             NUMBER(7),
    LAST_NAME           VARCHAR2(40),
    FIRST_NAME          VARCHAR2(18),
    QUALITY             VARCHAR2(5),
    STATUS              VARCHAR2(6),
    AT_CERN             VARCHAR2(1),
    PERCENTAGE_PRESENCE NUMBER(3),
    GENERIC_EMAIL       VARCHAR2(60),
    PREFERRED_EMAIL     VARCHAR2(60),
    SEX                 VARCHAR2(1),
    TELEPHONE1          VARCHAR2(5),
    TELEPHONE2          VARCHAR2(5),
    PORTABLE_PHONE      VARCHAR2(5),
    CONTRACT_END_DATE   DATE,
    EXPERIMENT          VARCHAR2(60) NOT NULL,
    BUILDING            VARCHAR2(10),
    FLOOR               VARCHAR2(2),
    ROOM                VARCHAR2(4),
    ORG_DEPARTMENT      VARCHAR2(3),
    ORG_GROUP           VARCHAR2(3),
    ORG_SECTION         VARCHAR2(3),
    ORGANIC_UNIT        VARCHAR2(11),
    ORCID               VARCHAR2(150)
)
/

COMMENT ON TABLE ATLAS_AUTHDB.CERNDB_PARTICIPANT_ATLAS IS 'SNAPSHOT OF FOUNDATION VIEW FOUNDATION_PUB.EXP_PARTICIPANT_ATLAS@CERNDB1'
/

CREATE TABLE ATLAS_AUTHDB.CERNDB_PARTICIPATION_ATLAS
(
    PERSON_ID       NUMBER(10)   NOT NULL,
    EXPERIMENT_NAME VARCHAR2(60) NOT NULL,
    INSTITUTE_CODE  VARCHAR2(6),
    IS_AUTHOR       VARCHAR2(150),
    IS_PRIMARY      VARCHAR2(150),
    START_DATE      DATE,
    END_DATE        DATE
)
/

COMMENT ON TABLE ATLAS_AUTHDB.CERNDB_PARTICIPATION_ATLAS IS 'SNAPSHOT OF FOUNDATION VIEW HRPUB.EXP_PARTICIPATION_ATLAS@CERNDB1'
/

CREATE TABLE ATLAS_AUTHDB.ATLAS_MEMBERS_CERNDB
(
    PERSON_ID           NUMBER(9)    NOT NULL,
    CERN_ID             NUMBER(7),
    LAST_NAME           VARCHAR2(40),
    FIRST_NAME          VARCHAR2(18),
    QUALITY             VARCHAR2(5),
    STATUS              VARCHAR2(6),
    AT_CERN             VARCHAR2(1),
    PERCENTAGE_PRESENCE NUMBER(3),
    GENERIC_EMAIL       VARCHAR2(60),
    PREFERRED_EMAIL     VARCHAR2(60),
    SEX                 VARCHAR2(1),
    TELEPHONE1          VARCHAR2(5),
    TELEPHONE2          VARCHAR2(5),
    PORTABLE_PHONE      VARCHAR2(5),
    CONTRACT_END_DATE   DATE,
    EXPERIMENT          VARCHAR2(60) NOT NULL,
    BUILDING            VARCHAR2(10),
    FLOOR               VARCHAR2(2),
    ROOM                VARCHAR2(4),
    DEPART              VARCHAR2(3),
    GRP                 VARCHAR2(3),
    SECT                VARCHAR2(3),
    ORGANIC_UNIT        VARCHAR2(11),
    ORCID               VARCHAR2(150)
)
/

CREATE TABLE ATLAS_AUTHDB.INSTITUTES_CERNDB (
    CODE              VARCHAR2(50),
    ENGLISH_NAME      VARCHAR2(255),
    ORIGINAL_NAME     VARCHAR2(255),
    NAME              VARCHAR2(255),
    PARENT_INSTITUTE  VARCHAR2(50),
    ADDRESS_ID        NUMBER,
    INSTITUTE_TYPE    VARCHAR2(50),
    COUNTRY_CODE      VARCHAR2(10),
    PLACE             VARCHAR2(100),
    URL               VARCHAR2(255),
    STATUS            VARCHAR2(50),
    LIBRARY           VARCHAR2(50)
)
/

CREATE TABLE ATLAS_AUTHDB.CERNDB_INSTITUTES_TABLE (
    CODE               VARCHAR2(255),
    ENGLISH_NAME       VARCHAR2(255),
    ORIGINAL_NAME      VARCHAR2(255),
    NAME               VARCHAR2(255),
    PARENT_INSTITUTE   VARCHAR2(255),
    ADDRESS_ID         NUMBER,
    INSTITUTE_TYPE     VARCHAR2(255),
    COUNTRY_CODE       VARCHAR2(20),
    PLACE              VARCHAR2(255),
    URL                VARCHAR2(255),
    STATUS             VARCHAR2(255),
    LIBRARY            VARCHAR2(255)
)
/

CREATE TABLE ATLAS_AUTHDB.INSTITUTE_ADDRESSES_CERNDB (
    ADDRESS_ID    NUMBER,
    STREET        VARCHAR2(255),
    POSTBOX       VARCHAR2(50),
    COUNTRY_CODE  VARCHAR2(10),
    POSTAL_CODE   VARCHAR2(20),
    PLACE         VARCHAR2(100),
    SUFFIX        VARCHAR2(50)
)
/

CREATE TABLE ATLAS_AUTHDB.ATLAS_TEAM_LEADERS_CERNDB (
    PERSON_ID               NUMBER,
    EGROUP_ID               NUMBER,
    PRIORITY                NUMBER,
    ROLE_TYPE               VARCHAR2(50),
    ENTITY1_TYPE            VARCHAR2(50),
    ENTITY1_ID              VARCHAR2(50),
    ENTITY2_TYPE            VARCHAR2(50),
    ENTITY2_ID              VARCHAR2(50),
    INHERITED_ROLE          VARCHAR2(50),
    INHERITANCE1_LEVEL      NUMBER,
    INHERITANCE2_LEVEL      NUMBER,
    ROLE_ASSIGNMENT_ID      NUMBER,
    EGROUP_ROLE_ASSIGNMENT_ID NUMBER
)
/
---
---
---

-- PHOTO VIEWS
CREATE TABLE ATLAS_AUTHDB.GLANCEDB_ALLOW_PHOTO
(
    ID                  NUMBER    NOT NULL,
    APPLICATION_ID      VARCHAR2(20),
    PERSON_ID           NUMBER(9),
    CONSENT             VARCHAR2(18),
    MODIFIED_ON         DATE,
    AGENT_PERSON_ID     NUMBER
)
/
---
---
---

-- OTP VIEWS
CREATE TABLE ATLAS_AUTHDB.OTP_PUB_COMMITMENTS (
    ID                            NUMBER,
    YEAR                          DATE,
    VALUE                         NUMBER,
    RES_REQUIREMENT_ID            NUMBER,
    INSTITUTE_ID                  NUMBER,
    DESCRIPTION                   VARCHAR2(255)
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_PUB_SUM_ALLOCATION_V (
    TASK_ID                       NUMBER,
    RES_REQUIREMENT_ID            NUMBER,
    ALLOCATED_PERSON_ID           NUMBER,
    ALLOCATED_PERSON_INSTITUTE_ID NUMBER,
    ALLOCATED_FTE                 NUMBER,
    ALLOCATED_HOURS               NUMBER,
    DT                            DATE,
    DATE_ID                       NUMBER,
    REQ_TYPE_CODE                 VARCHAR2(50),
    RECOGNITION_CODE              VARCHAR2(50),
    HOUR_FROM                     NUMBER,
    HOUR_TO                       NUMBER,
    WBS_NODE_ID                   NUMBER,
    SYSTEM_NODE_ID                NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_PUB_TASK (
    ID               NUMBER,
    SHORT_TITLE      VARCHAR2(255),
    CATEGORY_CODE    VARCHAR2(50),
    SYSTEM_ID        NUMBER,
    WBS_ID           NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_PUB_PERSON (
    ID              NUMBER,
    AIS_PERSON_ID   NUMBER,
    LAST_NAME       VARCHAR2(255),
    FIRST_NAME      VARCHAR2(255),
    EMAIL           VARCHAR2(255),
    PICTURE_URL     VARCHAR2(255),
    USERNAME        VARCHAR2(50),
    PORTABLE_PHONE  VARCHAR2(20),
    LNAME           VARCHAR2(255),
    FNAME           VARCHAR2(255)
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_TMP_PPT_MAO_TASK (
    ID            NUMBER,
    SHORT_TITLE   VARCHAR2(255),
    CATEGORY_CODE VARCHAR2(50),
    SYSTEM_ID     NUMBER,
    WBS_ID        NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_TMP_PPT_MAO_SYSTEM_NODE (
    ID        NUMBER,
    TITLE     VARCHAR2(255),
    PARENT_ID NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_RES_REQUIREMENT (
    ID                NUMBER,
    TASK_ID           NUMBER,
    TITLE             VARCHAR2(255),
    SHIFT_PATTERN_ID  NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_PUB_SYSTEM_NODE (
    ID        NUMBER,
    TITLE     VARCHAR2(255),
    PARENT_ID NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.OTP_PUB_WBS_NODE (
    ID        NUMBER,
    TITLE     VARCHAR2(255),
    PARENT_ID NUMBER
)
/

-- THIS VIEW IS CREATED BECAUSE IT USES TWO PROCEDURES FROM THE OTP SCHEMA
CREATE TABLE ATLAS_AUTHDB.OTP_INSTITUTES_TABLE (
    ID          NUMBER,
    ONAME       VARCHAR2(255),
    CLASS       VARCHAR2(50),
    ACTIVITY    VARCHAR2(4000),
    SYSTEM      VARCHAR2(255),
    TASK        VARCHAR2(255),
    YEAR        NUMBER,
    ALLOC       NUMBER(5, 2)
)
/
---
---
---

-- BELOW ARE MVS THAT DEPEND ON EXTERNAL SOURCES OR ARE NOT UP TO DATE
CREATE TABLE ATLAS_AUTHDB.INSTITUTE_COMMITMENT_MV
(
    ID                NUMBER(10)    NOT NULL,
    DESCRIPTION       VARCHAR2(4000),
    YEAR              NUMBER(10)    NOT NULL,
    INSTITUTE_ID      NUMBER(10),
    TASK_ID           NUMBER(10)    NOT NULL,
    SHORT_TITLE       VARCHAR2(255),
    CATEGORY_CODE     VARCHAR2(255) NOT NULL,
    REQ_FTE           NUMBER,
    ALLO_FTE          NUMBER,
    COMM_FTE          NUMBER,
    REQUIREMENT_ID    NUMBER(10),
    REQUIREMENT_TITLE VARCHAR2(255)
)
/

CREATE TABLE ATLAS_AUTHDB.INSTITUTE_COMMITMENT_MV_OLD
(
    ID            NUMBER(10)    NOT NULL,
    DESCRIPTION   VARCHAR2(4000),
    YEAR          NUMBER(10)    NOT NULL,
    INSTITUTE_ID  NUMBER(10),
    TASK_ID       NUMBER(10)    NOT NULL,
    SHORT_TITLE   VARCHAR2(255),
    CATEGORY_CODE VARCHAR2(255) NOT NULL,
    REQ_FTE       NUMBER,
    ALLO_FTE      NUMBER
)
/

CREATE TABLE ATLAS_AUTHDB.MEMB_SCAB_NOMINATION
(
    MEMB_ID               NUMBER(8) NOT NULL,
    COUNT                 NUMBER,
    SCORE                 NUMBER,
    IDS                   VARCHAR2(4000),
    COMMENTS              VARCHAR2(4000),
    ATLAS_FRACTIONS       VARCHAR2(4000),
    INTERESTS             VARCHAR2(4000),
    PRIORITIES            VARCHAR2(4000),
    SCAB_PRIORITIES       VARCHAR2(4000),
    DOWNGRADES            VARCHAR2(4000),
    UPGRADES              VARCHAR2(4000),
    DISCARDED             VARCHAR2(4000),
    SUBMITTERS_IDS        VARCHAR2(4000),
    SUBMITTERS_CCIDS      VARCHAR2(4000),
    SUBMITTERS_LASTNAMES  VARCHAR2(4000),
    SUBMITTERS_FIRSTNAMES VARCHAR2(4000),
    SUBMISSION_DATES      VARCHAR2(4000),
    RESPONSIBLES_IDS      VARCHAR2(4000),
    RESPONSIBLES_ROLES    VARCHAR2(4000),
    MODIFIERS_IDS         VARCHAR2(4000),
    MODIFIERS_CCIDS       VARCHAR2(4000),
    MODIFIERS_LASTNAMES   VARCHAR2(4000),
    MODIFIERS_FIRSTNAMES  VARCHAR2(4000),
    MODIFICATION_DATES    VARCHAR2(4000),
    SUB_IDS               VARCHAR2(4000),
    SUB_NAMES             VARCHAR2(4000),
    SUB_PARENTS_IDS       VARCHAR2(4000),
    SUB_PARENTS_NAMES     VARCHAR2(4000),
    ADV_IDS               VARCHAR2(4000),
    ADV_DESCRIPTIONS      VARCHAR2(4000 CHAR)
)
/

CREATE TABLE ATLAS_AUTHDB.MEMB_TDAQ_NOMINATION
(
    MEMB_ID               NUMBER(8) NOT NULL,
    COUNT                 NUMBER,
    SCORE                 NUMBER,
    IDS                   VARCHAR2(4000),
    COMMENTS              VARCHAR2(4000),
    ATLAS_FRACTIONS       VARCHAR2(4000),
    INTERESTS             VARCHAR2(4000),
    PRIORITIES            VARCHAR2(4000),
    MAX_PRIORITY          NUMBER,
    SCAB_PRIORITIES       VARCHAR2(4000),
    DOWNGRADES            VARCHAR2(4000),
    SUBMITTERS_IDS        VARCHAR2(4000),
    SUBMITTERS_CCIDS      VARCHAR2(4000),
    SUBMITTERS_LASTNAMES  VARCHAR2(4000),
    SUBMITTERS_FIRSTNAMES VARCHAR2(4000),
    SUBMISSION_DATES      VARCHAR2(4000),
    RESPONSIBLES_IDS      VARCHAR2(4000),
    RESPONSIBLES_ROLES    VARCHAR2(4000),
    MODIFIERS_IDS         VARCHAR2(4000),
    MODIFIERS_CCIDS       VARCHAR2(4000),
    MODIFIERS_LASTNAMES   VARCHAR2(4000),
    MODIFIERS_FIRSTNAMES  VARCHAR2(4000),
    MODIFICATION_DATES    VARCHAR2(4000),
    SUB_IDS               VARCHAR2(4000),
    SUB_NAMES             VARCHAR2(4000),
    SUB_PARENTS_IDS       VARCHAR2(4000),
    SUB_PARENTS_NAMES     VARCHAR2(4000),
    ADV_IDS               VARCHAR2(4000),
    ADV_DESCRIPTIONS      VARCHAR2(4000 CHAR)
)
/

--rollback DROP TABLE ATLAS_AUTHDB.CERNDB_PARTICIPANT_ATLAS 
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.CERNDB_PARTICIPATION_ATLAS
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.ATLAS_MEMBERS_CERNDB
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.GLANCEDB_ALLOW_PHOTO
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.INSTITUTES_CERNDB
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.CERNDB_INSTITUTES_TABLE
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.INSTITUTE_ADDRESSES_CERNDB
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.ATLAS_TEAM_LEADERS_CERNDB
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_SUM_ALLOCATION_V
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_TASK
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_PERSON
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_SYSTEM_NODE
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_TMP_PPT_MAO_TASK
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_TMP_PPT_MAO_SYSTEM_NODE
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_RES_REQUIREMENT
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_COMMITMENTS
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_PUB_WBS_NODE
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.OTP_INSTITUTES_TABLE
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.INSTITUTE_COMMITMENT_MV
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.INSTITUTE_COMMITMENT_MV_OLD
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.MEMB_SCAB_NOMINATION
--rollback /
--rollback DROP TABLE ATLAS_AUTHDB.MEMB_TDAQ_NOMINATION
--rollback /