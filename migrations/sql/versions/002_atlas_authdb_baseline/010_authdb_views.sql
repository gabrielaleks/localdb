--liquibase formatted sql
--changeset Gabriel:10 endDelimiter:/ rollbackEndDelimiter:/

create force view ATLAS_AUTHDB.OTP_ACTIVITIES
            (ALLOCATED_PERSON_ID, TASK_ID, YEAR, ALLOCATED_FTE, SHORT_TITLE, CATEGORY_CODE) as
SELECT ALLOCATED_PERSON_ID,
        TASK_ID,
        extract(YEAR FROM DT),
        SUM(ALLOCATED_FTE),
        SHORT_TITLE,
        CATEGORY_CODE
    FROM ATLAS_AUTHDB.otp_pub_sum_allocation_v,
        ATLAS_AUTHDB.OTP_PUB_TASK
    WHERE ATLAS_AUTHDB.otp_pub_sum_allocation_v.TASK_ID        = ATLAS_AUTHDB.OTP_PUB_TASK.ID
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.RECOGNITION_CODE = 'Duty'
    GROUP BY ALLOCATED_PERSON_ID,
        extract(YEAR FROM DT),
        CATEGORY_CODE,
        TASK_ID,
        SHORT_TITLE
    ORDER BY CATEGORY_CODE,
        SHORT_TITLE,
        extract(YEAR FROM DT)
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION (ALLOCATED_PERSON_ID, CONTRIBUTION, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
    CASE
      WHEN ROUND(MAX(otp_value), 2) >= 0.1
      THEN 'Yes'
      ELSE 'No'
    END,
    round(max(otp_value),2)
  FROM
    (SELECT ALLOCATED_PERSON_ID,
      SUM(
      CASE
        WHEN (CATEGORY_CODE = 'Class 1'
        OR CATEGORY_CODE    = 'Class 2')
        THEN ALLOCATED_FTE*1.5
        WHEN (CATEGORY_CODE = 'Class 3')
        THEN ALLOCATED_FTE
        WHEN (CATEGORY_CODE = 'Class 4')
        THEN ALLOCATED_FTE
        ELSE 0
      END ) otp_value
    FROM OTP_ACTIVITIES
    --WHERE YEAR >= 2012
    WHERE YEAR >= EXTRACT(YEAR FROM SYSDATE) - 3 AND YEAR <= EXTRACT(YEAR FROM SYSDATE)
    GROUP BY ALLOCATED_PERSON_ID
    )
  GROUP BY ALLOCATED_PERSON_ID, otp_value
/

create force view ATLAS_AUTHDB.OTP_PICTURES (ID, PICTURE_URL, PIC_ID) as
SELECT ID,
   TRIM(PICTURE_URL),
   SUBSTR( TRIM(PICTURE_URL), instr(TRIM(PICTURE_URL),'file_id=')+8 )
 FROM ATLAS_AUTHDB.otp_pub_person
/

create force view ATLAS_AUTHDB.ADDITIONAL_INST_STRAGG as
select memb_id, stragg(Inst_id) as inst_id, stragg(Inst_GHOST_id) as inst_ghost_id, stragg(START_DATE) as start_date, stragg(END_DATE) as end_date,
          stragg(START_DATE_EXT) as start_date_ext, stragg(END_DATE_EXT) as end_date_ext from additional_inst group by memb_id
/

create force view ATLAS_AUTHDB.PUBLICATIONS_CDS_ID as
select  type,
        case when type = 'final_cdsURL' then alias else null end as report,
        case  when (type like '%supportDocumentsURL%' or type like '%cdsURL') and href like '%/cdsweb.cern.ch/record/%' then substr(href, instr(href, 'record/')+7, 7)
              when type = 'final_arxivURL' then substr(href, length(href)-8, length(href)) 
              else href end as record_id,
        publication.id as publication_id
        
from    phase,
        phase_links,
        publication
where 
        phase_links.phase_id = phase.id and 
        publication.id = phase.publication_id and
        (((type like '%supportDocumentsURL%' or type like '%cdsURL') and href like '%/cdsweb.cern.ch/record/%') or type = 'final_arxivURL')
/

create force view ATLAS_AUTHDB.SC_TALK_MATERIAL as
SELECT    TALK.ID,
            TALK.CONF_ID,
            (SELECT 
                  stragg(TALK_MAT.LINK)
            FROM TALK_MAT
            WHERE TALK.ID = TALK_MAT.TALK_ID) AS TALK_MATERIAL
  FROM 
            TALK
  GROUP BY
            TALK.ID,
            TALK.CONF_ID
  ORDER BY TALK.ID DESC
/

create force view ATLAS_AUTHDB.APPROVED_APPOINTMENT as
SELECT a."ID",a."NAME",a."WARNING_DATE",a."SELECTION_BOARD",a."CB_VOTE",a."IB_VOTE",a."EB_MEMBER",a."CB_MEMBER",a."OT_VALUE",a."COMMENTS",a."ORDER_BY",a."AGENT",a."MANDATE"
FROM appointment a
LEFT JOIN (
  SELECT appointment_id, resolution, stamp
  FROM appointment_resolution ar
  WHERE stamp = (
      SELECT MAX(stamp) FROM appointment_resolution WHERE appointment_id = ar.appointment_id
  )
) latest_resolution ON a.id = latest_resolution.appointment_id
WHERE latest_resolution.resolution = 'APPROVED'
/

create force view ATLAS_AUTHDB.JSON_APPOINTMENT (ID, MEMB_ID, START_DATE, END_DATE, OBJ) as
SELECT ma.id
    , ma.memb_id
    , ma.start_date
    , ma.end_date
    , TO_CLOB(
        '{' ) || TO_CLOB(
            '"id":' ) || TO_CLOB( ma.id ) || TO_CLOB( ',' ) || TO_CLOB(
            '"appointment":{' ) || TO_CLOB(
                '"id":' )    || TO_CLOB( a.id )   || TO_CLOB( ',' ) || TO_CLOB(
                '"name":"' ) || TO_CLOB( a.name ) || TO_CLOB( '"' ) || TO_CLOB(
            '},' ) || TO_CLOB(
            '"start_date":"' ) || TO_CLOB( TO_CHAR( ma.start_date, 'yyyy-mm-dd' ) ) || TO_CLOB( '",' ) || TO_CLOB(
            '"end_date":"' )   || TO_CLOB( TO_CHAR( ma.end_date,   'yyyy-mm-dd' ) ) || TO_CLOB( '"' ) || TO_CLOB(
        '}' )
FROM memb_appointment ma
    JOIN appointment a ON a.id = ma.appointment_id
/

create force view ATLAS_AUTHDB.JSON_THESIS as
SELECT ac.id
    , ac.memb_id
    , DBMS_XMLGEN.CONVERT(EXTRACT(XMLTYPE('<?xml version="1.0"?><document>' || XMLAGG(XMLTYPE('<V>' || DBMS_XMLGEN.CONVERT(
        '{' ||
            '"id":' || ac.id || ',' ||
            '"title":"' || ac.thesis_title || '",' ||
            '"type":"' || ac.type || '",' ||
            '"link":"' || ac.link || '",' ||
            '"comments":"' || ac.comments || '",' ||
            '"rep_number":"' || ac.rep_number|| '"' ||
        '}'
    ) || '</V>')).getclobval() || '</document>'), '/document/V/text()').getclobval(),1) AS obj
FROM academic_record ac
GROUP BY ac.id
    , ac.memb_id
/

create force view ATLAS_AUTHDB.JSON_PAPER_EDBOARD (ID, MEMB_ID, OBJ) as
SELECT p.id
    , mp.memb_id,
        TO_CLOB( '{' ) ||
            TO_CLOB( '"publication":' ) ||
                TO_CLOB( '{' ) ||
                    TO_CLOB( '"id":' || p.id || ',' ) ||
                    TO_CLOB( '"ref_code":"' || p.ref_code || '",' ) ||
                    TO_CLOB( '"short_title":"' || p.short_title || '",' ) ||
                    TO_CLOB( '"full_title":"' || p.full_title || '"' ) ||
                TO_CLOB( '},' ) ||
            TO_CLOB( '"chair":' || CASE WHEN mp.memb_function = 'chair' THEN 'true' ELSE 'false' END || ',' ) ||
            TO_CLOB( '"contribution":"' || mp.memb_contribution || '"' ) ||
        TO_CLOB( '}' )
FROM publication p
    JOIN memb_publication mp ON mp.publication_id = p.id
WHERE mp.group_id = 'EDBOARD'
/

create force view ATLAS_AUTHDB.CJSON_APPOINTMENT (MEMB_ID, LIST) as
SELECT ma.memb_id, TO_CLOB(
  ma.id || ',"' ) || TO_CLOB(
  TO_CHAR( start_date, 'YYYY-MM-DD' ) || '","' ) || TO_CLOB(
  TO_CHAR( end_date,   'YYYY-MM-DD' ) || '",[' ) || TO_CLOB(
      a.id || ',"' ) || TO_CLOB( a.name ) || TO_CLOB( '"]' )
FROM memb_appointment ma
JOIN appointment a ON a.id = ma.appointment_id
/

create force view ATLAS_AUTHDB.EDBOARD as
(SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM memb_publication WHERE group_id = 'EDBOARD')
/

create force view ATLAS_AUTHDB.CONF_EDBOARD as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM confnote_memb_publication WHERE group_id = 'EDBOARD'
)
/

create force view ATLAS_AUTHDB.PUB_EDBOARD as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM pubnote_memb_publication WHERE group_id = 'EDBOARD'
)
/

create force view ATLAS_AUTHDB.EDITOR as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM memb_publication WHERE group_id = 'EDITOR'
)
/

create force view ATLAS_AUTHDB.CONF_EDITOR as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM confnote_memb_publication WHERE group_id = 'EDITOR'
)
/

create force view ATLAS_AUTHDB.PUB_EDITOR as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM pubnote_memb_publication WHERE group_id = 'EDITOR'
)
/

create force view ATLAS_AUTHDB.SCAB_NOMINATION_HIST as
SELECT "SINCE","UP_TO","WHAT_CHANGED","ID","MEMB_ID","COMMENTS","PRIORITY","ATLAS_FRACTION","INTEREST","SUBMITTED_BY","SUBMISSION_DATE","MODIFIED_BY","APP_RESP","IR_RESP","SCAB_PRIORITY","SYSTEMS_ID","PROF_ADVANCE_ID" FROM nominations_hist_new WHERE systems_id = 1
/

create force view ATLAS_AUTHDB.PC_SCAB_NOMINATION_HIST as
SELECT "SINCE","UP_TO","WHAT_CHANGED","ID","MEMB_ID","COMMENTS","PRIORITY","ATLAS_FRACTION","INTEREST","SUBMITTED_BY","SUBMISSION_DATE","MODIFIED_BY","APP_RESP","IR_RESP","SCAB_PRIORITY","SYSTEMS_ID","PROF_ADVANCE_ID" FROM scab_nomination_hist WHERE app_resp = 18
/

create force view ATLAS_AUTHDB.CONF_DUMP as
SELECT c.id id, c.short_name short_name, c.start_date start_date, cp.description priority, c.priority priority_id
FROM conf c
JOIN conf_priority cp ON c.priority = cp.id
/

create force view ATLAS_AUTHDB.TALK_WEIGHT_VIEW as
SELECT t.id talk_id, ttwv.weight_value type_weight, tcwv.weight_value category_weight, ttwv.weight_value*cpwv.weight_value*tcwv.weight_value*cswv.WEIGHT_VALUE talk_weight_value
    FROM talk t
    JOIN conf c ON c.id = t.conf_id
    JOIN talk_category_weight tcw ON t.category = tcw.TALK_CATEGORY_ID
    JOIN weight tcwv ON tcw.WEIGHT_ID = tcwv.ID
    AND (c.start_date BETWEEN tcwv.since AND tcwv.UP_TO OR c.start_date >= tcwv.SINCE AND tcwv.UP_TO IS NULL)
    JOIN talk_type_weight ttw ON t.type = ttw.TALK_TYPE_ID
    JOIN weight ttwv ON ttw.WEIGHT_ID = ttwv.ID
    AND (c.start_date BETWEEN ttwv.since AND ttwv.UP_TO OR c.start_date >= ttwv.SINCE AND ttwv.UP_TO IS NULL)
    JOIN CONF_PRIORITY_WEIGHT cpw ON c.PRIORITY = cpw.CONF_PRIORITY_ID
    JOIN weight cpwv ON cpw.WEIGHT_ID = cpwv.ID
    AND (c.start_date BETWEEN cpwv.since AND cpwv.UP_TO OR c.start_date >= cpwv.SINCE AND cpwv.UP_TO IS NULL)
    JOIN CONF_STATUS_WEIGHT csw ON c.STATUS_ID = csw.CONF_STATUS_ID
    JOIN weight cswv ON csw.WEIGHT_ID = cswv.ID
    AND (c.start_date BETWEEN cswv.since AND cswv.UP_TO OR c.start_date >= cswv.SINCE AND cswv.UP_TO IS NULL)
/

create force view ATLAS_AUTHDB.CONF_WEIGHT_VIEW as
SELECT c.id AS conf_id, cpwv.weight_value AS priority_weight, cswv.WEIGHT_VALUE
FROM conf c
JOIN CONF_PRIORITY_WEIGHT cpw ON c.PRIORITY = cpw.CONF_PRIORITY_ID
JOIN weight cpwv ON cpw.WEIGHT_ID = cpwv.ID
AND (c.start_date BETWEEN cpwv.since AND cpwv.UP_TO OR c.start_date >= cpwv.SINCE AND cpwv.UP_TO IS NULL)
JOIN CONF_STATUS_WEIGHT csw ON c.STATUS_ID = csw.CONF_STATUS_ID
JOIN weight cswv ON csw.WEIGHT_ID = cswv.ID
AND (c.start_date BETWEEN cswv.since AND cswv.UP_TO OR c.start_date >= cswv.SINCE AND cswv.UP_TO IS NULL)
/

create force view ATLAS_AUTHDB.SCABBER_TALK as
SELECT "ID","CONF_ID","TITLE","CATEGORY","ENTRY_DATE","PHYSKEY","SPEAKER_ID","INST_ID","EMAIL","STATUS","TYPE","COMMENTS","INDICO","CDS","CINCO_URL","SCOPE","TALK_DURATION","QUESTION_DURATION","DECLINATIONS","WEIGHT","TASK1_ID","TASK2_ID","PENALTY","REVIEWER_ID","TALK_ID","TYPE_WEIGHT","CATEGORY_WEIGHT","TALK_WEIGHT_VALUE" FROM talk t JOIN talk_weight_view ON talk_weight_view.talk_id = t.id
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."SCABBER_CONF" ("ID", "START_DATE", "END_DATE", "PRIORITY", "CONF_STATUS", "CONF_ID", "PRIORITY_WEIGHT", "STATUS_WEIGHT", "WEIGHT") AS
  (SELECT "ID",
  "START_DATE","END_DATE","PRIORITY","CONF_STATUS",
  "CONF_ID","PRIORITY_WEIGHT", "WEIGHT_VALUE", PRIORITY_WEIGHT*WEIGHT_VALUE FROM conf c JOIN conf_weight_view ON conf_weight_view.conf_id = c.id)
/

create force view ATLAS_AUTHDB.INST_ACTIVITIES_ALL as
SELECT a1.type, a.inst_id, 'activity' project_id FROM inst_activities_new a
JOIN inst_activities_list a1 ON a.activity_id = a1.id
UNION ALL
SELECT i1.name, ib.institute_id, 'system_ib' project_id  FROM in_system_ib ib
JOIN (SELECT * FROM activity WHERE id IN (SELECT id FROM system_ib)) i1 ON ib.system_ib_id = i1.id
UNION ALL
SELECT u1.type, up.inst_id, 'up_proj' project_id  FROM inst_upgrade_projects up
JOIN inst_upgrade_projects_list u1 ON up.upgrade_id = u1.id
/

create force view ATLAS_AUTHDB.INST_ACTIVITIES_ALL_LIST as
SELECT type FROM inst_activities_list
UNION
SELECT type FROM inst_upgrade_projects_list
UNION
SELECT name FROM (SELECT name from activity WHERE id IN (SELECT id FROM system_ib))
/

create force view ATLAS_AUTHDB.ANA_EDBOARD as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM ANALYSIS_SYS_MEMB_PUBLICATION WHERE group_id = 'EDBOARD'
)
/

create force view ATLAS_AUTHDB.ANA_EDITOR as
(
  SELECT "MEMB_ID","PUBLICATION_ID","GROUP_ID","MEMB_FUNCTION","MEMB_CONTRIBUTION" FROM ANALYSIS_SYS_MEMB_PUBLICATION WHERE group_id = 'EDITOR'
)
/

create force view ATLAS_AUTHDB.MEMBER_OTP as
SELECT task.id || '-' || EXTRACT(year FROM alloc.dt) AS id
        ,   alloc.allocated_person_id as member_id
        ,   task.wbs_id as WBS_id
        ,   EXTRACT(year FROM alloc.dt) AS year
        ,   ROUND(SUM(alloc.allocated_fte)*365, 2) AS num_of_shifts
        ,   task.short_title AS short_title
        ,   s.title AS system
        ,   task.category_code as category
        FROM ATLAS_AUTHDB.otp_pub_sum_allocation_v alloc
        JOIN ATLAS_AUTHDB.otp_tmp_ppt_mao_task task ON alloc.task_id = task.id
        JOIN ATLAS_AUTHDB.otp_tmp_ppt_mao_system_node s ON s.id = alloc.system_node_id
        WHERE alloc.recognition_code = 'Duty'
        GROUP BY alloc.allocated_person_id,
            task.id,
            EXTRACT(year FROM alloc.dt),
            task.wbs_id,
            task.short_title,
            s.title,
            task.category_code
/

create force view ATLAS_AUTHDB.SCABBER_ROUNDED as
SELECT memb_id -- <_frac> items are the ingredient divided by the nominee's score
,rank
,rank AS plot_flag
,round(round(score/5,1) * 5, 1) AS score -- rounds to x.0 or x.5 
,round(sum_prios,1) AS sum_prios
,round(round((sum_prios/score)/5,2)*5,2) AS sum_prios_frac  -- rounds to x.0 or x.05 
,round(round(otp_score/5,1)*5,1) AS otp_score -- rounds to x.0 or x.5
,round(round((otp_score/score)/5,2)*5,2) AS otp_score_frac  -- rounds to x.0 or x.05 
,round(upgrade_otp_score,1) AS upgrade_otp_score
,round(round((upgrade_otp_score/score)/5,2)*5,2) AS upgrade_otp_score_frac  -- rounds to x.0 or x.05 
,round(round(last_talk_weight/5,1) * 5, 1) AS last_talk_weight -- rounds to x.0 or x.5
,round(round((last_talk_weight/score)/5,2)*5,2) AS last_talk_weight_frac  -- rounds to x.0 or x.05 
,round(prof_ad_weight,1) AS prof_ad_weight
,round(round((prof_ad_weight/score)/5,2)*5,2) AS prof_ad_weight_frac  -- rounds to x.0 or x.05 
,round(otp_rate,1) AS otp_rate
,round(round(otp_value/5,1) * 5, 1) AS otp_value -- rounds to x.0 or x.5 
,round(round(otp_upgrade/5,1) * 5, 1) AS otp_upgrade -- rounds to x.0 or x.5 
,round(round(otp_total/5,1) * 5, 1) AS otp_total -- rounds to x.0 or x.5 
,conf_weight
,round(round(years_since_lt/5,1)*5,1) AS years_since_lt  -- rounds to x.0 or x.5 
,prof_ad_id
,sum_noms
,upgrade_noms
,calculation_date
FROM scabber
UNION
SELECT memb_id
, 0 -- rank = 0
, CASE
    WHEN ((otp_check is null and newly_qualified is null and upgrade_nom is null) and number_nomination is null) THEN 101 --!OTP & number of nom < 2
    WHEN ((otp_check is null and newly_qualified is null and upgrade_nom is null) and number_nomination is not null) THEN 102 --!OTP & number of nom >= 2
    WHEN ((otp_check is not null or newly_qualified is not null or upgrade_nom is not null) and number_nomination is null) THEN 103 --OTP & number of nom < 2
END
,round(round(score/5,1) * 5, 1) AS score -- rounds to x.0 or x.5
,round(sum_prios,1) AS sum_prios
,round(round((sum_prios/score)/5,2)*5,2) AS sum_prios_frac  -- rounds to x.0 or x.05 
,round(round(otp_score/5,1)*5,1) AS otp_score -- rounds to x.0 or x.5
,round(round((otp_score/score)/5,2)*5,2) AS otp_score_frac  -- rounds to x.0 or x.05 
,round(upgrade_otp_score,1) AS upgrade_otp_score
,round(round((upgrade_otp_score/score)/5,2)*5,2) AS upgrade_otp_score_frac  -- rounds to x.0 or x.05 
,round(round(last_talk_weight/5,1) * 5, 1) AS last_talk_weight -- rounds to x.0 or x.5
,round(round((last_talk_weight/score)/5,2)*5,2) AS last_talk_weight_frac  -- rounds to x.0 or x.05 
,round(prof_ad_weight,1) AS prof_ad_weight
,round(round((prof_ad_weight/score)/5,2)*5,2) AS prof_ad_weight_frac  -- rounds to x.0 or x.05 
,round(otp_rate,1) AS otp_rate
,round(round(otp_sum/5,1) * 5, 1) AS otp_value -- rounds to x.0 or x.5 
,round(round(otp_upgrade/5,1) * 5, 1) AS otp_upgrade -- rounds to x.0 or x.5 
,round(round(otp_total/5,1) * 5, 1) AS otp_total -- rounds to x.0 or x.5 
,conf_weight
,round(round(years_since_lt/5,1)*5,1) AS years_since_lt  -- rounds to x.0 or x.5 
,prof_ad_id
,sum_noms
,upgrade_noms
,calculation_date
FROM scabber_nominee_reason
/

create force view ATLAS_AUTHDB.V_ACTIVITY_LOOKUP as
SELECT a.id AS value, CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name AS label
    FROM activity a
        JOIN ACTIVITY_HIERARCHY ah ON a.id = ah.ACTIVITY_ID
        LEFT JOIN activity p ON p.id = ah.parent_activity_id
    ORDER BY COALESCE(p.name, a.name), a.name
/

create force view ATLAS_AUTHDB.V_MEMBER_NOMINATIONS as
SELECT N."ID",N."MEMBER_ID",N."PRIORITY",N."COMMENTS",N."ATLAS_FRACTION",N."CATEGORY_ID",N."TYPE_ID",N."PROFESSIONAL_ADVANCEMENT_ID",N."SUBMITTER_ID",N."SUBMISSION_DATE",N."MODIFIED_ON",N."AGENT_ID"
    , AN.ACTIVITY_ID
    , IN2.INSTITUTE_ID
    , COALESCE(AN.SYSTEM_ID, IN2.SYSTEM_ID) AS SYSTEM_ID
    , CASE WHEN ND.ID IS NULL THEN
        'N'
      ELSE
        'Y'
      END AS DISCARDED_FLAG
    , CASE WHEN NU.ID IS NULL THEN
        'N'
      ELSE
        'Y'
      END AS UPGRADE_FLAG
    , CASE WHEN NIUO.ID IS NULL THEN
        'N'
      ELSE
        'Y'
      END AS IGNORE_UPGRADE_OTP_FLAG
    FROM NOM_NOMINATION N
        LEFT JOIN INSTITUTE_NOMINATION IN2 ON N.ID = IN2.NOMINATION_ID AND N.MEMBER_ID = IN2.MEMBER_ID
        LEFT JOIN ACTIVITY_NOMINATION AN ON N.ID = AN.NOMINATION_ID AND N.MEMBER_ID = AN.MEMBER_ID
        LEFT JOIN NOM_DISCARDED ND ON ND.NOMINATION_ID = N.ID
        LEFT JOIN NOM_UPGRADE NU ON NU.NOMINATION_ID = N.ID
        LEFT JOIN NOM_IGNORE_UPGRADE_OTP NIUO ON NIUO.NOM_UPGRADE_ID = NU.ID
    WHERE N.ID IN (
        SELECT I.NOMINATION_ID
        FROM INSTITUTE_NOMINATION I
                 JOIN NOM_NOMINATION NN ON I.NOMINATION_ID = NN.ID
    )
       OR N.ID IN (
        SELECT A.NOMINATION_ID
        FROM ACTIVITY_NOMINATION A
        JOIN NOM_NOMINATION NN ON A.NOMINATION_ID = NN.ID
        JOIN AT_ACTIVITY_ACTION AAA ON A.ACTIVITY_ID = AAA.ACTIVITY_ID
        WHERE AAA.ACTION_ID = 2
    )
/

create force view ATLAS_AUTHDB.V_MEMBER_SCAB_NOMINATIONS as
SELECT "ID","MEMBER_ID","PRIORITY","COMMENTS","ATLAS_FRACTION","CATEGORY_ID","TYPE_ID","PROFESSIONAL_ADVANCEMENT_ID","SUBMITTER_ID","SUBMISSION_DATE","MODIFIED_ON","AGENT_ID","ACTIVITY_ID","INSTITUTE_ID","SYSTEM_ID","DISCARDED_FLAG","UPGRADE_FLAG","IGNORE_UPGRADE_OTP_FLAG"
    FROM V_MEMBER_NOMINATIONS
    WHERE SYSTEM_ID = 1
/

create force view ATLAS_AUTHDB.PC_SCAB_NOMINATION as
SELECT "MEMBER_ID",
           "COMMENTS",
           "PRIORITY",
           "ATLAS_FRACTION",
           "SUBMITTER_ID",
           "SUBMISSION_DATE",
           "AGENT_ID",
           "MODIFIED_ON",
           "ACTIVITY_ID",
           "ID"
    FROM V_MEMBER_SCAB_NOMINATIONS
    WHERE ACTIVITY_ID = 89
/

create force view ATLAS_AUTHDB.PC_MEMB_SCAB_SUBJECT as
SELECT DISTINCT pcn.member_id, na.activity_id, na.nomination_id
    FROM pc_scab_nomination pcn
        JOIN nom_additional_subject na ON na.nomination_id = pcn.id
    UNION
    SELECT DISTINCT pcn.member_id, atsn.activity_id, at.nomination_id
    FROM pc_scab_nomination pcn
        JOIN activity_nomination at ON at.nomination_id = pcn.id
        JOIN at_seeding_activity_nomination asn ON asn.at_nomination_id = at.id
        JOIN activity_nomination atsn ON atsn.id = asn.seeding_at_nomination_id
/

create force view ATLAS_AUTHDB.V_MEMBER_TDAQ_NOMINATIONS as
SELECT "ID","MEMBER_ID","PRIORITY","COMMENTS","ATLAS_FRACTION","CATEGORY_ID","TYPE_ID","PROFESSIONAL_ADVANCEMENT_ID","SUBMITTER_ID","SUBMISSION_DATE","MODIFIED_ON","AGENT_ID","ACTIVITY_ID","INSTITUTE_ID","SYSTEM_ID","DISCARDED_FLAG","UPGRADE_FLAG","IGNORE_UPGRADE_OTP_FLAG"
    FROM V_MEMBER_NOMINATIONS
    WHERE SYSTEM_ID = 3
/

create force view ATLAS_AUTHDB.V_SEEDING_NOMINATIONS_LOOKUP as
SELECT A.ID || '=>1' AS ID, P.CODE || ' ' || A.NAME || ': Highest (1)' AS LABEL
    FROM ACTIVITY A
    JOIN ACTIVITY_HIERARCHY AH on A.ID = AH.ACTIVITY_ID
    JOIN ACTIVITY P ON P.ID = AH.PARENT_ACTIVITY_ID
    UNION
    SELECT A.ID || '=>2' AS ID, P.CODE || ' ' || A.NAME || ': High (2)' AS LABEL
    FROM ACTIVITY A
    JOIN ACTIVITY_HIERARCHY AH on A.ID = AH.ACTIVITY_ID
    JOIN ACTIVITY P ON P.ID = AH.PARENT_ACTIVITY_ID
    UNION
    SELECT A.ID || '=>3' AS ID, P.CODE || ' ' || A.NAME || ': Average (3)' AS LABEL
    FROM ACTIVITY A
    JOIN ACTIVITY_HIERARCHY AH on A.ID = AH.ACTIVITY_ID
    JOIN ACTIVITY P ON P.ID = AH.PARENT_ACTIVITY_ID
    UNION
    SELECT A.ID || '=>4' AS ID, P.CODE || ' ' || A.NAME || ': Low (4)' AS LABEL
    FROM ACTIVITY A
    JOIN ACTIVITY_HIERARCHY AH on A.ID = AH.ACTIVITY_ID
    JOIN ACTIVITY P ON P.ID = AH.PARENT_ACTIVITY_ID
    UNION
    SELECT A.ID || '=>5' AS ID, P.CODE || ' ' || A.NAME || ': Lowest (5)' AS LABEL
    FROM ACTIVITY A
    JOIN ACTIVITY_HIERARCHY AH on A.ID = AH.ACTIVITY_ID
    JOIN ACTIVITY P ON P.ID = AH.PARENT_ACTIVITY_ID
/

create force view ATLAS_AUTHDB.V_AT_SEGREGATED_SEEDING_NOM as
SELECT AT_NOM.NOMINATION_ID
        , AT_NOM.ACTIVITY_ID
        , AT_NOM.MEMBER_ID
        , SEED_AT.ID AS SEED_ACTIVITY_ID
        , SEED_PAT.CODE || ' ' || SEED_AT.NAME SEED_ACTIVITY_LABEL
        , SEED.ID AS SEED_ID
        , SEED.COMMENTS AS SEED_COMMENTS
        , SEED.PRIORITY AS SEED_PRIORITY
        , CASE 
            WHEN NU.ID IS NULL 
            THEN
                'N'
            ELSE
                'Y'
        END AS UPGRADE_FLAG
    FROM ACTIVITY_NOMINATION AT_NOM
        JOIN AT_ACTIVITY_ACTION ATC ON ATC.ACTIVITY_ID = AT_NOM.ACTIVITY_ID AND ATC.ACTION_ID = 2
        JOIN NOM_NOMINATION NOM ON NOM.ID = AT_NOM.NOMINATION_ID
        JOIN ACTIVITY_NOMINATION AT_SEED ON NOM.MEMBER_ID = AT_SEED.MEMBER_ID AND NOM.ID != AT_SEED.NOMINATION_ID
        JOIN NOM_NOMINATION SEED ON AT_SEED.NOMINATION_ID = SEED.ID
        JOIN ACTIVITY SEED_AT ON SEED_AT.ID = AT_SEED.ACTIVITY_ID
        JOIN ACTIVITY_HIERARCHY SEED_ATH ON SEED_AT.ID = SEED_ATH.ACTIVITY_ID
        JOIN ACTIVITY SEED_PAT ON SEED_ATH.PARENT_ACTIVITY_ID = SEED_PAT.ID
        LEFT JOIN NOM_UPGRADE NU ON NU.NOMINATION_ID = AT_NOM.NOMINATION_ID
    WHERE AT_SEED.ACTIVITY_ID IN (
        SELECT ID
        FROM (SELECT AT.ID AS ID, AH.PARENT_ACTIVITY_ID AS PARENT_ID
                FROM ACTIVITY AT
                        JOIN ACTIVITY_HIERARCHY AH ON AH.ACTIVITY_ID = AT.ID) ACTIVITY
        START WITH ID = AT_NOM.ACTIVITY_ID
        CONNECT BY PARENT_ID = PRIOR ID
    )
/

create force view ATLAS_AUTHDB.V_ACTIVITY_SEEDING_NOMINATIONS as
(
    SELECT SEED_NOM.NOMINATION_ID,
           SEED_NOM.ACTIVITY_ID,
           SEED_NOM.MEMBER_ID,
           LISTAGG(NVL(TO_CHAR(SEED_NOM.SEED_ACTIVITY_ID), '-'), ';') WITHIN GROUP ( ORDER BY SEED_NOM.SEED_ID) AS SEED_ACTIVITIES_IDS,
           LISTAGG(NVL(TO_CHAR(SEED_NOM.SEED_ACTIVITY_LABEL), '-'), ';')
                   WITHIN GROUP ( ORDER BY SEED_NOM.SEED_ID)                                        AS SEED_ACTIVITIES,
           LISTAGG(NVL(TO_CHAR(SEED_NOM.SEED_ID), '-'), ';') WITHIN GROUP ( ORDER BY SEED_NOM.SEED_ID) AS SEED_IDS,
           LISTAGG(NVL(TO_CHAR(SEED_NOM.SEED_COMMENTS), '-'), ';') WITHIN GROUP ( ORDER BY SEED_NOM.SEED_ID) AS SEED_COMMENTS,
           LISTAGG(NVL(TO_CHAR(SEED_NOM.SEED_PRIORITY), '-'), ';') WITHIN GROUP ( ORDER BY SEED_NOM.SEED_ID) AS SEED_PRIORITIES
    FROM V_AT_SEGREGATED_SEEDING_NOM SEED_NOM
    GROUP BY SEED_NOM.NOMINATION_ID, SEED_NOM.ACTIVITY_ID, SEED_NOM.MEMBER_ID
)
/

create force view ATLAS_AUTHDB.V_ME_SEEDING_NOMS_PRIORITIES as
SELECT
    S.MEMBER_ID,
    S.SEED_ACTIVITY_ID || '=>' || S.SEED_PRIORITY AS MATCH
FROM V_AT_SEGREGATED_SEEDING_NOM S
/

create force view ATLAS_AUTHDB.NOMINATIONS as
SELECT
    n.id,
    n.member_id AS memb_id,
    comments,
    priority,
    atlas_fraction,
    submitter_id AS submitted_by,
    submission_date,
    n.agent_id AS modified_by,
    n.modified_on,
    professional_advancement_id AS prof_advance_id,
    appointment_id AS app_resp,
    institute_id AS ir_resp,
    rank AS scab_priority,
    COALESCE(an.system_id, ino.system_id) AS systems_id,
    CASE
        WHEN n.id IN (SELECT nomination_id FROM nom_upgrade) THEN 'Y'
        ELSE 'N'
    END AS UPGRADE,
    CASE
        WHEN n.id IN (SELECT nomination_id FROM nom_discarded) THEN 'Y'
        ELSE 'N'
    END AS DISCARDED
FROM nom_nomination n
LEFT JOIN institute_nomination ino ON ino.nomination_id = n.id
LEFT JOIN activity_nomination an ON an.nomination_id = n.id
LEFT JOIN at_nomination_appointment ana ON ana.activity_id = an.activity_id
LEFT JOIN scabber s ON s.memb_id = n.member_id
WHERE (n.id IN (
    SELECT i.nomination_id
    FROM institute_nomination i
    JOIN nom_nomination nn ON i.nomination_id = nn.id
) OR n.id IN (
    SELECT a.nomination_id
    FROM activity_nomination a
    JOIN nom_nomination nn ON a.nomination_id = nn.id
    JOIN at_activity_action aaa ON a.activity_id = aaa.activity_id
    WHERE aaa.action_id = 2
))
/

create force view ATLAS_AUTHDB.NOMINATIONS_HISTORY_COUNT (NOMINATION_ID, HISTORY_ENTRIES) as
SELECT nominations.id,
    COUNT(nominations_hist_new.id)
  FROM nominations,
    nominations_hist_new
  WHERE nominations.id = nominations_hist_new.id(+)
  GROUP BY nominations.id
/

create force view ATLAS_AUTHDB.MEMB_SUBJECT as
SELECT DISTINCT n.memb_id, na.activity_id, n.systems_id
    FROM nominations n
        JOIN nominations_activities na ON na.nomination_id = n.id
/

create force view ATLAS_AUTHDB.SCAB_NOMINATION as
SELECT "MEMB_ID","COMMENTS","PRIORITY","ATLAS_FRACTION","SUBMITTED_BY","SUBMISSION_DATE","MODIFIED_BY","MODIFIED_ON","APP_RESP","IR_RESP","ID","SCAB_PRIORITY","SYSTEMS_ID","PROF_ADVANCE_ID","UPGRADE","DISCARDED" FROM nominations WHERE systems_id = 1
/

create force view ATLAS_AUTHDB.MEMB_SCAB_SUBJECT as
SELECT DISTINCT n.memb_id, nas.activity_id
FROM nominations n
JOIN nom_additional_subject nas ON nas.nomination_id = n.id
WHERE n.systems_id = 1
/

create force view ATLAS_AUTHDB.MEMB_TDAQ_SUBJECT as
SELECT DISTINCT n.memb_id, na.activity_id
FROM nominations n
    JOIN nominations_activities na ON na.nomination_id = n.id
WHERE n.systems_id = 3
/

create force view ATLAS_AUTHDB.V_NOM_IGNORE_UPGRADE_OTP as
SELECT nominations.id as nomination_id
, 'Y' as flag
FROM nom_ignore_upgrade_otp
JOIN nom_upgrade ON nom_upgrade.id = nom_ignore_upgrade_otp.nom_upgrade_id
JOIN nominations ON nominations.id = nom_upgrade.nomination_id
/

create force view ATLAS_AUTHDB.INST as
SELECT
    IN_INSTITUTE.ID AS ID,
    IN_INSTITUTE.NAME AS NAME,
    IN_INSTITUTE.SHORT_NAME AS ONAME,
    IN_AUTHORLIST_IDENTIFICATION.ADDRESS AS ADDRESS,
    IN_INSTITUTE.COUNTRY_ID AS COUNTRY_ID,
    IN_INSTITUTE.PHONE AS PHONE,
    IN_INSTITUTE.FAX AS FAX,
    IN_CONTACT.NAME AS CONTACT,
    IN_CONTACT.EMAIL AS CONTACT_EMAIL,
    IN_INSTITUTE.WEBPAGE AS WEBPAGE,
    IN_INSTITUTE.TIME_ZONE AS TIME_ZONE,
    CASE
        WHEN IN_INCLUDE_DECEASED.INSTITUTE_ID IS NOT NULL THEN 1
        ELSE 0
    END AS INCL_DEAD,
    NULL AS CONTACT_ID,
    IN_SECRETARY.NAME AS SECRETARY,
    IN_SECRETARY.PHONE AS SEC_PHONE,
    IN_SECRETARY.EMAIL AS SEC_EMAIL,
    IN_HIERARCHY.PARENT_INSTITUTE_ID AS CLUSTER_ID,
    CASE
        WHEN IN_AFFILIATION.TYPE_ID = (SELECT ID FROM IN_AFFILIATION_TYPE WHERE CODE = 'CLUSTER') THEN 1
        ELSE 0
    END AS IS_CLUSTER,
    NULL AS CLUSTER_COM,
    IN_INSTITUTE.FUNDING_AGENCY_ID AS FUNDA_ID,
    NULL AS CONTACT_CERN_ID,
    NULL AS CTRLFLAG,
    IN_AUTHORLIST_IDENTIFICATION.PRE_ADDRESS AS PREADDRESS,
    IN_AUTHORLIST_IDENTIFICATION.INFN_ADDRESS AS INFNADDRESS,
    IN_INSTITUTE.MODIFIED_ON AS MODIFIED_ON,
    (SELECT CERN_CCID FROM ME_MEMBER M WHERE M.ID = IN_INSTITUTE.AGENT_ID) AS MODIFIED_BY,
    NULL AS FAKE_CLUSTER,
    IN_INSTITUTE.DOMAIN AS DOMAIN,
    IN_INSTITUTE.SPIRES_ICN AS SPIRES_ICN,
    IN_AFFILIATION.START_DATE AS AFF_START,
    IN_AFFILIATION.END_DATE AS AFF_END,
    NULL AS AFF_TYPE_ID,
    IN_ADDRESS.LINE_1 AS ADDRESS_2,
    IN_ADDRESS.LINE_2 AS ADDRESS_3,
    (SELECT CODE FROM IN_AFFILIATION_TYPE WHERE ID = IN_AFFILIATION.TYPE_ID) AS AFF_TYPE,
    IN_LOCATION.LATITUDE AS LATITUDE,
    IN_LOCATION.LONGITUDE AS LONGITUDE,
    IN_INSTITUTE.ROR AS ROR,
    IN_INSTITUTE.GRID AS GRID,
    IFC.FOUNDATION_INSTITUTE_CODE AS FOUNDATION_INSTITUTE_CODE
FROM
IN_INSTITUTE
LEFT JOIN IN_HIERARCHY
ON IN_HIERARCHY.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_INCLUDE_DECEASED
ON IN_INCLUDE_DECEASED.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_ADDRESS
ON IN_ADDRESS.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_SECRETARY
ON IN_SECRETARY.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_CONTACT
ON IN_CONTACT.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_AFFILIATION
ON IN_AFFILIATION.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_AFFILIATION_TYPE
ON IN_AFFILIATION_TYPE.ID = IN_AFFILIATION.TYPE_ID
LEFT JOIN IN_LOCATION
ON IN_LOCATION.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN IN_AUTHORLIST_IDENTIFICATION
ON IN_AUTHORLIST_IDENTIFICATION.INSTITUTE_ID = IN_INSTITUTE.ID
LEFT JOIN INSTITUTE_FOUNDATION_CODE IFC
ON IFC.INSTITUTE_ID = IN_INSTITUTE.ID
/

create force view ATLAS_AUTHDB.ALSOAT_HIST as
SELECT e.memb_id ,
    (
    CASE
      WHEN inst_footnote_atlas IS NOT NULL
      THEN
        (SELECT 'Also at '
          || inst.address
          || ', '
          || country.name
        FROM inst,
          country
        WHERE inst.id       =inst_footnote_atlas
        AND inst.country_id = country.id
        )
      WHEN inst_footnote_ext IS NOT NULL
      THEN
        (SELECT 'Also at '
          || inst_ghost.name
          || ', '
          || inst_ghost.country
        FROM inst_ghost
        WHERE id=inst_footnote_ext
        )
      ELSE ''
    END) as FOOTNOTE,
    inst_footnote_atlas,
    inst_footnote_ext,
    since,
    up_to
  FROM backup_employ_hist_new e
  WHERE bitand(what_changed,1048576)>0
  OR bitand(what_changed,2097152)   >0
  order by memb_id, up_to desc
/

create force view ATLAS_AUTHDB.JSON_INSTITUTE (ID, TYPE, OBJ) as
SELECT id
    , type
    , TO_CLOB(
    '{' ) || TO_CLOB(
        '"id":' )   || TO_CLOB( id ) || TO_CLOB( ',' ||
        '"type":"') || TO_CLOB( type ) || TO_CLOB( '",' ) || TO_CLOB(
        '"name":"' )       || TO_CLOB( TRIM(REPLACE(REPLACE(REPLACE(name,       '"', '\\"'), CHR(10),'\\n'), '\\', '\\\\' ) ) ) || TO_CLOB( '",' ) || TO_CLOB(
        '"short_name":"' ) || TO_CLOB( TRIM(REPLACE(REPLACE(REPLACE(short_name, '"', '\\"'), CHR(10),'\\n'), '\\', '\\\\' ) ) ) || TO_CLOB( '",' ) || TO_CLOB(
        '"country":"' )    || TO_CLOB( TRIM(REPLACE(REPLACE(country, '"', '\\"'), CHR(10),'\\n') ) ) || TO_CLOB( '",' ) || TO_CLOB(
        '"spires_icn":"' ) || TO_CLOB( TRIM(REPLACE(REPLACE(spires_icn, '"', '\\"'), CHR(10),'\\n') ) ) || TO_CLOB( '",' ) || TO_CLOB(
        '"domain":"' )     || TO_CLOB( TRIM(REPLACE(REPLACE(domain, '"', '\\"'), CHR(10),'\\n') ) ) || TO_CLOB( '",' ) || TO_CLOB(
        '"start_date":' )  || CASE WHEN start_date != 'null' THEN TO_CLOB( '"' ) || TO_CLOB( start_date ) || TO_CLOB( '"' ) ELSE TO_CLOB( 'null' ) END || TO_CLOB( ',' ) || TO_CLOB(
        '"end_date":' )    || CASE WHEN end_date   != 'null' THEN TO_CLOB( '"' ) || TO_CLOB( end_date   ) || TO_CLOB( '"' ) ELSE TO_CLOB( 'null' ) END || TO_CLOB(
    '}' )
FROM (
SELECT i.id
  , 'atlas' AS type
  , i.name
  , i.oname AS short_name
  , c.name AS country
  , i.spires_icn
  , i.domain
  , TO_CHAR( i.aff_start, 'yyyy-mm-dd' ) AS start_date
  , TO_CHAR( i.aff_end, 'yyyy-mm-dd' ) AS end_date
FROM inst i
JOIN country c ON c.id = i.country_id
UNION
SELECT g.id
  , 'ghost' AS type
  , g.name
  , 'null' AS short_name
  , g.country AS country
  , g.spires_icn
  , g.domain
  , 'null' AS start_date
  , 'null' AS end_date
FROM inst_ghost g
)
/

create force view ATLAS_AUTHDB.INST_DUMP as
SELECT i.id id, i.oname short_name, c.name country
FROM inst i
JOIN country c ON c.id = i.country_id
/

create force view ATLAS_AUTHDB.QUAL_OTP_TASK as
(SELECT qotp.id AS id,
       qotp.task_id AS otp_task_id,
       qotp.requirement_id AS otp_requirement_id,
       me.id AS memb_id
FROM qu_otp_task qotp
JOIN me_qualification mq ON mq.qualification_id = qotp.qualification_id
JOIN me_member me ON me.id = mq.member_id)
/

create force view ATLAS_AUTHDB.ANALYSYS_SUBGROUPS_SELECTION as
SELECT S.SUBGROUP_ID  AS value,
         ( S.SUBGROUP_ID
            || ' - '
            || DESCRIPTION ) AS label
  FROM SUBGROUPS S
  WHERE S.SUBGROUP_ID != 'EXOT-LLP'
        AND S.SUBGROUP_ID != 'EXOT-JTX'
  ORDER BY S.SUBGROUP_ID
/

create force view ATLAS_AUTHDB.ATLAS_MEMBER as
SELECT
    ME_MEMBER.ID AS ID,
    CERN_ID,
    FIRST_NAME,
    LAST_NAME,
    STATUS AS CERN_STATUS,
    CONTRACT_END_DATE,
    PERCENTAGE_PRESENCE,
    TELEPHONE1 AS CERN_PHONE,
    PORTABLE_PHONE AS CERN_MOBILE,
    CASE
        WHEN BUILDING IS NOT NULL
        THEN BUILDING || '-' || FLOOR || '-' || ROOM
        ELSE NULL
    END AS OFFICE
FROM MV_FOUNDATION_MEMBER, ME_MEMBER
WHERE ME_MEMBER.CERN_CCID = MV_FOUNDATION_MEMBER.PERSON_ID
UNION
    SELECT MEMBER_ID AS ID, CERN_ID, FIRST_NAME, LAST_NAME, STATUS, NULL AS CONTRACT_END_DATE, NULL AS PERCENTAGE_PRESENCE, NULL AS CERN_PHONE, NULL AS CERN_MOBILE, NULL AS OFFICE
    FROM LEGACY_MEMBER
ORDER BY ID
/

create force view ATLAS_AUTHDB.MEMBER_CURRENT_EMPLOYMENT as
SELECT MAX(ME.START_DATE) AS START_DATE, MEMB_ID AS MEMBER_ID
    FROM ME_EMPLOYMENT ME
    WHERE START_DATE <= SYSDATE
    GROUP BY MEMB_ID

    UNION

    SELECT MAX(ME.START_DATE) AS START_DATE, MEMB_ID AS MEMBER_ID
    FROM ME_EMPLOYMENT ME
    GROUP BY MEMB_ID
    HAVING COUNT(*) = 1
/

create force view ATLAS_AUTHDB.MEMBER_FIRST_EMPLOYMENT as
SELECT MIN(START_DATE) AS START_DATE, MEMB_ID AS MEMBER_ID
    FROM ME_EMPLOYMENT
    GROUP BY MEMB_ID
/

create force view ATLAS_AUTHDB.V_TDAQ_SUBJECT_LOOKUP as
SELECT a.id AS value, CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name AS label
FROM activity a
    JOIN ACTIVITY_HIERARCHY ah ON a.id = ah.ACTIVITY_ID
    LEFT JOIN activity p ON p.id = ah.parent_activity_id
WHERE a.id IN (SELECT activity_id FROM tdaq_subject)
ORDER BY COALESCE(p.name, a.name), a.name
/

create force view ATLAS_AUTHDB.V_ABSTRACT_KEYWORD as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(KWD.ID, '|') WITHIN GROUP (ORDER BY ABS.ID) AS ABSTRACT_KEYWORDS_IDS,
       LISTAGG(KWD.NAME, '|') WITHIN GROUP (ORDER BY ABS.ID) AS ABSTRACT_KEYWORDS_NAMES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_KEYWORD AAK ON ABS.ID = AAK.ABSTRACT_ID
LEFT JOIN ATLAS_PUB_TRACK.KWD_KEYWORD KWD ON KWD.ID = AAK.KEYWORD_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.V_ABSTRACT_CONFERENCE as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(CONF.ID, '|') WITHIN GROUP (ORDER BY CONF.ID) AS ABSTRACT_CONFERENCES_IDS,
       LISTAGG(CONF.SHORT_NAME, '|') WITHIN GROUP (ORDER BY CONF.ID) AS ABSTRACT_CONFERENCES_NAMES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_CONFERENCE AAC ON ABS.ID = AAC.ABSTRACT_ID
LEFT JOIN CONF ON CONF.ID = AAC.CONFERENCE_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.V_ABSTRACT_ANALYSIS as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(ANA.ID, '|') WITHIN GROUP (ORDER BY ANA.ID) AS ABSTRACT_ANALYSES_IDS,
       LISTAGG(ANA.REF_CODE, '|') WITHIN GROUP (ORDER BY ANA.ID) AS ABSTRACT_ANALYSES_REF_CODES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_ANALYSIS AAA ON ABS.ID = AAA.ABSTRACT_ID
LEFT JOIN ANALYSIS_SYS ANA ON ANA.ID = AAA.ANALYSIS_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.V_ABSTRACT_PAPER as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(PUB.ID, '|') WITHIN GROUP (ORDER BY PUB.ID) AS ABSTRACT_PAPERS_IDS,
       LISTAGG(PUB.REF_CODE, '|') WITHIN GROUP (ORDER BY PUB.ID) AS ABSTRACT_PAPERS_REF_CODES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_PAPER AAP ON ABS.ID = AAP.ABSTRACT_ID
LEFT JOIN PUBLICATION PUB ON PUB.ID = AAP.PAPER_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.V_ABSTRACT_CONFNOTE as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(CPUB.ID, '|') WITHIN GROUP (ORDER BY CPUB.ID) AS ABSTRACT_CONFNOTES_IDS,
       LISTAGG(CPUB.TEMP_REF_CODE, '|') WITHIN GROUP (ORDER BY CPUB.ID) AS ABSTRACT_CONFNOTES_REF_CODES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_CONFNOTE AACN ON ABS.ID = AACN.ABSTRACT_ID
LEFT JOIN CONFNOTE_PUBLICATION CPUB ON CPUB.ID = AACN.CONFNOTE_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.V_ABSTRACT_PUBNOTE as
SELECT ABS.ID AS ABSTRACT_ID,
       LISTAGG(PPUB.ID, '|') WITHIN GROUP (ORDER BY PPUB.ID) AS ABSTRACT_PUBNOTES_IDS,
       LISTAGG(PPUB.TEMP_REF_CODE, '|') WITHIN GROUP (ORDER BY PPUB.ID) AS ABSTRACT_PUBNOTES_REF_CODES
FROM ABS_ABSTRACT ABS
LEFT JOIN ABS_ABSTRACT_PUBNOTE P ON ABS.ID = P.ABSTRACT_ID
LEFT JOIN PUBNOTE_PUBLICATION PPUB ON PPUB.ID = P.PUBNOTE_ID
GROUP BY ABS.ID
/

create force view ATLAS_AUTHDB.MEMB as
SELECT INITCAP(LOWER(ATLAS_MEMBER.LAST_NAME))  AS LAST_NAME,
    INITCAP(LOWER(ATLAS_MEMBER.FIRST_NAME)) AS FIRST_NAME,
    ATLAS_MEMBER.CERN_ID                    AS CERN_ID,
    MEMBER_INFO.MEMBER_ID                   AS ID,
    MEMBER_INFO.INITIALS                    AS INITIALS,
    MEMBER_INFO.CERN_CCID                   AS CERN_CCID,
    ATLAS_MEMBER.CERN_STATUS                AS CERN_STATUS,
    ATLAS_MEMBER.CONTRACT_END_DATE          AS CONTRACT_END_DATE,
    ATLAS_MEMBER.PERCENTAGE_PRESENCE        AS PERCENTAGE_PRESENCE,
    ATLAS_MEMBER.CERN_PHONE                 AS CERN_PHONE,
    ATLAS_MEMBER.CERN_MOBILE                AS CERN_MOBILE,
    ATLAS_MEMBER.OFFICE                     AS OFFICE,
    CASE GAP.CONSENT
        WHEN 'Y' THEN 1
        WHEN 'N' THEN 0
    END                                     AS PHOTO,
    QUAL_INFO.QUAL_DATE                     AS QUAL_DATE,
    QUAL_INFO.QUAL_STRING                   AS QUAL_STRING,
    MEMBER_INFO.PRE_CREDIT                  AS PRE_CREDIT,
    MEMBER_INFO.COMMENTS                    AS COMMENTS,
    MEMBER_INFO.LAST_NAME_LTX               AS LAST_NAME_LTX,
    MEMBER_INFO.FIRST_NAME_LTX              AS FIRST_NAME_LTX,
    MEMBER_INFO.ALLOW_PUBLIC_APPOINTMENT    AS ALLOW_PUBLIC_APPOINTMENT,
    MEMBER_INFO.HR_IGNORE                   AS HR_IGNORE,
    MEMBER_INFO.DEATH_DATE                  AS DEATH_DATE,
    MEMBER_INFO.IS_DEAD                     AS IS_DEAD,
    MEMBER_INFO.OTP_ID                      AS OTP_ID,
    QUAL_INFO.QUAL_END                      AS QUAL_END,
    QUAL_INFO.QUAL_START                    AS QUAL_START,
    QUAL_INFO.QUAL_STATE                    AS QUAL_STATE,
    QUAL_INFO.QUAL_RESP                     AS QUAL_RESP,
    QUAL_INFO.PROPOSED_QUAL_START           AS PROPOSED_QUAL_START,
    MEMBER_INFO.MODIFIED_ON                 AS MODIFIED_ON,
    MEMBER_INFO.MODIFIED_BY                 AS MODIFIED_BY,
    MEMBER_INFO.UPDATE_EMAIL                AS UPDATE_EMAIL,
    MEMBER_INFO.STATUS_ATLAS_TEMP           AS STATUS_ATLAS_TEMP,
    QUAL_INFO.EXCLUDE_PREDT                 AS EXCLUDE_PREDT,
    MEMBER_INFO.COMMENTS_PREDT              AS COMMENTS_PREDT,
    MEMBER_INFO.INSPIRE                     AS INSPIRE,
    MEMBER_INFO.STATUS_ATLAS                AS STATUS_ATLAS,
    QUAL_INFO.QUAL_FLAG                     AS QUAL_FLAG,
    MEMBER_INFO.RPDTC                       AS RPDTC,
    MEMBER_INFO.TDAQ_AUTHOR                 AS TDAQ_AUTHOR,
    QUAL_INFO.QUAL_INTEGRATION              AS QUAL_INTEGRATION,
    QUAL_INFO.LOCAL_SUPERVISOR              AS LOCAL_SUPERVISOR,
    QUAL_INFO.TECH_SUPERVISOR               AS TECH_SUPERVISOR,
    QUAL_INFO.CHECKPOINT_DATE               AS CHECKPOINT_DATE,
    QUAL_INFO.CHECKPOINT_COMMENTS           AS CHECKPOINT_COMMENTS,
    QUAL_INFO.FINAL_REPORT_DATE             AS FINAL_REPORT_DATE,
    QUAL_INFO.FINAL_REPORT_COMMENTS         AS FINAL_REPORT_COMMENTS,
    QUAL_INFO.QUAL_ACTIVITY                 AS QUAL_ACTIVITY,
    MEMBER_INFO.AGENT                       AS AGENT,
    MEMBER_INFO.DELETED                     AS DELETED,
    MEMBER_INFO.ORCID                       AS ORCID,
    QUAL_INFO.QUAL_FTE                      AS QUAL_FTE,
    QUAL_INFO.QUAL_FUTURE                   AS QUAL_FUTURE,
    MEMBER_INFO.HIDE_ONLEAVE_DATE           AS HIDE_ONLEAVE_DATE,
    QUAL_INFO.SSA                           AS SSA,
    QUAL_INFO.SO_END_DATE                   AS SO_END_DATE,
    QUAL_INFO.IS_PRE_TDR                    AS IS_PRE_TDR,
    QUAL_INFO.FINAL_REPORT_DOCUMENTATION_URL  AS FINAL_REPORT_DOCUMENTATION_URL,
    MEMBER_INFO.INSPIRE_HEPNAME             AS INSPIRE_HEPNAME
FROM ATLAS_AUTHDB.MEMBER_INFORMATION MEMBER_INFO
JOIN ATLAS_MEMBER ON ATLAS_MEMBER.ID = MEMBER_INFO.MEMBER_ID
LEFT JOIN GLANCEDB_ALLOW_PHOTO GAP ON GAP.PERSON_ID = MEMBER_INFO.CERN_CCID
LEFT JOIN QUALIFICATION_INFORMATION QUAL_INFO ON QUAL_INFO.QU_QUALIFICATION_ID = MEMBER_INFO.QUALIFICATION_ID
/

create force view ATLAS_AUTHDB.MEMBERS_LASTEST_EMPLOY_RECORD as
SELECT M.ID,
        ME.ID                          AS EMPLOY_ID,
        LAST_NAME,
        FIRST_NAME,
        LAST_NAME_LTX,
        FIRST_NAME_LTX,
        INITIALS,
        PHOTO,
        CERN_ID,
        CERN_CCID,
        INSPIRE,
        HR_IGNORE,
        QUAL_START,
        QUAL_DATE,
        QUAL_END,
        SSA,
        CASE
        WHEN M.SO_END_DATE IS NULL
        THEN Add_months(SSA, 12 + ( CASE
                                        WHEN QUAL_DATE < SYSDATE
                                            AND ( END_DATE >= SYSDATE --ACTIVE
                                            AND QUAL_FLAG = 1
                                            OR END_DATE < SYSDATE --INACTIVE
                                            AND EXCLUDE_PREDT = 0 )
                                        THEN RPDTC
                                        ELSE 0
                                    END )
                        )
        ELSE M.SO_END_DATE
        END AS ESA,
        RPDTC,
        QUAL_FLAG,
        PRE_CREDIT,
        M.COMMENTS,
        NAME,
        EMAIL,
        PTYPE,
        MAILFLAG,
        MAILFLAG_STARTDATE,
        MAILFLAG_ENDDATE,
        START_DATE,
        END_DATE,
        DM.DEATH_DATE,
        I.ID                          AS inst_id,
        ME.MOFREE,
        ACTIVITY,
        EXCLUDE_PREDT,
        QUAL_STATE,
        INCL_DEAD,
        FOOTNOTE,
        INST_PHONE,
        INST_FOOTNOTE_ATLAS,
        INST_FOOTNOTE_EXT,
        TDAQ_AUTHOR,
        ( CASE
            WHEN END_DATE >= SYSDATE THEN 'ACTIVE'
            ELSE 'INACTIVE'
        END )                       AS status,
        STATUS_ATLAS,
        ( CASE
            WHEN ME.FUNDA_ID IS NOT NULL THEN ME.FUNDA_ID
            ELSE I.FUNDA_ID
        END )                       AS funda_id
FROM    ME_EMPLOYMENT ME,
        MEMB M,
        ME_DECEASED_MEMBER DM,
        INST I
WHERE  START_DATE = (SELECT Max (START_DATE)
                    FROM   ME_EMPLOYMENT
                    WHERE  MEMB_ID = ME.MEMB_ID
                            AND START_DATE <= SYSDATE)
        AND ME.MEMB_ID = M.ID
        AND ME.INST_ID = I.ID
        AND M.ID = DM.MEMBER_ID(+)
ORDER  BY MEMB_ID
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."FLAG_LISTS" ("MEMB_ID", "INST_ID", "INSPIRE", "AUTHORLIST", "LEFT_INSTITUTE", "MO_LIST", "UNDER_QUAL", "OP_TASK", "TDAQ_AUTHOR") AS
  SELECT id,
    inst_id,
    INSPIRE,
    -- Member currently in ATLAS and qualified as author
    (
    CASE
      WHEN (STATUS                = 'ACTIVE'
      OR STATUS                   = 'INACTIVE'
      AND add_months(END_DATE,1) >= sysdate)
      AND (QUAL_END              IS NULL
      OR QUAL_END                 > sysdate)
      AND QUAL_DATE               < sysdate
      THEN '1'
      ELSE '0'
    END) AS AUTHORLIST_ACTIVE,
    -- Member left ATLAS but is an author
    (
    CASE
       -- signing-only authors
      WHEN QUAL_DATE  < sysdate
      AND QUAL_END < sysdate
      AND SSA     <= sysdate
      AND ESA     >= sysdate
      THEN '1'
      ELSE '0'
    END) AS AUTHORLIST_INACTIVE,
    -- Member in the M share list
    (
    CASE
      WHEN STATUS    = 'ACTIVE'
      AND QUAL_START < sysdate
      AND (QUAL_END IS NULL
      OR QUAL_END    > sysdate)
      AND PTYPE     != 2
      AND PTYPE     != 3
      AND PTYPE     != 4
      AND PTYPE     != 7
      AND (mofree    = 0
      OR mofree     IS NULL)
      THEN '1'
      ELSE '0'
    END) AS MO_LIST,
    -- Member under qualification
    (
    CASE
      WHEN STATUS      = 'ACTIVE'
      AND QUAL_START   < SYSDATE
      AND ( QUAL_DATE IS NULL
      OR QUAL_DATE     > SYSDATE )
      AND ( QUAl_END  IS NULL
      OR QUAL_END      > SYSDATE )
      THEN '1'
      ELSE '0'
    END) AS UNDER_QUAL,
    -- Member in operation tasks. If student, counted as 75%.
    (
    CASE
      WHEN QUAL_START < sysdate
      AND (QUAL_END  IS NULL
      OR QUAL_END     > sysdate)
      AND STATUS      = 'ACTIVE'
      AND PTYPE      != 2
      AND PTYPE      != 3
      AND PTYPE      != 4
      AND PTYPE      != 7
      AND (mofree     = 0
      OR mofree      IS NULL)
      THEN '1' -- counted 100%
      WHEN QUAL_START < sysdate
      AND (QUAL_END  IS NULL
      OR QUAL_END     > sysdate)
      AND STATUS      = 'ACTIVE'
      AND (PTYPE      = 2
      OR PTYPE        = 3
      OR PTYPE        = 4
      OR PTYPE        = 7)
      AND (mofree     = 0
      OR mofree      IS NULL)
      THEN '2' -- counted 75%
      ELSE '0'
    END) AS OT,
    TDAQ_AUTHOR
  FROM MEMBERS_LASTEST_EMPLOY_RECORD
/

-- This was modified
create force view ATLAS_AUTHDB.PHOTO_URL as
SELECT ID, 'url' as url

 FROM MEMB where id is not null
/

create force view ATLAS_AUTHDB.CONFNOTE_SUMMARY as
SELECT CONFNOTE_PUBLICATION.ID                  AS ID,
    CONFNOTE_PUBLICATION.SHORT_TITLE              AS SHORT_TITLE,
    CONFNOTE_PUBLICATION.FULL_TITLE               AS FULL_TITLE,
    CONFNOTE_PUBLICATION.DATA_USED                AS DATA_USED,
    CONFNOTE_PUBLICATION.CONFNOTE_RUN             AS CONFNOTE_RUN,
    CONFNOTE_PUBLICATION.TARGET                   AS TARGET,
    CONFNOTE_PUBLICATION.LUMINOSITY               AS LUMINOSITY,
    CONFNOTE_PUBLICATION.LUMINOSITY_UNIT          AS LUMINOSITY_UNIT,
    CONFNOTE_PUBLICATION.PLANNED_JOURNAL          AS PLANNED_JOURNAL,
    CONFNOTE_PUBLICATION.TARGET_DATE_CONF         AS TARGET_DATE_CONF,
    CONFNOTE_PUBLICATION.TARGET_DATE_JOURNAL      AS TARGET_DATE_JOURNAL,
    CONFNOTE_PUBLICATION.LEAD_GROUP               AS LEAD_GROUP,
    CONFNOTE_PUBLICATION.SEQUENCE_NUMBER          AS SEQUENCE_NUMBER,
    CONFNOTE_PUBLICATION.INTERNAL_SEQUENCE_NUMBER AS INTERNAL_SEQUENCE_NUMBER,
    CONFNOTE_PUBLICATION.CREATION_DATE            AS CREATION_DATE,
    CONFNOTE_PUBLICATION.REF_CODE                 AS REF_CODE,
    CONFNOTE_PUBLICATION.TEMP_REF_CODE            AS TEMP_REF_CODE,
    CONFNOTE_PUBLICATION.INTERNAL_CODE            AS INTERNAL_CODE,
    CONFNOTE_PUBLICATION.STATUS                   AS STATUS,
    CONFNOTE_PUBLICATION.SUB_CERN_APPROVAL        AS SUB_CERN_APPROVAL,
    CONFNOTE_PUBLICATION.CERN_APPROVAL            AS CERN_APPROVAL,
    CONFNOTE_PUBLICATION.COMMMENTS                AS COMMENTS,
    CONFNOTE_PUBLICATION.JOURNAL_SUB              AS JOURNAL_SUB,
    CONFNOTE_PUBLICATION.JOURNAL                  AS JOURNAL,
    CONFNOTE_PUBLICATION.CDS_URL                  AS CDS_URL,
    CONFNOTE_PUBLICATION.ADDITIONAL_NOTES         AS ADDITIONAL_NOTES,
    stragg(DISTINCT GROUP_ID)                     AS OTHER_GROUPS,
    stragg(DISTINCT SUBGROUP_ID)                  AS OTHER_SUBGROUPS,
    stragg(DISTINCT EDIT.first_name
    || ' '
    || EDIT.last_name) AS EDITORS,
    stragg(DISTINCT CONT.first_name
    || ' '
    || CONT.last_name)                            AS CONTACT_EDITORS,
    stragg(DISTINCT EDBOARD.first_name
    || ' '
    || EDBOARD.last_name) AS EDITORIAL_BOARD,
    stragg(DISTINCT EDBCHAIR.first_name
    || ' '
    || EDBCHAIR.last_name)                  AS EDBCHAIR,
    stragg(DISTINCT ECM_VALUE)              AS ECM,
    CONFNOTE_PHASE_1.STATE                  AS PHASE1_STATE,
    CONFNOTE_PHASE_1.REVIEWERS_APPROVAL     AS PHASE1_REVIEWERS_APPROVAL,
    CONFNOTE_PHASE_1.PRESENTATION_PG        AS PHASE1_PRESENTATION_PG,
    CONFNOTE_PHASE_1.MAIL_TO_EDBOARD        AS PHASE1_MAIL_TO_EDBOARD,
    CONFNOTE_PHASE_1.INDICO_URL             AS PHASE1_INDICO_URL,
    CONFNOTE_PHASE_1.LPG_APPROVAL           AS PHASE1_LPG_APPROVAL,
    CONFNOTE_PHASE_1.PGC_SIGN_OFF_DRAFT     AS PHASE1_PGC_SIGN_OFF_DRAFT,
    CONFNOTE_PHASE_1.DRAFT_CDS_URL          AS PHASE1_DRAFT_CDS_URL,
    CONFNOTE_PHASE_1.PRESENTATION_ATLAS_GEN AS PHASE1_PRESENTATION_ATLAS_GEN,
    CONFNOTE_PHASE_1.ATLAS_APPROVAL         AS PHASE1_ATLAS_APPROVAL,
    CONFNOTE_PHASE_1.SIGN_OFF_DRAFT         AS PHASE1_SIGN_OFF_DRAFT,
    CONFNOTE_PHASE_1.ATLAS_RELEASE          AS PHASE1_ATLAS_RELEASE,
    CONFNOTE_PHASE_1.REVIEWERS_SIGN_OFF     AS PHASE1_REVIEWERS_SIGN_OFF,
    CONFNOTE_PHASE_1.PUBCOMM_SIGN_OFF       AS PHASE1_PUBCOMM_SIGN_OFF,
    CONFNOTE_PHASE_1.PHYSCOORD_SIGN_OFF     AS PHASE1_PHYSCOORD_SIGN_OFF,
    CONFNOTE_PHASE_1.SIGN_OFF_1             AS PHASE1_SIGN_OFF_1,
    CONFNOTE_PHASE_1.SIGN_OFF_1_MEMBER      AS PHASE1_SIGN_OFF_1_MEMBER,
    CONFNOTE_PHASE_1.SIGN_OFF_2             AS PHASE1_SIGN_OFF_2,
    CONFNOTE_PHASE_1.SIGN_OFF_2_MEMBER      AS PHASE1_SIGN_OFF_2_MEMBER,
    stragg(DISTINCT CASE WHEN TYPE = 'phase1_supersededByURL' THEN '' ELSE '['
    || ALIAS
    || '@'
    || href
    || '@'
    || type
    || ']' END) AS ALL_LINKS,
    SUPERSEDED.SUPERSEDED_URL AS SUPERSEDED_LINK,
    CONFNOTE_PUBLICATION.DELETION_REASON           AS DELETION_REASONS,
    CONFNOTE_PUBLICATION.DELETION_REQUEST          AS DELETION_REQUEST,
    CONFNOTE_PUBLICATION.DELETED                   AS DELETED
  FROM CONFNOTE_PUBLICATION,
    CONFNOTE_PUBLICATION_GROUP,
    CONFNOTE_PUBLICATION_SUBGROUP,
    MEMB EDIT,
    MEMB CONT,
    MEMB EDBOARD,
    MEMB EDBCHAIR,
    (SELECT PUB_ID, STRAGG(UTL_URL.ESCAPE('['||REF_CODE||'@'||FIGURES||'@phase1_supersededByURL]')) as SUPERSEDED_URL FROM (
  SELECT AR_1.PUBLICATION_ID as PUB_ID,  
  PUBLICATION.REF_CODE as REF_CODE,
AR_1.TYPE as PUB_TYPE,
NVL(PUBLICATION.FIGURES, 'https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/' || PUBLICATION.REF_CODE  ) as FIGURES,
AR_2.PUBLICATION_ID ||'@'|| AR_2.TYPE as RELATIONS,
PUBLICATION.SHORT_TITLE as SHORT_TITLE
FROM ANALYSIS_RELATIONSHIP  AR_1, ANALYSIS_RELATIONSHIP  AR_2, PUBLICATION
WHERE AR_1.CHILD_ID IS NOT NULL AND AR_1.CHILD_ID = AR_2.ID AND AR_2.TYPE = 'PAPER'
AND PUBLICATION.ID = AR_2.PUBLICATION_ID
UNION
SELECT AR_1.PUBLICATION_ID as PUB_ID,  
NVL(CONFNOTE_PUBLICATION.REF_CODE, CONFNOTE_PUBLICATION.TEMP_REF_CODE) as REF_CODE,
AR_1.TYPE as PUB_TYPE,
CONFNOTE_PUBLICATION.CDS_URL as FIGURES,
AR_2.PUBLICATION_ID ||'@'|| AR_2.TYPE as RELATIONS,
CONFNOTE_PUBLICATION.SHORT_TITLE as SHORT_TITLE
FROM ANALYSIS_RELATIONSHIP  AR_1, ANALYSIS_RELATIONSHIP  AR_2, CONFNOTE_PUBLICATION
WHERE AR_1.CHILD_ID IS NOT NULL AND AR_1.CHILD_ID = AR_2.ID AND AR_2.TYPE = 'CONF'
AND CONFNOTE_PUBLICATION.ID = AR_2.PUBLICATION_ID
UNION
SELECT AR_1.PUBLICATION_ID as PUB_ID,  
AR_1.TYPE as PUB_TYPE,
PUBNOTE_PUBLICATION.CDS_URL as FIGURES,
AR_2.PUBLICATION_ID ||'@'|| AR_2.TYPE as RELATIONS,
NVL(PUBNOTE_PUBLICATION.FINAL_REF_CODE, PUBNOTE_PUBLICATION.TEMP_REF_CODE) as REF_CODE,
PUBNOTE_PUBLICATION.SHORT_TITLE as SHORT_TITLE
FROM ANALYSIS_RELATIONSHIP  AR_1, ANALYSIS_RELATIONSHIP  AR_2, PUBNOTE_PUBLICATION
WHERE AR_1.CHILD_ID IS NOT NULL AND AR_1.CHILD_ID = AR_2.ID AND AR_2.TYPE = 'PUB'
AND PUBNOTE_PUBLICATION.ID = AR_2.PUBLICATION_ID) WHERE PUB_TYPE = 'CONF' GROUP BY PUB_ID, PUB_TYPE) SUPERSEDED,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM CONFNOTE_MEMB_PUBLICATION
    WHERE CONFNOTE_MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
    ) EDITORS,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM CONFNOTE_MEMB_PUBLICATION
    WHERE CONFNOTE_MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
    AND  CONFNOTE_MEMB_PUBLICATION.MEMB_FUNCTION = 'coEditor'
    ) CONTACT_EDITORS,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM CONFNOTE_MEMB_PUBLICATION
    WHERE CONFNOTE_MEMB_PUBLICATION.GROUP_ID = 'EDBOARD'
    ) EDITORIAL_BOARD,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM CONFNOTE_MEMB_PUBLICATION
    WHERE CONFNOTE_MEMB_PUBLICATION.GROUP_ID    = 'EDBOARD'
    AND CONFNOTE_MEMB_PUBLICATION.MEMB_FUNCTION = 'chair'
    ) EDITORIAL_BOARD_CHAIR,
    CONFNOTE_ECM,
    CONFNOTE_PHASE,
    CONFNOTE_PHASE_1,
    CONFNOTE_PHASE_LINKS
  WHERE CONFNOTE_PUBLICATION.ID     = EDITORS.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = SUPERSEDED.PUB_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = CONTACT_EDITORS.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = CONFNOTE_PUBLICATION_GROUP.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = CONFNOTE_PUBLICATION_SUBGROUP.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = EDITORIAL_BOARD.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = EDITORIAL_BOARD_CHAIR.PUBLICATION_ID (+)
  AND EDITORS.MEMB_ID               = EDIT.ID (+)
  AND CONTACT_EDITORS.MEMB_ID       = CONT.ID (+)
  AND EDITORIAL_BOARD.MEMB_ID       = EDBOARD.ID (+)
  AND EDITORIAL_BOARD_CHAIR.MEMB_ID = EDBCHAIR.ID (+)
  AND CONFNOTE_PUBLICATION.ID       = CONFNOTE_ECM.PUBLICATION_ID (+)
  AND CONFNOTE_PUBLICATION.ID       = CONFNOTE_PHASE.PUBLICATION_ID (+)
  AND CONFNOTE_PHASE.ID             = CONFNOTE_PHASE_1.PHASE_ID (+)
  AND CONFNOTE_PHASE_1.PHASE_ID     = CONFNOTE_PHASE_LINKS.PHASE_ID (+)
  GROUP BY CONFNOTE_PUBLICATION.ID,
    SUPERSEDED.SUPERSEDED_URL,
    SHORT_TITLE,
    FULL_TITLE,
    DATA_USED,
    CONFNOTE_RUN,
    TARGET,
    LUMINOSITY,
    LUMINOSITY_UNIT,
    LUMINOSITY_UNCERTAINTY,
    PLANNED_JOURNAL,
    STATUS,
    TARGET_DATE_CONF,
    TARGET_DATE_JOURNAL,
    LEAD_GROUP,
    SEQUENCE_NUMBER,
    INTERNAL_SEQUENCE_NUMBER,
    CREATION_DATE,
    REF_CODE,
    TEMP_REF_CODE,
    INTERNAL_CODE,
    SUB_CERN_APPROVAL,
    CERN_APPROVAL,
    COMMMENTS,
    JOURNAL_SUB,
    JOURNAL,
    CDS_URL,
    ADDITIONAL_NOTES,
    CONFNOTE_PHASE_1.STATE,
    CONFNOTE_PHASE_1.REVIEWERS_APPROVAL,
    CONFNOTE_PHASE_1.PRESENTATION_PG,
    CONFNOTE_PHASE_1.MAIL_TO_EDBOARD,
    CONFNOTE_PHASE_1.INDICO_URL,
    CONFNOTE_PHASE_1.LPG_APPROVAL,
    CONFNOTE_PHASE_1.PGC_SIGN_OFF_DRAFT,
    CONFNOTE_PHASE_1.DRAFT_CDS_URL,
    CONFNOTE_PHASE_1.PRESENTATION_ATLAS_GEN,
    CONFNOTE_PHASE_1.ATLAS_APPROVAL,
    CONFNOTE_PHASE_1.SIGN_OFF_DRAFT,
    CONFNOTE_PHASE_1.ATLAS_RELEASE,
    CONFNOTE_PHASE_1.REVIEWERS_SIGN_OFF,
    CONFNOTE_PHASE_1.PUBCOMM_SIGN_OFF,
    CONFNOTE_PHASE_1.PHYSCOORD_SIGN_OFF,
    CONFNOTE_PHASE_1.SIGN_OFF_1,
    CONFNOTE_PHASE_1.SIGN_OFF_1_MEMBER,
    CONFNOTE_PHASE_1.SIGN_OFF_2,
    CONFNOTE_PHASE_1.SIGN_OFF_2_MEMBER,
    CONFNOTE_PUBLICATION.DELETION_REASON,
    CONFNOTE_PUBLICATION.DELETION_REQUEST,
    CONFNOTE_PUBLICATION.DELETED
  ORDER BY TEMP_REF_CODE DESC,
    REF_CODE DESC,  
    CREATION_DATE DESC
/

create force view ATLAS_AUTHDB.EDBOARD_PAPERS_CONFNOTES as
SELECT
    MEMB.LAST_NAME ||' '|| MEMB.FIRST_NAME as name,
    'Paper' as paper_type,
    PUBLICATION.SHORT_TITLE as short_title,
    MEMB_PUBLICATION.MEMB_FUNCTION as memb_function,
    PUBLICATION.STATUS as status,
    PUBLICATION.REF_CODE as ref_code
FROM
    MEMB,
    PUBLICATION,
    MEMB_PUBLICATION
WHERE
        MEMB.ID = MEMB_PUBLICATION.MEMB_ID
    AND MEMB_PUBLICATION.PUBLICATION_ID = PUBLICATION.ID
    AND MEMB_PUBLICATION.GROUP_ID = 'EDBOARD'

UNION

SELECT
    MEMB.LAST_NAME||' '||MEMB.FIRST_NAME as name,
    'CONF' as paper_type,
    CONFNOTE_PUBLICATION.SHORT_TITLE as short_title,
    CONFNOTE_MEMB_PUBLICATION.MEMB_FUNCTION as memb_function,
    CONFNOTE_PUBLICATION.STATUS as status,
    CONFNOTE_PUBLICATION.REF_CODE as ref_code
FROM
    MEMB,
    CONFNOTE_PUBLICATION,
    CONFNOTE_MEMB_PUBLICATION
WHERE
    MEMB.ID = CONFNOTE_MEMB_PUBLICATION.MEMB_ID
    AND CONFNOTE_MEMB_PUBLICATION.PUBLICATION_ID = CONFNOTE_PUBLICATION.ID
    AND CONFNOTE_MEMB_PUBLICATION.GROUP_ID = 'EDBOARD'
/

create force view ATLAS_AUTHDB.ANALYSIS_SUMMARY as
SELECT PUBLICATION.ID               AS id,
    PUBLICATION.SHORT_TITLE           AS short_title,
    PUBLICATION.FULL_TITLE            AS full_title,
    PUBLICATION.DATA_USED             AS data_used,
    PUBLICATION.PUBLICATION_RUN       AS publication_run,
    PUBLICATION.LUMINOSITY            AS luminosity,
    PUBLICATION.LUMINOSITY_UNIT       AS luminosity_unit,
    PUBLICATION.HEP_DATA              AS hep_data,
    PUBLICATION.RIVET                 AS rivet,
    PUBLICATION.PLANNED_JOURNAL       AS planned_journal,
    PUBLICATION.TARGET_DATE_CONF      AS target_date_conf,
    PUBLICATION.TARGET_DATE_JOURNAL   AS target_date_journal,
    PUBLICATION.LEAD_GROUP            AS lead_group,
    stragg(DISTINCT GROUP_ID)         AS other_groups,
    stragg(DISTINCT SUBGROUP_ID)      AS other_subgroups,
    PUBLICATION.SEQUENCE_NUMBER       AS sequence_number,
    PUBLICATION.CREATION_DATE         AS creation_date,
    PUBLICATION.REF_CODE              AS ref_code,
    PUBLICATION.STATUS                AS status,
    PUBLICATION.SUB_CERN_APPROVAL     AS sub_cern_approval,
    PUBLICATION.CERN_APPROVAL         AS cern_approval,
    PUBLICATION.COMMMENTS             AS comments,
    PUBLICATION.JOURNAL_SUB           AS journal_sub,
    PUBLICATION.JOURNAL               AS journal,
    PUBLICATION.CDS_URL               AS cds_url,
    PUBLICATION.JOURNAL_APPROVAL_DATE AS journal_approval_date,
    stragg(DISTINCT EDIT.first_name
    || ' '
    || EDIT.last_name)                AS editors,
    stragg(DISTINCT CONT.first_name
    || ' '
    || CONT.last_name)                AS CONTACT_EDITORS,
    stragg(DISTINCT EDB.first_name
    || ' '
    || EDB.last_name)                 AS editorial_board,
    stragg(DISTINCT EDBCHAIR.first_name
    || ' '
    || EDBCHAIR.last_name)            AS EDBCHAIR,
    stragg(DISTINCT ECM_VALUE)        AS ecm,
    stragg(DISTINCT '['
    || ALL_LINKS.ALIAS
    ||'@'
    || ALL_LINKS.HREF
    || '@'
    || ALL_LINKS.TYPE
    ||']') all_links,
    PUBLICATION.SUB_GROUP                     AS sub_group,
    STRAGG( DISTINCT( ALL_LINKS.ARXIV_REF ) ) AS ARXIV_REFS,
    (SELECT STATE
    FROM PHASE_3,
      PHASE
    WHERE PHASE.ID    =PHASE_3.PHASE_ID
    AND PHASE         =3
    AND PUBLICATION_ID=PUBLICATION.ID
    ) AS PHASE3_STATE,
    PUBLICATION.DELETION_REASON           AS DELETION_REASONS,
    PUBLICATION.DELETION_REQUEST          AS DELETION_REQUEST,
    PUBLICATION.DELETED                   AS DELETED,
    (SELECT FIRST_NAME || ' ' || LAST_NAME FROM MEMB WHERE ID = (SELECT SPOKESPERSON_ID
    FROM PHASE_1,
      PHASE
    WHERE PHASE.ID    =PHASE_1.PHASE_ID
    AND PHASE         =1
    AND PUBLICATION_ID=PUBLICATION.ID
    AND SPOKESPERSON_NAME IS NOT NULL
    ))                                     AS SP_NAME,
    (SELECT FIRST_NAME || ' ' || LAST_NAME FROM MEMB WHERE ID = (SELECT PUBCOMM_ID
    FROM PHASE_3,
      PHASE
    WHERE PHASE.ID    =PHASE_3.PHASE_ID
    AND PHASE         =3
    AND PUBLICATION_ID=PUBLICATION.ID
    AND PUBCOMM_NAME IS NOT NULL
    ))                                     AS PCC_NAME,
    (SELECT SIGN_OFF
    FROM
      PHASE
    WHERE PHASE.PHASE = 4
    AND PHASE.PUBLICATION_ID=PUBLICATION.ID
    )                                     AS PUBLICATION_DATE,
    (SELECT CERN_REFERENCE_NUMBER
    FROM PHASE_3,
      PHASE
    WHERE PHASE.ID    =PHASE_3.PHASE_ID
    AND PHASE         =3
    AND PUBLICATION_ID=PUBLICATION.ID
    )                                     AS CERN_REFERENCE_NUMBER,
    (SELECT SPOKESPERSON_ID
      FROM PHASE_3,
          PHASE
      WHERE
          PHASE.ID          = PHASE_3.PHASE_ID
          AND PHASE         = 3
          AND PUBLICATION_ID = PUBLICATION.ID
   )                                      AS SPOKESPERSON_ID,
   (SELECT stragg(DISTINCT MEMB.first_name
                                        || ' '
                                        || MEMB.last_name)
                  FROM MEMB,
                      PHASE_3,
                      PHASE
                  WHERE
                      PHASE.ID          = PHASE_3.PHASE_ID
                      AND PHASE         = 3
                      AND PUBLICATION_ID = PUBLICATION.ID
                      AND MEMB.ID = PHASE_3.SPOKESPERSON_ID
   )             AS SPOKESPERSON_DELEGATED_NAME,
   (SELECT PUBCOMM_ID
      FROM PHASE_3,
          PHASE
      WHERE
          PHASE.ID          = PHASE_3.PHASE_ID
          AND PHASE         = 3
          AND PUBLICATION_ID = PUBLICATION.ID
   )                                      AS PUBCOMM_ID
  FROM PUBLICATION,
    PUBLICATION_ANALYSISGROUP,
    PUBLICATION_ANALYSISSUBGROUP,
    MEMB EDIT,
    MEMB CONT,
    MEMB EDB,
    MEMB EDBCHAIR,
    (SELECT MEMB_ID,
      PUBLICATION_ID,
      MEMB_FUNCTION
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
    ) EDITORS,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
      AND MEMB_FUNCTION = 'coEditor'
    ) CONTACT_EDITORS,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'EDBOARD'
    ) EDBOARD_EDITORS,
    (SELECT MEMB_ID,
      PUBLICATION_ID
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID    = 'EDBOARD'
    AND MEMB_PUBLICATION.MEMB_FUNCTION = 'chair'
    ) EDITORIAL_BOARD_CHAIR,
    ANALYSIS_ECM,
    (SELECT ID,
      PHASE_ID,
      ALIAS,
      HREF,
      TYPE,
      CASE
        WHEN TYPE='final_arxivURL'
        THEN UTL_URL.UNESCAPE(ALIAS)
      END AS ARXIV_REF
    FROM PHASE_LINKS
    ) ALL_LINKS,
    PHASE
  WHERE ALL_LINKS.PHASE_ID (+)      = PHASE.ID
  AND (PHASE.PUBLICATION_ID (+)     = PUBLICATION.ID)
  AND ( PUBLICATION.ID              = EDITORS.PUBLICATION_ID (+)
  AND PUBLICATION.ID                = CONTACT_EDITORS.PUBLICATION_ID (+)
  AND PUBLICATION.ID                = EDBOARD_EDITORS.PUBLICATION_ID (+)
  AND PUBLICATION.ID                = EDITORIAL_BOARD_CHAIR.PUBLICATION_ID (+)
  AND PUBLICATION.ID                = PUBLICATION_ANALYSISGROUP.PUBLICATION_ID (+)
  AND PUBLICATION.ID                = PUBLICATION_ANALYSISSUBGROUP.PUBLICATION_ID (+)
  AND EDITORS.MEMB_ID               = EDIT.ID (+)
  AND CONTACT_EDITORS.MEMB_ID       = CONT.ID (+)
  AND EDBOARD_EDITORS.MEMB_ID       = EDB.ID (+)
  AND EDITORIAL_BOARD_CHAIR.MEMB_ID = EDBCHAIR.ID (+)
  AND PUBLICATION.ID                = ANALYSIS_ECM.PUBLICATION_ID (+))
  GROUP BY PUBLICATION.ID,
    SHORT_TITLE,
    FULL_TITLE,
    DATA_USED,
    PUBLICATION_RUN,
    LUMINOSITY,
    LUMINOSITY_UNIT,
    LUMINOSITY_UNCERTAINTY,
    HEP_DATA,
    RIVET,
    PLANNED_JOURNAL,
    STATUS,
    TARGET_DATE_CONF,
    TARGET_DATE_JOURNAL,
    LEAD_GROUP,
    SEQUENCE_NUMBER,
    CREATION_DATE,
    REF_CODE,
    SUB_CERN_APPROVAL,
    CERN_APPROVAL,
    COMMMENTS,
    JOURNAL_SUB,
    JOURNAL,
    CDS_URL,
    PAPER_REFERENCE,
    JOURNAL_APPROVAL_DATE,
    JOURNAL_PUBLICATION_URL,
    PUBLICATION.SUB_GROUP,
    PUBLICATION.DELETION_REASON,
    PUBLICATION.DELETION_REQUEST,
    PUBLICATION.DELETED
  ORDER BY REF_CODE,
    CREATION_DATE DESC
/

create force view ATLAS_AUTHDB.ANALYSIS as
SELECT PUBLICATION.ID                                 AS publication_id,
    PHASE_PHASE1.ID                                     AS phase_1_id,
    PHASE_PHASE2.ID                                     AS phase_2_id,
    PHASE_SUBMISSION.ID                                 AS final_phase_id,
    PUBLICATION.SHORT_TITLE                             AS short_title,
    PUBLICATION.FULL_TITLE                              AS full_title,
    PUBLICATION.PLANNED_JOURNAL                         AS planned_journal,
    PUBLICATION.TARGET_DATE_CONF                        AS target_date_conf,
    PUBLICATION.TARGET_DATE_JOURNAL                     AS target_date_journal,
    PUBLICATION.REF_CODE                                AS ref_code,
    PUBLICATION.SUB_CERN_APPROVAL                       AS sub_cern_approval,
    PUBLICATION.CERN_APPROVAL                           AS cern_approval,
    PUBLICATION.COMMMENTS                               AS comments,
    PUBLICATION.JOURNAL_SUB                             AS journal_sub,
    PUBLICATION.JOURNAL                                 AS journal,
    PUBLICATION.CDS_URL                                 AS publication_cds_url,
    PUBLICATION.LEAD_GROUP                              AS lead_group,
    PUBLICATION.SEQUENCE_NUMBER                         AS sequence_number,
    PUBLICATION.CREATION_DATE                           AS creation_date,
    PUBLICATION.STATUS                                  AS publication_status,
    PUBLICATION.JOURNAL_PUBLICATION_URL                 AS journal_publication_url,
    PUBLICATION.PAPER_REFERENCE                         AS paper_reference,
    PUBLICATION.LUMINOSITY                              AS luminosity,
    PUBLICATION.LUMINOSITY_UNIT                         AS luminosity_unit,
    PUBLICATION.LUMINOSITY_UNCERTAINTY                  AS paper_luminosity_uncertainty,
    PUBLICATION.DATA_USED                               AS data_used,
    PUBLICATION.PUBLICATION_RUN                         AS publication_run,
    PUBLICATION.JOURNAL_APPROVAL_DATE                   AS journal_approval_date,
    PUBLICATION.ARXIV_URL                               AS arxiv_url,
    PUBLICATION.TRACKING_REF                            AS tracking_ref,
    PUBLICATION.REFEREE_REPORT_ON                       AS referee_report_on,
    PUBLICATION.PROOFS_ON                               AS proofs_on,
    stragg(DISTINCT PUBLICATION_ANALYSISGROUP.GROUP_ID) AS other_groups,
    PUBLICATION.FINAL_TITLE_TEX                         AS final_title_tex,
    PUBLICATION.SUB_GROUP                               AS sub_group,
    PUBLICATION.FIGURES                                 AS figures,
    PHASE_1.REVIEWERS_SIGN_OFF                          AS p_1_reviewers_sign_off,
    PHASE_1.PRESENTATION_PG                             AS p_1_presentation_pg,
    PHASE_1.LPG_APPROVAL                                AS p_1_lpg_approval,
    PHASE_1.DRAFT_CDS_URL                               AS p_1_draft_cds_url,
    PHASE_1.SIGN_OFF_DRAFT                              AS p_1_sign_off_draft,
    PHASE_1.PRESENTATION_ATLAS_GEN                      AS p_1_presentation_atlas_general,
    PHASE_1.STATE                                       AS p_1_state,
    PHASE_1.ATLAS_RELEASE                               AS p_1_atlas_release,
    PHASE_1.PRESENTATION_ATLAS_URL                      AS p_1_presentation_atlas_url,
    PHASE_1.REVIEWERS_APPROVAL                          AS p_1_reviewers_approval,
    PHASE_1.PUBCOMM_SIGN_OFF                            AS p_1_pubcomm_sign_off,
    PHASE_1.PHYSCOORD_SIGN_OFF                          AS p_1_pc_sign_off,
    PHASE_1.SUPPORT_DOCUMENTS_URL                       AS p_1_support_documents_url,
    PHASE_1.MAIL_TO_EDBOARD                             AS p_1_mail_to_edboard,
    PHASE_1.PGC_SIGN_OFF_DRAFT                          AS p_1_pgc_sign_off_draft,
    PHASE_1.DRAFTCDSURL                                 AS p_1_draftcdsurl,
    PHASE_1.AUTHORLIST_ID                               AS p_1_authorlist_id,    
    PHASE2.START_DATE                                   AS p_1_start_date,
    PHASE2.LPG_SIGN_OFF                                 AS p_1_lpg_sign_off,
    PHASE2.OPEN_DISCUSSION_DATE                         AS p_1_open_discussion_date,
    PHASE2.OPEN_DISCUSSION_INDICO_URL                   AS p_1_open_discussion_indico_url,
    PHASE2.RELEASE_ATLAS_COMMENTS                       AS p_1_release_atlas_comments,
    PHASE2.END_ATLAS_COMMENTS                           AS p_1_end_atlas_comments,
    PHASE2.FINAL_APPROVAL                               AS p_1_final_approval,
    PHASE2.FINAL_CDS_LINK                               AS p_1_final_cds_link,
    PHASE2.STATE                                        AS p_2_state,
    PHASE2.LPG_FINAL_SIGN_OFF                           AS p_2_lpg_final_sign_off,
    PHASE2.DRAFT_CDS_URL                                AS p_2_draft_cds_url,
    PHASE2.LPG_APPROVAL                                 AS p_2_lpg_approval,
    PHASE2.LOOP_SEQUENCE                                AS p_2_loop_sequence,
    PHASE2.PUBLIC_READING_INDICO_URL                    AS p_2_public_reading_indico_url,
    PHASE2.PUBLIC_READING_DATE                          AS p_2_public_reading_date,
    PHASE2.PUBCOMM_SIGN_OFF                             AS p_2_pubcomm_sign_off,
    PHASE2.PUBCOMMCHAIR_SIGN_OFF                        AS p_2_pubcommchair_sign_off,
    PHASE2.PHYSCOORD_SIGN_OFF                           AS p_2_pc_sign_off,
    PHASE2.AUTHORLIST_ARCHIVED_ON                       AS p_2_author_list_archived_on,
    PHASE2.CERN_REFERENCE_NUMBER                        AS p_2_cern_reference_number,
    PHASE2.EDBOARD_SIGNOFF_DATE                         AS p_2_edboard_sign_off_date,
    PHASE2.PUBCOMM_NAME                                 AS p_2_pubcomm_name,
    PHASE_1.SPOKESPERSON_NAME                            AS p_2_spokesperson_name,
    PHASE2.SPOKESPERSON_SIGN_OFF                        AS p_2_spokesperson_sign_off,
    PHASE2.CERN_SIGN_OFF                                AS p_2_cern_sign_off,
    SUBMISSION.RELEASE_CERN_COMMENTS                    AS s_release_cern_comments,
    SUBMISSION.STATE                                    AS s_state,
    PHASE_SUBMISSION.INDICO_URL                         AS s_indico_url,
    PHASE_SUBMISSION.SIGN_OFF                           AS s_sign_off,
    (SELECT CERN_REFERENCE_NUMBER
     FROM PHASE_3,
          PHASE
     WHERE PHASE.ID    =PHASE_3.PHASE_ID
     AND PHASE         =3
     AND PUBLICATION_ID=PUBLICATION.ID
     )                                     AS CERN_REFERENCE_NUMBER,
    stragg(DISTINCT EDIT.first_name
    || ' '
    || EDIT.last_name
    || ' : '
    || EDITORS.MEMB_FUNCTION ) AS editors,
    stragg(DISTINCT CONT.first_name
    || ' '
    || CONT.last_name
    || ' : '
    ||CONTACT_EDITORS.MEMB_FUNCTION) AS contact_editors,
    stragg(DISTINCT EDB.first_name
    || ' '
    || EDB.last_name
    || ' : '
    || EDBOARD_EDITORS.MEMB_FUNCTION) AS editorial_board,
    stragg(DISTINCT ECM_VALUE)        AS ecm,
    stragg(DISTINCT '['
    || ALIAS
    ||'@'
    || HREF
    || '@'
    || TYPE
    ||']') AS ALL_LINKS,
    PUBLICATION.HEP_DATA,
    PUBLICATION.RIVET
  FROM PUBLICATION
  LEFT OUTER JOIN PHASE PHASE_PHASE1
  ON PHASE_PHASE1.PUBLICATION_ID = PUBLICATION.ID
  AND PHASE_PHASE1.PHASE         = 1
  LEFT OUTER JOIN PHASE_1
  ON PHASE_PHASE1.ID = PHASE_1.PHASE_ID
  LEFT OUTER JOIN PHASE PHASE_PHASE2
  ON PHASE_PHASE2.PUBLICATION_ID = PUBLICATION.ID
  AND PHASE_PHASE2.PHASE         = 3
  LEFT OUTER JOIN PHASE_3 PHASE2
  ON PHASE_PHASE2.ID = PHASE2.PHASE_ID
  LEFT OUTER JOIN PHASE PHASE_SUBMISSION
  ON PHASE_SUBMISSION.PUBLICATION_ID = PUBLICATION.ID
  AND PHASE_SUBMISSION.PHASE         = 4
  LEFT OUTER JOIN SUBMISSION
  ON PHASE_SUBMISSION.ID = SUBMISSION.PHASE_ID
  LEFT OUTER JOIN PHASE_LINKS
  ON PHASE_LINKS.PHASE_ID IN (PHASE_PHASE1.ID , PHASE_PHASE2.ID, PHASE_SUBMISSION.ID)
  LEFT OUTER JOIN (
    (SELECT MEMB_ID,
      PUBLICATION_ID,
      MEMB_FUNCTION
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'CONTACT'
    ) CONTACT_EDITORS
  LEFT OUTER JOIN MEMB CONT
  ON CONTACT_EDITORS.MEMB_ID        = CONT.ID )
  ON CONTACT_EDITORS.PUBLICATION_ID = PUBLICATION.ID
  LEFT OUTER JOIN (
    (SELECT MEMB_ID,
      PUBLICATION_ID,
      MEMB_FUNCTION
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'EDBOARD'
    ) EDBOARD_EDITORS
  LEFT OUTER JOIN MEMB EDB
  ON EDBOARD_EDITORS.MEMB_ID        = EDB.ID )
  ON EDBOARD_EDITORS.PUBLICATION_ID = PUBLICATION.ID
  LEFT OUTER JOIN (
    (SELECT MEMB_ID,
      PUBLICATION_ID,
      MEMB_FUNCTION
    FROM MEMB_PUBLICATION
    WHERE MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
    ) EDITORS
  LEFT OUTER JOIN MEMB EDIT
  ON EDITORS.MEMB_ID        = EDIT.ID )
  ON EDITORS.PUBLICATION_ID = PUBLICATION.ID
  LEFT OUTER JOIN PUBLICATION_ANALYSISGROUP
  ON PUBLICATION_ANALYSISGROUP.PUBLICATION_ID = PUBLICATION.ID
  LEFT OUTER JOIN ANALYSIS_ECM
  ON ANALYSIS_ECM.PUBLICATION_ID = PUBLICATION.ID
  GROUP BY PUBLICATION.ID,
    PHASE_PHASE1.ID,
    PHASE_PHASE2.ID,
    PHASE_SUBMISSION.ID,
    PUBLICATION.SHORT_TITLE,
    PUBLICATION.FULL_TITLE,
    PUBLICATION.PLANNED_JOURNAL,
    PUBLICATION.TARGET_DATE_CONF,
    PUBLICATION.TARGET_DATE_JOURNAL,
    PUBLICATION.REF_CODE,
    PUBLICATION.SUB_CERN_APPROVAL,
    PUBLICATION.CERN_APPROVAL,
    PUBLICATION.COMMMENTS,
    PUBLICATION.JOURNAL_SUB,
    PUBLICATION.JOURNAL,
    PUBLICATION.CDS_URL,
    PUBLICATION.LEAD_GROUP,
    PUBLICATION.SEQUENCE_NUMBER,
    PUBLICATION.CREATION_DATE,
    PUBLICATION.STATUS,
    PUBLICATION.JOURNAL_PUBLICATION_URL,
    PUBLICATION.PAPER_REFERENCE,
    PUBLICATION.LUMINOSITY,
    PUBLICATION.LUMINOSITY_UNIT,
    PUBLICATION.LUMINOSITY_UNCERTAINTY,
    PUBLICATION.DATA_USED,
    PUBLICATION.PUBLICATION_RUN,
    PUBLICATION.JOURNAL_APPROVAL_DATE,
    PUBLICATION.ARXIV_URL,
    PUBLICATION.TRACKING_REF,
    PUBLICATION.REFEREE_REPORT_ON,
    PUBLICATION.PROOFS_ON,
    PUBLICATION.FINAL_TITLE_TEX,
    PUBLICATION.SUB_GROUP,
    PUBLICATION.FIGURES,
    PHASE_1.REVIEWERS_SIGN_OFF,
    PHASE_1.PRESENTATION_PG,
    PHASE_1.LPG_APPROVAL,
    PHASE_1.DRAFT_CDS_URL,
    PHASE_1.SIGN_OFF_DRAFT,
    PHASE_1.PRESENTATION_ATLAS_GEN,
    PHASE_1.STATE,
    PHASE_1.ATLAS_RELEASE,
    PHASE_1.PRESENTATION_ATLAS_URL,
    PHASE_1.REVIEWERS_APPROVAL,
    PHASE_1.PUBCOMM_SIGN_OFF,
    PHASE_1.PHYSCOORD_SIGN_OFF,
    PHASE_1.SUPPORT_DOCUMENTS_URL,
    PHASE_1.MAIL_TO_EDBOARD,
    PHASE_1.PGC_SIGN_OFF_DRAFT,
    PHASE_1.DRAFTCDSURL,
    PHASE_1.AUTHORLIST_ID,
    PHASE2.START_DATE,
    PHASE2.LPG_SIGN_OFF,
    PHASE2.OPEN_DISCUSSION_DATE,
    PHASE2.OPEN_DISCUSSION_INDICO_URL,
    PHASE2.RELEASE_ATLAS_COMMENTS,
    PHASE2.END_ATLAS_COMMENTS,
    PHASE2.FINAL_APPROVAL,
    PHASE2.FINAL_CDS_LINK,
    PHASE2.STATE,
    PHASE2.LPG_FINAL_SIGN_OFF,
    PHASE2.DRAFT_CDS_URL,
    PHASE2.LPG_APPROVAL,
    PHASE2.LOOP_SEQUENCE,
    PHASE2.PUBLIC_READING_INDICO_URL,
    PHASE2.PUBLIC_READING_DATE,
    PHASE2.PUBCOMM_SIGN_OFF,
    PHASE2.PUBCOMMCHAIR_SIGN_OFF,
    PHASE2.PHYSCOORD_SIGN_OFF,
    PHASE2.AUTHORLIST_ARCHIVED_ON,
    PHASE2.CERN_REFERENCE_NUMBER,
    PHASE2.EDBOARD_SIGNOFF_DATE,
    PHASE2.PUBCOMM_NAME,
    PHASE_1.SPOKESPERSON_NAME,
    PHASE2.SPOKESPERSON_SIGN_OFF,
    PHASE2.CERN_SIGN_OFF,
    SUBMISSION.RELEASE_CERN_COMMENTS,
    SUBMISSION.STATE,
    PHASE_SUBMISSION.INDICO_URL,
    PHASE_SUBMISSION.SIGN_OFF,
    PUBLICATION.HEP_DATA,
    PUBLICATION.RIVET
  ORDER BY PUBLICATION.REF_CODE,
    PUBLICATION.CREATION_DATE DESC
/

create force view ATLAS_AUTHDB.ALSOAT (MEMB_ID, FOOTNOTE, INST_FOOTNOTE_ATLAS, INST_FOOTNOTE_EXT) as
SELECT m.id ,
    (
    CASE
      WHEN inst_footnote_atlas IS NOT NULL
      THEN
        (SELECT 'Also at '
          || inst.address
          || ', '
          || country.name
        FROM inst,
          country
        WHERE inst.id       =inst_footnote_atlas
        AND inst.country_id = country.id
        )
      WHEN inst_footnote_ext IS NOT NULL
      THEN
        (SELECT 'Also at '
          || inst_ghost.name
          || ', '
          || inst_ghost.country
        FROM inst_ghost
        WHERE id=inst_footnote_ext
        )
      ELSE ''
    END) ,
    inst_footnote_atlas ,
    inst_footnote_ext
  FROM members_lastest_employ_record m
  WHERE inst_footnote_atlas IS NOT NULL
  OR inst_footnote_ext      IS NOT NULL
/

create force view ATLAS_AUTHDB.FLAG_COUNT as
SELECT INST.ID AS ID
  ,(select count(*) 
    FROM MEMBERS_LASTEST_EMPLOY_RECORD
    WHERE status = 'ACTIVE'
          AND MEMBERS_LASTEST_EMPLOY_RECORD.INST_ID = INST.ID) as MEMBERS
   ,(select count(authorlist)
    FROM FLAG_LISTS
    WHERE authorlist = 1
    AND FLAG_LISTS.inst_id = inst.id) as AUTHORLIST
   ,(select count(mo_list)
    FROM FLAG_LISTS
    WHERE mo_list = 1
    AND FLAG_LISTS.inst_id = inst.id) as MOLIST
   ,(select count(op_task)
    FROM FLAG_LISTS
    WHERE op_task = 1
    AND FLAG_LISTS.inst_id = inst.id) + (select count(op_task)
                                         FROM FLAG_LISTS
                                         WHERE op_task = 2
                                         AND FLAG_LISTS.inst_id = inst.id)*0.75 as OPTASK
   ,(select count(under_qual)
    FROM FLAG_LISTS
    WHERE under_qual = 1
    AND FLAG_LISTS.inst_id = inst.id) as UNDERQUAL
   ,(select count(left_institute)
    FROM FLAG_LISTS
    WHERE left_institute = 1
    AND FLAG_LISTS.inst_id = inst.id) as LEFTINST
FROM INST,
    MEMBERS_LASTEST_EMPLOY_RECORD,
    FLAG_LISTS
WHERE (MEMBERS_LASTEST_EMPLOY_RECORD.inst_id = inst.id)
    AND (FLAG_LISTS.inst_id = inst.id)
GROUP BY inst.id
/

create force view ATLAS_AUTHDB.FLAGS as
select m.id
      --,m.last_name
      --,m.first_name
      ,case when f.authorlist = 1     then 'A' end ||
       case when f.left_institute = 1 then 'a' end ||
       case when f.under_qual = 1     then 'q' end ||
       case when f.mo_list = 1        then 'M' end ||
       case when f.op_task = 1        then 'O' end ||
       case when f.op_task = 2        then 'o' end as composition
from members_lastest_employ_record m
    ,flag_lists f
    --,inst i
    --,funda fu
where m.id = f.memb_id
    and (m.status = 'ACTIVE' or f.left_institute = 1)
    --and m.inst_id = i.id
    --and i.funda_id = fu.id
    --and fu.id=34;
/

create force view ATLAS_AUTHDB.PROJ_RESP as
Select project.id as PROJECT_ID,
      project.description as PROJECT_NAME,
      project_appointment.appointment_id as APPOINTMENT_ID,
      (memb.first_name||' '||memb.last_name) as responsible,
      memb_appointment.start_date as start_date,
      memb_appointment.end_date as end_date
from project,
      project_appointment,
      memb_appointment,
      memb
where project.id = project_appointment.project_id
      and memb_appointment.appointment_id = project_appointment.appointment_id
      and memb.id = memb_appointment.memb_id
order by project.id,project_appointment.appointment_id
/

create force view ATLAS_AUTHDB.MEMB_RESP as
select (RECENT.first_name ||' '|| RECENT.last_name) as NAME,
        proj_resp.project_name as PROJECT,
        PROJ_RESP.responsible as RESPONSIBLE,
        RECENT.qual_start as QUALIFICATION_START,
        RECENT.qual_date as QUALIFICATION_DATE,
        RECENT.qual_end as QUALIFICATION_END,
        RECENT.qual_string COMMENTS
from (select * from memb where qual_start > to_date('01-01-2009', 'DD-MM-YYYY')) RECENT,
      proj_resp
where RECENT.qual_resp = proj_resp.project_id (+)
      and RECENT.qual_date between PROJ_RESP.start_date and PROJ_RESP.end_date
order by PROJECT,qualification_start
/

create force view ATLAS_AUTHDB.CURR_RESP as
select project_name as PROJECT, stragg(responsible) as RESPONSIBLE from proj_resp where end_date > SYSDATE  and start_date < SYSDATE group by project_name
/

create force view ATLAS_AUTHDB.DOWNGRADED as
select unique(nominations.ID) as ID,
        nominations.priority as NEW_PRIORITY,
        A.priority as OLD_PRIORITY,
        nominations.memb_id,
        (select first_name from memb where id = nominations.memb_id) as FIRST_NAME,
        (select last_name from memb where id = nominations.memb_id) as LAST_NAME,
        (select (first_name ||' '||last_name) from memb where cern_ccid = nominations.modified_by) as MODIFIED_BY
from (select * from nominations_hist_new where up_to > to_date('27/05/2013', 'dd/mm/yyyy')) A,
      nominations
where nominations.id = A.id
  and A.priority < nominations.priority
  and nominations.systems_id = 1
order by ID
/

create force view ATLAS_AUTHDB.SC_SUMMARY
            (ID, NAME, SHORT_NAME, LOCATION, WEBSITE, START_DATE, END_DATE, COMMENTS, CTE_NOTES, PROC_CONTACT,
             PROC_CONTACT_EMAIL, PROCINST_PUB_EXPDATE, PROCINST_PUB_DATE, SUBMIT_DEADLINE, PUBCOMM_PROC_CONTACT,
             EST_TALKS, CONF_STATUS, ABS_DEADLINE, APP_DEADLINE, CTE_DEADLINE, TALKS_NUMBER, TALKS_IDS, TASK1MEMBERS,
             TASK2MEMBERS, TASK1EMAILS, TASK2EMAILS, PRIORITY, TALKS_ACTUAL_NUMBER, TALKS_ACTUAL_POSTER_NUMBER)
as
SELECT CONF.ID, 
          CONF.NAME, 
          CONF.SHORT_NAME, 
          CONF.LOCATION, 
          CONF.WEBSITE, 
          CONF.START_DATE, 
          CONF.END_DATE, 
          CONF.COMMENTS, 
          CONF.CTE_NOTES, 
          CONF.PROC_CONTACT, 
          CONF.PROC_CONTACT_EMAIL, 
          CONF.PROCINST_PUB_EXPDATE, 
          CONF.PROCINST_PUB_DATE,
          CONF.SUBMIT_DEADLINE,
          CONF.PUBCOMM_PROC_CONTACT, 
          CONF.EST_TALKS,
          CONF.CONF_STATUS,
          CONF.ABS_DEADLINE,
          CONF.APP_DEADLINE,
          CONF.CTE_DEADLINE,
          (SELECT COUNT(TALK.ID)
            FROM TALK
            WHERE TALK.CONF_ID = CONF.ID
          ) AS TALKS_NUMBER,
          (SELECT
              stragg(ID)
            FROM
              TALK
            WHERE
              CONF_ID = CONF.ID
          ),
          stragg(DISTINCT TASK1.first_name
            || ' '
            || TASK1.last_name)                     AS Task1members,
          stragg(DISTINCT TASK2.first_name
            || ' '
            || TASK2.last_name)                     AS Task2members,
          stragg(DISTINCT ' ' || EMAIL1.EMAIL)       AS Task1Emails,
          stragg(DISTINCT ' ' || EMAIL2.EMAIL)       AS Task2Emails,
          CONF.PRIORITY,
          (SELECT COUNT(TALK.ID)
            FROM TALK
            WHERE TALK.CONF_ID = CONF.ID AND
            (TALK.STATUS != 6 AND
            TALK.STATUS != 7)
            AND TALK.TYPE != 3
          ) AS TALKS_ACTUAL_NUMBER,
          (SELECT COUNT(TALK.ID)
            FROM TALK
            WHERE TALK.CONF_ID = CONF.ID AND
            (TALK.STATUS != 6 AND
            TALK.STATUS != 7)
            AND TALK.TYPE = 3
          ) AS TALKS_ACTUAL_POSTER_NUMBER
  FROM CONF,
      MEMB TASK1,
        MEMBERS_LASTEST_EMPLOY_RECORD EMAIL1,
        (SELECT MEMB_ID, CONF_ID
          FROM MEMB_CONF
          WHERE MEMB_CONF.GROUP_ID = 'TASK1'
        ) TASK1MEMBERS,
        (SELECT ID, EMAIL
           FROM MEMBERS_LASTEST_EMPLOY_RECORD
        ) EMAIL1MEMBERS,
        MEMB TASK2,
        MEMBERS_LASTEST_EMPLOY_RECORD EMAIL2,
        (SELECT MEMB_ID, CONF_ID
          FROM MEMB_CONF
          WHERE MEMB_CONF.GROUP_ID = 'TASK2'
        ) TASK2MEMBERS,
        (SELECT ID, EMAIL
          FROM MEMBERS_LASTEST_EMPLOY_RECORD
        ) EMAIL2MEMBERS
  WHERE
      CONF.ID                = TASK1MEMBERS.CONF_ID (+)
    AND TASK1MEMBERS.MEMB_ID = TASK1.ID (+)
    AND TASK1MEMBERS.MEMB_ID = EMAIL1MEMBERS.ID (+)
    AND EMAIL1MEMBERS.ID = EMAIL1.ID (+)
    AND CONF.ID            = TASK2MEMBERS.CONF_ID (+)
    AND TASK2MEMBERS.MEMB_ID = TASK2.ID (+)
    AND TASK2MEMBERS.MEMB_ID = EMAIL2MEMBERS.ID (+)
    AND EMAIL2MEMBERS.ID = EMAIL2.ID (+)
  GROUP BY
  CONF.ID, 
          CONF.NAME, 
          CONF.SHORT_NAME, 
          CONF.LOCATION, 
          CONF.WEBSITE, 
          CONF.START_DATE, 
          CONF.END_DATE, 
          CONF.COMMENTS, 
          CONF.CTE_NOTES, 
          CONF.PROC_CONTACT, 
          CONF.PROC_CONTACT_EMAIL, 
          CONF.PROCINST_PUB_EXPDATE, 
          CONF.PROCINST_PUB_DATE,
          CONF.SUBMIT_DEADLINE,
          CONF.PUBCOMM_PROC_CONTACT, 
          CONF.EST_TALKS,
          CONF.CONF_STATUS,
          CONF.PRIORITY,
          CONF.ABS_DEADLINE,
          CONF.APP_DEADLINE,
          CONF.CTE_DEADLINE
  ORDER BY START_DATE DESC
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."QUALIFICATION_VIEW" ("ID", "QUAL_STATE", "QUAL_STRING", "QUAL_INTEGRATION", "PROPOSED_QUAL_START", "QUAL_RESP", "QUAL_START", "CHECKPOINT_WARNING_DATE", "LOCAL_ID", "LOCAL_NAME", "LOCAL_EMAIL", "TECH_ID", "TECH_NAME", "TECH_EMAIL", "CHECKPOINT_COMMENTS", "CHECKPOINT_DATE", "FINAL_REPORT_COMMENTS", "FINAL_REPORT_DATE", "QUAL_DATE") AS 
  SELECT m.id
  , m.qual_state
  , m.qual_string
  , m.qual_integration
  , m.proposed_qual_start
  , m.qual_resp
  , m.qual_start
  , ADD_MONTHS( TO_DATE( TO_CHAR( m.qual_start, 'YYYY-MM-DD' ), 'YYYY-MM-DD' ), 4 ) AS checkpoint_warning_date
  , l_supervisor.id
  , CASE WHEN l_supervisor.id IS NOT NULL THEN UPPER(l_supervisor.last_name) || ', '  || l_supervisor.first_name ELSE null END as local_name 
  , l_supervisor.email AS local_email
  , t_supervisor.id
  , CASE WHEN t_supervisor.id IS NOT NULL THEN UPPER(t_supervisor.last_name) || ', '  || t_supervisor.first_name ELSE null END as tech_name 
  , t_supervisor.email AS tech_email
  , m.checkpoint_comments
  , m.checkpoint_date
  , m.final_report_comments
  , m.final_report_date
  , m.qual_date
FROM memb m
  LEFT JOIN members_lastest_employ_record l_supervisor ON l_supervisor.id = m.local_supervisor
  LEFT JOIN members_lastest_employ_record t_supervisor ON t_supervisor.id = m.tech_supervisor
/

create force view ATLAS_AUTHDB.JSON_NOMINATION (ID, MEMB_ID, SYSTEMS_ID, OBJ) as
SELECT n.id
    , n.memb_id
    , n.systems_id, TO_CLOB(
    '{' ||
        '"id":'             || n.id || ',' ||
        '"comments":"' )    || TO_CLOB( REPLACE(REPLACE(n.comments, '"', '\"'),CHR(10),'\n') ) || TO_CLOB( '",' ||
        '"priority":'       || n.priority || ',' ||
        '"scab_priority":') || TO_CLOB( CASE WHEN n.scab_priority  IS NOT NULL THEN TO_CHAR( n.scab_priority )  ELSE 'null' END ) || TO_CLOB( ',' ||
        '"atlas_fraction":')|| TO_CLOB( CASE WHEN n.atlas_fraction IS NOT NULL THEN TO_CHAR( n.atlas_fraction ) ELSE 'null' END ) || TO_CLOB( ',' ||
        '"submitter":{' ||
            '"id":')          || TO_CLOB( submitter.id )         || TO_CLOB( ',' ||
            '"cern_ccid":')   || TO_CLOB( submitter.cern_ccid )  || TO_CLOB( ',' ||
            '"last_name":"')  || TO_CLOB( submitter.last_name )  || TO_CLOB( '",' ||
            '"first_name":"') || TO_CLOB( submitter.first_name ) || TO_CLOB( '"' ||
        '},' ||
        '"responsible":{' ||
            '"id":')      || TO_CLOB( responsible.id ) || TO_CLOB( ',' ||
            '"type":"')   || TO_CLOB( responsible.type )|| TO_CLOB( '",' ||
            '"name":"')   || TO_CLOB( responsible.name )|| TO_CLOB( '"' ||
        '}' ||
    '}' )
FROM nominations n
JOIN (
    SELECT id AS n_id
        , CASE WHEN ir_resp IS NOT NULL THEN ir_resp ELSE app_resp END AS id
        , CASE WHEN ir_resp IS NOT NULL THEN 'institute' ELSE 'appointment' END AS type
        , CASE WHEN ir_resp IS NOT NULL
            THEN (
                SELECT oname FROM inst WHERE inst.id = ir_resp
            ) ELSE (
                SELECT name FROM appointment WHERE id = app_resp
            )
        END AS name
    FROM nominations
) responsible ON responsible.n_id = n.id
JOIN memb submitter ON submitter.cern_ccid = n.submitted_by
/

create force view ATLAS_AUTHDB.JSON_MEMB as
SELECT m.id,
        TO_CLOB( '{' ) ||
            TO_CLOB( '"id":'             || m.id || ',' ) ||
            TO_CLOB( '"cern_ccid":'      || m.cern_ccid || ',' ) ||
            TO_CLOB( '"last_name":"'     || m.last_name || '",' ) ||
            TO_CLOB( '"first_name":"'    || m.first_name || '",' ) ||
            TO_CLOB( '"nominations":['   || jn.objs  || '],' ) ||
            TO_CLOB( '"appointments":['  || ja.objs  || '],' ) ||
            TO_CLOB( '"theses":['        || jt.objs  || '],' ) ||
            TO_CLOB( '"edboards":['      || jpe.objs || ']' ) ||
        TO_CLOB( '}' ) AS obj
FROM memb m
    LEFT JOIN (
        SELECT memb_id
          , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, obj || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS objs
        FROM json_nomination
        GROUP BY memb_id
    ) jn ON jn.memb_id = m.id
    LEFT JOIN (
        SELECT memb_id
          , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, obj || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS objs
        FROM json_appointment
        GROUP BY memb_id
    ) ja ON ja.memb_id = m.id
    LEFT JOIN (
        SELECT memb_id
          , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, obj || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS objs
        FROM json_thesis
        GROUP BY memb_id
    ) jt ON jt.memb_id = m.id
    LEFT JOIN (
        SELECT memb_id
          , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, obj || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS objs
        FROM json_paper_edboard
        GROUP BY memb_id
    ) jpe ON jpe.memb_id = m.id
/

create force view ATLAS_AUTHDB.CJSON_NOMINATION (MEMB_ID, LIST) as
SELECT n.memb_id, TO_CLOB(
        n.id || ',"' ) || TO_CLOB(
        REPLACE(REPLACE(n.comments, '"','\"'),CHR(10),'\n') ) || TO_CLOB('",' ||
        priority || ',' ) || TO_CLOB(
        CASE WHEN scab_priority  IS NOT NULL THEN TO_CHAR(scab_priority)  ELSE 'null' END || ',' ) || TO_CLOB(
        CASE WHEN atlas_fraction IS NOT NULL THEN TO_CHAR(atlas_fraction) ELSE 'null' END ||
        ',[' ) || TO_CLOB( --submitter
            submitter.id || ',"' || submitter.last_name || '","' || submitter.first_name ) || TO_CLOB( '"' || 
        '],[' ) || TO_CLOB( --submitter
            responsible.id || ',"' || responsible.type || '","' || responsible.name ) || TO_CLOB( '"' || 
        ']')
FROM nominations n
JOIN memb submitter ON submitter.cern_ccid = n.submitted_by
JOIN (
    SELECT id AS n_id
        , CASE WHEN ir_resp IS NOT NULL THEN ir_resp ELSE app_resp END AS id
        , CASE WHEN ir_resp IS NOT NULL THEN 'institute' ELSE 'appointment' END AS type
        , CASE WHEN ir_resp IS NOT NULL
            THEN (
                SELECT oname FROM inst WHERE inst.id = ir_resp
            ) ELSE (
                SELECT name FROM appointment WHERE id = app_resp
            )
        END AS name
    FROM nominations
) responsible ON responsible.n_id = n.id
/

create force view ATLAS_AUTHDB.CJSON_MEMB (ID, OBJ) as
SELECT m.id, TO_CLOB(
    '[{"metadata":[' ||
        '"id","cern_ccid","last_name","first_name","nominations",[' ||
            '"id","comments","priority","scab_priority","atlas_fraction","submitter",[' ||
                '"id","cern_ccid","last_name","first_name"' ||
            '],"responsible",[' ||
                '"id","type","name"' ||
            ']' ||
        ']' ||
    ']},{"results":[') || TO_CLOB(
        LISTAGG( '[' || m.id || ',' || m.cern_ccid || ',"' || m.last_name || '","' || m.first_name || '",[' || cjn.lists || ']', ',' ) WITHIN GROUP (ORDER BY m.id)
    ) ||
    TO_CLOB( ']]}]')
FROM memb m
    JOIN (
        SELECT cjson_nomination.memb_id
            , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, cjson_nomination.list || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS lists
        FROM cjson_nomination
        GROUP BY cjson_nomination.memb_id
    ) cjn ON cjn.memb_id = m.id
GROUP BY m.id
/

create force view ATLAS_AUTHDB.Q2 as
SELECT "ID","LAST_NAME","FIRST_NAME","INITIALS","CERN_ID","CERN_CCID","QUAL_DATE","QUAL_STRING","PRE_CREDIT","COMMENTS","LAST_NAME_LTX","FIRST_NAME_LTX","PHOTO","HR_IGNORE","QUAL_END","QUAL_START","QUAL_STATE","QUAL_RESP","PROPOSED_QUAL_START","MODIFIED_ON","MODIFIED_BY","UPDATE_EMAIL","STATUS_ATLAS_TEMP","EXCLUDE_PREDT","COMMENTS_PREDT","INSPIRE","STATUS_ATLAS","SSA","QUAL_FLAG","RPDTC","TDAQ_AUTHOR","QUAL_INTEGRATION","LOCAL_SUPERVISOR","TECH_SUPERVISOR","CHECKPOINT_DATE","CHECKPOINT_COMMENTS","FINAL_REPORT_DATE","FINAL_REPORT_COMMENTS" FROM memb WHERE qual_state > 2
/

create force view ATLAS_AUTHDB.PUBLICATION_AMI as
SELECT 'paper' AS type
,   p.id
,   p.creation_date
,   NULL AS temp_ref_code
,   p.ref_code
,   p.lead_group
,   DECODE(lp.phase, 1, 'Phase 1',
                     3, 'Phase 2',
                     4, 'Submission'
    ) AS phase
,   DECODE(phase_state.state, 'finished', 'finished', 'active') AS state
,   p.short_title
,   p.full_title
,   m.cern_ccid
,   m.first_name
,   m.last_name
,   mp.memb_function AS function
FROM (SELECT * FROM publication WHERE deleted = 0) p
JOIN phase lp ON lp.publication_id = p.id AND lp.phase = (SELECT MAX(phase) FROM phase WHERE publication_id = p.id)
JOIN (
    SELECT phase_id, state FROM phase_1
    UNION
    SELECT phase_id, state FROM phase_3
    UNION
    SELECT phase_id, state FROM submission
) phase_state ON phase_state.phase_id = lp.id
JOIN memb_publication mp ON mp.publication_id = p.id
JOIN memb m ON m.id = mp.memb_id

UNION

SELECT 'confnote' AS type
,   p.id
,   p.creation_date
,   p.temp_ref_code
,   p.ref_code
,   p.lead_group
,   DECODE(lp.phase, 1, 'Phase 1',
                     3, 'Phase 2',
                     4, 'Submission'
    ) AS phase
,   DECODE(phase_state.state, 'finished', 'finished', 'active') AS state
,   p.short_title
,   p.full_title
,   m.cern_ccid
,   m.first_name
,   m.last_name
,   mp.memb_function AS function
FROM (SELECT * FROM confnote_publication WHERE deleted = 0) p
JOIN confnote_phase lp ON lp.publication_id = p.id AND lp.phase = (SELECT MAX(phase) FROM confnote_phase WHERE publication_id = p.id)
JOIN (
    SELECT phase_id, state FROM confnote_phase_1
) phase_state ON phase_state.phase_id = lp.id
JOIN confnote_memb_publication mp ON mp.publication_id = p.id
JOIN memb m ON m.id = mp.memb_id
/

create force view ATLAS_AUTHDB.NOMINEE as
SELECT "ID","LAST_NAME","FIRST_NAME","INITIALS","CERN_ID","CERN_CCID","QUAL_DATE","QUAL_STRING","PRE_CREDIT","COMMENTS","LAST_NAME_LTX","FIRST_NAME_LTX","PHOTO","HR_IGNORE","QUAL_END","QUAL_START","QUAL_STATE","QUAL_RESP","PROPOSED_QUAL_START","MODIFIED_ON","MODIFIED_BY","UPDATE_EMAIL","STATUS_ATLAS_TEMP","EXCLUDE_PREDT","COMMENTS_PREDT","INSPIRE","STATUS_ATLAS","SSA","QUAL_FLAG","RPDTC","TDAQ_AUTHOR","QUAL_INTEGRATION","LOCAL_SUPERVISOR","TECH_SUPERVISOR","CHECKPOINT_DATE","CHECKPOINT_COMMENTS","FINAL_REPORT_DATE","FINAL_REPORT_COMMENTS","QUAL_ACTIVITY","AGENT","DELETED","ORCID" FROM memb WHERE id IN (SELECT DISTINCT(memb_id) FROM nominations)
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."OTP_ACTIVITIES_TODAY" ("ALLOCATED_PERSON_ID", "TASK_ID", "DATE", "ALLOCATED_FTE", "SHORT_TITLE", "CATEGORY_CODE") AS
SELECT ALLOCATED_PERSON_ID,
        TASK_ID,
        DT,
        SUM(ALLOCATED_FTE),
        SHORT_TITLE,
        CATEGORY_CODE
    FROM ATLAS_AUTHDB.otp_pub_sum_allocation_v,
        ATLAS_AUTHDB.OTP_PUB_TASK,
        MEMB
    WHERE
    memb.id = ATLAS_AUTHDB.otp_pub_sum_allocation_v.allocated_person_id
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.TASK_ID        = ATLAS_AUTHDB.OTP_PUB_TASK.ID
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.RECOGNITION_CODE = 'Duty'
    AND
    DT >= (CASE
    WHEN memb.qual_date >= ADD_MONTHS(SYSDATE, -(12 * 3)) THEN memb.qual_date
    WHEN memb.qual_date is null THEN TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) + 3), 'dd-mm-yyyy') -- non-authors must have otp = 0, forcing dt >= future and dt <= today
    ELSE TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy')
    END)
    AND DT <= SYSDATE
    GROUP BY ALLOCATED_PERSON_ID,
        DT,
        CATEGORY_CODE,
        TASK_ID,
        SHORT_TITLE
    ORDER BY CATEGORY_CODE,
        SHORT_TITLE,
        DT
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION_TODAY (ALLOCATED_PERSON_ID, CONTRIBUTION, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
    CASE
      WHEN ROUND(MAX(otp_value), 2) >= 0.1
      THEN 'Yes'
      ELSE 'No'
    END,
    round(max(otp_value),2)
  FROM
    (SELECT ALLOCATED_PERSON_ID,
      SUM(
      CASE
        WHEN (CATEGORY_CODE = 'Class 1'
        OR CATEGORY_CODE    = 'Class 2')
        THEN ALLOCATED_FTE*1.5
        WHEN (CATEGORY_CODE = 'Class 3')
        THEN ALLOCATED_FTE
        WHEN (CATEGORY_CODE = 'Class 4')
        THEN ALLOCATED_FTE
        ELSE 0
      END ) otp_value
    FROM OTP_ACTIVITIES_TODAY
    GROUP BY ALLOCATED_PERSON_ID
    )
  GROUP BY ALLOCATED_PERSON_ID, otp_value
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."OTP_ACTIVITIES_TODAY_ONLEAVE" ("ALLOCATED_PERSON_ID", "TASK_ID", "DATE", "ALLOCATED_FTE", "SHORT_TITLE", "CATEGORY_CODE") AS
SELECT ALLOCATED_PERSON_ID,
        TASK_ID,
        DT,
        SUM(ALLOCATED_FTE),
        SHORT_TITLE,
        CATEGORY_CODE
    FROM ATLAS_AUTHDB.otp_pub_sum_allocation_v,
        ATLAS_AUTHDB.OTP_PUB_TASK,
        MEMB
    WHERE
    memb.id = ATLAS_AUTHDB.otp_pub_sum_allocation_v.allocated_person_id
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.TASK_ID        = ATLAS_AUTHDB.OTP_PUB_TASK.ID
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.RECOGNITION_CODE = 'Duty'
    AND 
    DT >= (CASE 
    WHEN (memb.qual_date >= TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy') 
        AND (EXTRACT(YEAR FROM memb.qual_date)) < '2018')
        THEN memb.qual_date -- check ATGLANCE-2862
    WHEN (memb.qual_date >= TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy') 
        AND (EXTRACT(YEAR FROM memb.qual_date)) >= '2018')
        THEN memb.qual_start -- check ATGLANCE-2862
    WHEN memb.qual_date is null THEN TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) + 3), 'dd-mm-yyyy') -- non-authors must have otp = 0, forcing dt >= future and dt <= today 
    ELSE TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy')
    END)
    AND DT <= SYSDATE
    AND NOT EXISTS(
        SELECT 1 
        FROM employ_on_leave d 
        WHERE d.start_date <= dt
        AND   d.end_date   >= dt
        AND employ_id=(select employ_id from members_lastest_employ_record where id=allocated_person_id)
    )
    GROUP BY ALLOCATED_PERSON_ID,
        DT,
        CATEGORY_CODE,
        TASK_ID,
        SHORT_TITLE
    ORDER BY CATEGORY_CODE,
        SHORT_TITLE,
        DT
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION_TODAY_ONLEAVE (ALLOCATED_PERSON_ID, CONTRIBUTION, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
    CASE
      WHEN ROUND(MAX(otp_value), 2) >= 0.1
      THEN 'Yes'
      ELSE 'No'
    END,
    round(max(otp_value),2)
  FROM
    (SELECT ALLOCATED_PERSON_ID,
      SUM(
      CASE
        WHEN (CATEGORY_CODE = 'Class 1'
        OR CATEGORY_CODE    = 'Class 2')
        THEN ALLOCATED_FTE*1.5
        WHEN (CATEGORY_CODE = 'Class 3')
        THEN ALLOCATED_FTE
        WHEN (CATEGORY_CODE = 'Class 4')
        THEN ALLOCATED_FTE
        ELSE 0
      END ) otp_value
    FROM OTP_ACTIVITIES_TODAY_ONLEAVE
    GROUP BY ALLOCATED_PERSON_ID
    )
  GROUP BY ALLOCATED_PERSON_ID, otp_value
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION_UPGRADE (ALLOCATED_PERSON_ID, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
    round(max(otp_value),2)
  FROM
    (SELECT ALLOCATED_PERSON_ID,
      SUM(
      CASE
        WHEN (CATEGORY_CODE = 'Upgrade Construction')
        THEN ALLOCATED_FTE
        ELSE 0
      END ) otp_value
    FROM OTP_ACTIVITIES_TODAY_ONLEAVE
    GROUP BY ALLOCATED_PERSON_ID
    )
  GROUP BY ALLOCATED_PERSON_ID, otp_value
/

CREATE FORCE VIEW "ATLAS_AUTHDB"."OTP_ACTIVITIES_UPGRADE" ("ALLOCATED_PERSON_ID", "TASK_ID", "DATE", "ALLOCATED_FTE", "SHORT_TITLE", "CATEGORY_CODE") AS 
SELECT ALLOCATED_PERSON_ID,
        TASK_ID,
        DT,
        SUM(ALLOCATED_FTE),
        SHORT_TITLE,
        CATEGORY_CODE
    FROM ATLAS_AUTHDB.otp_pub_sum_allocation_v,
        ATLAS_AUTHDB.OTP_PUB_TASK,
        MEMB
    WHERE
    memb.id = ATLAS_AUTHDB.otp_pub_sum_allocation_v.allocated_person_id
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.TASK_ID        = ATLAS_AUTHDB.OTP_PUB_TASK.ID
    AND ATLAS_AUTHDB.otp_pub_sum_allocation_v.RECOGNITION_CODE = 'Duty'
    AND CATEGORY_CODE = 'Upgrade Construction'
    AND 
    DT >= (CASE 
    WHEN (memb.qual_start >= TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy') 
        AND memb.qual_start >= TO_DATE('01-01-2018', 'dd-mm-yyyy'))
        THEN memb.qual_start
    WHEN (TO_DATE('01-01-2018', 'dd-mm-yyyy') >= TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy'))
        THEN TO_DATE('01-01-2018', 'dd-mm-yyyy')
    WHEN memb.qual_date is null THEN TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) + 3), 'dd-mm-yyyy') -- non-authors must have otp = 0, forcing dt >= future and dt <= today 
    ELSE TO_DATE('01-01-' || (EXTRACT(YEAR FROM SYSDATE) - 3), 'dd-mm-yyyy')
    END)
    AND DT <= SYSDATE
    AND NOT EXISTS(
        SELECT 1 
        FROM employ_on_leave d 
        WHERE d.start_date <= dt
        AND   d.end_date   >= dt
        AND employ_id=(select employ_id from members_lastest_employ_record where id=allocated_person_id)
    )
    GROUP BY ALLOCATED_PERSON_ID,
        DT,
        CATEGORY_CODE,
        TASK_ID,
        SHORT_TITLE
    ORDER BY CATEGORY_CODE,
        SHORT_TITLE,
        DT
/

create force view ATLAS_AUTHDB.V_MEMBER_QUAL_OTP_TASK as
SELECT QOT.ID, MEMB_ID AS MEMBER_ID, OTP_TASK_ID, OTP_REQUIREMENT_ID, QUAL_DATE, TO_NUMBER('0.24') AS OTP_VALUE
    FROM QUAL_OTP_TASK QOT
        JOIN MEMB M ON M.ID = QOT.MEMB_ID
    ORDER BY QOT.ID
/

create force view ATLAS_AUTHDB.V_INSTITUTE_NOMINATIONS as
SELECT IT_NOM.MEMBER_ID AS member_id
    , IT_NOM.SYSTEM_ID AS system_id
    , N.ID AS nomination_id
    , COALESCE(N.COMMENTS, '-') AS comments
    , N.PRIORITY AS priorities
    , NVL(TRIM(N.ATLAS_FRACTION), -1) AS atlas_fractions
    , TO_CHAR(n.submission_date, 'yyyy-mm-dd') AS submission_dates
    , TO_CHAR(n.modified_on, 'yyyy-mm-dd') AS modified_on_dates
    , COALESCE(TO_CHAR(P.ID), '0') AS advancements_ids
    , COALESCE(TO_CHAR(P.DESCRIPTION), 'No Professional Advancement / Not applicable') AS advancements
    , C.DESCRIPTION AS categories
    , T.DESCRIPTION AS types
    , IT_NOM.INSTITUTE_ID AS institutes_ids
    , I.ONAME AS institutes_onames
    , s.id AS submitters_ids
    , s.cern_ccid AS submitters_ccids
    , s.last_name AS submitters_lastnames
    , s.first_name AS submitters_firstnames
    , m.id AS modifiers_ids
    , m.cern_ccid AS modifiers_ccids
    , m.last_name AS modifiers_lastnames
    , m.first_name AS modifiers_firstnames
    , COALESCE(sub.ids, '-') AS sub_ids
    , COALESCE(sub.names, '-') AS sub_names
    , COALESCE(sub.parents_ids, '-') AS parents_ids
    , COALESCE(sub.parents_names, '-') AS parents_names
    FROM INSTITUTE_NOMINATION IT_NOM
    JOIN NOM_NOMINATION N ON N.ID = IT_NOM.NOMINATION_ID
    JOIN NOM_CATEGORY C ON C.ID = N.CATEGORY_ID
    JOIN NOM_TYPE T ON T.ID = N.TYPE_ID
    JOIN memb s ON s.id = n.SUBMITTER_ID
    JOIN memb m ON m.id = n.AGENT_ID
    JOIN INST I ON i.id = it_nom.institute_id
    LEFT JOIN PROF_ADVANCE P ON P.ID = N.PROFESSIONAL_ADVANCEMENT_ID
    LEFT JOIN (
        SELECT na.nomination_id
        ,   LISTAGG( NVL(TO_CHAR(child.id), '-'), ';') WITHIN GROUP (ORDER BY child.id) AS ids
        ,   LISTAGG( NVL(TO_CHAR(child.name), '-'), ';') WITHIN GROUP (ORDER BY child.id) AS names
        ,   LISTAGG( NVL(TO_CHAR(parent.id),   '-'), ';') WITHIN GROUP (ORDER BY child.id) AS parents_ids
        ,   LISTAGG( NVL(TO_CHAR(parent.name), '-'), ';') WITHIN GROUP (ORDER BY child.id) AS parents_names
        FROM NOM_ADDITIONAL_SUBJECT na
        JOIN activity child ON child.id = na.activity_id
        LEFT JOIN activity_hierarchy ah ON ah.activity_id = child.id
        LEFT JOIN activity parent ON parent.id = ah.parent_activity_id
        GROUP BY na.nomination_id
        ) sub ON sub.nomination_id = n.id
    ORDER BY MEMBER_ID
/

create force view ATLAS_AUTHDB.V_ACTIVITY_NOMINATIONS as
SELECT A.member_id AS member_id
    , A.SYSTEM_ID AS system_id
    , N.ID AS nomination_id
    , COALESCE(N.COMMENTS, '-') AS comments
    , N.PRIORITY AS priorities
    , TO_CHAR(n.submission_date,'yyyy-mm-dd') AS submission_dates
    , TO_CHAR(n.modified_on,'yyyy-mm-dd') AS modified_on_dates
    , CASE
        WHEN U.ID IS NOT NULL
        THEN 'Y'
        ELSE 'N'
    END AS upgrades
    , CASE
        WHEN IUO.ID IS NOT NULL
        THEN 'Y'
        ELSE 'N'
      END AS ignore_otp_upgrades
    , C.DESCRIPTION AS categories
    , T.DESCRIPTION AS types
    , A.ACTIVITY_ID AS activities_ids
    , AT.NAME AS activities_names
    , s.id AS submitters_ids
    , s.cern_ccid AS submitters_ccids
    , s.last_name AS submitters_lastnames
    , s.first_name AS submitters_firstnames
    , m.id AS modifiers_ids
    , m.cern_ccid AS modifiers_ccids
    , m.last_name AS modifiers_lastnames
    , m.first_name AS modifiers_firstnames
    , COALESCE(sub.ids, '-') AS sub_ids
    , COALESCE(sub.names, '-') AS sub_names
    , COALESCE(sub.parents_ids, '-') AS parents_ids
    , COALESCE(sub.parents_names, '-') AS parents_names
    , COALESCE(seed.seed_activities, '-') AS seed_activities
    , COALESCE(seed.seed_priorities, '-') AS seed_priorities
    FROM ACTIVITY_NOMINATION A
    JOIN NOM_NOMINATION N ON N.ID = A.NOMINATION_ID
    JOIN memb s ON s.id = n.SUBMITTER_ID
    JOIN memb m ON m.id = n.AGENT_ID
    JOIN NOM_CATEGORY C ON C.ID = N.CATEGORY_ID
    JOIN NOM_TYPE T ON T.ID = N.TYPE_ID
    JOIN ACTIVITY AT ON AT.ID = A.ACTIVITY_ID
    JOIN AT_ACTIVITY_ACTION AC ON AC.ACTIVITY_ID = AT.ID AND AC.ACTION_ID = 2
    LEFT JOIN NOM_UPGRADE U ON U.NOMINATION_ID = N.ID
    LEFT JOIN NOM_IGNORE_UPGRADE_OTP IUO ON IUO.NOM_UPGRADE_ID = U.ID
    LEFT JOIN (
        SELECT ns.nomination_id
        ,   LISTAGG( NVL(TO_CHAR(child.id), '-'), ';'  ) WITHIN GROUP (ORDER BY child.id) AS ids
        ,   LISTAGG( NVL(TO_CHAR(child.name), '-'), ';' ) WITHIN GROUP (ORDER BY child.id) AS names
        ,   LISTAGG( NVL(TO_CHAR(parent.id),   '-'), ';' ) WITHIN GROUP (ORDER BY child.id) AS parents_ids
        ,   LISTAGG( NVL(TO_CHAR(parent.name), '-'), ';' ) WITHIN GROUP (ORDER BY child.id) AS parents_names
        FROM (
                SELECT DISTINCT na.activity_id, na.nomination_id
                    FROM nom_additional_subject na
                UNION
                SELECT DISTINCT pmss.activity_id, pmss.nomination_id
                    FROM pc_memb_scab_subject pmss) ns
        JOIN activity child ON child.id = ns.activity_id
        LEFT JOIN activity_hierarchy ah ON ah.activity_id = child.id
        LEFT JOIN activity parent ON parent.id = ah.parent_activity_id
        GROUP BY ns.nomination_id
        ) sub ON sub.nomination_id = n.id
    LEFT JOIN V_ACTIVITY_SEEDING_NOMINATIONS SEED ON N.ID = SEED.NOMINATION_ID
    ORDER BY A.member_id
/

create force view ATLAS_AUTHDB.V_ME_PERSONAL_INPUT
            (MEMBER_ID, LAST_MODIFIED, BLACK_DATES, OTHER_CONSTRAINTS, APPLYING_FOR_JOB) as
SELECT
        MEMB.ID,
        MAX(PILM.GLOBAL_MODIFIED_ON),
        MTB.BLACK_DATES,
        MOC.COMMENTS,
        CASE WHEN MAFJ.MEMBER_ID IS NOT NULL THEN 'Y' ELSE 'N' END
    FROM MEMB LEFT JOIN
    (
        SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
        FROM ME_OTHER_CONSTRAINT
        GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_APPLYING_FOR_A_JOB
            GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_TALK_BLACKDATE
            GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_TALK_PREFERRED_TOPIC
            GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_VETOED_CONF_PRIORITY
            GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_VETOED_TALK_TYPE
            GROUP BY MEMBER_ID
        UNION
            SELECT MEMBER_ID, MAX(MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_PREFERRED_CONF
            GROUP BY MEMBER_ID
        UNION
            SELECT CONF.MEMBER_ID, MAX(TOPIC.MODIFIED_ON) AS GLOBAL_MODIFIED_ON
            FROM ME_PREFERRED_CONF_TOPIC TOPIC
            LEFT JOIN ME_PREFERRED_CONF CONF ON CONF.id = TOPIC.PREFERRED_CONF_ID
            GROUP BY CONF.MEMBER_ID
    ) PILM ON PILM.MEMBER_ID = MEMB.ID LEFT JOIN
    (
        SELECT
            MEMBER_ID,
            LISTAGG(START_DATE || ' ' || END_DATE, '|') WITHIN GROUP (ORDER BY MEMBER_ID) AS BLACK_DATES
        FROM ME_TALK_BLACKDATE
        GROUP BY MEMBER_ID
    ) MTB ON MTB.MEMBER_ID = MEMB.ID LEFT JOIN
    ME_OTHER_CONSTRAINT MOC ON MOC.MEMBER_ID = MEMB.ID LEFT JOIN
    ME_APPLYING_FOR_A_JOB MAFJ ON MAFJ.MEMBER_ID = MEMB.ID
    GROUP BY MEMB.ID, MTB.BLACK_DATES, MOC.COMMENTS, MAFJ.MEMBER_ID
WITH READ ONLY
/

create force view ATLAS_AUTHDB.V_ANA_VALID_IDS_FOR_NOMINATION as
SELECT ANA.ID FROM ANALYSIS_SYS ANA
INNER JOIN ANALYSIS_SYS_FUNCTION ANSF ON ANA.ID = ANSF.PUBLICATION_ID
INNER JOIN ANALYSIS_SYS_MEMB_FUNCTION ANSMF ON ANSMF.FUNCTION_ID = ANSF.ID
INNER JOIN MEMB M ON M.ID = ANSMF.MEMB_ID
WHERE (
        ANA.STATUS NOT IN ('phase0_closed')
        OR ANA.ID IN (
            SELECT UNIQUE ana.id
            FROM analysis_sys ana
                     JOIN publication p ON p.analysis_id = ana.id
                     LEFT JOIN phase sub ON sub.publication_id = p.id
            WHERE p.deleted <> 1
              AND (p.id NOT IN (
                SELECT p.id
                FROM publication p
                         JOIN phase ph ON p.id = ph.publication_id
                    AND ph.phase = 4
            )
                OR (sub.phase = 4
                        AND sub.start_date BETWEEN ADD_MONTHS(SYSDATE, -6) AND SYSDATE
                    OR sub.start_date IS NULL)
                )
        )
        OR ANA.ID IN (
            SELECT ana.id
            FROM analysis_sys ana
                     JOIN confnote_publication p ON p.analysis_id = ana.id
                     JOIN confnote_phase cp ON cp.publication_id = p.id
                     JOIN confnote_phase_1 cp1 ON cp1.phase_id = cp.id
                AND cp1.sign_off_2 BETWEEN ADD_MONTHS(SYSDATE, -6) AND SYSDATE
        )
        OR ANA.ID IN (
            SELECT ana.id
            FROM analysis_sys ana
                     JOIN pubnote_publication p ON p.analysis_id = ana.id
                     JOIN pubnote_phase pp ON pp.publication_id = p.id
                     JOIN pubnote_phase_1 pp1 ON pp1.phase_id = pp.id
                AND pp1.sign_off_2 BETWEEN ADD_MONTHS(SYSDATE, -6) AND SYSDATE
        )
    )
    AND ANA.deleted <> 1
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION_HGTD (ALLOCATED_PERSON_ID, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
    round(max(otp_value),2)
  FROM
    (SELECT ALLOCATED_PERSON_ID,
      SUM(
      CASE
        WHEN (CATEGORY_CODE = 'Upgrade Construction' AND short_title like '%HGTD%')
        THEN ALLOCATED_FTE
        ELSE 0
      END ) otp_value
    FROM OTP_ACTIVITIES_TODAY_ONLEAVE
    GROUP BY ALLOCATED_PERSON_ID
    )
  GROUP BY ALLOCATED_PERSON_ID, otp_value
/

create force view ATLAS_AUTHDB.OTP_CONTRIBUTION_HGTD_2020 (ALLOCATED_PERSON_ID, ROUND_OTP) as
SELECT ALLOCATED_PERSON_ID,
ROUND(MAX(OTP_VALUE),2)
FROM
(
    SELECT ALLOCATED_PERSON_ID,
        SUM(
            CASE
            WHEN (CATEGORY_CODE = 'Upgrade Construction' AND short_title like '%HGTD%' AND "DATE" > '01/01/2020' AND "DATE" < '31/12/2020')
            THEN ALLOCATED_FTE
            ELSE 0
            END
        ) OTP_VALUE
    FROM OTP_ACTIVITIES_TODAY_ONLEAVE
    GROUP BY ALLOCATED_PERSON_ID
)
GROUP BY ALLOCATED_PERSON_ID, OTP_VALUE
/

create force view ATLAS_AUTHDB.V_ABSTRACT as
SELECT ABS.ID                                                       AS ID,
       ABS.YEAR                                                     AS YEAR,
       ABS.SEQUENCE                                                 AS SEQUENCE,
       ABS.TITLE                                                    AS TALK_TITLE,
       ABS.ABSTRACT                                                 AS TALK_ABSTRACT,
       ABS.COMMENTS                                                 AS OTHER_COMMENTS,
       ABS.CREATION_DATE                                            AS CREATION_DATE,
       ABS.GROUP_ID                                                 AS GROUP_ID,
       ABS.CREATOR_ID                                               AS CREATOR_ID,
       MEMB.FIRST_NAME                                              AS CREATOR_FIRST_NAME,
       MEMB.LAST_NAME                                               AS CREATOR_LAST_NAME,
       CASE WHEN ABR.ABSTRACT_ID IS NULL THEN 'N' ELSE 'Y' END      AS RETIREMENT_FLAG,
       CASE WHEN ABSF.ABSTRACT_ID IS NULL THEN 'N' ELSE 'Y' END     AS SELECTION_FEEDBACK_FLAG,
       LG.DESCRIPTION                                               AS GROUP_DESCRIPTION,
       ABK.ABSTRACT_KEYWORDS_IDS                                    AS ABSTRACT_KEYWORDS_IDS,
       ABK.ABSTRACT_KEYWORDS_NAMES                                  AS ABSTRACT_KEYWORDS_NAMES,
       ABC.ABSTRACT_CONFERENCES_IDS                                 AS ABSTRACT_CONFERENCES_IDS,
       ABC.ABSTRACT_CONFERENCES_NAMES                               AS ABSTRACT_CONFERENCES_NAMES,
       ABA.ABSTRACT_ANALYSES_IDS                                    AS ABSTRACT_ANALYSES_IDS,
       ABA.ABSTRACT_ANALYSES_REF_CODES                              AS ABSTRACT_ANALYSES_REF_CODES,
       ABP.ABSTRACT_PAPERS_IDS                                      AS ABSTRACT_PAPERS_IDS,
       ABP.ABSTRACT_PAPERS_REF_CODES                                AS ABSTRACT_PAPERS_REF_CODES,
       ABCN.ABSTRACT_CONFNOTES_IDS                                  AS ABSTRACT_CONFNOTES_IDS,
       ABCN.ABSTRACT_CONFNOTES_REF_CODES                            AS ABSTRACT_CONFNOTES_REF_CODES,
       ABPN.ABSTRACT_PUBNOTES_IDS                                   AS ABSTRACT_PUBNOTES_IDS,
       ABPN.ABSTRACT_PUBNOTES_REF_CODES                             AS ABSTRACT_PUBNOTES_REF_CODES,
       TYPE.ID                                                      AS TYPE_ID,
       TYPE.NAME                                                    AS TYPE_NAME
FROM ABS_ABSTRACT ABS
         JOIN LEADGROUPS LG ON LG.GROUP_ID = ABS.GROUP_ID
         JOIN MEMB ON MEMB.ID = ABS.CREATOR_ID
         LEFT JOIN ABS_ABSTRACT_RETIREMENT ABR ON ABR.ABSTRACT_ID = ABS.ID
         LEFT JOIN ABS_SELECTION_FEEDBACK ABSF ON ABSF.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_KEYWORD ABK ON ABK.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_CONFERENCE ABC ON ABC.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_ANALYSIS ABA ON ABA.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_PAPER ABP ON ABP.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_CONFNOTE ABCN ON ABCN.ABSTRACT_ID = ABS.ID
         JOIN V_ABSTRACT_PUBNOTE ABPN ON ABPN.ABSTRACT_ID = ABS.ID
         JOIN ABS_ABSTRACT_TYPE TYPE ON TYPE.ID = ABS.TYPE_ID
/

create force view ATLAS_AUTHDB.EMPLOY as
SELECT
    ME.ID,
    ME.MEMB_ID,
    ME.INST_ID,
    ME.START_DATE,
    ME.END_DATE,
    ME.DEATH_DATE,
    ME.INST_PHONE,
    M.CERN_PHONE,
    M.CERN_MOBILE,
    M.OFFICE,
    ME.EMAIL,
    ME.STUDENT,
    ME.ACTIVITY,
    ME.PTYPE,
    ME.MOFREE,
    ME.SUBDET,
    ME.FOOTNOTE,
    ME.AFLFLAG,
    ME.MAILFLAG,
    ME.COMMENTS,
    ME.MODIFIED_ON,
    ME.MODIFIED_BY,
    ME.INST_FOOTNOTE_ATLAS,
    ME.INST_FOOTNOTE_EXT,
    ME.MAILFLAG_STARTDATE,
    ME.MAILFLAG_ENDDATE,
    ME.FUNDA_ID,
    ME.AGENT,
    ME.ONLEAVE_STARTDATE,
    ME.ONLEAVE_ENDDATE,
    ME.FOOTNOTE_ENDDATE,
    ME.FOOTNOTE_STARTDATE
FROM ME_EMPLOYMENT ME
JOIN MEMB M ON M.ID = ME.MEMB_ID
/

create force view ATLAS_AUTHDB.INSTITUTES_REP as
SELECT
    INST.ID AS INSTITUTE_ID,
    FUNDA.ID AS FUNDA_ID,
    INSTITUTION_TABLE.ID AS INSTITUTION_ID,
    -- Institute Representative
    stragg(DISTINCT INST_REP_MEMB.first_name || ' ' || INST_REP_MEMB.LAST_NAME) AS INSTITUTE_REP,
    stragg(DISTINCT INST_REP_EMPLOY.EMAIL) AS INSTITUTE_REP_EMAIL,
    stragg(DISTINCT INST_REP_MEMB.CERN_CCID) AS INSTITUTE_REP_CCID,
    -- NCP Representative
    stragg(DISTINCT NCP_MEMB.first_name || ' ' || NCP_MEMB.LAST_NAME) AS NCP_REP,
    stragg(DISTINCT NCP_EMPLOY.EMAIL) AS NCP_REP_EMAIL,
    -- Institution Representative
    CASE
        WHEN (stragg(DISTINCT INSTU_MEMB.first_name) IS NOT NULL)
        THEN stragg(DISTINCT INSTU_MEMB.first_name || ' ' || INSTU_MEMB.LAST_NAME)
        ELSE stragg(DISTINCT INST_REP_MEMB.first_name || ' ' || INST_REP_MEMB.LAST_NAME)
    END AS INSTITUTION_REP,
    COALESCE(stragg(DISTINCT INSTU_EMPLOY.EMAIL), stragg(DISTINCT INST_REP_EMPLOY.EMAIL)) AS INSTITUTION_REP_EMAIL,
    CASE
        WHEN INST.AFF_TYPE = 'REGULAR'
        THEN 'Institute/Institution'
        WHEN INST.AFF_TYPE = 'CLUSTER'
        THEN 'Cluster Institution'
        WHEN INST.AFF_TYPE = 'CLUSTER_MEMBER'
        THEN 'Institute inside a cluster'
        WHEN INST.AFF_TYPE = 'ASSOCIATED'
        THEN 'Sub-institute'
        ELSE 'Undefined'
    END AS INSTITUTE_CATEGORY
FROM
    INST,
    FUNDA,
    INST CLUSTER_TABLE,
    INST INSTITUTION_TABLE,
    (
        SELECT *
        FROM INSTITUTE_REP
        WHERE TYPE = 'ATLAS'
        OR TYPE  = 'DEP'
    ) INST_REP,
    MEMB INST_REP_MEMB,
    EMPLOY INST_REP_EMPLOY,
    NCP_REP NCP_REP,
    MEMB NCP_MEMB,
    EMPLOY NCP_EMPLOY,
    (
        SELECT *
        FROM INSTITUTE_REP
        WHERE TYPE = 'ATLAS'
        OR TYPE  = 'DEP'
    ) INSTU_REP,
    MEMB INSTU_MEMB,
    EMPLOY INSTU_EMPLOY
WHERE INST.ID = INST_REP.INST_ID (+)
AND INST_REP.MEMB_ID = INST_REP_MEMB.ID (+)
AND INST_REP_MEMB.ID = INST_REP_EMPLOY.MEMB_ID (+)
AND INST_REP_EMPLOY.END_DATE (+) >= SYSDATE
AND INST_REP_EMPLOY.START_DATE (+) <= SYSDATE
AND FUNDA.ID = NCP_REP.FUNDA_ID (+)
AND NCP_REP.MEMB_ID = NCP_MEMB.ID (+)
AND NCP_MEMB.ID = NCP_EMPLOY.MEMB_ID (+)
AND NCP_EMPLOY.END_DATE (+) >= SYSDATE
AND NCP_EMPLOY.START_DATE (+) <= SYSDATE
AND INSTITUTION_TABLE.ID = INSTU_REP.INST_ID (+)
AND INSTU_REP.MEMB_ID = INSTU_MEMB.ID (+)
AND INSTU_MEMB.ID = INSTU_EMPLOY.MEMB_ID (+)
AND INSTU_EMPLOY.END_DATE (+) >= SYSDATE
AND INSTU_EMPLOY.START_DATE (+) <= SYSDATE
AND INST.FUNDA_ID = FUNDA.ID
AND CLUSTER_TABLE.ID (+) = INST.CLUSTER_ID
AND INSTITUTION_TABLE.ID (+) = CLUSTER_TABLE.CLUSTER_ID
GROUP BY
    INST.ID,
    FUNDA.ID,
    INSTITUTION_TABLE.ID,
    INST.AFF_TYPE
/

create force view ATLAS_AUTHDB.MEMB_SHORT as
SELECT
        m.id,
        LAST_NAME,
        FIRST_NAME,
        CERN_CCID,
        EMAIL,
        i.NAME AS institute_name,
        i.ID   AS inst_id,
        M.OFFICE,
        PHONE,
        M.CERN_PHONE,
        M.CERN_MOBILE,
        (
        CASE
            WHEN end_date >= sysdate
            THEN 'ACTIVE'
            ELSE 'INACTIVE'
        END) AS status,
        STATUS_ATLAS,
        country.name AS country,
        m.qual_date,
        m.qual_end,
        e.end_date,
        m.pre_credit,
        m.exclude_predt
    FROM
        employ e,
        memb m,
        inst i,
        country
    WHERE
        start_date =
        (SELECT
            MAX (start_date)
        FROM
            employ
        WHERE
            memb_id = e.memb_id
            AND start_date < SYSDATE
        )
    AND e.memb_id    = m.id (+)
    AND e.INST_ID    = i.id (+)
    AND i.country_id = country.id (+)
    ORDER BY
        memb_id
/

create force view ATLAS_AUTHDB.ACTIVE_MEMBER_WITH_PARENT_INST as
SELECT memb.id AS memb_id,
    employ.inst_id,
    possible_institutes.parent_id AS inst_parent_id,
    memb.first_name,
    memb.last_name,
    memb.qual_date,
    employ.ptype,
    employ.email,
    employ.activity,
    atlas_members_cerndb.sex,
    country.name AS country,
    inst.oname,
    flag_lists.authorlist AS flag_authorlist,
    flag_lists.under_qual AS flag_under_qual
  FROM memb,
    atlas_members_cerndb,
    employ,
    (SELECT inst.id AS inst_id,
      inst.id       AS parent_id
    FROM inst
    WHERE (id      = cluster_id
    AND cluster_id < 1000)
    OR (id        <> cluster_id
    AND cluster_id > 1000)
    UNION
    SELECT id    AS inst_id,
      cluster_id AS parent_id
    FROM inst
    WHERE (id     <> cluster_id
    AND cluster_id < 1000)
    ) possible_institutes,
    inst,
    country,
    flag_lists
  WHERE atlas_members_cerndb.person_id = memb.cern_ccid
  AND employ.memb_id                   = memb.id
  AND employ.start_date                =
    (SELECT MAX (e.start_date)
    FROM employ e
    WHERE e.start_date < SYSDATE
    AND e.memb_id      = memb.id
    )
  AND employ.end_date            >= SYSDATE
  AND possible_institutes.inst_id = employ.inst_id
  AND inst.id                     = employ.inst_id
  AND country.id                  = inst.country_id
  AND flag_lists.memb_id          = memb.id
/

create force view ATLAS_AUTHDB.USERGROUP_CONTACT (GROUP_ID, DESCRIPTION, NAME, SYSTEMS_ID, CONTACT, CERN_CCID) as
SELECT DISTINCT usergroup.group_id,
    usergroup.description,
    usergroup.name,
    usergroup.systems_id,
    (
    CASE
      WHEN group_id = 'INSTITUTE_REP'
      THEN INSTITUTE_REP_EMAIL
      WHEN group_id = 'INSTITUTION_REP'
      THEN INSTITUTION_REP_EMAIL
      WHEN group_id = 'NCP'
      THEN NCP_REP_EMAIL
      ELSE usergroup.contact
    END),
    m.cern_ccid
  FROM usergroup,
    members_lastest_employ_record m,
    institutes_rep
  WHERE m.inst_id = institutes_rep.institute_id
/

create force view ATLAS_AUTHDB.CDS_MEMBERS_VIEW as
select  
        memb.first_name,
        memb.last_name,
        memb.first_name_ltx,
        memb.last_name_ltx,
        memb.initials,
        memb.cern_ccid,
        memb.inspire,
        inst.name as institute,
        e.email
from  memb,
      employ e,
      inst
where
      memb.id = e.memb_id and
      inst.id = e.inst_id and
      e.start_date = (select max(start_date) from employ where memb_id = e.memb_id)
/

create force view ATLAS_AUTHDB.JSON_CDS_AUTHORS (QUERY, OBJ) as
SELECT m.first_name || ' ' || m.last_name,
    TO_CLOB( '{' ) ||
    TO_CLOB( '"id":'             || m.id || ',' ) ||
    TO_CLOB( '"cern_ccid":'      || m.cern_ccid || ',' ) ||
    TO_CLOB( '"last_name":"'     || m.last_name || '",' ) ||
    TO_CLOB( '"first_name":"'    || m.first_name || '",' ) ||
    TO_CLOB( '"affiliations":['  || affiliations.objs  || ']' ) ||
    TO_CLOB( '}' ) AS obj
FROM memb m
JOIN (
    SELECT aff.memb_id
      , DBMS_XMLGEN.CONVERT(RTRIM(XMLAGG(XMLELEMENT( E, obj || ',' )).EXTRACT('//text()').getClobVal(), ','),1) AS objs
    FROM (
        SELECT memb_id
          , type
          , id
        FROM
        (
            SELECT le.memb_id AS memb_id
                , 'atlas' AS type
                , le.inst_id AS id
                , 1 AS rank
            FROM employ le
            WHERE le.start_date = (SELECT MAX(start_date) FROM employ WHERE memb_id = le.memb_id)
        
            UNION
        
            SELECT ai.memb_id AS memb_id
                , CASE WHEN ai.inst_id IS NOT NULL THEN 'atlas' ELSE 'ghost' END AS type
                , CASE WHEN ai.inst_id IS NOT NULL THEN ai.inst_id ELSE inst_ghost_id END AS id
                , 2 AS rank
            FROM additional_inst ai
        )
        ORDER BY rank
    ) aff JOIN json_institute ji ON ji.id = aff.id AND ji.type = aff.type
    GROUP BY aff.memb_id
) affiliations ON affiliations.memb_id = m.id
/

create force view ATLAS_AUTHDB.TALK_DUMP as
SELECT tst.description status, t.status status_id, t.id talk_id, t.conf_id conf_id, cat.name category, t.category category_id, 
t.speaker_id speaker_id, amc.sex speaker_gender, pt.id speaker_ptype_id, pt.description speaker_ptype, i.name institute, e.inst_id inst_id, tt.label type, t.type type_id, ts.description scope, 
t.scope scope_id, t.talk_duration talk_duration, t.question_duration question_duration
FROM talk t
JOIN conf c ON t.conf_id = c.id
JOIN employ e ON e.memb_id = t.speaker_id AND e.end_date >= c.start_date AND e.start_date <= c.start_date
JOIN ptype pt ON e.ptype = pt.id
JOIN memb m ON m.id = t.speaker_id
JOIN atlas_members_cerndb amc ON amc.person_id = m.cern_ccid
LEFT JOIN category cat ON t.category = cat.id
LEFT JOIN talk_type tt ON t.type = tt.id
LEFT JOIN talk_scope ts ON t.scope = ts.id
LEFT JOIN talk_status tst ON t.status = tst.id
LEFT JOIN inst i ON e.inst_id = i.id
/

create force view ATLAS_AUTHDB.MEMBERS_DUMP as
SELECT m.id id, m.cern_ccid ccid, pt.id person_type_id, pt.description person_type, amc.sex gender, m.qual_date qualification_date, m.qual_end qualification_end_date, le.inst_id inst_id
FROM memb m
JOIN employ le ON le.memb_id = m.id AND
le.start_date = (SELECT max(start_date) from employ WHERE memb_id = m.id)
JOIN inst i ON i.id = le.inst_id
JOIN ptype pt ON le.ptype = pt.id
JOIN atlas_members_cerndb amc ON m.cern_ccid = amc.person_id
WHERE m.qual_date is not null OR le.end_date > CURRENT_DATE
/

create force view ATLAS_AUTHDB.TALK_BY_CONF as
SELECT
    t.id talk_id,
    t.title talk_title,
    tc.description talk_category,
    (m.first_name || ' ' || m.last_name) speaker_full_name,
    t.speaker_id speaker_id,
    i.oname institute,
    e.inst_id inst_id,
    t.STATUS as talk_status_id,
    ts.description talk_status,
    tt.short_name talk_type,
    tt.description talk_detailed_type,
    t.conf_id conf_id,
    c.short_name conf_short_name,
    cs.name as conf_status,
    c.name conf_name,
    c.website conf_website,
    c.start_date conf_start_date,
    c.location conf_location,
    co.short_name conf_country,
    tabs.doc_name abstract,
    tsli.link slides,
    tpro.link proceedings,
    talks_number conf_talks_number
FROM conf c
LEFT JOIN general_country co ON c.general_country_id = co.id
LEFT JOIN talk t ON t.CONF_ID = c.id
LEFT JOIN talk_status ts ON ts.id = t.status
LEFT JOIN talk_type tt ON tt.id = t.type
JOIN conf_status cs ON c.status_id = cs.id
LEFT JOIN (
    SELECT conf.id conf_id_t, count(*) talks_number
    FROM conf
    LEFT JOIN talk on conf.id = talk.conf_id
    WHERE talk.status in (1, 2, 3, 4, 5)
    GROUP BY conf.id ) ON conf_id_t = c.id
LEFT JOIN employ e ON e.memb_id = t.speaker_id AND e.end_date >= c.start_date AND e.start_date <= c.start_date
LEFT JOIN memb m ON t.speaker_id = m.id
LEFT JOIN inst i ON e.inst_id = i.id
LEFT JOIN talk_category tc ON t.category = tc.id
LEFT JOIN talk_abstract tabs ON tabs.talk_id = t.id
LEFT JOIN talk_mat tsli ON tsli.talk_id = t.id AND tsli.type = 'slides'
LEFT JOIN talk_mat tpro ON tpro.talk_id = t.id AND tpro.type = 'proceedings'
WHERE t.conf_id != 13723 AND (t.status IN (1, 2, 3, 4, 5, 6) or t.status is null) -- Please refer to https://its.cern.ch/jira/browse/ATGLANCE-5584 for context on conference 13723
ORDER BY conf_start_date DESC, conf_id DESC
/

create force view ATLAS_AUTHDB.EMPLOYMENT_INFORMATION as
SELECT
        ID            AS EMPLOYMENT_ID,
        MEMB_ID       AS MEMBER_ID,
        E.START_DATE  AS LAST_EMPLOYMENT_START,
        E.END_DATE    AS LAST_EMPLOYMENT_END,
        E.EMAIL,
        E.CERN_PHONE,
        E.CERN_MOBILE,
        E.OFFICE,
        E.PTYPE,
        E.INST_ID     AS LAST_EMPLOYMENT_INST_ID,
        FE.START_DATE AS FIRST_EMPLOYMENT_START
FROM   EMPLOY E
        JOIN MEMBER_CURRENT_EMPLOYMENT LE
        ON E.MEMB_ID = LE.MEMBER_ID
        JOIN MEMBER_FIRST_EMPLOYMENT FE
        ON E.MEMB_ID = FE.MEMBER_ID
        WHERE LE.START_DATE = E.START_DATE
/

create force view ATLAS_AUTHDB.SSC_UPGRADE_SUPER_SEARCH as
SELECT DISTINCT
        m.id                                              AS id
    ,   m.cern_ccid                                       AS cern_ccid
    ,   m.cern_id                                         AS cern_id
    ,   cern.sex                                          AS gender
    ,   m.last_name                                       AS last_name
    ,   m.first_name                                      AS first_name
    ,   m.initials                                        AS initials
    ,   e.ptype                                           AS ptype
    ,   e.email                                           AS email
    ,   p.description                                     AS profession
    ,   i.id                                              AS affiliation
    ,   i.oname                                           AS institute
    ,   c.id                                              AS country_id
    ,   c.name                                            AS country
    ,   last_talk.id                                      AS last_talk_id
    ,   last_talk.type                                    AS last_talk_type
    ,   last_talk.title                                   AS last_talk_title
    ,   TO_CHAR(last_talk.talk_date,'yyyy-mm-dd')         AS last_talk_date
    ,   last_talk.conf_id                                 AS last_talk_conf_id
    ,   last_talk.conf_short_name                         AS last_talk_conf_short_name
    ,   last_talk.conf_priority                           AS last_talk_conf_priority
    ,   last_talk.conf_name                               AS last_talk_conf_name
    ,   last_talk.conf_website                            AS last_talk_conf_website
    ,   last_talk.category                                AS last_talk_category
    ,   last_talk.weight                                  AS last_talk_weight
    ,   NULL                                              AS last_talk_penalty
    ,   pi.id                                             AS pi_id
    ,   pi.applying_for_job                               AS applying_for_job
    ,   pi.last_modified                                  AS timestamp
    ,   oc.comments                                       AS other_constraints
    ,   bd.dates                                          AS black_dates
    ,   t.topics                                          AS topics
    ,   t.natural_ids                                     AS topics_natural_id
    ,   c1.short_name                                     AS first_conference
    ,   c1.name                                           AS first_conference_name
    ,   c1.website                                        AS first_conference_website
    ,   TO_CHAR(c1.start_date, 'yyyy-mm-dd')              AS first_conference_start
    ,   c2.short_name                                     AS second_conference
    ,   c2.name                                           AS second_conference_name
    ,   c2.website                                        AS second_conference_website
    ,   TO_CHAR(c2.start_date, 'yyyy-mm-dd')              AS second_conference_start
    ,   c3.short_name                                     AS third_conference
    ,   c3.name                                           AS third_conference_name
    ,   c3.website                                        AS third_conference_website
    ,   TO_CHAR(c3.start_date, 'yyyy-mm-dd')              AS third_conference_start
    ,   c4.short_name                                     AS fourth_conference
    ,   c4.name                                           AS fourth_conference_name
    ,   c4.website                                        AS fourth_conference_website
    ,   TO_CHAR(c4.start_date, 'yyyy-mm-dd')              AS fourth_conference_start
    ,   c5.short_name                                     AS fifth_conference
    ,   c5.name                                           AS fifth_conference_name
    ,   c5.website                                        AS fifth_conference_website
    ,   TO_CHAR(c5.start_date, 'yyyy-mm-dd')              AS fifth_conference_start
    ,   pct1.conference_preferred_topics                  AS first_conf_preferred_topics
    ,   pct2.conference_preferred_topics                  AS second_conf_preferred_topics
    ,   pct3.conference_preferred_topics                  AS third_conf_preferred_topics
    ,   pct4.conference_preferred_topics                  AS fourth_conf_preferred_topics
    ,   pct5.conference_preferred_topics                  AS fifth_conf_preferred_topics
    ,   mvtt.vetoed_talk_types                            AS vetoed_talk_types
    ,   mvcp.vetoed_conf_priorities                       AS vetoed_conf_priorities
    ,   mvc.vetoed_countries                              AS vetoed_countries
    FROM memb m
        INNER JOIN employ e ON e.memb_id = m.id
            AND e.start_date = (
                SELECT MAX(start_date) FROM employ WHERE memb_id = m.id
            )
            AND ADD_MONTHS(e.end_date,6) >= TRUNC(SYSDATE)
        INNER JOIN atlas_members_cerndb cern ON cern.person_id = m.cern_ccid
        JOIN ptype      p ON p.id = e.ptype
        JOIN inst       i ON i.id = e.inst_id
        JOIN country    c ON c.id = i.country_id
        LEFT JOIN (
            SELECT t.speaker_id
            ,   t.type
            ,   t.id
            ,   t.title               AS title
            ,   t.category            AS category
            ,   c.id                  AS conf_id
            ,   c.priority            AS conf_priority
            ,   c.short_name          AS conf_short_name
            ,   c.name                AS conf_name
            ,   c.website             AS conf_website
            ,   c.start_date          AS talk_date
            ,   twv.talk_weight_value AS weight
            FROM talk t
                JOIN (
                    SELECT speaker_id
                    ,   id
                    FROM (
                        SELECT t.speaker_id
                        ,   t.id
                        ,   ROW_NUMBER() OVER (PARTITION BY t.speaker_id ORDER BY c.start_date DESC) AS rn
                        FROM talk t
                            JOIN conf c ON c.id = t.conf_id
                            JOIN talk_weight_view twv ON t.id = twv.talk_id
                            WHERE twv.talk_weight_value != 0
                    )
                    WHERE rn = 1
                ) lt ON lt.id = t.id
                JOIN conf c ON c.id = t.conf_id
                JOIN talk_weight_view twv ON t.id = twv.talk_id
            WHERE t.category IN (0, 15, 16, 17, 18, 19, 21, 22, 24) -- only talks from some categories
        ) last_talk ON last_talk.speaker_id = m.id
        LEFT JOIN memb pi ON pi.id = m.id
        LEFT JOIN v_me_personal_input pi ON pi.member_id = m.id
        LEFT JOIN photo_url photo ON photo.id = m.id
        LEFT JOIN (
            SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
            FROM conf, me_preferred_conf pc
            WHERE conf.id = pc.conf_id AND pc.rank = 1 AND conf.start_date > SYSDATE
        ) c1 ON c1.member_id = m.id
        LEFT JOIN (
            SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
            FROM conf, me_preferred_conf pc
            WHERE conf.id = pc.conf_id AND pc.rank = 2 AND conf.start_date > SYSDATE
        ) c2 ON c2.member_id = m.id
        LEFT JOIN (
            SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
            FROM conf, me_preferred_conf pc
            WHERE conf.id = pc.conf_id AND pc.rank = 3 AND conf.start_date > SYSDATE
        ) c3 ON c3.member_id = m.id
        LEFT JOIN (
            SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
            FROM conf, me_preferred_conf pc
            WHERE conf.id = pc.conf_id AND pc.rank = 4 AND conf.start_date > SYSDATE
        ) c4 ON c4.member_id = m.id
        LEFT JOIN (
            SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
            FROM conf, me_preferred_conf pc
            WHERE conf.id = pc.conf_id AND pc.rank = 5 AND conf.start_date > SYSDATE
        ) c5 ON c5.member_id = m.id
    LEFT JOIN (
            SELECT member_id
            ,   LISTAGG(
                    TO_CHAR( start_date, 'yyyy-mm-dd' )
                    || ' ' ||
                    TO_CHAR( end_date, 'yyyy-mm-dd' )
                , ',' ) WITHIN GROUP (ORDER BY start_date) AS dates
            FROM me_talk_blackdate
            GROUP BY member_id
        ) bd ON bd.member_id = pi.id
        LEFT JOIN (
            SELECT
                mvtt.member_id,
                LISTAGG(tt.description, ',') WITHIN GROUP (ORDER BY mvtt.modified_on) AS vetoed_talk_types
            FROM me_vetoed_talk_type mvtt
            JOIN talk_type tt ON tt.id = mvtt.talk_type_id
            GROUP BY mvtt.member_id
        ) mvtt ON mvtt.member_id = pi.id
        LEFT JOIN (
            SELECT
                mvcp.member_id,
                LISTAGG(cp.description, ',') WITHIN GROUP (ORDER BY mvcp.modified_on) AS vetoed_conf_priorities
            FROM me_vetoed_conf_priority mvcp
            JOIN conf_priority cp ON cp.id = mvcp.conf_priority_id
            GROUP BY mvcp.member_id
        ) mvcp ON mvcp.member_id = pi.id
        LEFT JOIN (
            SELECT
                mvc.member_id,
                LISTAGG(gc.short_name, ',') WITHIN GROUP (ORDER BY mvc.modified_on) AS vetoed_countries
            FROM me_vetoed_country mvc
            JOIN general_country gc ON gc.id = mvc.general_country_id
            GROUP BY mvc.member_id
        ) mvc ON mvc.member_id = pi.id
        LEFT JOIN (
            SELECT member_id
            FROM me_applying_for_a_job
        ) afj ON afj.member_id = pi.id
        LEFT JOIN (
            SELECT
                member_id,
                comments
            FROM me_other_constraint
        ) oc ON oc.member_id = pi.id
        LEFT JOIN (
            SELECT mpct.member_id
            ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
            FROM me_preferred_conf_topic mpct
                LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
                LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
            WHERE pc.rank = 1
            GROUP BY mpct.member_id
        ) pct1 ON pct1.member_id = m.id
        LEFT JOIN (
            SELECT mpct.member_id
            ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
            FROM me_preferred_conf_topic mpct
                LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
                LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
            WHERE pc.rank = 2
            GROUP BY mpct.member_id
        ) pct2 ON pct2.member_id = m.id
        LEFT JOIN (
            SELECT mpct.member_id
            ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
            FROM me_preferred_conf_topic mpct
                LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
                LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
            WHERE pc.rank = 3
            GROUP BY mpct.member_id
        ) pct3 ON pct3.member_id = m.id
        LEFT JOIN (
            SELECT mpct.member_id
            ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
            FROM me_preferred_conf_topic mpct
                LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
                LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
            WHERE pc.rank = 4
            GROUP BY mpct.member_id
        ) pct4 ON pct4.member_id = m.id
        LEFT JOIN (
            SELECT mpct.member_id
            ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
            FROM me_preferred_conf_topic mpct
                LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
                LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
            WHERE pc.rank = 5
            GROUP BY mpct.member_id
        ) pct5 ON pct5.member_id = m.id
        JOIN (
            SELECT mtt.member_id
            ,   LISTAGG(mtt.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mtt.rank) AS topics
            ,   LISTAGG(mtt.rank || '-*-' || a.natural_id, '|') WITHIN GROUP (ORDER BY mtt.rank) AS natural_ids
            FROM me_talk_preferred_topic mtt
                JOIN activity a ON a.id = mtt.activity_id
                LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
                LEFT JOIN activity p ON p.id = ah.parent_activity_id
    -- This is how we limit the results only to Upgrade SSC related data
                WHERE a.natural_id IN 
                            (
                                'UPSC',
                                'UPGR',
                                'UPG', 
                                'UPGP',
                                'TUPG',
                                'P1UP',
                                'P2UP',
                                'LU2E',
                                'UPCO',
                                'MP2U',
                                'UPGD',
                                'UPDH' 
                            )
            GROUP BY mtt.member_id
        ) t ON t.member_id = m.id
/

create force view ATLAS_AUTHDB.SSC_HGTD_SUPER_SEARCH as
SELECT DISTINCT
    m.id                                                AS id
    , m.cern_ccid                                       AS cern_ccid
    , m.cern_id                                         AS cern_id
    , cern.sex                                          AS gender
    , m.last_name                                       AS last_name
    , m.first_name                                      AS first_name
    , m.initials                                        AS initials
    , e.ptype                                           AS ptype
    , e.email                                           AS email
    , p.description                                     AS profession
    , i.id                                              AS affiliation
    , i.oname                                           AS institute
    , c.id                                              AS country_id
    , c.name                                            AS country
    , last_talk.id                                      AS last_talk_id
    , last_talk.type                                    AS last_talk_type
    , last_talk.title                                   AS last_talk_title
    , TO_CHAR(last_talk.talk_date,'yyyy-mm-dd')         AS last_talk_date
    , last_talk.conf_id                                 AS last_talk_conf_id
    , last_talk.conf_short_name                         AS last_talk_conf_short_name
    , last_talk.conf_priority                           AS last_talk_conf_priority
    , last_talk.conf_name                               AS last_talk_conf_name
    , last_talk.conf_website                            AS last_talk_conf_website
    , last_talk.category                                AS last_talk_category
    , last_talk.weight                                  AS last_talk_weight
    , NULL                                              AS last_talk_penalty
    , pi.id                                             AS pi_id
    , pi.applying_for_job                               AS applying_for_job
    , pi.last_modified                                  AS timestamp
    , oc.comments                                       AS other_constraints
    , bd.dates                                          AS black_dates
    , t.topics                                          AS topics
    , t.natural_ids                                     AS topics_natural_id
    , c1.short_name                                     AS first_conference
    , c1.name                                           AS first_conference_name
    , c1.website                                        AS first_conference_website
    , TO_CHAR(c1.start_date, 'yyyy-mm-dd')              AS first_conference_start
    , c2.short_name                                     AS second_conference
    , c2.name                                           AS second_conference_name
    , c2.website                                        AS second_conference_website
    , TO_CHAR(c2.start_date, 'yyyy-mm-dd')              AS second_conference_start
    , c3.short_name                                     AS third_conference
    , c3.name                                           AS third_conference_name
    , c3.website                                        AS third_conference_website
    , TO_CHAR(c3.start_date, 'yyyy-mm-dd')              AS third_conference_start
    , c4.short_name                                     AS fourth_conference
    , c4.name                                           AS fourth_conference_name
    , c4.website                                        AS fourth_conference_website
    , TO_CHAR(c4.start_date, 'yyyy-mm-dd')              AS fourth_conference_start
    , c5.short_name                                     AS fifth_conference
    , c5.name                                           AS fifth_conference_name
    , c5.website                                        AS fifth_conference_website
    , TO_CHAR(c5.start_date, 'yyyy-mm-dd')              AS fifth_conference_start
    , pct1.conference_preferred_topics                  AS first_conf_preferred_topics
    , pct2.conference_preferred_topics                  AS second_conf_preferred_topics
    , pct3.conference_preferred_topics                  AS third_conf_preferred_topics
    , pct4.conference_preferred_topics                  AS fourth_conf_preferred_topics
    , pct5.conference_preferred_topics                  AS fifth_conf_preferred_topics
    , mvtt.vetoed_talk_types                            AS vetoed_talk_types
    , mvcp.vetoed_conf_priorities                       AS vetoed_conf_priorities
    , mvc.vetoed_countries                              AS vetoed_countries
FROM memb m
    INNER JOIN employ e ON e.memb_id = m.id
        AND e.start_date = (
            SELECT MAX(start_date) FROM employ WHERE memb_id = m.id
        )
        AND ADD_MONTHS(e.end_date,6) >= TRUNC(SYSDATE)
    INNER JOIN atlas_members_cerndb cern ON cern.person_id = m.cern_ccid
    JOIN ptype      p ON p.id = e.ptype
    JOIN inst       i ON i.id = e.inst_id
    JOIN country    c ON c.id = i.country_id
    LEFT JOIN (
        SELECT t.speaker_id
        ,   t.type
        ,   t.id
        ,   t.title               AS title
        ,   t.category            AS category
        ,   c.id                  AS conf_id
        ,   c.priority            AS conf_priority
        ,   c.short_name          AS conf_short_name
        ,   c.name                AS conf_name
        ,   c.website             AS conf_website
        ,   c.start_date          AS talk_date
        ,   twv.talk_weight_value AS weight
        FROM talk t
            JOIN (
                SELECT speaker_id
                ,   id
                FROM (
                    SELECT t.speaker_id
                    ,   t.id
                    ,   ROW_NUMBER() OVER (PARTITION BY t.speaker_id ORDER BY c.start_date DESC) AS rn
                    FROM talk t
                        JOIN conf c ON c.id = t.conf_id
                        JOIN talk_weight_view twv ON t.id = twv.talk_id
                        WHERE twv.talk_weight_value != 0
                )
                WHERE rn = 1
            ) lt ON lt.id = t.id
            JOIN conf c ON c.id = t.conf_id
            JOIN talk_weight_view twv ON t.id = twv.talk_id
        WHERE t.category IN (27)
    ) last_talk ON last_talk.speaker_id = m.id
    LEFT JOIN memb pi ON pi.id = m.id
    LEFT JOIN v_me_personal_input pi ON pi.member_id = m.id
    LEFT JOIN photo_url photo ON photo.id = m.id
    LEFT JOIN (
        SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
        FROM conf, me_preferred_conf pc
        WHERE conf.id = pc.conf_id AND pc.rank = 1 AND conf.start_date > SYSDATE
    ) c1 ON c1.member_id = m.id
    LEFT JOIN (
        SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
        FROM conf, me_preferred_conf pc
        WHERE conf.id = pc.conf_id AND pc.rank = 2 AND conf.start_date > SYSDATE
    ) c2 ON c2.member_id = m.id
    LEFT JOIN (
        SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
        FROM conf, me_preferred_conf pc
        WHERE conf.id = pc.conf_id AND pc.rank = 3 AND conf.start_date > SYSDATE
    ) c3 ON c3.member_id = m.id
    LEFT JOIN (
        SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
        FROM conf, me_preferred_conf pc
        WHERE conf.id = pc.conf_id AND pc.rank = 4 AND conf.start_date > SYSDATE
    ) c4 ON c4.member_id = m.id
    LEFT JOIN (
        SELECT conf.id, conf.short_name, conf.name, conf.website, conf.start_date, pc.member_id
        FROM conf, me_preferred_conf pc
        WHERE conf.id = pc.conf_id AND pc.rank = 5 AND conf.start_date > SYSDATE
    ) c5 ON c5.member_id = m.id
    LEFT JOIN (
        SELECT member_id
        ,   LISTAGG(
                TO_CHAR( start_date, 'yyyy-mm-dd' )
                || ' ' ||
                TO_CHAR( end_date, 'yyyy-mm-dd' )
            , ',' ) WITHIN GROUP (ORDER BY start_date) AS dates
        FROM me_talk_blackdate
        GROUP BY member_id
    ) bd ON bd.member_id = pi.id
    LEFT JOIN (
        SELECT
            mvtt.member_id,
            LISTAGG(tt.description, ',') WITHIN GROUP (ORDER BY mvtt.modified_on) AS vetoed_talk_types
        FROM me_vetoed_talk_type mvtt
        JOIN talk_type tt ON tt.id = mvtt.talk_type_id
        GROUP BY mvtt.member_id
    ) mvtt ON mvtt.member_id = pi.id
    LEFT JOIN (
        SELECT
            mvcp.member_id,
            LISTAGG(cp.description, ',') WITHIN GROUP (ORDER BY mvcp.modified_on) AS vetoed_conf_priorities
        FROM me_vetoed_conf_priority mvcp
        JOIN conf_priority cp ON cp.id = mvcp.conf_priority_id
        GROUP BY mvcp.member_id
    ) mvcp ON mvcp.member_id = pi.id
    LEFT JOIN (
        SELECT
            mvc.member_id,
            LISTAGG(gc.short_name, ',') WITHIN GROUP (ORDER BY mvc.modified_on) AS vetoed_countries
        FROM me_vetoed_country mvc
        JOIN general_country gc ON gc.id = mvc.general_country_id
        GROUP BY mvc.member_id
    ) mvc ON mvc.member_id = pi.id
    LEFT JOIN (
        SELECT member_id
        FROM me_applying_for_a_job
    ) afj ON afj.member_id = pi.id
    LEFT JOIN (
        SELECT
            member_id,
            comments
        FROM me_other_constraint
    ) oc ON oc.member_id = pi.id
    LEFT JOIN (
        SELECT mpct.member_id
        ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
        FROM me_preferred_conf_topic mpct
            LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
            LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
        WHERE pc.rank = 1
        GROUP BY mpct.member_id
    ) pct1 ON pct1.member_id = m.id
    LEFT JOIN (
        SELECT mpct.member_id
        ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
        FROM me_preferred_conf_topic mpct
            LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
            LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
        WHERE pc.rank = 2
        GROUP BY mpct.member_id
    ) pct2 ON pct2.member_id = m.id
    LEFT JOIN (
        SELECT mpct.member_id
        ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
        FROM me_preferred_conf_topic mpct
            LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
            LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
        WHERE pc.rank = 3
        GROUP BY mpct.member_id
    ) pct3 ON pct3.member_id = m.id
    LEFT JOIN (
        SELECT mpct.member_id
        ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
        FROM me_preferred_conf_topic mpct
            LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
            LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
        WHERE pc.rank = 4
        GROUP BY mpct.member_id
    ) pct4 ON pct4.member_id = m.id
    LEFT JOIN (
        SELECT mpct.member_id
        ,   LISTAGG(mpct.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mpct.rank) AS conference_preferred_topics
        FROM me_preferred_conf_topic mpct
            LEFT JOIN me_talk_preferred_topic mtt ON mtt.id = mpct.talk_preferred_topic_id
            LEFT JOIN me_preferred_conf pc ON pc.id = mpct.preferred_conf_id
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
        WHERE pc.rank = 5
        GROUP BY mpct.member_id
    ) pct5 ON pct5.member_id = m.id
    JOIN (
        SELECT mtt.member_id
        ,   LISTAGG(mtt.rank || '-*-' || CASE WHEN p.name IS NOT NULL THEN p.name || ' - ' ELSE '' END || a.name, '|') WITHIN GROUP (ORDER BY mtt.rank) AS topics
        ,   LISTAGG(mtt.rank || '-*-' || a.natural_id, '|') WITHIN GROUP (ORDER BY mtt.rank) AS natural_ids
        FROM me_talk_preferred_topic mtt
            JOIN activity a ON a.id = mtt.activity_id
            LEFT JOIN activity_hierarchy ah ON ah.activity_id = mtt.activity_id
            LEFT JOIN activity p ON p.id = ah.parent_activity_id
            -- This is how we limit the results only to Upgrade SSC related data
            WHERE a.natural_id IN 
                        (
                            'HGTD',
                            'DEMT',
                            'ELEC',
                            'LTDC',
                            'MECH',
                            'MDAU',
                            'SENS',
                            'SAIN',
                            'SPPH',
                            'TBAM'
                        )
        GROUP BY mtt.member_id
    ) t ON t.member_id = m.id
/

--rollback DECLARE
--rollback   V_VIEW_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR VIEW_REC IN (SELECT VIEW_NAME FROM ALL_VIEWS WHERE OWNER = 'ATLAS_AUTHDB') LOOP
--rollback     V_VIEW_NAME := VIEW_REC.VIEW_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP VIEW ATLAS_AUTHDB.' || V_VIEW_NAME;
--rollback   END LOOP;
--rollback END;
--rollback /