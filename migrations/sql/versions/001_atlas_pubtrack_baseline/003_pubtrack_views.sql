--liquibase formatted sql
--changeset Gabriel:3 endDelimiter:/ rollbackEndDelimiter:/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_LEADGROUP_ACTIVITY_APPOINTMENT" ("ACTIVITY_ID", "APPOINTMENT_ID") AS
  SELECT
        LEADGROUPS.ACTIVITY_ID,
        ANALYSIS_LEADGROUP_APPOINTMENT.APPOINTMENT AS APPOINTMENT_ID
    FROM
        ATLAS_AUTHDB.ANALYSIS_LEADGROUP_APPOINTMENT
    JOIN ATLAS_AUTHDB.LEADGROUPS ON LEADGROUPS.GROUP_ID = ANALYSIS_LEADGROUP_APPOINTMENT.LEADGROUP
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_SUBGROUP_ACTIVITY_APPOINTMENT" ("ACTIVITY_ID", "APPOINTMENT_ID") AS
  SELECT
        SUBGROUPS.ACTIVITY_ID,
        ANALYSIS_SUBGROUP_APPOINTMENT.APPOINTMENT AS APPOINTMENT_ID
    FROM
        ATLAS_AUTHDB.ANALYSIS_SUBGROUP_APPOINTMENT
    JOIN ATLAS_AUTHDB.SUBGROUPS ON SUBGROUPS.SUBGROUP_ID = ANALYSIS_SUBGROUP_APPOINTMENT.SUBGROUP
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_ANALYSIS_SYS" ("ID", "SHORT_TITLE", "FULL_TITLE", "PUB_SHORT_TITLE", "CREATION_DATE", "REF_CODE", "STATUS", "DELETION_REQUEST", "DELETION_REASON", "DELETED", "TASK_ID") AS
  SELECT
    PUBLICATION.ID,
    TASK.SHORT_TITLE,
    TASK.FULL_TITLE,
    TASK.PUBLIC_SHORT_TITLE AS PUB_SHORT_TITLE,
    PUBLICATION.CREATION_DATE,
    PUBLICATION.REF_CODE,
    PUBLICATION.STATUS,
    PUBLICATION.DELETION_REQUEST,
    PUBLICATION.DELETION_REASON,
    PUBLICATION.DELETED,
    PUBLICATION.TASK_ID
FROM ATLAS_AUTHDB.ANALYSIS_SYS PUBLICATION
LEFT JOIN TASK ON PUBLICATION.TASK_ID = TASK.ID
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_CONFNOTE" ("ID", "SHORT_TITLE", "FULL_TITLE", "PUB_SHORT_TITLE", "CREATION_DATE", "TEMP_REF_CODE", "STATUS", "DELETION_REQUEST", "DELETION_REASON", "DELETED", "TASK_ID") AS
  SELECT
    PUBLICATION.ID,
    TASK.SHORT_TITLE,
    TASK.FULL_TITLE,
    TASK.PUBLIC_SHORT_TITLE AS PUB_SHORT_TITLE,
    PUBLICATION.CREATION_DATE,
    PUBLICATION.TEMP_REF_CODE,
    PUBLICATION.STATUS,
    PUBLICATION.DELETION_REQUEST,
    PUBLICATION.DELETION_REASON,
    PUBLICATION.DELETED,
    PUBLICATION.TASK_ID
FROM ATLAS_AUTHDB.CONFNOTE_PUBLICATION PUBLICATION
LEFT JOIN TASK ON PUBLICATION.TASK_ID = TASK.ID
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_PAPER" ("ID", "SHORT_TITLE", "FULL_TITLE", "PUB_SHORT_TITLE", "CREATION_DATE", "REF_CODE", "STATUS", "DELETION_REQUEST", "DELETION_REASON", "DELETED", "JOURNAL_APPROVAL_DATE", "JOURNAL_SUB", "TASK_ID") AS
  SELECT
    PUBLICATION.ID,
    TASK.SHORT_TITLE,
    TASK.FULL_TITLE,
    TASK.PUBLIC_SHORT_TITLE AS PUB_SHORT_TITLE,
    PUBLICATION.CREATION_DATE,
    PUBLICATION.REF_CODE,
    PUBLICATION.STATUS,
    PUBLICATION.DELETION_REQUEST,
    PUBLICATION.DELETION_REASON,
    PUBLICATION.DELETED,
    PUBLICATION.JOURNAL_APPROVAL_DATE,
    PUBLICATION.JOURNAL_SUB,
    PUBLICATION.TASK_ID
FROM ATLAS_AUTHDB.PUBLICATION PUBLICATION
LEFT JOIN TASK ON PUBLICATION.TASK_ID = TASK.ID
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_PUBNOTE" ("ID", "SHORT_TITLE", "FULL_TITLE", "PUB_SHORT_TITLE", "CREATION_DATE", "TEMP_REF_CODE", "STATUS", "DELETION_REQUEST", "DELETION_REASON", "DELETED", "TASK_ID") AS
  SELECT
    PUBLICATION.ID,
    TASK.SHORT_TITLE,
    TASK.FULL_TITLE,
    TASK.PUBLIC_SHORT_TITLE AS PUB_SHORT_TITLE,
    PUBLICATION.CREATION_DATE,
    PUBLICATION.TEMP_REF_CODE,
    PUBLICATION.STATUS,
    PUBLICATION.DELETION_REQUEST,
    PUBLICATION.DELETION_REASON,
    PUBLICATION.DELETED,
    PUBLICATION.TASK_ID
FROM ATLAS_AUTHDB.PUBNOTE_PUBLICATION PUBLICATION
LEFT JOIN TASK ON PUBLICATION.TASK_ID = TASK.ID
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."V_SUPPORTING_INT_DOC" ("ID", "TASK_ID", "LABEL", "URL") AS
  SELECT
    SN.ID,
    SN.TASK_ID,
    LINK.LABEL,
    LINK.URL
FROM TASK_SUPPORTING_INT_DOC SN
LEFT JOIN LINK ON SN.LINK_ID = LINK.ID
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."ANALYSIS_MEMBER_INFORMATION" ("ROW_ID", "PUBLICATION_ID", "MEMB_ID", "GROUP_ID", "MEMB_FUNCTION", "MEMB_CONTRIBUTION", "FIRST_NAME", "LAST_NAME", "CERN_CCID", "PHOTO_PERMISSION", "CERN_ID", "EMPLOY_ID", "START_DATE", "END_DATE", "EMAIL", "CERN_PHONE", "CERN_MOBILE", "INST_PHONE", "OFFICE", "INST_ID", "PTYPE_ID", "PTYPE_DESCRIPTION", "COMMENTS", "MOFREE", "MAILFLAG", "AFLFLAG", "ONAME", "INST_NAME") AS
  WITH
memb_function_subquery AS (
    SELECT analysis_sys.id                                               AS publication_id,
        analysis_sys.deletion_memberid                                  AS memb_id,
        'DELETION_REQUESTER'                                            AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.analysis_sys analysis_sys
    UNION
    SELECT analysis_sys.id                                               AS publication_id,
        task_analysis_team_member.member_id             AS memb_id,
        'EDITOR'                                                        AS group_id,
        CASE
        WHEN coEditor.id IS NOT NULL THEN 'coEditor'
        ELSE null END                                         AS memb_function,
        ''                                                              AS memb_contribution
    FROM    atlas_authdb.analysis_sys analysis_sys,
            task_analysis_team_member
            LEFT JOIN task_contact_editor coEditor
            ON coEditor.id = task_analysis_team_member.id
    WHERE analysis_sys.task_id = task_analysis_team_member.task_id
    UNION
    SELECT analysis_sys.id                                               AS publication_id,
        task_add_member_in_egroup.member_id             AS memb_id,
        'ADDMEMB'                                                       AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.analysis_sys analysis_sys,
        task_add_member_in_egroup
    WHERE analysis_sys.task_id = task_add_member_in_egroup.task_id
    UNION
    SELECT analysis_sys_memb_publication.publication_id                  AS publication_id,
        analysis_sys_memb_publication.memb_id                           AS memb_id,
        analysis_sys_memb_publication.group_id                          AS group_id,
        analysis_sys_memb_publication.memb_function                     AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.analysis_sys_memb_publication analysis_sys_memb_publication
    WHERE analysis_sys_memb_publication.group_id NOT IN ('EDITOR', 'ADDMEMB')
),
member_info_subquery AS (
    SELECT *
FROM (
    SELECT
        memb.id AS memb_id,
        memb.first_name AS first_name,
        memb.last_name AS last_name,
        memb.cern_ccid AS cern_ccid,
        memb.photo AS photo_permission,
        memb.cern_id AS cern_id,
        memb.cern_phone                AS cern_phone,
        memb.cern_mobile               AS cern_mobile,
        memb.office                    AS office,
        last_employ.id AS employ_id,
        last_employ.start_date AS start_date,
        last_employ.end_date AS end_date,
        last_employ.email AS email,
        last_employ.inst_phone AS inst_phone,
        last_employ.inst_id AS inst_id,
        last_employ.ptype AS ptype_id,
        ptype.description AS ptype_description,
        last_employ.comments AS comments,
        last_employ.mofree AS mofree,
        last_employ.mailflag AS mailflag,
        last_employ.aflflag AS aflflag,
        inst.oname AS oname,
        inst.name AS inst_name,
        ROW_NUMBER() OVER (PARTITION BY memb.id ORDER BY last_employ.end_date DESC) AS rn
    FROM
        atlas_authdb.memb memb
    JOIN
        atlas_authdb.me_employment last_employ ON last_employ.memb_id = memb.id
    JOIN
        atlas_authdb.inst inst ON last_employ.inst_id = inst.id
    JOIN
        atlas_authdb.ptype ptype ON last_employ.ptype = ptype.id
)
WHERE
    rn = 1
)
SELECT ROW_NUMBER() over (ORDER BY memb_function_subquery.publication_id ASC) AS ROW_ID,
    memb_function_subquery.publication_id,
    memb_function_subquery.memb_id,
    memb_function_subquery.group_id,
    memb_function_subquery.memb_function,
    memb_function_subquery.memb_contribution,
    member_info_subquery.first_name,
    member_info_subquery.last_name,
    member_info_subquery.cern_ccid,
    member_info_subquery.photo_permission,
    member_info_subquery.cern_id,
    member_info_subquery.employ_id,
    member_info_subquery.start_date,
    member_info_subquery.end_date,
    member_info_subquery.email,
    member_info_subquery.cern_phone,
    member_info_subquery.cern_mobile,
    member_info_subquery.inst_phone,
    member_info_subquery.office,
    member_info_subquery.inst_id,
    member_info_subquery.ptype_id,
    member_info_subquery.ptype_description,
    member_info_subquery.comments,
    member_info_subquery.mofree,
    member_info_subquery.mailflag,
    member_info_subquery.aflflag,
    member_info_subquery.oname,
    member_info_subquery.inst_name
FROM memb_function_subquery
JOIN member_info_subquery on member_info_subquery.memb_id = memb_function_subquery.memb_id
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."CONFNOTE_MEMBER_INFORMATION" ("ROW_ID", "PUBLICATION_ID", "MEMB_ID", "GROUP_ID", "MEMB_FUNCTION", "MEMB_CONTRIBUTION", "FIRST_NAME", "LAST_NAME", "CERN_CCID", "PHOTO_PERMISSION", "CERN_ID", "EMPLOY_ID", "START_DATE", "END_DATE", "EMAIL", "CERN_PHONE", "CERN_MOBILE", "INST_PHONE", "OFFICE", "INST_ID", "PTYPE_ID", "PTYPE_DESCRIPTION", "COMMENTS", "MOFREE", "MAILFLAG", "AFLFLAG", "ONAME", "INST_NAME") AS
  WITH
memb_function_subquery AS (
    SELECT confnote_publication.id                                       AS publication_id,
        confnote_publication.deletion_memberid                          AS memb_id,
        'DELETION_REQUESTER'                                            AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.confnote_publication confnote_publication
    UNION
    SELECT confnote_publication.id                                       AS publication_id,
        task_analysis_team_member.member_id             AS memb_id,
        'EDITOR'                                                        AS group_id,
        CASE
        WHEN coEditor.id IS NOT NULL THEN 'coEditor'
        ELSE null END                                         AS memb_function,
        ''                                                              AS memb_contribution
    FROM    atlas_authdb.confnote_publication confnote_publication,
            task_analysis_team_member
            LEFT JOIN task_contact_editor coEditor
            ON coEditor.id = task_analysis_team_member.id
    WHERE confnote_publication.task_id = task_analysis_team_member.task_id
    UNION
    SELECT confnote_publication.id                                       AS publication_id,
        task_add_member_in_egroup.member_id             AS memb_id,
        'ADDMEMB'                                                       AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.confnote_publication confnote_publication,
        task_add_member_in_egroup
    WHERE confnote_publication.task_id = task_add_member_in_egroup.task_id
    UNION
    SELECT confnote_memb_publication.publication_id                      AS publication_id,
        confnote_memb_publication.memb_id                               AS memb_id,
        confnote_memb_publication.group_id                              AS group_id,
        confnote_memb_publication.memb_function                         AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.confnote_memb_publication confnote_memb_publication
    WHERE confnote_memb_publication.group_id NOT IN ('EDITOR', 'ADDMEMB')
),
member_info_subquery AS(
    SELECT *
FROM (
    SELECT
        memb.id AS memb_id,
        memb.first_name AS first_name,
        memb.last_name AS last_name,
        memb.cern_ccid AS cern_ccid,
        memb.photo AS photo_permission,
        memb.cern_id AS cern_id,
        memb.cern_phone                AS cern_phone,
        memb.cern_mobile               AS cern_mobile,
        memb.office                    AS office,
        last_employ.id AS employ_id,
        last_employ.start_date AS start_date,
        last_employ.end_date AS end_date,
        last_employ.email AS email,
        last_employ.inst_phone AS inst_phone,
        last_employ.inst_id AS inst_id,
        last_employ.ptype AS ptype_id,
        ptype.description AS ptype_description,
        last_employ.comments AS comments,
        last_employ.mofree AS mofree,
        last_employ.mailflag AS mailflag,
        last_employ.aflflag AS aflflag,
        inst.oname AS oname,
        inst.name AS inst_name,
        ROW_NUMBER() OVER (PARTITION BY memb.id ORDER BY last_employ.end_date DESC) AS rn
    FROM
        atlas_authdb.memb memb
    JOIN
        atlas_authdb.me_employment last_employ ON last_employ.memb_id = memb.id
    JOIN
        atlas_authdb.inst inst ON last_employ.inst_id = inst.id
    JOIN
        atlas_authdb.ptype ptype ON last_employ.ptype = ptype.id
)
WHERE
    rn = 1
)
SELECT ROW_NUMBER() over (ORDER BY memb_function_subquery.publication_id ASC) AS ROW_ID,
    memb_function_subquery.publication_id,
    memb_function_subquery.memb_id,
    memb_function_subquery.group_id,
    memb_function_subquery.memb_function,
    memb_function_subquery.memb_contribution,
    member_info_subquery.first_name,
    member_info_subquery.last_name,
    member_info_subquery.cern_ccid,
    member_info_subquery.photo_permission,
    member_info_subquery.cern_id,
    member_info_subquery.employ_id,
    member_info_subquery.start_date,
    member_info_subquery.end_date,
    member_info_subquery.email,
    member_info_subquery.cern_phone,
    member_info_subquery.cern_mobile,
    member_info_subquery.inst_phone,
    member_info_subquery.office,
    member_info_subquery.inst_id,
    member_info_subquery.ptype_id,
    member_info_subquery.ptype_description,
    member_info_subquery.comments,
    member_info_subquery.mofree,
    member_info_subquery.mailflag,
    member_info_subquery.aflflag,
    member_info_subquery.oname,
    member_info_subquery.inst_name
FROM memb_function_subquery
JOIN member_info_subquery on member_info_subquery.memb_id = memb_function_subquery.memb_id
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."PUBNOTE_MEMBER_INFORMATION" ("ROW_ID", "PUBLICATION_ID", "MEMB_ID", "GROUP_ID", "MEMB_FUNCTION", "MEMB_CONTRIBUTION", "FIRST_NAME", "LAST_NAME", "CERN_CCID", "PHOTO_PERMISSION", "CERN_ID", "EMPLOY_ID", "START_DATE", "END_DATE", "EMAIL", "CERN_PHONE", "CERN_MOBILE", "INST_PHONE", "OFFICE", "INST_ID", "PTYPE_ID", "PTYPE_DESCRIPTION", "COMMENTS", "MOFREE", "MAILFLAG", "AFLFLAG", "ONAME", "INST_NAME") AS
  WITH
memb_function_subquery AS (
    SELECT pubnote_publication.id                                        AS publication_id,
        pubnote_publication.deletion_memberid                           AS memb_id,
        'DELETION_REQUESTER'                                            AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.pubnote_publication pubnote_publication
    UNION
    SELECT pubnote_publication.id                                        AS publication_id,
        task_analysis_team_member.member_id             AS memb_id,
        'EDITOR'                                                        AS group_id,
        CASE
        WHEN coEditor.id IS NOT NULL THEN 'coEditor'
        ELSE null END                                         AS memb_function,
        ''                                                              AS memb_contribution
    FROM    atlas_authdb.pubnote_publication pubnote_publication,
            task_analysis_team_member
            LEFT JOIN task_contact_editor coEditor
            ON coEditor.id = task_analysis_team_member.id
    WHERE pubnote_publication.task_id = task_analysis_team_member.task_id
    UNION
    SELECT pubnote_publication.id                                        AS publication_id,
        task_add_member_in_egroup.member_id             AS memb_id,
        'ADDMEMB'                                                       AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.pubnote_publication pubnote_publication,
        task_add_member_in_egroup
    WHERE pubnote_publication.task_id = atlas_pub_track.task_add_member_in_egroup.task_id
    UNION
    SELECT pubnote_memb_publication.publication_id                       AS publication_id,
        pubnote_memb_publication.memb_id                                AS memb_id,
        pubnote_memb_publication.group_id                               AS group_id,
        pubnote_memb_publication.memb_function                          AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.pubnote_memb_publication pubnote_memb_publication
    WHERE pubnote_memb_publication.group_id NOT IN ('EDITOR', 'ADDMEMB')
),
member_info_subquery AS (
    SELECT *
FROM (
    SELECT
        memb.id AS memb_id,
        memb.first_name AS first_name,
        memb.last_name AS last_name,
        memb.cern_ccid AS cern_ccid,
        memb.photo AS photo_permission,
        memb.cern_id AS cern_id,
        memb.cern_phone                AS cern_phone,
        memb.cern_mobile               AS cern_mobile,
        memb.office                    AS office,
        last_employ.id AS employ_id,
        last_employ.start_date AS start_date,
        last_employ.end_date AS end_date,
        last_employ.email AS email,
        last_employ.inst_phone AS inst_phone,
        last_employ.inst_id AS inst_id,
        last_employ.ptype AS ptype_id,
        ptype.description AS ptype_description,
        last_employ.comments AS comments,
        last_employ.mofree AS mofree,
        last_employ.mailflag AS mailflag,
        last_employ.aflflag AS aflflag,
        inst.oname AS oname,
        inst.name AS inst_name,
        ROW_NUMBER() OVER (PARTITION BY memb.id ORDER BY last_employ.end_date DESC) AS rn
    FROM
        atlas_authdb.memb memb
    JOIN
        atlas_authdb.me_employment last_employ ON last_employ.memb_id = memb.id
    JOIN
        atlas_authdb.inst inst ON last_employ.inst_id = inst.id
    JOIN
        atlas_authdb.ptype ptype ON last_employ.ptype = ptype.id
)
WHERE
    rn = 1
)
SELECT ROW_NUMBER() over (ORDER BY memb_function_subquery.publication_id ASC) AS ROW_ID,
    memb_function_subquery.publication_id,
    memb_function_subquery.memb_id,
    memb_function_subquery.group_id,
    memb_function_subquery.memb_function,
    memb_function_subquery.memb_contribution,
    member_info_subquery.first_name,
    member_info_subquery.last_name,
    member_info_subquery.cern_ccid,
    member_info_subquery.photo_permission,
    member_info_subquery.cern_id,
    member_info_subquery.employ_id,
    member_info_subquery.start_date,
    member_info_subquery.end_date,
    member_info_subquery.email,
    member_info_subquery.cern_phone,
    member_info_subquery.cern_mobile,
    member_info_subquery.inst_phone,
    member_info_subquery.office,
    member_info_subquery.inst_id,
    member_info_subquery.ptype_id,
    member_info_subquery.ptype_description,
    member_info_subquery.comments,
    member_info_subquery.mofree,
    member_info_subquery.mailflag,
    member_info_subquery.aflflag,
    member_info_subquery.oname,
    member_info_subquery.inst_name
FROM memb_function_subquery
JOIN member_info_subquery on member_info_subquery.memb_id = memb_function_subquery.memb_id
/

CREATE FORCE VIEW "ATLAS_PUB_TRACK"."PAPER_MEMBER_INFORMATION" ("ROW_ID", "PUBLICATION_ID", "MEMB_ID", "GROUP_ID", "MEMB_FUNCTION", "MEMB_CONTRIBUTION", "FIRST_NAME", "LAST_NAME", "CERN_CCID", "PHOTO_PERMISSION", "CERN_ID", "EMPLOY_ID", "START_DATE", "END_DATE", "EMAIL", "CERN_PHONE", "CERN_MOBILE", "INST_PHONE", "OFFICE", "INST_ID", "PTYPE_ID", "PTYPE_DESCRIPTION", "COMMENTS", "MOFREE", "MAILFLAG", "AFLFLAG", "ONAME", "INST_NAME") AS
  WITH
memb_function_subquery AS (
    SELECT publication.id                                                AS publication_id,
        publication.deletion_memberid                                   AS memb_id,
        'DELETION_REQUESTER'                                            AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.publication publication
    UNION
    SELECT publication.id                                                AS publication_id,
        task_analysis_team_member.member_id             AS memb_id,
        'EDITOR'                                                        AS group_id,
        CASE
            WHEN coEditor.id IS NOT NULL THEN 'coEditor'
            ELSE null END                                         AS memb_function,
        ''                                                              AS memb_contribution
    FROM    atlas_authdb.publication publication,
            task_analysis_team_member
            LEFT JOIN task_contact_editor coEditor
            ON coEditor.id = task_analysis_team_member.id
    WHERE publication.task_id = task_analysis_team_member.task_id
    UNION
    SELECT publication.id                                                AS publication_id,
        task_add_member_in_egroup.member_id             AS memb_id,
        'ADDMEMB'                                                       AS group_id,
        null                                                            AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.publication publication,
        task_add_member_in_egroup
    WHERE publication.task_id = task_add_member_in_egroup.task_id
    UNION
    SELECT memb_publication.publication_id                               AS publication_id,
        memb_publication.memb_id                                        AS memb_id,
        memb_publication.group_id                                       AS group_id,
        memb_publication.memb_function                                  AS memb_function,
        ''                                                              AS memb_contribution
    FROM atlas_authdb.memb_publication memb_publication
    WHERE memb_publication.group_id NOT IN ('EDITOR', 'ADDMEMB')
),
member_info_subquery AS (
    SELECT *
FROM (
    SELECT
        memb.id AS memb_id,
        memb.first_name AS first_name,
        memb.last_name AS last_name,
        memb.cern_ccid AS cern_ccid,
        memb.photo AS photo_permission,
        memb.cern_id AS cern_id,
        memb.cern_phone                AS cern_phone,
        memb.cern_mobile               AS cern_mobile,
        memb.office                    AS office,
        last_employ.id AS employ_id,
        last_employ.start_date AS start_date,
        last_employ.end_date AS end_date,
        last_employ.email AS email,
        last_employ.inst_phone AS inst_phone,
        last_employ.inst_id AS inst_id,
        last_employ.ptype AS ptype_id,
        ptype.description AS ptype_description,
        last_employ.comments AS comments,
        last_employ.mofree AS mofree,
        last_employ.mailflag AS mailflag,
        last_employ.aflflag AS aflflag,
        inst.oname AS oname,
        inst.name AS inst_name,
        ROW_NUMBER() OVER (PARTITION BY memb.id ORDER BY last_employ.end_date DESC) AS rn
    FROM
        atlas_authdb.memb memb
    JOIN
        atlas_authdb.me_employment last_employ ON last_employ.memb_id = memb.id
    JOIN
        atlas_authdb.inst inst ON last_employ.inst_id = inst.id
    JOIN
        atlas_authdb.ptype ptype ON last_employ.ptype = ptype.id
)
WHERE
    rn = 1
)
SELECT ROW_NUMBER() over (ORDER BY memb_function_subquery.publication_id ASC) AS ROW_ID,
    memb_function_subquery.publication_id,
    memb_function_subquery.memb_id,
    memb_function_subquery.group_id,
    memb_function_subquery.memb_function,
    memb_function_subquery.memb_contribution,
    member_info_subquery.first_name,
    member_info_subquery.last_name,
    member_info_subquery.cern_ccid,
    member_info_subquery.photo_permission,
    member_info_subquery.cern_id,
    member_info_subquery.employ_id,
    member_info_subquery.start_date,
    member_info_subquery.end_date,
    member_info_subquery.email,
    member_info_subquery.cern_phone,
    member_info_subquery.cern_mobile,
    member_info_subquery.inst_phone,
    member_info_subquery.office,
    member_info_subquery.inst_id,
    member_info_subquery.ptype_id,
    member_info_subquery.ptype_description,
    member_info_subquery.comments,
    member_info_subquery.mofree,
    member_info_subquery.mailflag,
    member_info_subquery.aflflag,
    member_info_subquery.oname,
    member_info_subquery.inst_name
FROM memb_function_subquery
JOIN member_info_subquery on member_info_subquery.memb_id = memb_function_subquery.memb_id
/

--rollback DECLARE
--rollback   V_VIEW_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR VIEW_REC IN (SELECT VIEW_NAME FROM ALL_VIEWS WHERE OWNER = 'ATLAS_PUB_TRACK') LOOP
--rollback     V_VIEW_NAME := VIEW_REC.VIEW_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP VIEW ATLAS_PUB_TRACK.' || V_VIEW_NAME;
--rollback   END LOOP;
--rollback END;
--rollback /