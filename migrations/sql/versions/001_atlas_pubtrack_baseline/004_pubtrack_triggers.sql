--liquibase formatted sql
--changeset Gabriel:4 endDelimiter:/ rollbackEndDelimiter:/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_ECM_UNIT
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_ECM_UNIT
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_UNIT (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_UNIT (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_UNIT (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_UNIT (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_ECM_VALUE
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_ECM_VALUE
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_VALUE (
          ID,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_VALUE (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_ECM_VALUE (
      ID,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_LUMINOSITY_UNIT
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_LUMINOSITY_UNIT
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_LUMINOSITY_UNIT (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_LUMINOSITY_UNIT (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_LUMINOSITY_UNIT (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_LUMINOSITY_UNIT (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_RUN
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_RUN
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_RUN (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_RUN (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_RUN (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_RUN (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_TYPE
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_TYPE
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_TYPE (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_TYPE (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_TYPE (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_COL_YEAR
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.COL_YEAR
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_YEAR (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_YEAR (
          ID,
          CODE,
          NAME,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_YEAR (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.COL_YEAR (
      ID,
      CODE,
      NAME,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_GRP_GROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.GRP_GROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_GROUP (
        ID,
        AGENT_ID,
        MODIFIED_ON,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'U',
      NULL,
      NULL,
      NULL
    );

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_GROUP (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_GROUP (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_GRP_SUBGROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.GRP_SUBGROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_SUBGROUP (
        ID,
        AGENT_ID,
        MODIFIED_ON,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'U',
      NULL,
      NULL,
      NULL
    );

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_SUBGROUP (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.GRP_SUBGROUP (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_KWD_CATEGORY
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.KWD_CATEGORY
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY (
          ID,
          CODE,
          NAME,
          PARENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.PARENT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY (
          ID,
          CODE,
          NAME,
          PARENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.PARENT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PARENT_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.PARENT_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY (
          ID,
          CODE,
          NAME,
          PARENT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.PARENT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'PARENT_ID',
          TO_CHAR(:OLD.PARENT_ID),
          TO_CHAR(:NEW.PARENT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY (
      ID,
      CODE,
      NAME,
      PARENT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.PARENT_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY (
      ID,
      CODE,
      NAME,
      PARENT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.PARENT_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_KWD_CATEGORY_GROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.KWD_CATEGORY_GROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CATEGORY_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.CATEGORY_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY_GROUP (
          ID,
          CATEGORY_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CATEGORY_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CATEGORY_ID',
          TO_CHAR(:OLD.CATEGORY_ID),
          TO_CHAR(:NEW.CATEGORY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.GROUP_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.GROUP_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY_GROUP (
          ID,
          CATEGORY_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CATEGORY_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'GROUP_ID',
          TO_CHAR(:OLD.GROUP_ID),
          TO_CHAR(:NEW.GROUP_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY_GROUP (
      ID,
      CATEGORY_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CATEGORY_ID,
      :OLD.GROUP_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_CATEGORY_GROUP (
      ID,
      CATEGORY_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CATEGORY_ID,
      :NEW.GROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_KWD_KEYWORD
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.KWD_KEYWORD
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
          ID,
          CODE,
          NAME,
          CATEGORY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.CATEGORY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
          ID,
          CODE,
          NAME,
          CATEGORY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.CATEGORY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CATEGORY_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.CATEGORY_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
          ID,
          CODE,
          NAME,
          CATEGORY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.CATEGORY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CATEGORY_ID',
          TO_CHAR(:OLD.CATEGORY_ID),
          TO_CHAR(:NEW.CATEGORY_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'NULL') <> COALESCE(TO_CHAR(:NEW.RANK), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
          ID,
          CODE,
          NAME,
          CATEGORY_ID,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.CATEGORY_ID,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
      ID,
      CODE,
      NAME,
      CATEGORY_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.CATEGORY_ID,
      :OLD.RANK,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.KWD_KEYWORD (
      ID,
      CODE,
      NAME,
      CATEGORY_ID,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.CATEGORY_ID,
      :NEW.RANK,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_STAT_INTERPRETATION
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.STAT_INTERPRETATION
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'NULL') <> COALESCE(TO_CHAR(:NEW.RANK), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.RANK,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_INTERPRETATION (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.RANK,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_STAT_MVA_ML_TOOL
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.STAT_MVA_ML_TOOL
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'NULL') <> COALESCE(TO_CHAR(:NEW.RANK), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.RANK,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_MVA_ML_TOOL (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.RANK,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_STAT_STAT_TOOL
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.STAT_STAT_TOOL
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.CODE), 'NULL') <> COALESCE(TO_CHAR(:NEW.CODE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CODE',
          TO_CHAR(:OLD.CODE),
          TO_CHAR(:NEW.CODE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'NULL') <> COALESCE(TO_CHAR(:NEW.NAME), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RANK), 'NULL') <> COALESCE(TO_CHAR(:NEW.RANK), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL (
          ID,
          CODE,
          NAME,
          RANK,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.CODE,
          :NEW.NAME,
          :NEW.RANK,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'RANK',
          TO_CHAR(:OLD.RANK),
          TO_CHAR(:NEW.RANK)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.CODE,
      :OLD.NAME,
      :OLD.RANK,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.STAT_STAT_TOOL (
      ID,
      CODE,
      NAME,
      RANK,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.CODE,
      :NEW.NAME,
      :NEW.RANK,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.SHORT_TITLE), 'NULL') <> COALESCE(TO_CHAR(:NEW.SHORT_TITLE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
          ID,
          SHORT_TITLE,
          PUBLIC_SHORT_TITLE,
          FULL_TITLE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SHORT_TITLE,
          :NEW.PUBLIC_SHORT_TITLE,
          :NEW.FULL_TITLE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'SHORT_TITLE',
          TO_CHAR(:OLD.SHORT_TITLE),
          TO_CHAR(:NEW.SHORT_TITLE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PUBLIC_SHORT_TITLE), 'NULL') <> COALESCE(TO_CHAR(:NEW.PUBLIC_SHORT_TITLE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
          ID,
          SHORT_TITLE,
          PUBLIC_SHORT_TITLE,
          FULL_TITLE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SHORT_TITLE,
          :NEW.PUBLIC_SHORT_TITLE,
          :NEW.FULL_TITLE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'PUBLIC_SHORT_TITLE',
          TO_CHAR(:OLD.PUBLIC_SHORT_TITLE),
          TO_CHAR(:NEW.PUBLIC_SHORT_TITLE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FULL_TITLE), 'NULL') <> COALESCE(TO_CHAR(:NEW.FULL_TITLE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
          ID,
          SHORT_TITLE,
          PUBLIC_SHORT_TITLE,
          FULL_TITLE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SHORT_TITLE,
          :NEW.PUBLIC_SHORT_TITLE,
          :NEW.FULL_TITLE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'FULL_TITLE',
          TO_CHAR(:OLD.FULL_TITLE),
          TO_CHAR(:NEW.FULL_TITLE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.COMMENTS), 'NULL') <> COALESCE(TO_CHAR(:NEW.COMMENTS), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
          ID,
          SHORT_TITLE,
          PUBLIC_SHORT_TITLE,
          FULL_TITLE,
          COMMENTS,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SHORT_TITLE,
          :NEW.PUBLIC_SHORT_TITLE,
          :NEW.FULL_TITLE,
          :NEW.COMMENTS,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'COMMENTS',
          TO_CHAR(:OLD.COMMENTS),
          TO_CHAR(:NEW.COMMENTS)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
      ID,
      SHORT_TITLE,
      PUBLIC_SHORT_TITLE,
      FULL_TITLE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.SHORT_TITLE,
      :OLD.PUBLIC_SHORT_TITLE,
      :OLD.FULL_TITLE,
      :OLD.COMMENTS,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK (
      ID,
      SHORT_TITLE,
      PUBLIC_SHORT_TITLE,
      FULL_TITLE,
      COMMENTS,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.SHORT_TITLE,
      :NEW.PUBLIC_SHORT_TITLE,
      :NEW.FULL_TITLE,
      :NEW.COMMENTS,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_ADD_MEMBER_IN_EGROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_ADD_MEMBER_IN_EGROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ADD_MEMBER_IN_EGROUP (
          ID,
          TASK_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ADD_MEMBER_IN_EGROUP (
          ID,
          TASK_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ADD_MEMBER_IN_EGROUP (
      ID,
      TASK_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ADD_MEMBER_IN_EGROUP (
      ID,
      TASK_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_ANALYSIS_TEAM_MEMBER
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_ANALYSIS_TEAM_MEMBER
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ANALYSIS_TEAM_MEMBER (
          ID,
          TASK_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ANALYSIS_TEAM_MEMBER (
          ID,
          TASK_ID,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ANALYSIS_TEAM_MEMBER (
      ID,
      TASK_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_ANALYSIS_TEAM_MEMBER (
      ID,
      TASK_ID,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_COLLISION
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_COLLISION
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.YEAR_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.YEAR_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'YEAR_ID',
          TO_CHAR(:OLD.YEAR_ID),
          TO_CHAR(:NEW.YEAR_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RUN_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.RUN_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'RUN_ID',
          TO_CHAR(:OLD.RUN_ID),
          TO_CHAR(:NEW.RUN_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TYPE_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TYPE_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TYPE_ID',
          TO_CHAR(:OLD.TYPE_ID),
          TO_CHAR(:NEW.TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ECM_VALUE_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.ECM_VALUE_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'ECM_VALUE_ID',
          TO_CHAR(:OLD.ECM_VALUE_ID),
          TO_CHAR(:NEW.ECM_VALUE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ECM_UNIT_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.ECM_UNIT_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'ECM_UNIT_ID',
          TO_CHAR(:OLD.ECM_UNIT_ID),
          TO_CHAR(:NEW.ECM_UNIT_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LUMINOSITY_VALUE), 'NULL') <> COALESCE(TO_CHAR(:NEW.LUMINOSITY_VALUE), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'LUMINOSITY_VALUE',
          TO_CHAR(:OLD.LUMINOSITY_VALUE),
          TO_CHAR(:NEW.LUMINOSITY_VALUE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LUMINOSITY_UNIT_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.LUMINOSITY_UNIT_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
          ID,
          TASK_ID,
          YEAR_ID,
          RUN_ID,
          TYPE_ID,
          ECM_VALUE_ID,
          ECM_UNIT_ID,
          LUMINOSITY_VALUE,
          LUMINOSITY_UNIT_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.YEAR_ID,
          :NEW.RUN_ID,
          :NEW.TYPE_ID,
          :NEW.ECM_VALUE_ID,
          :NEW.ECM_UNIT_ID,
          :NEW.LUMINOSITY_VALUE,
          :NEW.LUMINOSITY_UNIT_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'LUMINOSITY_UNIT_ID',
          TO_CHAR(:OLD.LUMINOSITY_UNIT_ID),
          TO_CHAR(:NEW.LUMINOSITY_UNIT_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
      ID,
      TASK_ID,
      YEAR_ID,
      RUN_ID,
      TYPE_ID,
      ECM_VALUE_ID,
      ECM_UNIT_ID,
      LUMINOSITY_VALUE,
      LUMINOSITY_UNIT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.YEAR_ID,
      :OLD.RUN_ID,
      :OLD.TYPE_ID,
      :OLD.ECM_VALUE_ID,
      :OLD.ECM_UNIT_ID,
      :OLD.LUMINOSITY_VALUE,
      :OLD.LUMINOSITY_UNIT_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_COLLISION (
      ID,
      TASK_ID,
      YEAR_ID,
      RUN_ID,
      TYPE_ID,
      ECM_VALUE_ID,
      ECM_UNIT_ID,
      LUMINOSITY_VALUE,
      LUMINOSITY_UNIT_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.YEAR_ID,
      :NEW.RUN_ID,
      :NEW.TYPE_ID,
      :NEW.ECM_VALUE_ID,
      :NEW.ECM_UNIT_ID,
      :NEW.LUMINOSITY_VALUE,
      :NEW.LUMINOSITY_UNIT_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_CONTACT_EDITOR
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_CONTACT_EDITOR
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_CONTACT_EDITOR (
        ID,
        AGENT_ID,
        MODIFIED_ON,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'U',
      NULL,
      NULL,
      NULL
    );

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_CONTACT_EDITOR (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_CONTACT_EDITOR (
      ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_KEYWORD
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_KEYWORD
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_KEYWORD (
          ID,
          TASK_ID,
          KEYWORD_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.KEYWORD_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.KEYWORD_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.KEYWORD_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_KEYWORD (
          ID,
          TASK_ID,
          KEYWORD_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.KEYWORD_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'KEYWORD_ID',
          TO_CHAR(:OLD.KEYWORD_ID),
          TO_CHAR(:NEW.KEYWORD_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_KEYWORD (
      ID,
      TASK_ID,
      KEYWORD_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.KEYWORD_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_KEYWORD (
      ID,
      TASK_ID,
      KEYWORD_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.KEYWORD_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_LEADING_GROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_LEADING_GROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_GROUP (
          ID,
          TASK_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.GROUP_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.GROUP_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_GROUP (
          ID,
          TASK_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'GROUP_ID',
          TO_CHAR(:OLD.GROUP_ID),
          TO_CHAR(:NEW.GROUP_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_GROUP (
      ID,
      TASK_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.GROUP_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_GROUP (
      ID,
      TASK_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.GROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_MEMB_CONTRIBUTION
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_MEMB_CONTRIBUTION
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.SUPPORTING_INT_DOC_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.SUPPORTING_INT_DOC_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION (
          ID,
          SUPPORTING_INT_DOC_ID,
          CONTRIBUTION,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SUPPORTING_INT_DOC_ID,
          :NEW.CONTRIBUTION,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'SUPPORTING_INT_DOC_ID',
          TO_CHAR(:OLD.SUPPORTING_INT_DOC_ID),
          TO_CHAR(:NEW.SUPPORTING_INT_DOC_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CONTRIBUTION), 'NULL') <> COALESCE(TO_CHAR(:NEW.CONTRIBUTION), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION (
          ID,
          SUPPORTING_INT_DOC_ID,
          CONTRIBUTION,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SUPPORTING_INT_DOC_ID,
          :NEW.CONTRIBUTION,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'CONTRIBUTION',
          TO_CHAR(:OLD.CONTRIBUTION),
          TO_CHAR(:NEW.CONTRIBUTION)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION (
          ID,
          SUPPORTING_INT_DOC_ID,
          CONTRIBUTION,
          MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.SUPPORTING_INT_DOC_ID,
          :NEW.CONTRIBUTION,
          :NEW.MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION (
      ID,
      SUPPORTING_INT_DOC_ID,
      CONTRIBUTION,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.SUPPORTING_INT_DOC_ID,
      :OLD.CONTRIBUTION,
      :OLD.MEMBER_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MEMB_CONTRIBUTION (
      ID,
      SUPPORTING_INT_DOC_ID,
      CONTRIBUTION,
      MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.SUPPORTING_INT_DOC_ID,
      :NEW.CONTRIBUTION,
      :NEW.MEMBER_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_MVA_ML_TOOL
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_MVA_ML_TOOL
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MVA_ML_TOOL (
          ID,
          TASK_ID,
          MVA_ML_TOOL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MVA_ML_TOOL_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.MVA_ML_TOOL_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.MVA_ML_TOOL_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MVA_ML_TOOL (
          ID,
          TASK_ID,
          MVA_ML_TOOL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.MVA_ML_TOOL_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'MVA_ML_TOOL_ID',
          TO_CHAR(:OLD.MVA_ML_TOOL_ID),
          TO_CHAR(:NEW.MVA_ML_TOOL_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MVA_ML_TOOL (
      ID,
      TASK_ID,
      MVA_ML_TOOL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.MVA_ML_TOOL_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_MVA_ML_TOOL (
      ID,
      TASK_ID,
      MVA_ML_TOOL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.MVA_ML_TOOL_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_OTHER_GROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_OTHER_GROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_OTHER_GROUP (
          ID,
          TASK_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.GROUP_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.GROUP_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_OTHER_GROUP (
          ID,
          TASK_ID,
          GROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.GROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'GROUP_ID',
          TO_CHAR(:OLD.GROUP_ID),
          TO_CHAR(:NEW.GROUP_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_OTHER_GROUP (
      ID,
      TASK_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.GROUP_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_OTHER_GROUP (
      ID,
      TASK_ID,
      GROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.GROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_STAT_INTERPRETATION
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_STAT_INTERPRETATION
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_INTERPRETATION (
          ID,
          TASK_ID,
          STAT_INTERPRETATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STAT_INTERPRETATION_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STAT_INTERPRETATION_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.STAT_INTERPRETATION_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_INTERPRETATION (
          ID,
          TASK_ID,
          STAT_INTERPRETATION_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STAT_INTERPRETATION_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'STAT_INTERPRETATION_ID',
          TO_CHAR(:OLD.STAT_INTERPRETATION_ID),
          TO_CHAR(:NEW.STAT_INTERPRETATION_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_INTERPRETATION (
      ID,
      TASK_ID,
      STAT_INTERPRETATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.STAT_INTERPRETATION_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_INTERPRETATION (
      ID,
      TASK_ID,
      STAT_INTERPRETATION_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.STAT_INTERPRETATION_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_STAT_TOOL
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_STAT_TOOL
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_TOOL (
          ID,
          TASK_ID,
          STAT_TOOL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STAT_TOOL_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STAT_TOOL_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.STAT_TOOL_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_TOOL (
          ID,
          TASK_ID,
          STAT_TOOL_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STAT_TOOL_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'STAT_TOOL_ID',
          TO_CHAR(:OLD.STAT_TOOL_ID),
          TO_CHAR(:NEW.STAT_TOOL_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_TOOL (
      ID,
      TASK_ID,
      STAT_TOOL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.STAT_TOOL_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STAT_TOOL (
      ID,
      TASK_ID,
      STAT_TOOL_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.STAT_TOOL_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_STA_IN_EGROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_STA_IN_EGROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STA_IN_EGROUP (
          ID,
          TASK_ID,
          STA_MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STA_MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.STA_MEMBER_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.STA_MEMBER_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STA_IN_EGROUP (
          ID,
          TASK_ID,
          STA_MEMBER_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.STA_MEMBER_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'STA_MEMBER_ID',
          TO_CHAR(:OLD.STA_MEMBER_ID),
          TO_CHAR(:NEW.STA_MEMBER_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STA_IN_EGROUP (
      ID,
      TASK_ID,
      STA_MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.STA_MEMBER_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_STA_IN_EGROUP (
      ID,
      TASK_ID,
      STA_MEMBER_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.STA_MEMBER_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_SUBGROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_SUBGROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUBGROUP (
        TASK_ID,
        SUBGROUP_ID,
        AGENT_ID,
        MODIFIED_ON,
        ACTION,
        DIFF_COLUMN,
        OLD_VALUE,
        NEW_VALUE
      )
    VALUES (
      :NEW.TASK_ID,
      :NEW.SUBGROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'U',
      NULL,
      NULL,
      NULL
    );

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUBGROUP (
      TASK_ID,
      SUBGROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.TASK_ID,
      :OLD.SUBGROUP_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUBGROUP (
      TASK_ID,
      SUBGROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.TASK_ID,
      :NEW.SUBGROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_SUPPORTING_INT_DOC
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_SUPPORTING_INT_DOC
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUPPORTING_INT_DOC (
          ID,
          TASK_ID,
          LINK_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.LINK_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.LINK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.LINK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUPPORTING_INT_DOC (
          ID,
          TASK_ID,
          LINK_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.LINK_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'LINK_ID',
          TO_CHAR(:OLD.LINK_ID),
          TO_CHAR(:NEW.LINK_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUPPORTING_INT_DOC (
      ID,
      TASK_ID,
      LINK_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.LINK_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_SUPPORTING_INT_DOC (
      ID,
      TASK_ID,
      LINK_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.LINK_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE TRIGGER ATLAS_PUB_TRACK.HTR_TASK_LEADING_SUBGROUP
    AFTER INSERT OR UPDATE OR DELETE
    ON ATLAS_PUB_TRACK.TASK_LEADING_SUBGROUP
    FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.TASK_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.TASK_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_SUBGROUP (
          ID,
          TASK_ID,
          SUBGROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.SUBGROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'TASK_ID',
          TO_CHAR(:OLD.TASK_ID),
          TO_CHAR(:NEW.TASK_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SUBGROUP_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.SUBGROUP_ID), 'NULL') THEN
      INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_SUBGROUP (
          ID,
          TASK_ID,
          SUBGROUP_ID,
          AGENT_ID,
          MODIFIED_ON,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.TASK_ID,
          :NEW.SUBGROUP_ID,
          :NEW.AGENT_ID,
          SYSTIMESTAMP,
          'U',
          'SUBGROUP_ID',
          TO_CHAR(:OLD.SUBGROUP_ID),
          TO_CHAR(:NEW.SUBGROUP_ID)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_SUBGROUP (
      ID,
      TASK_ID,
      SUBGROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.TASK_ID,
      :OLD.SUBGROUP_ID,
      :OLD.AGENT_ID,
      SYSTIMESTAMP,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO ATLAS_PUB_TRACK_HISTORY.TASK_LEADING_SUBGROUP (
      ID,
      TASK_ID,
      SUBGROUP_ID,
      AGENT_ID,
      MODIFIED_ON,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.TASK_ID,
      :NEW.SUBGROUP_ID,
      :NEW.AGENT_ID,
      SYSTIMESTAMP,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

--rollback DECLARE
--rollback   V_TRIGGER_NAME VARCHAR2(100);
--rollback BEGIN
--rollback   FOR TRIGGER_REC IN (SELECT TRIGGER_NAME FROM ALL_TRIGGERS WHERE OWNER = 'ATLAS_PUB_TRACK') LOOP
--rollback     V_TRIGGER_NAME := TRIGGER_REC.TRIGGER_NAME;
--rollback     EXECUTE IMMEDIATE 'DROP TRIGGER ATLAS_PUB_TRACK.' || V_TRIGGER_NAME;
--rollback   END LOOP;
--rollback END;
--rollback /