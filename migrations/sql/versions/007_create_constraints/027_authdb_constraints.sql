--liquibase formatted sql
--changeset Gabriel:27 endDelimiter:/ rollbackEndDelimiter:/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_NOMINATION_DRAFT_PK
        primary key (ID, MEMB_ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint CHECK_ATLAS_FRACTION
        check (ATLAS_FRACTION BETWEEN 0 AND 100)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint CHECK_VALID_PRIORITY
        check (PRIORITY BETWEEN 1 AND 5)
/

alter table ATLAS_AUTHDB.IB_MESSAGE
    add constraint IB_MESSAGE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORIES
    add primary key (ID)
/

alter table ATLAS_AUTHDB.COUNTRY
    add constraint COUNTRY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_PUBNOTE
    add constraint SYSTEM_PUBNOTE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_PUBNOTE
    add constraint SYSTEM_PUBNOTE_U1
        unique (PUBNOTE_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISGROUP
    add primary key (PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.NOMINATION_CATEGORY
    add constraint NC_CAT
        check (category IN ('PA', 'CP'))
/

alter table ATLAS_AUTHDB.SUBSYSTEM
    add constraint SUBSYSTEMS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_INACTIVE
    add constraint APP_CATEGORY_INACTIVE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_INACTIVE
    add constraint APP_CATEGORY_INACTIVE_FK1
        foreign key (APPOINTMENT_CATEGORY_ID) references ATLAS_AUTHDB.APPOINTMENT_CATEGORIES
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PUBNOTE
    add constraint ABS_ABSTRACT_PUBNOTE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PUBNOTE
    add constraint ABS_PUBNOTE_UK1
        unique (ABSTRACT_ID, PUBNOTE_ID)
/

alter table ATLAS_AUTHDB.QU_STATE
    add constraint QU_STATE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_ADDRESS
    add constraint IN_ADDRESS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_ADDRESS
    add constraint IN_ADDRESS_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE_1
    add primary key (PHASE_ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
    add constraint STA_SHORT_TERM_REGISTERED_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
    add constraint STA_SHORT_TERM_REGISTERED_AK_1
        unique (SHORT_TERM_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION
    add constraint PUBLICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISGROUP
    add foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
        on delete cascade
/

-- Was moved here since a primary key is a pre-condition for a foreign key
alter table ATLAS_PUB_TRACK.TASK
    add constraint TASK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION
    add constraint PUBLICATION_FK3
        foreign key (TASK_ID) references ATLAS_PUB_TRACK.TASK (ID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_EGROUP
    add constraint STA_TOPIC_EGROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_EGROUP
    add constraint STA_TOPIC_EGROUP_U1
        unique (STA_TOPIC_ID)
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_INT
    add constraint PUBNOTE_STAT_INT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_INT
    add constraint PUBNOTE_STAT_INT_AK_1
        unique (PUBLICATION_ID, INTERPRETATION_ID)
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER_COMMON_CHOICE
    add constraint PTRIGGER_COMMON_CHOICE_PK
        primary key (TRIGGER_ID, COMMON_CHOICE_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_ANALYSIS
    add constraint ABS_ABSTRACT_ANALYSIS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_ANALYSIS
    add constraint ABS_ANALYSIS_UK1
        unique (ABSTRACT_ID, ANALYSIS_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_HIERARCHY
    add constraint ACTIVITY_HIERARCHY_PK
        primary key (ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_HIERARCHY
    add constraint ACTIVITY_HIERARCHY_PARENT
        foreign key (PARENT_ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY_HIERARCHY
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_FUNCTION
    add constraint ANALYSIS_SYS_FUNCTION_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.INST_ACTIVITIES_LIST
    add constraint INST_ACTIVITIES_LIST_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_VETOED_COUNTRY
    add constraint ME_VETOED_COUNTRY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHASE
    add constraint PHASE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHASE
    add unique (PUBLICATION_ID, PHASE)
/

alter table ATLAS_AUTHDB.PHASE
    add constraint PHASE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.FUNDA
    add constraint FUNDA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE
    add constraint PLOT_PHASE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFIRMED_TALK
    add constraint CONFIRMED_TALK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.FUNDA_DOI
    add constraint FUNDA_DOI_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.FUNDA_DOI
    add constraint FUNDA_DOI_CHK1
        check (type IN ('FUNDA', 'FOUNDATION'))
/

alter table ATLAS_AUTHDB.ND_UPGRADE
    add constraint ND_UPGRADE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_UPGRADE
    add constraint ND_UPGRADE_UNIQUE_NOMINATION
        unique (ND_ID)
/

alter table ATLAS_AUTHDB.ND_UPGRADE
    add constraint ND_UPGRADE_ND_NOMINATION_DRAFT
        foreign key (ND_ID, ND_MEMB_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT
            on delete cascade
/

alter table ATLAS_AUTHDB.ACADEMIC_RECORD
    add constraint ACADEMIC_RECORD_CHK1
        check (TYPE IN ('PhD', 'Master', 'Bachelor', 'Internal Note', 'Other'))
/

alter table ATLAS_AUTHDB.CONFNOTE_ECM
    add primary key (PUBLICATION_ID, ECM_VALUE)
/

alter table ATLAS_AUTHDB.GITLAB_GROUP
    add constraint GITLAB_GROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_FUNCTION
    add constraint ANALYSIS_SYS_FUNCTION_MEMB_FK1
        foreign key (FUNCTION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_FUNCTION
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint ME_PREFERRED_CONF_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint ME_PREFERRED_CONF_U1
        unique (RANK, MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint MAX_PREFERRED_CONF
        check (RANK >= 1 AND RANK <= 5)
/

alter table ATLAS_AUTHDB.ME_VETOED_TALK_TYPE
    add constraint ME_VETOED_TALK_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_VETOED_ALL_PHYS_ANA_TALK_TYPE
    add constraint ME_VETOED_ALL_PHYS_ANA_TALK_TYPE_PK
        primary key (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_LINKS
    add constraint ANALYSIS_SYS_LINKS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHASE_3
    add primary key (PHASE_ID, LOOP_SEQUENCE)
/

alter table ATLAS_AUTHDB.PHASE_3
    add constraint PHASE_3_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.IN_AFFILIATION
    add constraint IN_AFFILIATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_AFFILIATION
    add constraint IN_AFFILIATION_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.MEMB_PUBLICATION
    add constraint MEMB_PUBLICATION_UK1
        unique (MEMB_ID, PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.MEMB_PUBLICATION
    add constraint MEMB_PUBLICATION_FK2
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_MAT
    add constraint TALKMAT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_MAT
    add constraint TALK_MAT_FK
        foreign key (TALK_ID) references ATLAS_AUTHDB.TALK
            on delete cascade
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT
    add constraint PUB_NOMINATION_DRAFT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT
    add constraint MEMBER_PUBLICATION_ND
        unique (PUBLICATION_ID, MEMBER_ID)
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT
    add constraint PUB_NOMINATION_DRAFT_FK
        foreign key (NOMINATION_DRAFT_ID, MEMBER_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT
            on delete cascade
/

alter table ATLAS_AUTHDB.LEGACY_MEMBER
    add constraint LEGACY_MEMBER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.LEGACY_MEMBER
    add constraint LEGACY_MEMBER_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.WEIGHT
    add constraint WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.WEIGHT
    add constraint VALID_PERIOD
        check (SINCE < UP_TO OR UP_TO IS NULL)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GITLAB_GROUP
    add constraint ANALYSIS_SYS_GITLAB_GROUP_PK
        primary key (PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GITLAB_GROUP
    add constraint ANALYSIS_SYS_GITLAB_GROUP_FK2
        foreign key (GROUP_ID) references ATLAS_AUTHDB.GITLAB_GROUP
/

alter table ATLAS_AUTHDB.NOMINATIONS_BACKUP
    add constraint NOMINATIONS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOMINATIONS_BACKUP
    add constraint NOMINATION_SINGLE_SUBMITTER
        unique (MEMB_ID, APP_RESP, IR_RESP, SYSTEMS_ID)
/

alter table ATLAS_AUTHDB.ADBEXCP
    add constraint ADBEXCP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.GRT_GRANT
    add constraint GRT_GRANT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.GRT_GRANT
    add constraint GRT_GRANT_U1
        unique (CODE)
/

alter table ATLAS_AUTHDB.BKG_BACKGROUND_COMMANDS
    add primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_GROUP
    add primary key (PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.SUBGROUPS
    add constraint SUBGROUPS_PK
        primary key (SUBGROUP_ID)
/

alter table ATLAS_AUTHDB.SUBGROUPS
    add constraint SUBGROUPS_UK1
        unique (ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.STA_AFFILIATION
    add constraint STA_AFFILIATION_PK
        primary key (STA_ID)
/

alter table ATLAS_AUTHDB.IB_TOPIC_EMAIL
    add constraint IB_TOPIC_EMAIL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_PRIORITY_WEIGHT
    add constraint CONF_PRIORITY_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_PRIORITY_WEIGHT
    add constraint CONF_PRIORITY_WEIGHT_WEIGHT
        foreign key (WEIGHT_ID) references ATLAS_AUTHDB.WEIGHT
/

alter table ATLAS_AUTHDB.CONF_STATUS_WEIGHT
    add constraint CONF_STATUS_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_STATUS_WEIGHT
    add constraint CONF_STATUS_WEIGHT_WEIGHT
        foreign key (WEIGHT_ID) references ATLAS_AUTHDB.WEIGHT
/

alter table ATLAS_AUTHDB.CONFNOTE_MEMB_PUBLICATION
    add constraint CONFNOTE_MEMB_PUBLICATION_UK1
        unique (MEMB_ID, PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.PLOT_SUPP_NOTES
    add constraint PLOT_SUPP_NOTES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.GLOSSARY
    add primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_GROUP
    add primary key (PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.PHASE_1
    add primary key (PHASE_ID)
/

alter table ATLAS_AUTHDB.PHASE_1
    add constraint PHASE_1_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.SUBMISSION
    add primary key (PHASE_ID)
/

alter table ATLAS_AUTHDB.SUBMISSION
    add foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
        on delete cascade
/

alter table ATLAS_AUTHDB.TALK_CATEGORY
    add constraint TALK_CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_CATEGORY_FK
        foreign key (CATEGORY) references ATLAS_AUTHDB.TALK_CATEGORY
/

alter table ATLAS_AUTHDB.IB_TOPIC
    add constraint IB_TOPIC_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IB_MESSAGE
    add constraint IB_MESSAGES_IB_TOPICS
        foreign key (TOPIC_ID) references ATLAS_AUTHDB.IB_TOPIC
/

alter table ATLAS_AUTHDB.IB_TOPIC_EMAIL
    add constraint IB_TOPICS_EMAILS_IB_TOPICS
        foreign key (TOPIC_ID) references ATLAS_AUTHDB.IB_TOPIC
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHYSICS_TRIGGER
    add constraint ANALYSIS_SYS_PTRIGGER_PK
        primary key (PUBLICATION_ID, TRIGGER_ID)
/

alter table ATLAS_AUTHDB.PLOT_MEMB_CONTRIBUTION
    add constraint PLOT_MEMB_CONTRIBUTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_MEMB_CONTRIBUTION
    add constraint PLOT_MEMB_CONTRIBUTION_FK2
        foreign key (SUPP_ID) references ATLAS_AUTHDB.PLOT_SUPP_NOTES
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP
    add constraint SYSTEM_IB_EGROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP
    add constraint SYSTEM_IB_EGROUP_U1
        unique (SYSTEM_IB_ID, TYPE_ID)
/

alter table ATLAS_AUTHDB.TALK_PRELIST_VOLUNTEER
    add constraint TALK_PRELIST_VOLUNTEER_PK
        primary key (TALK_ID, NOMINEE_ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE_1
    add constraint PLOT_PHASE_1_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE_1
    add constraint PLOT_PHASE_1_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PLOT_PHASE
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_U1
        unique (INSTITUTE_SYSTEM_IB_ID, MEMBER_ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_U2
        unique (INSTITUTE_SYSTEM_IB_ID, RANK)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_C1
        check (RANK >= 1 AND RANK <= 3)
/

alter table ATLAS_AUTHDB.ACTIVITY_TYPE
    add constraint ACTIVITY_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INST_ACTIVITIES_NEW
    add constraint INST_ACTIVITIES_NEW_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.INST_ACTIVITIES_LIST
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_TYPE
    add constraint ABS_ABSTRACT_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_PRIORITY
    add constraint CONF_PRIORITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_PRIORITY_WEIGHT
    add constraint CONF_PRT_WEIGHT_CONF_PRT
        foreign key (CONF_PRIORITY_ID) references ATLAS_AUTHDB.CONF_PRIORITY
/

alter table ATLAS_AUTHDB.USERS_USERGROUP
    add constraint USERS_USERGROUP_FK3
        foreign key (NCP_FUNDA_ID) references ATLAS_AUTHDB.FUNDA
/

alter table ATLAS_AUTHDB.TALK_MAT_REQ
    add constraint TALK_MAT_REQ_CHECK
        check (type IN ('UPLOAD', 'REVIEW', 'REHEARSAL', 'REHEARSAL_REMINDER', 'FINAL_UPLOAD', 'FINAL_UPLOAD_REMINDER',
                        'PROCEEDINGS'))
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_TOOL
    add constraint PUBLICATION_STAT_TOOL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_TOOL
    add constraint PUBLICATION_STAT_TOOL_AK_1
        unique (PUBLICATION_ID, TOOL_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_TOOL
    add constraint PUBLICATION_STAT_TOOL_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.CONF_CONTACT
    add constraint CONF_CT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE_LINKS
    add constraint PLOT_PHASE_LINKS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE_LINKS
    add constraint PLOT_PHASE_LINKS_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PLOT_PHASE
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION_DRAFT
    add constraint ACTIVITY_NOMINATION_DRAFT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION_DRAFT
    add constraint AT_ND
        unique (MEMB_ID, ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION_DRAFT
    add constraint APPOINTMENT_NOMINATION_DRAFT
        foreign key (NOMINATION_DRAFT_ID, MEMB_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT
/

alter table ATLAS_AUTHDB.GENERAL_COUNTRY
    add constraint GENERAL_COUNTRY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.COUNTRY
    add constraint COUNTRY_FK1
        foreign key (ID) references ATLAS_AUTHDB.GENERAL_COUNTRY
/

alter table ATLAS_AUTHDB.ME_VETOED_COUNTRY
    add constraint ME_VETOED_COUNTRY_FK2
        foreign key (GENERAL_COUNTRY_ID) references ATLAS_AUTHDB.GENERAL_COUNTRY
/

alter table ATLAS_AUTHDB.PUBNOTE_MEMB_CONTRIBUTION
    add constraint PUBNOTE_MEMB_CONTRIBUTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    add constraint CONFNOTE_PUBLICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_ECM
    add foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
        on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_GROUP
    add foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
        on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_MEMB_PUBLICATION
    add constraint CONFNOTE_MEMB_PUBLICATION_FK2
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    add constraint CONFNOTE_PUBLICATION_FK3
        foreign key (TASK_ID) references ATLAS_PUB_TRACK.TASK (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_COLLISION
    add constraint CONFNOTE_COLLISION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.CATEGORY_MANAGING_APPOINTMENT
    add constraint CATEGORY_MANAGING_APPOINT_UK1
        unique (APPOINTMENT_ID, CATEGORY_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_KEYWORD
    add constraint ABS_ABSTRACT_KEYWORD_PK
        primary key (ID)
/

-- Was moved here since a primary key is a pre-condition for a foreign key
alter table ATLAS_PUB_TRACK.KWD_KEYWORD
    add constraint KWD_KEYWORD_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_KEYWORD
    add constraint ABS_ABSTRACT_KEYWORD_FK2
        foreign key (KEYWORD_ID) references ATLAS_PUB_TRACK.KWD_KEYWORD (ID)
/

alter table ATLAS_AUTHDB.STA_AUTHORSHIP
    add constraint STA_AUTHORSHIP_UNIQUE
        unique (STA_ID, PAPER_ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_ADD_CONTACT
    add constraint IN_SYSTEM_IB_ADD_CONTACT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_ADD_CONTACT
    add constraint IN_SYSTEM_IB_ADD_CONTACT_U1
        unique (MEMBER_ID, INSTITUTE_SYSTEM_IB_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_KEYWORD
    add constraint CONFNOTE_KEYWORD_FK2
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.KEYWORD
    add constraint KEYWORDS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_KEYWORD
    add constraint CONFNOTE_KEYWORD_FK1
        foreign key (KEYWORD_ID) references ATLAS_AUTHDB.KEYWORD
/

alter table ATLAS_AUTHDB.INST_SYSTEM_IB_LIST_BACKUP
    add constraint INST_SYSTEM_IB_LIST_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_TOOL
    add constraint ANALYSIS_SYS_STAT_TOOL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_TOOL
    add constraint ANALYSIS_SYS_STAT_TOOL_AK_1
        unique (PUBLICATION_ID, TOOL_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_SUPP_NOTES
    add constraint ANALYSIS_SYS_SUPP_NOTES_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_SECRETARY
    add constraint IN_SECRETARY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_SECRETARY
    add constraint IN_SECRETARY_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
    add constraint PK_MEMB_TALK_PREFERENCES
        primary key (MEMB_ID)
/

alter table ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
    add check (applying_for_job in ('Y', 'N'))
/

alter table ATLAS_AUTHDB.PTYPE
    add constraint PTYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION_DRAFT
    add constraint INSTITUTE_NOMINATION_DRAFT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION_DRAFT
    add constraint INST_ND
        unique (INSTITUTE_ID, MEMB_ID)
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION_DRAFT
    add constraint INSTITUTE_NOMINATION_DRAFT
        foreign key (NOMINATION_DRAFT_ID, MEMB_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PUBNOTE
    add constraint ABS_ABSTRACT_PUBNOTE_FK2
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_ANALYSIS
    add constraint ABS_ABSTRACT_ANALYSIS_FK2
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_KEYWORD
    add constraint ABS_ABSTRACT_KEYWORD_FK1
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_UK1
        unique (YEAR, SEQUENCE)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_FK4
        foreign key (TYPE_ID) references ATLAS_AUTHDB.ABS_ABSTRACT_TYPE
/

alter table ATLAS_AUTHDB.SHORT_TERM
    add constraint SHORT_TERM_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SHORT_TERM
    add constraint AFF_CHECK
        check (CASE WHEN inst_id IS NOT NULL THEN 1 ELSE 0 END + CASE WHEN ghost_id IS NOT NULL THEN 1 ELSE 0 END = 1)
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint MA_PK
        primary key (MEETING_ID, INSTITUTION_ID)
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint MA_SINGLE
        check ((CASE WHEN memb_id IS NOT NULL THEN 1 ELSE 0 END +
                CASE WHEN proxy_id IS NOT NULL THEN 1 ELSE 0 END) = 1)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFNOTE
    add constraint ABS_ABSTRACT_CONFNOTE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFNOTE
    add constraint ABS_CONFNOTE_UK1
        unique (ABSTRACT_ID, CONFNOTE_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFNOTE
    add constraint ABS_ABSTRACT_CONFNOTE_FK2
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFNOTE
    add constraint ABS_ABSTRACT_CONFNOTE_FK3
        foreign key (CONFNOTE_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.NOMINATIONS_ACTIVITIES
    add constraint NOMINATIONS_ACTIVITIES_PK
        primary key (NOMINATION_ID, ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.NOMINATIONS_ACTIVITIES
    add constraint "BIN$P9ndH8Fjg2rgU64tuLxMMw==$0"
        check ("NOMINATION_ID" IS NOT NULL)
/

alter table ATLAS_AUTHDB.NOMINATIONS_ACTIVITIES
    add constraint "BIN$P9ndH8Fkg2rgU64tuLxMMw==$0"
        check ("ACTIVITY_ID" IS NOT NULL)
/

alter table ATLAS_AUTHDB.DOCTRINE_MIGRATION_VERSIONS
    add primary key (VERSION)
/

alter table ATLAS_AUTHDB.INSTITUTE_REP
    add constraint IREP_UNIQ
        unique (MEMB_ID, INST_ID, TYPE)
/

alter table ATLAS_AUTHDB.INSTITUTE_REP
    add constraint IR_TYPE_CHECK
        check (type IN
               ('ATLAS', 'DEP', 'SEC', 'UPGRADE', 'TDAQ', 'FWD', 'COMP', 'ID', 'ITK', 'LAR', 'OUTREACH', 'TILE', 'MUON',
                'HGTD'))
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_RETIREMENT
    add constraint ABS_ABSTRACT_RETIREMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_RETIREMENT
    add constraint ABS_ABSTRACT_RETIREMENT_UK1
        unique (ABSTRACT_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_RETIREMENT
    add constraint ABS_ABSTRACT_RETIREMENT_FK1
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.PUBLICATION_COLLISION
    add constraint PUBLICATION_COLLISION_FK
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.CONF
    add constraint CONF_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_CONF_FK
        foreign key (CONF_ID) references ATLAS_AUTHDB.CONF
            on delete cascade
/

alter table ATLAS_AUTHDB.CONFIRMED_TALK
    add constraint CONFIRMED_TALK_FK1
        foreign key (CONF_ID) references ATLAS_AUTHDB.CONF
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint ME_PREFERRED_CONF_CONF_FK
        foreign key (CONF_ID) references ATLAS_AUTHDB.CONF
            on delete cascade
/

alter table ATLAS_AUTHDB.MEMB_CONF
    add constraint MEMB_CONF_FK2
        foreign key (CONF_ID) references ATLAS_AUTHDB.CONF
            on delete cascade
/

alter table ATLAS_AUTHDB.CONF_CONTACT
    add constraint CONF_CT_FK
        foreign key (CONF_ID) references ATLAS_AUTHDB.CONF
/

alter table ATLAS_AUTHDB.CONF
    add constraint CONF_GENERAL_COUNTRY_ID
        foreign key (GENERAL_COUNTRY_ID) references ATLAS_AUTHDB.GENERAL_COUNTRY
/

alter table ATLAS_AUTHDB.CONF
    add constraint CONF_PRIORITY_ID
        foreign key (PRIORITY) references ATLAS_AUTHDB.CONF_PRIORITY
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_KEYWORD
    add constraint ANALYSIS_SYS_KEYWORD_PK1
        primary key (KEYWORD_ID, PUBLICATION_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_KEYWORD
    add constraint ANALYSIS_SYS_KEYWORD_FK2
        foreign key (KEYWORD_ID) references ATLAS_AUTHDB.KEYWORD
/

alter table ATLAS_AUTHDB.ACTIVITY
    add constraint ACTIVITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISGROUP
    add constraint PAPER_GROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PUBLICATION
    add constraint PAPER_LEADGROUP_ID_ACTIVITY
        foreign key (LEAD_GROUP_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ACTIVITY_HIERARCHY
    add constraint ACTIVITY_HIERARCHY_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_GROUP
    add constraint CONFNOTE_GROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.SUBGROUPS
    add constraint SUBGROUPS_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_GROUP
    add constraint PUBNOTE_GROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION_DRAFT
    add constraint ACTIVITY_ND_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    add constraint CONFNOTE_LEADGROUP_ID_ACTIVITY
        foreign key (LEAD_GROUP_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ACTIVITY
    add constraint ACTIVITY_U1
        unique (NATURAL_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY
    add constraint ACTIVITY_ACTIVITY_TYPE
        foreign key (TYPE_ID) references ATLAS_AUTHDB.ACTIVITY_TYPE
/

alter table ATLAS_AUTHDB.SYSTEM_IB_CHAIR
    add constraint SYSTEM_IB_CHAIR_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_IB_CHAIR
    add constraint SYSTEM_IB_CHAIR_U1
        unique (SYSTEM_IB_ID, APPOINTMENT_ID)
/

alter table ATLAS_AUTHDB.TALK_PRELIST_REGULAR
    add constraint TALK_PRELIST_REGULAR_PK
        primary key (TALK_ID, NOMINEE_ID)
/

alter table ATLAS_AUTHDB.CDS_CMS_DATA
    add constraint CDS_CMS_DATA_PK
        primary key (RECID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM
    add constraint STA_SHORT_TERM_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
    add constraint ST_REGISTERED_ST
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM
/

alter table ATLAS_AUTHDB.PRIVILEGE_USERGROUP
    add constraint PRIVILEGE_USERGROUP_PK
        primary key (PRIVILEGE_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.INST_ACTIVITIES
    add constraint INST_ACTIVITIES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ADBPMAP
    add constraint ADBPMAP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ADBEXCP
    add constraint ADBEXCP_PAPER_ID_FK
        foreign key (PAPER_ID) references ATLAS_AUTHDB.ADBPMAP
/

alter table ATLAS_AUTHDB.QU_FUTURE_STATUS
    add constraint QU_FUTURE_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_KEYWORD
    add constraint PUBNOTE_KEYWORD_FK1
        foreign key (KEYWORD_ID) references ATLAS_AUTHDB.KEYWORD
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_SUBSYSTEM
    add constraint TALK_CATEGORY_SUBSYSTEM_PK
        primary key (TALK_CATEGORY_ID)
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_SUBSYSTEM
    add constraint TC_SUBSYSTEM_SUBSYSTEM
        foreign key (SUBSYSTEM_ID) references ATLAS_AUTHDB.SUBSYSTEM
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_SUBSYSTEM
    add constraint TC_SUBSYSTEM_TC
        foreign key (TALK_CATEGORY_ID) references ATLAS_AUTHDB.TALK_CATEGORY
/

alter table ATLAS_AUTHDB.NOMINATIONS_BUP
    add constraint NOMINATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOMINATION_CATEGORY
    add constraint NC_N_FK
        foreign key (NOMINATION_ID) references ATLAS_AUTHDB.NOMINATIONS_BUP
/

alter table ATLAS_AUTHDB.ND_CATEGORY
    add constraint ND_CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_CATEGORY
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.ND_CATEGORY
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP_TYPE
    add constraint SYSTEM_IB_EGROUP_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP_TYPE
    add constraint SYSTEM_IB_EGROUP_TYPE_U1
        unique (CODE)
/

alter table ATLAS_AUTHDB.CONF_STATUS
    add constraint CONF_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONF_STATUS_WEIGHT
    add constraint CONF_STATUS_WEIGHT_CONF_STATUS
        foreign key (CONF_STATUS_ID) references ATLAS_AUTHDB.CONF_STATUS
/

alter table ATLAS_AUTHDB.CONF
    add constraint CONF_STATUS_ID
        foreign key (STATUS_ID) references ATLAS_AUTHDB.CONF_STATUS
/

alter table ATLAS_AUTHDB.NOM_TYPE
    add constraint NOM_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_COMMENT
    add constraint ND_COMMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_COMMENT
        foreign key (COMMENT_ID) references ATLAS_AUTHDB.ND_COMMENT
/

alter table ATLAS_AUTHDB.PROJECT
    add constraint PROJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_MEMB_PUBLICATION
    add constraint PLOT_MEMB_PUBLICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_ACTIVITY
    add constraint TALK_ACTIVITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_MAT_REQ
    add constraint TALK_MR_ACT_FK
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.TALK_ACTIVITY
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_ACTIVITY
    add constraint TALK_ACTIVITY_TALK_FK
        foreign key (TALK_ID) references ATLAS_AUTHDB.TALK
            on delete cascade
/

alter table ATLAS_AUTHDB.APPOINTMENT_TREE
    add constraint APPOINTMENT_TREE_UK
        unique (APPOINTMENT_ID, PARENT_ID)
/

alter table ATLAS_AUTHDB.NO_IGNORE_UPGRADE_OTP
    add constraint NO_IGNORE_UPGRADE_OTP_PK
        primary key (NOMINATION_ID)
/

alter table ATLAS_AUTHDB.NO_IGNORE_UPGRADE_OTP
    add constraint NOMINATIONS_IGNORE_UPGRADE_OTP
        foreign key (NOMINATION_ID) references ATLAS_AUTHDB.NOMINATIONS_BACKUP
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADING_SUBGROUP
    add constraint ANALYSIS_LEADING_SUBGROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADING_SUBGROUP
    add constraint ANALYSIS_UNIQUE
        unique (ANALYSIS_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    add constraint ACTIVITY_RESPONSIBLE_MEMBER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    add constraint ACTIVITY_ACTIVITY_RESPONSIBLE_MEMBER_U
        unique (ACTIVITY_ID, MEMBER_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    add constraint ACTIVITY_RESPONSIBLE_MEMBER_FK2
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.TALK_INVITATION
    add constraint TALK_INV_ACT_FK
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.TALK_ACTIVITY
            on delete cascade
/

alter table ATLAS_AUTHDB.NOM_DISCARDED
    add constraint NOM_DISCARDED_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_DISCARDED
    add constraint UNIQUE_NOM_DISCARDED
        unique (NOMINATION_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint ACTIVITY_NOMINATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint UNIQUE_ACTIVITY_NOMINATION
        unique (ACTIVITY_ID, MEMBER_ID, SYSTEM_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint ACTIVITY_NOMINATION_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION_GROUP
    add constraint PLOT_PUBLICATION_GROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SHORT_TERM_DRAFT
    add constraint SHORT_TERM_DRAFT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CITATIONS_ATLAS
    add constraint CITATIONS_ATLAS_PK
        primary key (REF_CODE)
/

alter table ATLAS_AUTHDB.AT_SEEDING_ACTIVITY_NOMINATION
    add constraint AT_SEEDING_AT_NOMINATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_SEEDING_ACTIVITY_NOMINATION
    add constraint AT_SEEDING_NOMINATION_AK_1
        unique (SEEDING_AT_NOMINATION_ID)
/

alter table ATLAS_AUTHDB.AT_SEEDING_ACTIVITY_NOMINATION
    add constraint AT_ACTIVITY_NOMINATION
        foreign key (AT_NOMINATION_ID) references ATLAS_AUTHDB.ACTIVITY_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.AT_SEEDING_ACTIVITY_NOMINATION
    add constraint AT_SEEDING_ACTIVITY_NOMINATION
        foreign key (SEEDING_AT_NOMINATION_ID) references ATLAS_AUTHDB.ACTIVITY_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.IN_INCLUDE_DECEASED
    add constraint IN_INCLUDE_DECEASED_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_INCLUDE_DECEASED
    add constraint IN_INCLUDE_DECEASED_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.USERGROUP
    add constraint USERGROUP_PK
        primary key (GROUP_ID)
/

alter table ATLAS_AUTHDB.USERS_USERGROUP
    add constraint USERS_USERGROUP_FK2
        foreign key (GROUP_ID) references ATLAS_AUTHDB.USERGROUP
/

alter table ATLAS_AUTHDB.PRIVILEGE_USERGROUP
    add constraint PRIVILEGE_USERGROUP_FK2
        foreign key (GROUP_ID) references ATLAS_AUTHDB.USERGROUP
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION
    add constraint PLOT_PUBLICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE
    add constraint PLOT_PHASE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PLOT_PUBLICATION
/

alter table ATLAS_AUTHDB.PLOT_SUPP_NOTES
    add constraint PLOT_SUPP_NOTES_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PLOT_PUBLICATION
/

alter table ATLAS_AUTHDB.PLOT_MEMB_PUBLICATION
    add constraint PLOT_MEMB_PUBLICATION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PLOT_PUBLICATION
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION_GROUP
    add constraint PLOT_PUBLICATION_GROUP_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PLOT_PUBLICATION
/

alter table ATLAS_AUTHDB.PROF_ADVANCE
    add constraint PROF_ADVANCE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_SUBGROUP
    add constraint PUBNOTE_PUBLICATION_SUBG_PK
        primary key (PUBLICATION_ID, SUBGROUP_ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_SUBGROUP
    add constraint PUBNOTE_SUBGROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ABS_SELECTION_FEEDBACK
    add constraint ABS_SELECTION_FEEDBACK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_SELECTION_FEEDBACK
    add constraint ABS_SELECTION_FEEDBACK_UK1
        unique (ABSTRACT_ID)
/

alter table ATLAS_AUTHDB.ABS_SELECTION_FEEDBACK
    add constraint ABS_SELECTION_FEEDBACK_FK1
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.SYSTEM_IB
    add constraint SYSTEM_IB_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP
    add constraint SYSTEM_IB_EGROUP_FK1
        foreign key (SYSTEM_IB_ID) references ATLAS_AUTHDB.SYSTEM_IB
/

alter table ATLAS_AUTHDB.SYSTEM_IB_CHAIR
    add constraint SYSTEM_IB_CHAIR_FK2
        foreign key (SYSTEM_IB_ID) references ATLAS_AUTHDB.SYSTEM_IB
/

alter table ATLAS_AUTHDB.SYSTEM_IB
    add constraint SYSTEM_IB_FK1
        foreign key (ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_MVA
    add constraint ANALYSIS_SYS_STAT_MVA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_MVA
    add constraint ANALYSIS_SYS_STAT_MVA_AK_1
        unique (PUBLICATION_ID, MVA_ID)
/

alter table ATLAS_AUTHDB.ACITIVITY_APPOINTMENT_ROLE
    add constraint ACITIVITY_APPOINTMENT_ROLE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_RELATIONSHIP
    add constraint ANALYSIS_RELATIONSHIP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOM_NOMINATION_PK
        primary key (ID, MEMBER_ID)
/

alter table ATLAS_AUTHDB.NOM_DISCARDED
    add constraint NOM_DISCARDED_NOM_NOMINATION
        foreign key (NOMINATION_ID, MEMBER_ID) references ATLAS_AUTHDB.NOM_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint ACTIVITY_NOMINATION_NOMINATION
        foreign key (NOMINATION_ID, MEMBER_ID) references ATLAS_AUTHDB.NOM_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint UNIQUE_NOMINATION_ID
        unique (ID)
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOM_NOMINATION_DRAFT_NOM_TYPE
        foreign key (TYPE_ID) references ATLAS_AUTHDB.NOM_TYPE
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOM_NOMINATION_PROF_ADVANCE
        foreign key (PROFESSIONAL_ADVANCEMENT_ID) references ATLAS_AUTHDB.PROF_ADVANCE
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint CHECK_NOM_ATLAS_FRACTION
        check (ATLAS_FRACTION BETWEEN 0 AND 100)
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint CHECK_NOM_VALID_PRIORITY
        check (PRIORITY BETWEEN 1 AND 5)
/

alter table ATLAS_AUTHDB.MEMB_CONTRIBUTION
    add constraint MEMB_CONTRIBUTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE
    add constraint PUBNOTE_PHASE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE
    add unique (PUBLICATION_ID, PHASE)
/

alter table ATLAS_AUTHDB.IN_HIERARCHY
    add constraint IN_HIERARCHY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_HIERARCHY
    add constraint IN_HIERARCHY_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.PRIVILEGE
    add constraint PRIVILEGE_PK
        primary key (PRIVILEGE_ID)
/

alter table ATLAS_AUTHDB.PRIVILEGE_USERGROUP
    add constraint PRIVILEGE_USERGROUP_FK1
        foreign key (PRIVILEGE_ID) references ATLAS_AUTHDB.PRIVILEGE
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATIONGROUPS
    add constraint CONFNOTE_PUBLICATIONGROUPS_PK
        primary key (GROUP_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_MEMB_PUBLICATION
    add constraint CONFNOTE_MEMB_PUBLICATION_FK3
        foreign key (GROUP_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATIONGROUPS
/

alter table ATLAS_AUTHDB.INST_SYSTEM_IB_BACKUP
    add constraint INST_SYSTEM_IB_FK1
        foreign key (SYSTEM_IB_ID) references ATLAS_AUTHDB.INST_SYSTEM_IB_LIST_BACKUP
/

alter table ATLAS_AUTHDB.ME_VETOED_CONF_PRIORITY
    add constraint ME_VETOED_CONF_PRIORITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_VETOED_CONF_PRIORITY
    add constraint ME_VC_PRIORITY_CONF_FK
        foreign key (CONF_PRIORITY_ID) references ATLAS_AUTHDB.CONF_PRIORITY
/

alter table ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    add constraint ACTIVITY_APPOINTMENT_NEW_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACITIVITY_APPOINTMENT_ROLE
    add constraint ACITIVITY_ROLE_ACTIVITY_APP
        foreign key (ACTIVITY_APPOINTMENT_ID) references ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
/

alter table ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    add constraint ACTIVITY_APPOINTMENT_UNIQUE
        unique (ACTIVITY_ID, APPOINTMENT_ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    add constraint ACTIVITY_APPOINTMENT_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.IN_AFFILIATION_TYPE
    add constraint IN_AFFILIATION_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_AFFILIATION
    add constraint IN_AFFILIATION_FK2
        foreign key (TYPE_ID) references ATLAS_AUTHDB.IN_AFFILIATION_TYPE
/

alter table ATLAS_AUTHDB.IN_AFFILIATION_TYPE
    add constraint IN_AFFILIATION_TYPE_U1
        unique (CODE)
/

alter table ATLAS_AUTHDB.ACTIVITY_STATUS
    add constraint ACTIVITY_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACTIVITY
    add constraint ACTIVITY_ACTIVITY_STATUS
        foreign key (STATUS_ID) references ATLAS_AUTHDB.ACTIVITY_STATUS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_CONTRIBUTION
    add constraint ANALYSIS_SYS_MEMB_CONT_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_CONTRIBUTION
    add constraint ANALYSIS_SYS_MEMB_CONT_FK1
        foreign key (SUPP_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_SUPP_NOTES
/

alter table ATLAS_AUTHDB.SUPP_NOTES
    add constraint SUPP_NOTES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SUPP_NOTES
    add constraint SUPP_NOTES_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.MEETING
    add constraint MEETING_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint FK_MA_MEETING
        foreign key (MEETING_ID) references ATLAS_AUTHDB.MEETING
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_MVA
    add constraint PUBNOTE_STAT_MVA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_MVA
    add constraint PUBNOTE_STAT_MVA_AK_1
        unique (PUBLICATION_ID, MVA_ID)
/

alter table ATLAS_AUTHDB.AT_PUBLICATION_LEADGROUP
    add constraint AT_PUBLICATION_LEADGROUP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_PUBLICATION_LEADGROUP
    add constraint AT_PUBLICATION_LEADGROUP_U1
        unique (ACTIVITY_ID, LEADGROUP_ID)
/

alter table ATLAS_AUTHDB.AT_PUBLICATION_LEADGROUP
    add constraint AT_PUBLICATION_LEADGROUP_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.TALK_PRELIST
    add constraint TALK_PRELIST_PK
        primary key (TALK_ID, NOMINEE_ID)
/

alter table ATLAS_AUTHDB.TALK_PRELIST_VOLUNTEER
    add constraint TALK_PRELIST_VOLUNTEER_FK
        foreign key (TALK_ID, NOMINEE_ID) references ATLAS_AUTHDB.TALK_PRELIST
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_PRELIST_REGULAR
    add constraint TALK_PRELIST_REGULAR_FK
        foreign key (TALK_ID, NOMINEE_ID) references ATLAS_AUTHDB.TALK_PRELIST
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_PRELIST
    add constraint TALK_PRELIST_TALK_FK
        foreign key (TALK_ID) references ATLAS_AUTHDB.TALK
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_PRELIST
    add constraint TALK_PRELIST_STATUS
        check (status IN ('PRE-INVITED', 'PRE-ACCEPTED', 'ACCEPTED', 'DECLINED', 'INVITED', 'CONFIRMED', 'UNRESPONSIVE',
                          'CONFIRMED_NOTIFIED', 'UNSET'))
/

alter table ATLAS_AUTHDB.KEYWORD_CATEGORY
    add constraint KEYWORD_CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.KEYWORD_CATEGORY_LEADGROUP
    add constraint KCL_CATEGORY_FK
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.KEYWORD_CATEGORY
/

alter table ATLAS_AUTHDB.KEYWORD
    add constraint KEYWORD_FK
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.KEYWORD_CATEGORY
/

alter table ATLAS_AUTHDB.ME_ALLOW_PHOTO
    add constraint ME_ALLOW_PHOTO_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_ALLOW_PHOTO
    add constraint ME_ALLOW_PHOTO_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.PROJECT_ACTIVITY
    add constraint PROJECT_ACTIVITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PROJECT_ACTIVITY
    add constraint PROJECT_ID_FK
        foreign key (PROJECT_ID) references ATLAS_AUTHDB.PROJECT
/

alter table ATLAS_AUTHDB.TALK_TYPE
    add constraint TALK_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_TYPE_FK
        foreign key (TYPE) references ATLAS_AUTHDB.TALK_TYPE
/

alter table ATLAS_AUTHDB.ME_VETOED_TALK_TYPE
    add constraint ME_VETOED_TALK_CT_TYPE_FK
        foreign key (TALK_TYPE_ID) references ATLAS_AUTHDB.TALK_TYPE
/

alter table ATLAS_AUTHDB.ME_TALK_BLACKDATE
    add constraint ME_TALK_BLACKDATE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEMS
    add constraint SYSTEMS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint ACTIVITY_NOMINATION_SYSTEMS
        foreign key (SYSTEM_ID) references ATLAS_AUTHDB.SYSTEMS
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_INT
    add constraint PUBLICATION_STAT_INT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_INT
    add constraint PUBLICATION_STAT_INT_AK_1
        unique (PUBLICATION_ID, INTERPRETATION_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_INT
    add constraint PUBLICATION_STAT_INT_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER
    add constraint PHYSICS_TRIGGER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER_COMMON_CHOICE
    add constraint P_TRIGGER_COMMON_CHOICE_FK1
        foreign key (TRIGGER_ID) references ATLAS_AUTHDB.PHYSICS_TRIGGER
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHYSICS_TRIGGER
    add constraint ANALYSIS_SYS_PTRIGGER_FK2
        foreign key (TRIGGER_ID) references ATLAS_AUTHDB.PHYSICS_TRIGGER
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER
    add constraint PHYSICS_TRIGGER_UNIQUE
        unique (NAME, YEAR, CATEGORY_ID)
/

alter table ATLAS_AUTHDB.MENTORING_SUBJECT
    add constraint MENTORING_SUBJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ST_PUBLICATION
    add constraint ST_PUBLICATION_PK1
        primary key (PUBLICATION_ID, SHORT_TERM_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.ST_PUBLICATION
    add constraint ST_PUBLICATION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.ST_PUBLICATION
    add constraint ST_PUBLICATION_FK2
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE_1
    add primary key (PHASE_ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE_1
    add constraint PUBNOTE_PHASE_1_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PUBNOTE_PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.PHASE_2
    add primary key (PHASE_ID, LOOP_SEQUENCE)
/

alter table ATLAS_AUTHDB.PHASE_2
    add constraint PHASE_2_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.BACKUP_QUAL_FUTURE
    add constraint QUAL_FUTURE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT_STATUS
    add constraint PUB_ND_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT_STATUS
    add constraint PUB_ND_STATUS_AK_1
        unique (PUBLICATION_ID)
/

alter table ATLAS_AUTHDB.TARGET_CONFERENCE
    add constraint TARGET_CONFERENCE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT_STATUS
    add constraint ND_NOMINATION_DRAFT_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT_STATUS
    add constraint NOMINATION_DRAFT_STATUS_FK1
        foreign key (STATUS_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT_STATUS
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATIONGROUPS
    add constraint PUBNOTE_PUBLICATIONGROUPS_PK
        primary key (GROUP_ID)
/

alter table ATLAS_AUTHDB.MEMB_TALK_TOPICS
    add primary key (MEMB_ID, RANK)
/

alter table ATLAS_AUTHDB.MEMB_TALK_TOPICS
    add constraint SYS_C002245063
        foreign key (MEMB_ID) references ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_ST_PUBLICATION
    add constraint ANALYSIS_SYS_ST_PUBL_PK1
        primary key (PUBLICATION_ID, SHORT_TERM_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_ST_PUBLICATION
    add constraint ANALYSIS_SYS_ST_PUBL_FK2
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
/

alter table ATLAS_AUTHDB.STAT_MVA
    add constraint STAT_MVA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_MVA
    add constraint ANALYSIS_SYS_STAT_MVA_FK2
        foreign key (MVA_ID) references ATLAS_AUTHDB.STAT_MVA
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_MVA
    add constraint PUBNOTE_STAT_MVA_FK2
        foreign key (MVA_ID) references ATLAS_AUTHDB.STAT_MVA
/

alter table ATLAS_AUTHDB.ACTIVITY_CATEGORY_AREA
    add constraint ACTIVITY_CATEGORY_AREA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INST_ACTIVITIES
    add constraint INST_ACTIVITIES_FK2
        foreign key (AREA_ID) references ATLAS_AUTHDB.ACTIVITY_CATEGORY_AREA
/

alter table ATLAS_AUTHDB.MEMB_ARCH_DELETED
    add constraint MEMB_ARCH_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PREVIOUS_ANALYSES
    add constraint ANALYSIS_SYS_PREVIOUS_ANA_PK1
        primary key (PUBLICATION_ID, PREVIOUS_ANALYSIS_ID, PREVIOUS_ANALYSIS_TYPE)
/

alter table ATLAS_AUTHDB.AT_NOMINATION_APPOINTMENT
    add constraint AT_NOMINATION_APPOINTMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_NOMINATION_APPOINTMENT
    add constraint UNIQUE_AT_NOMINATION_APP
        unique (ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.AT_NOMINATION_APPOINTMENT
    add constraint AT_NOMINATION_APP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ADBPARC
    add constraint ADBPARC_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ADBPARC
    add constraint ADBPARC_PAPER_ID_FK
        foreign key (PAPER_ID) references ATLAS_AUTHDB.ADBPMAP
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_TOOL
    add constraint CONFNOTE_STAT_TOOL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_TOOL
    add constraint CONFNOTE_STAT_TOOL_AK_1
        unique (PUBLICATION_ID, TOOL_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_TOOL
    add constraint CONFNOTE_STAT_TOOL_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_TOOL
    add constraint PUBNOTE_STAT_TOOL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_TOOL
    add constraint PUBNOTE_STAT_TOOL_AK_1
        unique (PUBLICATION_ID, TOOL_ID)
/

alter table ATLAS_AUTHDB.IN_AUTHORLIST_IDENTIFICATION
    add constraint IN_AUTHORLIST_ID_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_AUTHORLIST_IDENTIFICATION
    add constraint IN_AUTHORLIST_ID_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_PUBLICATION
    add constraint ANALYSIS_SYS_MEMB_PUB_PK1
        primary key (GROUP_ID, MEMB_ID, PUBLICATION_ID)
/

alter table ATLAS_AUTHDB.ND_PRIORITY
    add constraint ND_PRIORITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_PRIORITY
        foreign key (PRIORITY_ID) references ATLAS_AUTHDB.ND_PRIORITY
/

alter table ATLAS_AUTHDB.ANALYSIS_ECM
    add primary key (PUBLICATION_ID, ECM_VALUE_WITHOUT_UNIT)
/

alter table ATLAS_AUTHDB.ANALYSIS_ECM
    add foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
        on delete cascade
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint EMPLOY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.EMPLOY_ACTIVITY
    add constraint EMPLOY_ACTIVITY_EMPLOY_FK1
        foreign key (EMPLOY_ID) references ATLAS_AUTHDB.ME_EMPLOYMENT
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint FUNDA_ID_FK
        foreign key (FUNDA_ID) references ATLAS_AUTHDB.FUNDA
/

alter table ATLAS_AUTHDB.PUBNOTE_ST_PUBLICATION
    add constraint PUBNOTE_ST_PUBLICATION_PK1
        primary key (PUBLICATION_ID, SHORT_TERM_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_ST_PUBLICATION
    add constraint PUBNOTE_ST_PUBLICATION_FK2
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
/

alter table ATLAS_AUTHDB.PUBNOTE_SUPP_NOTES
    add constraint PUBNOTE_SUPP_NOTES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_EX_OFFICIO
    add constraint APPOINTMENT_EX_OFFICIO_CHK1
        check (type in ('EGROUP', 'MEMBER', 'SERVICEACC', 'EXTERNAL'))
/

alter table ATLAS_AUTHDB.TALK_WEIGHT
    add constraint TALK_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint ME_TALK_PREFERRED_TOPIC_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint MEMBER_TALK_TOPICS_AK_1
        unique (ACTIVITY_ID, MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint ME_TALK_PREFERRED_TOPIC_AK_2
        unique (MEMBER_ID, RANK)
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint MEMBER_TALK_TOPIC_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint UP_TO_TEN_CHOICES
        check (RANK >= 1 AND RANK <= 10)
/

alter table ATLAS_AUTHDB.CITATIONS_ATLAS_PUBLISHED
    add constraint CITATIONS_ATLAS_PUBLISHED_PK
        primary key (REF_CODE, INSPIRE_RECORD)
/

alter table ATLAS_AUTHDB.STA_CONTACT
    add constraint STA_CONTACT_UNIQUE
        unique (STA_ID, CONTACT_ID)
/

alter table ATLAS_AUTHDB.IN_LOCATION
    add constraint IN_LOCATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_LOCATION
    add constraint IN_LOCATION_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE_LINKS
    add constraint CONFNOTE_PHASE_LINKS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    add constraint PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    add constraint PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION_U1
        unique (REMOVAL_ID, PAPER_ID)
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    add constraint PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION_FK2
        foreign key (PAPER_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.PROJECT_APPOINTMENT
    add constraint PROJECT_APPOINTMENT_PK
        primary key (PROJECT_ID, APPOINTMENT_ID)
/

alter table ATLAS_AUTHDB.PROJECT_APPOINTMENT
    add constraint PROJECT_APPOINTMENT_FK1
        foreign key (PROJECT_ID) references ATLAS_AUTHDB.PROJECT
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_SUBGROUP
    add constraint ANALYSIS_SYS_SUBGROUP_PK1
        primary key (PUBLICATION_ID, SUBGROUP_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADING_SUBGROUP
    add constraint ANALYSIS_LEADING_SUBGROUP_FK1
        foreign key (ANALYSIS_ID, SUBGROUP_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_SUBGROUP
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_SUBGROUP
    add constraint ANALYSIS_SUBGROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ME_APPLYING_FOR_A_JOB
    add constraint ME_APPLYING_FOR_A_JOB_PK
        primary key (MEMBER_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_ST_PUBLICATION
    add constraint CONFNOTE_ST_PUBLICATION_PK1
        primary key (PUBLICATION_ID, SHORT_TERM_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_ST_PUBLICATION
    add constraint CONFNOTE_ST_PUBLICATION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.CONFNOTE_ST_PUBLICATION
    add constraint CONFNOTE_ST_PUBLICATION_FK2
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM_REGISTERED
/

alter table ATLAS_AUTHDB.T_EMPLOY
    add constraint T_EMPLOY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_SIGNOFF
    add constraint ANALYSIS_SYS_PHASE_SIGNOFF_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_SIGNOFF
    add constraint ANALYSIS_SYS_MEMB_SIGNOFF_FK2
        foreign key (SIGNOFF_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_SIGNOFF
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_SUBGROUP
    add constraint CONFNOTE_PUBLICATION_SUBG_PK
        primary key (PUBLICATION_ID, SUBGROUP_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_SUBGROUP
    add constraint CONFNOTE_PUBLICATION_SUBG_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION_SUBGROUP
    add constraint CONFNOTE_SUBGROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    add constraint PUBNOTE_PUBLICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SYSTEM_PUBNOTE
    add constraint SYSTEM_PUBNOTE_FK1
        foreign key (PUBNOTE_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PUBNOTE
    add constraint ABS_ABSTRACT_PUBNOTE_FK3
        foreign key (PUBNOTE_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_INT
    add constraint PUBNOTE_STAT_INT_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_GROUP
    add foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
        on delete cascade
/

alter table ATLAS_AUTHDB.PUBNOTE_COLLISION
    add constraint PUBNOTE_COLLISION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_KEYWORD
    add constraint PUBNOTE_KEYWORD_FK2
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION_SUBGROUP
    add constraint PUBNOTE_PUBLICATION_SUBG_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE
    add constraint PUBNOTE_PHASE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_MVA
    add constraint PUBNOTE_STAT_MVA_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_TOOL
    add constraint PUBNOTE_STAT_TOOL_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_ST_PUBLICATION
    add constraint PUBNOTE_ST_PUBLICATION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    add constraint PUBNOTE_LEADGROUP_ID_ACTIVITY
        foreign key (LEAD_GROUP_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    add constraint PUBNOTE_PUBLICATION_FK3
        foreign key (TASK_ID) references ATLAS_PUB_TRACK.TASK (ID)
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
    add constraint PROPOSAL_C_AUTHORLIST_REMOVAL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    add constraint PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION_FK1
        foreign key (REMOVAL_ID) references ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
    add constraint PROPOSAL_C_AUTHORLIST_REMOVAL_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ROLE
    add constraint ROLE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ACITIVITY_APPOINTMENT_ROLE
    add constraint ACITIVITY_ROLE_ROLE
        foreign key (ROLE_ID) references ATLAS_AUTHDB.ROLE
/

alter table ATLAS_AUTHDB.AUTHORSHIP2
    add constraint AUTHORSHIP_G_CK
        check (group_id IN ('TDAQ', 'UPGRADE'))
/

alter table ATLAS_AUTHDB.AUTHORSHIP2
    add constraint AUTHORSHIP_PD
        check (end_date >= start_date)
/

alter table ATLAS_AUTHDB.STAT_INTERPRETATION
    add constraint STAT_INTERPRETATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_INT
    add constraint PUBNOTE_STAT_INT_FK2
        foreign key (INTERPRETATION_ID) references ATLAS_AUTHDB.STAT_INTERPRETATION
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_INT
    add constraint PUBLICATION_STAT_INT_FK2
        foreign key (INTERPRETATION_ID) references ATLAS_AUTHDB.STAT_INTERPRETATION
/

alter table ATLAS_AUTHDB.INSTITUTE_FOUNDATION_CODE
    add constraint INSTITUTE_FOUNDATION_CODE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_DRAFT
    add constraint STA_SHORT_TERM_DRAFT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_DRAFT
    add constraint STA_SHORT_TERM_DRAFT_AK_1
        unique (SHORT_TERM_ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM_DRAFT
    add constraint ST_DRAFT_ST
        foreign key (SHORT_TERM_ID) references ATLAS_AUTHDB.STA_SHORT_TERM
/

alter table ATLAS_AUTHDB.ND_ADDITIONAL_SUBJECT
    add constraint ND_ADDITIONAL_SUBJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_ADDITIONAL_SUBJECT
    add constraint ND_ADDITIONAL_SUBJECT_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ND_ADDITIONAL_SUBJECT
    add constraint ND_ADDITIONAL_SUBJECT_ND_ND
        foreign key (NOMINATION_DRAFT_ID, MEMB_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT
            on delete cascade
/

alter table ATLAS_AUTHDB.TDAQ_SUBJECT
    add constraint TDAQ_SUBJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TDAQ_SUBJECT
    add constraint TDAQ_SUBJECT_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.NOM_ADDITIONAL_SUBJECT
    add constraint NOM_ADDITIONAL_SUBJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_ADDITIONAL_SUBJECT
    add constraint NOMINATION_ADDITIONAL_SUBJECT
        foreign key (NOMINATION_ID, MEMBER_ID) references ATLAS_AUTHDB.NOM_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE_LINKS
    add constraint PUBNOTE_PHASE_LINKS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_PHASE_LINKS
    add constraint PUBNOTE_PHASE_LINKS_PHASE_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PUBNOTE_PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GROUP
    add constraint ANALYSIS_SYS_GROUP_PK1
        primary key (PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GROUP
    add constraint ANALYSIS_GROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.FUNDA_DOI_ACKNOWLEDGED
    add constraint FUNDA_DOI_ACKNOWLEDGED_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.FUNDA_DOI_ACKNOWLEDGED
    add constraint FUNDA_DOI_ACKNOWLEDGED_U1
        unique (FUNDA_DOI_ID)
/

alter table ATLAS_AUTHDB.FUNDA_DOI_ACKNOWLEDGED
    add constraint FUNDA_DOI_ACKNOWLEDGED_FK1
        foreign key (FUNDA_DOI_ID) references ATLAS_AUTHDB.FUNDA_DOI
/

alter table ATLAS_AUTHDB.CATEGORY_WEIGHT
    add constraint CATEGORY_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_UPGRADE
    add constraint NOM_UPGRADE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_UPGRADE
    add constraint UNIQUE_NOM_UPGRADE
        unique (NOMINATION_ID)
/

alter table ATLAS_AUTHDB.NOM_UPGRADE
    add constraint NOM_UPGRADE_NOM_NOMINATION
        foreign key (NOMINATION_ID, MEMBER_ID) references ATLAS_AUTHDB.NOM_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.NOM_IGNORE_UPGRADE_OTP
    add constraint NOM_IGNORE_UPGRADE_OTP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_IGNORE_UPGRADE_OTP
    add constraint NO_IGNORE_UPG_OTP_UNIQUE_NOM
        unique (NOM_UPGRADE_ID)
/

alter table ATLAS_AUTHDB.NOM_IGNORE_UPGRADE_OTP
    add constraint NOM_IGNORE_UPG_OTP_NOM
        foreign key (NOM_UPGRADE_ID) references ATLAS_AUTHDB.NOM_UPGRADE
/

alter table ATLAS_AUTHDB.PUBNOTE_MEMB_PUBLICATION
    add constraint PUBNOTE_MEMB_PUBLICATION_UK1
        unique (MEMB_ID, PUBLICATION_ID, GROUP_ID)
/

alter table ATLAS_AUTHDB.PUBNOTE_MEMB_PUBLICATION
    add constraint PUBNOTE_MEMB_PUBLICATION_FK2
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.PUBNOTE_MEMB_PUBLICATION
    add constraint PUBNOTE_MEMB_PUBLICATION_FK3
        foreign key (GROUP_ID) references ATLAS_AUTHDB.PUBNOTE_PUBLICATIONGROUPS
/

alter table ATLAS_AUTHDB.MEMB_TALK_BLACKDATES_BACKUP
    add foreign key (MEMB_ID) references ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
        on delete cascade
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB
    add constraint IN_SYSTEM_IB_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_FK1
        foreign key (INSTITUTE_SYSTEM_IB_ID) references ATLAS_AUTHDB.IN_SYSTEM_IB
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_ADD_CONTACT
    add constraint IN_SYSTEM_IB_ADD_CONTACT_FK1
        foreign key (INSTITUTE_SYSTEM_IB_ID) references ATLAS_AUTHDB.IN_SYSTEM_IB
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB
    add constraint IN_SYSTEM_IB_U1
        unique (INSTITUTE_ID, SYSTEM_IB_ID)
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB
    add constraint IN_SYSTEM_IB_FK2
        foreign key (SYSTEM_IB_ID) references ATLAS_AUTHDB.SYSTEM_IB
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_TREE
    add constraint APPOINTMENT_CATEGORY_TREE_UK
        unique (CATEGORY_ID, PARENT_CATEGORY_ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_TREE
    add constraint APPOINTMENT_CATEGORY_TREE_FK1
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.APPOINTMENT_CATEGORIES
            on delete cascade
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_TREE
    add constraint APPOINTMENT_CATEGORY_TREE_FK2
        foreign key (PARENT_CATEGORY_ID) references ATLAS_AUTHDB.APPOINTMENT_CATEGORIES
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_TREE
    add constraint CHECK_SELF_PARENTING
        check (CATEGORY_ID != PARENT_CATEGORY_ID)
/

alter table ATLAS_AUTHDB.ND_IGNORE_UPGRADE_OTP
    add constraint ND_IGNORE_UPGRADE_OTP_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_IGNORE_UPGRADE_OTP
    add constraint ND_IGNORE_UPG_OTP_UNIQUE_NOM
        unique (ND_UPGRADE_ID)
/

alter table ATLAS_AUTHDB.ND_IGNORE_UPGRADE_OTP
    add constraint ND_IGNORE_UPG_OTP_ND_UPGRADE
        foreign key (ND_UPGRADE_ID) references ATLAS_AUTHDB.ND_UPGRADE
/

alter table ATLAS_AUTHDB.MEMB_APPOINTMENT
    add constraint MEMB_APPOINTMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFERENCE
    add constraint ABS_ABSTRACT_CONFERENCE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFERENCE
    add constraint ABS_ABSTRACT_CONFERENCE_UK1
        unique (ABSTRACT_ID, CONFERENCE_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFERENCE
    add constraint ABS_ABSTRACT_CONFERENCE_FK1
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFERENCE
    add constraint ABS_ABSTRACT_CONFERENCE_FK3
        foreign key (CONFERENCE_ID) references ATLAS_AUTHDB.CONF
/

alter table ATLAS_AUTHDB.STA_TOPIC
    add constraint STA_TOPIC_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.SSC
    add constraint SSC_PK
        primary key (GROUP_ID, MEMB_ID)
/

alter table ATLAS_AUTHDB.CONF_SUPP_NOTES
    add constraint CONF_SUPP_NOTES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_CATEGORY
    add constraint NOM_CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOMINATION_NOM_CATEGORY
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.NOM_CATEGORY
/

alter table ATLAS_AUTHDB.AT_NOMINATION_DRAFT_STATUS
    add constraint AT_NOMINATION_DRAFT_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_NOMINATION_DRAFT_STATUS
    add constraint AT_ND_STATUS_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.AT_NOMINATION_DRAFT_STATUS
    add constraint AT_ND_STATUS_ND_ND_STATUS
        foreign key (STATUS_ID) references ATLAS_AUTHDB.ND_NOMINATION_DRAFT_STATUS
/

alter table ATLAS_AUTHDB.STAT_TOOL
    add constraint STAT_TOOL_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_TOOL
    add constraint PUBLICATION_STAT_TOOL_FK2
        foreign key (TOOL_ID) references ATLAS_AUTHDB.STAT_TOOL
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_TOOL
    add constraint ANALYSIS_SYS_STAT_TOOL_FK2
        foreign key (TOOL_ID) references ATLAS_AUTHDB.STAT_TOOL
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_TOOL
    add constraint CONFNOTE_STAT_TOOL_FK2
        foreign key (TOOL_ID) references ATLAS_AUTHDB.STAT_TOOL
/

alter table ATLAS_AUTHDB.PUBNOTE_STAT_TOOL
    add constraint PUBNOTE_STAT_TOOL_FK2
        foreign key (TOOL_ID) references ATLAS_AUTHDB.STAT_TOOL
/

alter table ATLAS_AUTHDB.BACKUP_QUAL_OTP_TASK
    add constraint OTP_TASK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.BACKUP_QUAL_OTP_TASK
    add constraint QUAL_OTP_TASK_AK_1
        unique (MEMB_ID)
/

alter table ATLAS_AUTHDB.QU_OTP_TASK
    add constraint QU_OTP_TASK_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_OTP_TASK
    add constraint QU_OTP_TASK_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.LEADGROUPS
    add constraint LEADGROUPS_PK
        primary key (GROUP_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADGROUP_APPOINTMENT
    add constraint ANALYSIS_LEADGROUP_APPOIN_FK1
        foreign key (LEADGROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.KEYWORD_CATEGORY_LEADGROUP
    add constraint KCL_LEADGROUP_FK
        foreign key (LEADGROUP_ID) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.PUBLICATION
    add constraint PUBLICATION_LEADGROUPS_FK1
        foreign key (LEAD_GROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    add constraint CONFNOTE_PUBLICATION_LEAD_FK1
        foreign key (LEAD_GROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_FK2
        foreign key (GROUP_ID) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION_GROUP
    add constraint PLOT_PUBLICATION_GROUP_FK2
        foreign key (GROUP_ID) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION
    add constraint PLOT_PUBLICATION_FK1
        foreign key (LEAD_GROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.AT_PUBLICATION_LEADGROUP
    add constraint AT_PUBLICATION_LEADGROUP_FK2
        foreign key (LEADGROUP_ID) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    add constraint PUBNOTE_PUBLICATION_LEAD_FK1
        foreign key (LEAD_GROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.LEADGROUPS
    add constraint LEADGROUPS_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.AUTHORSHIP
    add constraint AUTH_EMP_FK
        foreign key (EMPLOY_ID) references ATLAS_AUTHDB.ME_EMPLOYMENT
            on delete cascade
/

alter table ATLAS_AUTHDB.AUTHORSHIP
    add constraint AUTH_DATES_CHECK
        check (end_date >= start_date)
/

alter table ATLAS_AUTHDB.APPOINTMENT
    add constraint APPOINTMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADGROUP_APPOINTMENT
    add constraint ANALYSIS_LEADGROUP_APPOIN_FK2
        foreign key (APPOINTMENT) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.DEPRECATED_ACTIVITY_APPOINTMENT
    add constraint ACTIVITY_APPOINTMENT_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.SYSTEM_IB_CHAIR
    add constraint SYSTEM_IB_CHAIR_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.APPOINTMENT_TREE
    add constraint APPOINTMENT_TREE_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.APPOINTMENT_TREE
    add constraint APPOINTMENT_TREE_FK2
        foreign key (PARENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    add constraint ACTIVITY_APP_APP
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.AT_NOMINATION_APPOINTMENT
    add constraint AT_NOMINATION_APP_APP
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.APPOINTMENT_EX_OFFICIO
    add constraint APPOINTMENT_EX_OFFICIO_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.PROJECT_APPOINTMENT
    add constraint PROJECT_APPOINTMENT_FK2
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.MEMB_APPOINTMENT
    add constraint APPOINTMENT_ID_FK
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.APPOINTMENT
    add constraint CODE_UNIQUE
        unique (CODE)
/

alter table ATLAS_AUTHDB.HRINST
    add constraint HRINST_PK
        primary key (HRID)
/

alter table ATLAS_AUTHDB.PUBLICATIONGROUPS
    add constraint PUBLICATIONGROUPS_PK
        primary key (GROUP_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PAPER
    add constraint ABS_ABSTRACT_PAPER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PAPER
    add constraint ABS_PAPER_UK1
        unique (ABSTRACT_ID, PAPER_ID)
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PAPER
    add constraint ABS_ABSTRACT_PAPER_FK2
        foreign key (ABSTRACT_ID) references ATLAS_AUTHDB.ABS_ABSTRACT
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PAPER
    add constraint ABS_ABSTRACT_PAPER_FK3
        foreign key (PAPER_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.CONFNOTE_MEMB_CONTRIBUTION
    add constraint CONFNOTE_MEMB_CONTRIBUTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TOKEN
    add primary key (TOKEN)
/

alter table ATLAS_AUTHDB.TOKEN
    add constraint TOKEN_FMT
        check (LENGTH(token) = 32)
/

alter table ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    add constraint AT_ACTIVITY_ACTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    add constraint AT_ACTIVITY_ACTION_UNIQUE
        unique (ACTIVITY_ID, ACTION_ID)
/

alter table ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    add constraint AT_ACTIVITY_ACTION_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ND_TYPE
    add constraint ND_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_TYPE
        foreign key (TYPE_ID) references ATLAS_AUTHDB.ND_TYPE
/

alter table ATLAS_AUTHDB.APPOINTMENT_RESPONSIBLE
    add constraint APPOINTMENT_RESPONSIBLE_PK
        primary key (APPOINTMENT_ID, APPOINTMENT_RESPONSIBLE_ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_RESPONSIBLE
    add constraint APPOINTMENT_RESPONSIBLE_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.APPOINTMENT_RESPONSIBLE
    add constraint APPOINTMENT_RESPONSIBLE_FK2
        foreign key (APPOINTMENT_RESPONSIBLE_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.PLOT_COLLISION
    add constraint PLOT_COLLISION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_COLLISION
    add constraint PLOT_COLLISION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PLOT_PUBLICATION
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_INT
    add constraint CONFNOTE_STAT_INT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_INT
    add constraint CONFNOTE_STAT_INT_AK_1
        unique (PUBLICATION_ID, INTERPRETATION_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_INT
    add constraint CONFNOTE_STAT_INT_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_INT
    add constraint CONFNOTE_STAT_INT_FK2
        foreign key (INTERPRETATION_ID) references ATLAS_AUTHDB.STAT_INTERPRETATION
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INSTITUTE_NOMINATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INSTITUTE_NOMINATION
        unique (INSTITUTE_ID, MEMBER_ID, SYSTEM_ID)
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INSTITUTE_NOMINATION_SYSTEMS
        foreign key (SYSTEM_ID) references ATLAS_AUTHDB.SYSTEMS
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INST_NOMINATION_NOMINATION
        foreign key (NOMINATION_ID, MEMBER_ID) references ATLAS_AUTHDB.NOM_NOMINATION
            on delete cascade
/

alter table ATLAS_AUTHDB.INST_GHOST
    add constraint INST_GHOST_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ADDITIONAL_INST
    add constraint INST_GHOST_FK
        foreign key (INST_GHOST_ID) references ATLAS_AUTHDB.INST_GHOST
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint FOOTNOTE_EXT_FK
        foreign key (INST_FOOTNOTE_EXT) references ATLAS_AUTHDB.INST_GHOST
            on delete set null
/

alter table ATLAS_AUTHDB.TALK_CALL
    add primary key (ACTIVITY_ID)
/

alter table ATLAS_AUTHDB.TALK_CALL
    add constraint TALK_CALL_FK
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.TALK_ACTIVITY
            on delete cascade
/

alter table ATLAS_AUTHDB.PHASE_LINKS
    add constraint PHASE_LINKS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHASE_LINKS
    add constraint PHASE_LINKS_PHASE_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEETING
    add constraint ANALYSIS_SYS_MEETING_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEETING_CONTENT
    add constraint ANALYSIS_SYS_MEETING_CONT_FK1
        foreign key (MEETING_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_MEETING
/

alter table ATLAS_AUTHDB.CATEGORY
    add constraint CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CATEGORY
    add constraint CATEGORY_SYSTEMS_FK1
        foreign key (SYSTEMS_ID) references ATLAS_AUTHDB.SYSTEMS
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER_CATEGORY
    add constraint PHYSICS_TRIGGER_CATEGORY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER
    add constraint PHYSICS_TRIGGER_FK1
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.PHYSICS_TRIGGER_CATEGORY
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_MVA
    add constraint CONFNOTE_STAT_MVA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_MVA
    add constraint CONFNOTE_STAT_MVA_AK_1
        unique (PUBLICATION_ID, MVA_ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_MVA
    add constraint CONFNOTE_STAT_MVA_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
/

alter table ATLAS_AUTHDB.CONFNOTE_STAT_MVA
    add constraint CONFNOTE_STAT_MVA_FK2
        foreign key (MVA_ID) references ATLAS_AUTHDB.STAT_MVA
/

alter table ATLAS_AUTHDB.ACITIVITY_TYPE_HIERARCHY
    add constraint ACITIVITY_TYPE_HIERARCHY_PK
        primary key (TYPE_ID)
/

alter table ATLAS_AUTHDB.ACITIVITY_TYPE_HIERARCHY
    add constraint ACTIVITY_TYPE_HIERARCHY
        foreign key (TYPE_ID) references ATLAS_AUTHDB.ACTIVITY_TYPE
/

alter table ATLAS_AUTHDB.ACITIVITY_TYPE_HIERARCHY
    add constraint ACTIVITY_TYPE_PARENT
        foreign key (PARENT_TYPE_ID) references ATLAS_AUTHDB.ACITIVITY_TYPE_HIERARCHY
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_MVA
    add constraint PUBLICATION_STAT_MVA_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_MVA
    add constraint PUBLICATION_STAT_MVA_AK_1
        unique (PUBLICATION_ID, MVA_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_MVA
    add constraint PUBLICATION_STAT_MVA_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.PUBLICATION_STAT_MVA
    add constraint PUBLICATION_STAT_MVA_FK2
        foreign key (MVA_ID) references ATLAS_AUTHDB.STAT_MVA
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
    add constraint ANALYSIS_SYS_PHASE_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_SIGNOFF
    add constraint ANALYSIS_SYS_PHASE_SIGNOFF_KF1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
/

alter table ATLAS_AUTHDB.ME_OTHER_CONSTRAINT
    add constraint ME_OTHER_CONSTRAINT_PK
        primary key (MEMBER_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISSUBGROUP
    add constraint PUBLICATION_ANALYSISSUBGR_PK
        primary key (PUBLICATION_ID, SUBGROUP_ID)
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISSUBGROUP
    add constraint PAPER_SUBGROUP_ACTIVITY
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.PUBLICATION_ANALYSISSUBGROUP
    add constraint PUBLICATION_ANALYSISSUBGR_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.APPOINTMENT_INACTIVE
    add constraint APPOINTMENT_INACTIVE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_INACTIVE
    add constraint APPOINTMENT_INACTIVE_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.ME_GRANT
    add constraint ME_GRANT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_GRANT
    add constraint ME_GRANT_FK1
        foreign key (FUNDA_DOI_ID) references ATLAS_AUTHDB.FUNDA_DOI
/

alter table ATLAS_AUTHDB.ME_GRANT
    add constraint ME_GRANT_FK2
        foreign key (GRANT_ID) references ATLAS_AUTHDB.GRT_GRANT
/

alter table ATLAS_AUTHDB.MEMB_TALK_OTHER_TOPICS_BACKUP
    add primary key (MEMB_ID, OTHER_TOPIC)
/

alter table ATLAS_AUTHDB.MEMB_TALK_OTHER_TOPICS_BACKUP
    add foreign key (MEMB_ID) references ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
        on delete cascade
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF_TOPIC
    add constraint ME_PREFERRED_CONF_TOPIC_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF_TOPIC
    add constraint ME_PREFERRED_CONF_TOPIC_CHK1
        check ((RANK >= 1 AND RANK <= 5))
/

alter table ATLAS_AUTHDB.COMMON_CHOICE
    add constraint COMMON_CHOICES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PHYSICS_TRIGGER_COMMON_CHOICE
    add constraint P_TRIGGER_COMMON_CHOICE_FK2
        foreign key (COMMON_CHOICE_ID) references ATLAS_AUTHDB.COMMON_CHOICE
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY
    add constraint APPOINTMENT_CATEGORY_UK
        unique (APPOINTMENT_ID, CATEGORY_ID)
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY
    add constraint APPOINTMENT_CATEGORY_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
            on delete cascade
/

alter table ATLAS_AUTHDB.PLOT_CIRCULATION_TYPE
    add constraint PLOT_CIRCULATION_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.PLOT_PHASE_1
    add constraint PLOT_PHASE_1_FK2
        foreign key (CIRCULATION_TYPE) references ATLAS_AUTHDB.PLOT_CIRCULATION_TYPE
/

alter table ATLAS_AUTHDB.STA_TYPE
    add constraint STA_TYPE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_SHORT_TERM
    add constraint SHORT_TERM_TYPE
        foreign key (TYPE_ID) references ATLAS_AUTHDB.STA_TYPE
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_WEIGHT
    add constraint TALK_CATEGORY_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_WEIGHT
    add constraint TALK_CATEGORY_WEIGHT_WEIGHT
        foreign key (WEIGHT_ID) references ATLAS_AUTHDB.WEIGHT
/

alter table ATLAS_AUTHDB.TALK_CATEGORY_WEIGHT
    add constraint TALK_CAT_WEIGHT_TALK_CAT
        foreign key (TALK_CATEGORY_ID) references ATLAS_AUTHDB.TALK_CATEGORY
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_LINKS
    add constraint ANALYSIS_SYS_PHASE_LINKS_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_LINKS
    add constraint ANALYSIS_SYS_PHASE_LINKS_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE_HIST
    add constraint MAH_MEETING_FK
        foreign key (MEETING_ID) references ATLAS_AUTHDB.MEETING
/

alter table ATLAS_AUTHDB.INST_UPGRADE_PROJECTS_LIST
    add constraint INST_UPGRADE_PROJECTS_LIST_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.INST_UPGRADE_PROJECTS
    add constraint INST_UPGRADE_PROJECTS_FK1
        foreign key (UPGRADE_ID) references ATLAS_AUTHDB.INST_UPGRADE_PROJECTS_LIST
/

alter table ATLAS_AUTHDB.IN_INSTITUTE
    add constraint INST_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_ADDRESS
    add constraint IN_ADDRESS_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_INST_FK
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.ACADEMIC_RECORD
    add constraint ACADEMIC_RECORD_INST_FK1
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_AFFILIATION
    add constraint IN_AFFILIATION_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.INST_UPGRADE_PROJECTS
    add constraint INST_UPGRADE_PROJECTS_FK2
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.INST_ACTIVITIES_NEW
    add constraint INST_ACTIVITIES_NEW_FK2
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.USERS_USERGROUP
    add constraint USERS_USERGROUP_FK4
        foreign key (IR_INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.USERS_USERGROUP
    add constraint USERS_USERGROUP_FK5
        foreign key (IR_INSTITUTION_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_SECRETARY
    add constraint IN_SECRETARY_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION_DRAFT
    add constraint INSTITUTE_ND_INSTITUTE
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.ADDITIONAL_INST
    add constraint INST_FK
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint FK_MA_INSTITUTION
        foreign key (INSTITUTION_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.INSTITUTE_REP
    add constraint IR_IID_FK
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_INCLUDE_DECEASED
    add constraint IN_INCLUDE_DECEASED_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.IN_HIERARCHY
    add constraint IN_HIERARCHY_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.IN_HIERARCHY
    add constraint IN_HIERARCHY_FK2
        foreign key (PARENT_INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.INST_SYSTEM_IB_BACKUP
    add constraint INST_SYSTEM_IB_FK2
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.ADBPARC
    add constraint ADBPARC_INST_ID_FK
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_AUTHORLIST_IDENTIFICATION
    add constraint IN_AUTHORLIST_ID_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint FOOTNOTE_ATLAS_FK
        foreign key (INST_FOOTNOTE_ATLAS) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete set null
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint INST_ID_FK
        foreign key (INST_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_LOCATION
    add constraint IN_LOCATION_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.INSTITUTE_FOUNDATION_CODE
    add constraint INSTITUTE_FOUNDATION_CODE_INST
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB
    add constraint IN_SYSTEM_IB_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.HRINST
    add constraint HRINST_ID_FK
        foreign key (ATLASID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INSTITUTE_NOMINATION_INST
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE_HIST
    add constraint MAH_INSTITUTION_FK
        foreign key (INSTITUTION_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE_HIST
    add constraint MAH_PROXY_FK
        foreign key (PROXY_ID) references ATLAS_AUTHDB.IN_INSTITUTE
/

alter table ATLAS_AUTHDB.IN_INSTITUTE
    add constraint COUNTRY_ID_FK
        foreign key (COUNTRY_ID) references ATLAS_AUTHDB.COUNTRY
/

alter table ATLAS_AUTHDB.IN_INSTITUTE
    add constraint INST_FUNDA_FK
        foreign key (FUNDING_AGENCY_ID) references ATLAS_AUTHDB.FUNDA
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE
    add constraint CONFNOTE_PHASE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE_1
    add constraint CONFNOTE_PHASE_1_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.CONFNOTE_PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE_LINKS
    add constraint CONFNOTE_PHASE_LINKS_PHASE_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.CONFNOTE_PHASE
            on delete cascade
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE
    add unique (PUBLICATION_ID, PHASE)
/

alter table ATLAS_AUTHDB.CONFNOTE_PHASE
    add constraint CONFNOTE_PHASE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.CONFNOTE_PUBLICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_DECLINED
    add constraint TALK_DECLINED_PK
        primary key (TALK_ID, MEMB_ID)
/

alter table ATLAS_AUTHDB.TALK_DECLINED
    add constraint TALK_DECLINED_TALK_FK1
        foreign key (TALK_ID) references ATLAS_AUTHDB.TALK
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_RESOLUTION
    add constraint TR_ACT_ID_FK
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.TALK_ACTIVITY
            on delete cascade
/

alter table ATLAS_AUTHDB.TALK_RESOLUTION
    add constraint TR_RESOLUTION
        check (resolution IN
               ('PRE-INVITED', 'PRE-ACCEPTED', 'INVITED', 'ACCEPTED', 'DECLINED', 'CONFIRMED', 'UNRESPONSIVE',
                'CONFIRMED_NOTIFIED', 'UNSET'))
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE_0
    add constraint ANALYSIS_SYS_PHASE_0_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
/

alter table ATLAS_AUTHDB.EGROUP_APPOINTMENT
    add constraint EGROUP_APPOINTMENT_APPOIN_FK1
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_INT
    add constraint ANALYSIS_SYS_STAT_INT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_INT
    add constraint ANALYSIS_SYS_STAT_INT_AK_1
        unique (PUBLICATION_ID, INTERPRETATION_ID)
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_INT
    add constraint ANALYSIS_SYS_STAT_INT_FK2
        foreign key (INTERPRETATION_ID) references ATLAS_AUTHDB.STAT_INTERPRETATION
/

alter table ATLAS_AUTHDB.PUBLICATION_KEYWORD
    add constraint PUBLICATION_KEYWORD_KEYWORD_FK
        foreign key (KEYWORD_ID) references ATLAS_AUTHDB.KEYWORD
/

alter table ATLAS_AUTHDB.PUBLICATION_KEYWORD
    add constraint PUBLICATION_KEYWORD_PUBL_FK
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.PUBLICATION
/

alter table ATLAS_AUTHDB.TALK_TYPE_WEIGHT
    add constraint TALK_TYPE_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_TYPE_WEIGHT
    add constraint TALK_TYPE_WEIGHT_TALK_TYPE
        foreign key (TALK_TYPE_ID) references ATLAS_AUTHDB.TALK_TYPE
/

alter table ATLAS_AUTHDB.TALK_TYPE_WEIGHT
    add constraint TALK_TYPE_WEIGHT_WEIGHT
        foreign key (WEIGHT_ID) references ATLAS_AUTHDB.WEIGHT
/

alter table ATLAS_AUTHDB.IN_CONTACT
    add constraint IN_CONTACT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.IN_CONTACT
    add constraint IN_CONTACT_U1
        unique (INSTITUTE_ID)
/

alter table ATLAS_AUTHDB.IN_CONTACT
    add constraint IN_CONTACT_FK1
        foreign key (INSTITUTE_ID) references ATLAS_AUTHDB.IN_INSTITUTE
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_WATCHER
    add constraint QU_WATCHER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_ACTION
    add constraint AT_ACTION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    add constraint AT_ACTIVITY_ACTION_AT_ACTION
        foreign key (ACTION_ID) references ATLAS_AUTHDB.AT_ACTION
/

alter table ATLAS_AUTHDB.GITLAB_PROJECT
    add constraint GITLAB_PROJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.GITLAB_PROJECT
    add constraint GITLAB_PROJECT_FK1
        foreign key (GROUP_ID) references ATLAS_AUTHDB.GITLAB_GROUP
/

alter table ATLAS_AUTHDB.CONF_WEIGHT
    add constraint CONF_WEIGHT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.APP_PROJ_PERMISSION
    add foreign key (PROJ_ID) references ATLAS_AUTHDB.PROJECT
/

alter table ATLAS_AUTHDB.APP_PROJ_PERMISSION
    add foreign key (APP_ID) references ATLAS_AUTHDB.APPOINTMENT
/

alter table ATLAS_AUTHDB.ME_MEMBER
    add constraint MEMB_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ND_NOMINATION_DRAFT
    add constraint ND_NOMINATION_DRAFT_MEMB
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORIES
    add constraint APPOINTMENT_CATEGORIES_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SYSTEM_PUBNOTE
    add constraint SYSTEM_PUBNOTE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SCABBER_NOMINEE_REASON_TEST
    add constraint SCABBER_NOMINEE_REASON_TE_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY_INACTIVE
    add constraint APP_CATEGORY_INACTIVE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PUBNOTE
    add constraint ABS_ABSTRACT_PUBNOTE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_STATE
    add constraint QU_STATE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_ADDRESS
    add constraint IN_ADDRESS_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK
    add constraint TALK_MEMB_FK
        foreign key (SPEAKER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_ANALYSIS
    add constraint ABS_ABSTRACT_ANALYSIS_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_HIERARCHY
    add constraint ACTIVITY_HIERARCHY_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_COUNTRY
    add constraint ME_VETOED_COUNTRY_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_COUNTRY
    add constraint ME_VETOED_COUNTRY_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.CONFIRMED_TALK
    add constraint CONFIRMED_TALK_FK2
        foreign key (MAIN_SPEAKER) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACADEMIC_RECORD
    add constraint ACADEMIC_RECORD_MEMB_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_FUNCTION
    add constraint ANALYSIS_SYS_FUNCTION_MEMB_FK2
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint ME_PREFERRED_CONF_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF
    add constraint ME_PREFERRED_CONF_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_TALK_TYPE
    add constraint ME_VETOED_TALK_TYPE_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_TALK_TYPE
    add constraint ME_VETOED_TALK_TYPE_MEMBER
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_AFFILIATION
    add constraint IN_AFFILIATION_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEMB_PUBLICATION
    add constraint MEMB_PUBLICATION_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEMB_CONF
    add constraint MEMB_CONF_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.LEGACY_MEMBER
    add constraint LEGACY_MEMBER_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.LEGACY_MEMBER
    add constraint LEGACY_MEMBER_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.WEIGHT
    add constraint WEIGHT_MEMB
        foreign key (REQUESTER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOMINATIONS_BACKUP
    add constraint NOMINATIONS_MEMB_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ADBEXCP
    add constraint ADBEXCP_MEMB_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.GRT_GRANT
    add constraint GRT_GRANT_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_SIGNOFF
    add constraint ANALYSIS_SYS_MEMB_SIGNOFF_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.CONFNOTE_MEMB_PUBLICATION
    add constraint CONFNOTE_MEMB_PUBLICATION_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PLOT_MEMB_CONTRIBUTION
    add constraint PLOT_MEMB_CONTRIBUTION_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP
    add constraint SYSTEM_IB_EGROUP_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_PRELIST_VOLUNTEER
    add constraint TALK_PRELIST_VOLUNTEER_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_FK2
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_RESPONSIBLE
    add constraint IN_SYSTEM_IB_RESPONSIBLE_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_TYPE
    add constraint ACTIVITY_TYPE_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.GENERAL_COUNTRY
    add constraint GENERAL_COUNTRY_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_KEYWORD
    add constraint ABS_ABSTRACT_KEYWORD_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_ADD_CONTACT
    add constraint IN_SYSTEM_IB_ADD_CONTACT_FK2
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB_ADD_CONTACT
    add constraint IN_SYSTEM_IB_ADD_CONTACT_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SECRETARY
    add constraint IN_SECRETARY_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEMB_TALK_PREFERENCES_BACKUP
    add constraint FK_TALK_PREFERENCES_MEMB_ID
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_FK1
        foreign key (CREATOR_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT
    add constraint ABS_ABSTRACT_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ADDITIONAL_INST
    add constraint ADDITIONAL_INST_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint FK_MA_MEMB
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFNOTE
    add constraint ABS_ABSTRACT_CONFNOTE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.INSTITUTE_REP
    add constraint IR_MID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_RETIREMENT
    add constraint ABS_ABSTRACT_RETIREMENT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY
    add constraint ACTIVITY_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SYSTEM_IB_CHAIR
    add constraint SYSTEM_IB_CHAIR_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_PRELIST_REGULAR
    add constraint TALK_PRELIST_REGULAR_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_FUTURE_STATUS
    add constraint QU_FUTURE_STATUS_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SYSTEM_IB_EGROUP_TYPE
    add constraint SYSTEM_IB_EGROUP_TYPE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ND_COMMENT
    add constraint ND_COMMENT_MEMB
        foreign key (SUBMITTER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PLOT_MEMB_PUBLICATION
    add constraint PLOT_MEMB_PUBLICATION_FK2
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_ACTIVITY
    add constraint TALK_ACT_ABOUT_FK
        foreign key (ABOUT) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ANALYSIS_LEADING_SUBGROUP
    add constraint ANALYSIS_LEADING_SUBGROUP_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    add constraint ACTIVITY_RESPONSIBLE_MEMBER_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_RESPONSIBLE_MEMBER
    add constraint ACTIVITY_RESPONSIBLE_MEMBER_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_DISCARDED
    add constraint NOM_DISCARDED_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_NOMINATION
    add constraint ACTIVITY_NOMINATION_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_INCLUDE_DECEASED
    add constraint IN_INCLUDE_DECEASED_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PLOT_PUBLICATION
    add constraint PLOT_PUBLICATION_FK2
        foreign key (DELETION_MEMBERID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_SELECTION_FEEDBACK
    add constraint ABS_SELECTION_FEEDBACK_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SYSTEM_IB
    add constraint SYSTEM_IB_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOMINATION_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOMINATION_NOMINEE
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_NOMINATION
    add constraint NOMINATION_SUBMITTER
        foreign key (SUBMITTER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_HIERARCHY
    add constraint IN_HIERARCHY_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_CONF_PRIORITY
    add constraint ME_VC_PRIORITY_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_CONF_PRIORITY
    add constraint ME_VC_PRIORITY_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_APPOINTMENT_NEW
    add constraint ACTIVITY_APPOINTMENT_NEW_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_AFFILIATION_TYPE
    add constraint IN_AFFILIATION_TYPE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ACTIVITY_STATUS
    add constraint ACTIVITY_STATUS_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_CONTRIBUTION
    add constraint ANALYSIS_SYS_MEMB_CONT_FK2
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.AT_PUBLICATION_LEADGROUP
    add constraint AT_PUBLICATION_LEADGROUP_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_PRELIST
    add constraint TALK_PRELIST_SLL
        foreign key (NEXT) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_PRELIST
    add constraint TALK_PRELIST_SPEAKER_FK
        foreign key (NOMINEE_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_ALLOW_PHOTO
    add constraint ME_ALLOW_PHOTO_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_ALLOW_PHOTO
    add constraint ME_ALLOW_PHOTO_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_TALK_BLACKDATE
    add constraint ME_TALK_BLACKDATE_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_TALK_BLACKDATE
    add constraint ME_TALK_BLACKDATE_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SCABBER_NOMINEE_REASON_2
    add constraint SCABBER_NOMINEE_REASON_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.TARGET_CONFERENCE
    add constraint TARGET_CONFERENCE_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ADBPARC
    add constraint ADBPARC_MEMB_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_AUTHORLIST_IDENTIFICATION
    add constraint IN_AUTHORLIST_ID_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_PUBLICATION
    add constraint ANALYSIS_SYS_MEMB_PUB_FK2
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ND_PRIORITY
    add constraint ND_PRIORITY_MEMB
        foreign key (SUBMITTER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_EMPLOYMENT
    add constraint MEMB_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APPOINTMENT_EX_OFFICIO
    add constraint APPOINTMENT_EX_OFFICIO_FK2
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint MEMBER_TALK_TOPIC_MODIFIER
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_TALK_PREFERRED_TOPIC
    add constraint ME_MEMBER_ID
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.IN_LOCATION
    add constraint IN_LOCATION_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION
    add constraint PROPOSAL_C_AUTHORLIST_PAPER_INCLUSION_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_APPLYING_FOR_A_JOB
    add constraint ME_APPLYING_FOR_A_JOB_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_APPLYING_FOR_A_JOB
    add constraint ME_APPLYING_FOR_A_JOB_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
    add constraint PROPOSAL_C_AUTHORLIST_REMOVAL_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PROPOSAL_C_AUTHORLIST_REMOVAL
    add constraint PROPOSAL_C_AUTHORLIST_REMOVAL_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.AUTHORSHIP2
    add constraint AUTHORSHIP_M_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.INSTITUTE_FOUNDATION_CODE
    add constraint INSTITUTE_FOUNDATION_CODE_AGENT_ID
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TDAQ_SUBJECT
    add constraint TDAQ_SUBJECT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.FUNDA_DOI_ACKNOWLEDGED
    add constraint FUNDA_DOI_ACKNOWLEDGED_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_UPGRADE
    add constraint NOM_UPGRADE_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.NOM_IGNORE_UPGRADE_OTP
    add constraint NOM_IGNORE_UPGRADE_OTP_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.PUBNOTE_MEMB_PUBLICATION
    add constraint PUBNOTE_MEMB_PUBLICATION_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_SYSTEM_IB
    add constraint IN_SYSTEM_IB_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEMB_APPOINTMENT
    add constraint MEMB_APPOINTMENT_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_CONFERENCE
    add constraint ABS_ABSTRACT_CONFERENCE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.SSC
    add constraint SSC_MEMB_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.AT_NOMINATION_DRAFT_STATUS
    add constraint AT_NOMINATION_DRAFT_STATUS_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.BACKUP_QUAL_OTP_TASK
    add constraint FK_MEMB_ID
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_OTP_TASK
    add constraint QU_OTP_TASK_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_PAPER
    add constraint ABS_ABSTRACT_PAPER_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.AT_ACTIVITY_ACTION
    add constraint AT_ACTIVITY_ACTION_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.INSTITUTE_NOMINATION
    add constraint INSTITUTE_NOMINATION_AGENT
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEMB_ABSENCE
    add constraint MEMB_ABSENCE_MFK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_OTHER_CONSTRAINT
    add constraint ME_OTHER_CONSTRAINT_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_OTHER_CONSTRAINT
    add constraint ME_OTHER_CONSTRAINT_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APPOINTMENT_INACTIVE
    add constraint APPOINTMENT_INACTIVE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_GRANT
    add constraint ME_GRANT_FK3
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_GRANT
    add constraint ME_GRANT_FK4
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_PREFERRED_CONF_TOPIC
    add constraint ME_PREFERRED_CONF_TOPIC_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APPOINTMENT_CATEGORY
    add constraint APP_CATEGORY_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE_HIST
    add constraint MAH_MEMB_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_INSTITUTE
    add constraint INST_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_DECLINED
    add constraint TALK_DECLINED_MEMB_FK1
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.TALK_RESOLUTION
    add constraint TR_MEMB_ID_FK
        foreign key (MEMB_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.IN_CONTACT
    add constraint IN_CONTACT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_WATCHER
    add constraint QU_WATCHER_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_WATCHER
    add constraint QU_WATCHER_FK3
        foreign key (WATCHER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.AT_ACTION
    add constraint AT_ACTION_AGENT_ID_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_MEMBER
    add constraint MEMB_CCID_UNIQUE
        unique (CERN_CCID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_EGROUP
    add constraint STA_TOPIC_EGROUP_FK1
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE
    add constraint FK_MA_AGENT
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.TALK_ACTIVITY
    add constraint TALK_ACTIVITY_AGENT_FK
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.MEETING
    add constraint MEETING_AGENT_FK
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.TOKEN
    add constraint AGENT_FK
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.MEETING_ATTENDEE_HIST
    add constraint MAH_AGENT_FK
        foreign key (AGENT) references ATLAS_AUTHDB.ME_MEMBER (CERN_CCID)
/

alter table ATLAS_AUTHDB.ME_MEMBER
    add constraint MEMB_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_ALL_PHYS_ANA_TALK_TYPE
    add constraint ME_VETOED_ALL_PHYS_ANA_TALK_TYPE_MEMB_FK
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_VETOED_ALL_PHYS_ANA_TALK_TYPE
    add constraint ME_VETOED_ALL_PHYS_ANA_TALK_TYPE_AGENT_FK
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.CONFNOTE_REVIEW_CYCLE
    add constraint CONFNOTE_REVIEW_CYCLE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.CONFNOTE_REVIEW_CYCLE
    add unique (PHASE_ID, PHASE_LOOP, REVIEW_TYPE, SEQUENCE_NUMBER)
/

alter table ATLAS_AUTHDB.CONFNOTE_REVIEW_CYCLE
    add constraint CONFNOTE_REVIEW_CYCLE_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.CONFNOTE_PHASE
/

alter table ATLAS_AUTHDB.REVIEW_CYCLE
    add constraint REVIEW_CYCLE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.REVIEW_CYCLE
    add unique (PHASE_ID, PHASE_LOOP, REVIEW_TYPE, SEQUENCE_NUMBER)
/

alter table ATLAS_AUTHDB.REVIEW_CYCLE
    add constraint REVIEW_CYCLE_FK1
        foreign key (PHASE_ID) references ATLAS_AUTHDB.PHASE
/

alter table ATLAS_AUTHDB.ACTIVITIES
    add constraint ACTIVITIES_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.DEPRECATED_ACTIVITY_APPOINTMENT
    add constraint ACTIVITY_APPOINTMENT_FK2
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITIES
/

alter table ATLAS_AUTHDB.EMPLOY_ACTIVITY
    add constraint EMPLOY_ACTIVITY_ACTIVITIE_FK1
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITIES
/

alter table ATLAS_AUTHDB.PROJECT_ACTIVITY
    add constraint ACTIVITY_ID_FK
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITIES
/

alter table ATLAS_AUTHDB.MEMB_TALK_TOPICS
    add constraint SYS_C002245064
        foreign key (TOPIC) references ATLAS_AUTHDB.ACTIVITIES
            on delete cascade
/

alter table ATLAS_AUTHDB.MEMB_TALK_OTHER_TOPICS_BACKUP
    add foreign key (OTHER_TOPIC) references ATLAS_AUTHDB.ACTIVITIES
/

alter table ATLAS_AUTHDB.ACTIVITIES
    add constraint ACTIVITIES_ACTIVITIES_FK1
        foreign key (PARENT_ID) references ATLAS_AUTHDB.ACTIVITIES
/

alter table ATLAS_AUTHDB.ACTIVITIES
    add constraint ACTIVITIES_CATEGORY_FK1
        foreign key (CATEGORY_ID) references ATLAS_AUTHDB.CATEGORY
/

alter table ATLAS_AUTHDB.ACTIVITIES
    add constraint NEW_ACTIVITY_ID
        foreign key (NEW_ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.TALK_ABSTRACT
    add constraint TALK_ABSTRACT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.TALK_ABSTRACT
    add constraint TALK_ABSTRACT_FK1
        foreign key (TALK_ID) references ATLAS_AUTHDB.TALK
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS
    add constraint ANALYSYS_SYS_PK1
        primary key (ID)
/

alter table ATLAS_AUTHDB.PUBLICATION
    add constraint PUBLICATION_FK2
        foreign key (ANALYSIS_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ABS_ABSTRACT_ANALYSIS
    add constraint ABS_ABSTRACT_ANALYSIS_FK3
        foreign key (ANALYSIS_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_FUNCTION
    add constraint ANALYSIS_SYS_FUNCTION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_LINKS
    add constraint ANALYSIS_SYS_LINKS_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT
    add constraint PUBLICATION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GITLAB_GROUP
    add constraint ANALYSIS_SYS_GITLAB_GROUP_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHYSICS_TRIGGER
    add constraint ANALYSIS_SYS_PTRIGGER_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.CONFNOTE_PUBLICATION
    add constraint CONFNOTE_PUBLICATION_FK2
        foreign key (ANALYSIS_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHYSICS_SIGNATURE
    add constraint ANALYSIS_SYS_PHYSICS_SIGN_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_TOOL
    add constraint ANALYSIS_SYS_STAT_TOOL_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_SUPP_NOTES
    add constraint ANALYSIS_SYS_SUPP_NOTES_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_KEYWORD
    add constraint ANALYSIS_SYS_KEYWORD_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_MVA
    add constraint ANALYSIS_SYS_STAT_MVA_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.PUB_NOMINATION_DRAFT_STATUS
    add constraint ND_PUBLICATION_FK
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
            on delete cascade
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_ST_PUBLICATION
    add constraint ANALYSIS_SYS_ST_PUBL_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PREVIOUS_ANALYSES
    add constraint ANALYSIS_SYS_PREVIOUS_ANA_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEMB_PUBLICATION
    add constraint ANALYSIS_SYS_MEMB_PUB_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_SUBGROUP
    add constraint ANALYSIS_SYS_SUBGROUP_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.PUBNOTE_PUBLICATION
    add constraint PUBNOTE_PUBLICATION_FK2
        foreign key (ANALYSIS_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_GROUP
    add constraint ANALYSYS_SYS_GROUP_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_COLLISION
    add constraint ANALYSIS_SYS_COLLISION_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_TIMELINE
    add constraint ANALYSIS_SYS_TIMELINE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_MEETING
    add constraint ANALYSIS_SYS_MEETING_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_PHASE
    add constraint ANALYSIS_SYS_PHASE_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS_STAT_INT
    add constraint ANALYSIS_SYS_STAT_INT_FK1
        foreign key (PUBLICATION_ID) references ATLAS_AUTHDB.ANALYSIS_SYS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS
    add constraint ANALYSIS_LEADGROUP_ID_ACTIVITY
        foreign key (LEAD_GROUP_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS
    add constraint ANALYSIS_SYS_FK1
        foreign key (LEAD_GROUP) references ATLAS_AUTHDB.LEADGROUPS
/

alter table ATLAS_AUTHDB.ANALYSIS_SYS
    add constraint ANALYSIS_SYS_FK2
        foreign key (TASK_ID) references ATLAS_PUB_TRACK.TASK (ID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_FILE
    add constraint STA_TOPIC_FILE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_FILE
    add constraint STA_TOPIC_FILE_AK_1
        unique (TOPIC_ID)
/

alter table ATLAS_AUTHDB.STA_TOPIC_FILE
    add constraint ST_TOPIC_FILE
        foreign key (TOPIC_ID) references ATLAS_AUTHDB.STA_TOPIC
/

alter table ATLAS_AUTHDB.ME_DELETED
    add constraint ME_DELETED_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_DELETED
    add constraint ME_DELETED_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_DELETED
    add constraint ME_DELETED_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_DELETED
    add constraint ME_DELETED_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_STATUS
    add constraint ME_STATUS_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_MEMBER
    add constraint MEMB_STATUS_FK
        foreign key (STATUS_ID) references ATLAS_AUTHDB.ME_STATUS
/

alter table ATLAS_AUTHDB.ME_STATUS
    add constraint ME_STATUS_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_HIDE_ONLEAVE_DATE
    add constraint ME_HIDE_ONLEAVE_DATE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_HIDE_ONLEAVE_DATE
    add constraint ME_HIDE_ONLEAVE_DATE_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_HIDE_ONLEAVE_DATE
    add constraint ME_HIDE_ONLEAVE_DATE_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_HIDE_ONLEAVE_DATE
    add constraint ME_HIDE_ONLEAVE_DATE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_HR_IGNORE
    add constraint ME_HR_IGNORE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_HR_IGNORE
    add constraint ME_HR_IGNORE_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_HR_IGNORE
    add constraint ME_HR_IGNORE_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_HR_IGNORE
    add constraint ME_HR_IGNORE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_PRE_DATA_TAKEN
    add constraint ME_PRE_DATA_TAKEN_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_PRE_DATA_TAKEN
    add constraint ME_PRE_DATA_TAKEN_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_PRE_DATA_TAKEN
    add constraint ME_PRE_DATA_TAKEN_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_PRE_DATA_TAKEN
    add constraint ME_PRE_DATA_TAKEN_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_QUALIFICATION
    add constraint QU_QUALIFICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_OTP_TASK
    add constraint QU_OTP_TASK_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_WATCHER
    add constraint QU_WATCHER_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_QUALIFICATION
    add constraint QU_QUALIFICATION_FK1
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_QUALIFICATION
    add constraint QU_QUALIFICATION_FK2
        foreign key (STATE_ID) references ATLAS_AUTHDB.QU_STATE
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_U2
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_FK2
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.ME_QUALIFICATION
    add constraint ME_QUALIFICATION_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_SIGNING_ONLY
    add constraint QU_SIGNING_ONLY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_SIGNING_ONLY
    add constraint QU_SIGNING_ONLY_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_SIGNING_ONLY
    add constraint QU_SIGNING_ONLY_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_SIGNING_ONLY
    add constraint QU_SIGNING_ONLY_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_CHECKPOINT
    add constraint QU_CHECKPOINT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_CHECKPOINT
    add constraint QU_CHECKPOINT_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_CHECKPOINT
    add constraint QU_CHECKPOINT_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_CHECKPOINT
    add constraint QU_CHECKPOINT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_PROJECT
    add constraint QU_PROJECT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_PROJECT
    add constraint QU_PROJECT_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_PROJECT
    add constraint QU_PROJECT_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_PROJECT
    add constraint QU_PROJECT_FK2
        foreign key (PROJECT_ID) references ATLAS_AUTHDB.PROJECT
/

alter table ATLAS_AUTHDB.QU_PROJECT
    add constraint QU_PROJECT_FK3
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_FINAL_REPORT
    add constraint QU_FINAL_REPORT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_FINAL_REPORT
    add constraint QU_FINAL_REPORT_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_FINAL_REPORT
    add constraint QU_FINAL_REPORT_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_FINAL_REPORT
    add constraint QU_FINAL_REPORT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_EXCLUDE_PRE_DATA_TAKEN
    add constraint QU_EXCLUDE_PDT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_EXCLUDE_PRE_DATA_TAKEN
    add constraint QU_EXCLUDE_PDT_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_EXCLUDE_PRE_DATA_TAKEN
    add constraint QU_EXCLUDE_PDT_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_EXCLUDE_PRE_DATA_TAKEN
    add constraint QU_EXCLUDE_PDT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    add constraint QU_TECHNICAL_SUPERVISOR_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    add constraint QU_TECHNICAL_SUPERVISOR_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    add constraint QU_TECHNICAL_SUPERVISOR_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    add constraint QU_TECHNICAL_SUPERVISOR_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_TECHNICAL_SUPERVISOR
    add constraint QU_TECHNICAL_SUPERVISOR_FK3
        foreign key (TECHNICAL_SUPERVISOR_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    add constraint QU_LOCAL_SUPERVISOR_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    add constraint QU_LOCAL_SUPERVISOR_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    add constraint QU_LOCAL_SUPERVISOR_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    add constraint QU_LOCAL_SUPERVISOR_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_LOCAL_SUPERVISOR
    add constraint QU_LOCAL_SUPERVISOR_FK3
        foreign key (LOCAL_SUPERVISOR_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_QUAL_FLAG
    add constraint QU_QUAL_FLAG_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_QUAL_FLAG
    add constraint QU_QUAL_FLAG_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_QUAL_FLAG
    add constraint QU_QUAL_FLAG_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_QUAL_FLAG
    add constraint QU_QUAL_FLAG_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_ACTIVITY
    add constraint QU_ACTIVITY_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_ACTIVITY
    add constraint QU_ACTIVITY_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_ACTIVITY
    add constraint QU_ACTIVITY_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_ACTIVITY
    add constraint QU_ACTIVITY_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_ACTIVITY
    add constraint QU_ACTIVITY_FK3
        foreign key (ACTIVITY_ID) references ATLAS_AUTHDB.ACTIVITY
/

alter table ATLAS_AUTHDB.QU_FUTURE
    add constraint QU_FUTURE_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.QU_FUTURE
    add constraint QU_FUTURE_U1
        unique (QUALIFICATION_ID)
/

alter table ATLAS_AUTHDB.QU_FUTURE
    add constraint QU_FUTURE_FK1
        foreign key (QUALIFICATION_ID) references ATLAS_AUTHDB.QU_QUALIFICATION
            on delete cascade
/

alter table ATLAS_AUTHDB.QU_FUTURE
    add constraint QU_FUTURE_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.QU_FUTURE
    add constraint QU_FUTURE_FK3
        foreign key (FUTURE_STATUS_ID) references ATLAS_AUTHDB.QU_FUTURE_STATUS
/

alter table ATLAS_AUTHDB.ME_DECEASED_MEMBER
    add constraint ME_DECEASED_MEMBER_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_DECEASED_MEMBER
    add constraint ME_DECEASED_MEMBER_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_DECEASED_MEMBER
    add constraint ME_DECEASED_MEMBER_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_DECEASED_MEMBER
    add constraint ME_DECEASED_MEMBER_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_ALLOW_PUBLIC_APPOINTMENT
    add constraint ME_ALLOW_PUBLIC_APPOINTMENT_PK
        primary key (ID)
/

alter table ATLAS_AUTHDB.ME_ALLOW_PUBLIC_APPOINTMENT
    add constraint ME_ALLOW_PUBLIC_APPOINTMENT_U1
        unique (MEMBER_ID)
/

alter table ATLAS_AUTHDB.ME_ALLOW_PUBLIC_APPOINTMENT
    add constraint ME_ALLOW_PUBLIC_APPOINTMENT_FK1
        foreign key (MEMBER_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.ME_ALLOW_PUBLIC_APPOINTMENT
    add constraint ME_ALLOW_PUBLIC_APPOINTMENT_FK2
        foreign key (AGENT_ID) references ATLAS_AUTHDB.ME_MEMBER
/

alter table ATLAS_AUTHDB.APP_OTP_PERIOD
    add constraint APP_OTP_FK
        foreign key (APPOINTMENT_ID) references ATLAS_AUTHDB.APPOINTMENT
/

--rollback DECLARE
--rollback BEGIN
--rollback    FOR CONSTRAINTS IN (
--rollback        SELECT CONSTRAINT_NAME AS NAME, TABLE_NAME
--rollback        FROM USER_CONSTRAINTS
--rollback        WHERE CONSTRAINT_TYPE IN ('P', 'R', 'U', 'C')
--rollback        AND TABLE_NAME != 'DATABASECHANGELOG'
--rollback        AND TABLE_NAME != 'DATABASECHANGELOGLOCK'
--rollback        AND OWNER = 'ATLAS_AUTHDB'
--rollback        AND GENERATED = 'USER NAME'
--rollback        AND CONSTRAINT_NAME NOT LIKE 'BIN%'
--rollback    )
--rollback    LOOP
--rollback       EXECUTE IMMEDIATE 'ALTER TABLE ATLAS_AUTHDB.' || CONSTRAINTS.TABLE_NAME || ' DROP CONSTRAINT ' || CONSTRAINTS.NAME;
--rollback    END LOOP;
--rollback END;
--rollback /