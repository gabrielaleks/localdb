--liquibase formatted sql
--changeset Gabriel:31 endDelimiter:/ rollbackEndDelimiter:/

-- This is a necessary step after the MV logs for this MV were created
ALTER MATERIALIZED VIEW ATLAS_AUTHDB.MEMBER_INFORMATION
REFRESH FAST ON COMMIT
/

--rollback ALTER MATERIALIZED VIEW ATLAS_AUTHDB.MEMBER_INFORMATION
--rollback REFRESH COMPLETE
--rollback /