--liquibase formatted sql
--changeset Gabriel:22 endDelimiter:/ rollbackEndDelimiter:/

ALTER TABLE GLANCE_PHOTO_SERVICE.ALLOW_PHOTO
	ADD CONSTRAINT ALLOW_PHOTO_PK
		PRIMARY KEY (ID)
/

--rollback ALTER TABLE GLANCE_PHOTO_SERVICE.ALLOW_PHOTO DROP CONSTRAINT ALLOW_PHOTO_PK
--rollback /