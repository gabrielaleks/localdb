--liquibase formatted sql
--changeset Gabriel:23 endDelimiter:/ rollbackEndDelimiter:/

CREATE OR REPLACE TRIGGER GLANCE_PHOTO_SERVICE.HTR_ALLOW_PHOTO
AFTER INSERT OR UPDATE OR DELETE ON GLANCE_PHOTO_SERVICE.ALLOW_PHOTO
FOR EACH ROW
BEGIN
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.ID), 'NULL') THEN
      INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
          ID,
          APPLICATION_ID,
          PERSON_ID,
          CONSENT,
          MODIFIED_ON,
          AGENT_PERSON_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.APPLICATION_ID,
          :NEW.PERSON_ID,
          :NEW.CONSENT,
          SYSTIMESTAMP,
          :NEW.AGENT_PERSON_ID,
          'U',
          'ID',
          TO_CHAR(:OLD.ID),
          TO_CHAR(:NEW.ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.APPLICATION_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.APPLICATION_ID), 'NULL') THEN
      INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
          ID,
          APPLICATION_ID,
          PERSON_ID,
          CONSENT,
          MODIFIED_ON,
          AGENT_PERSON_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.APPLICATION_ID,
          :NEW.PERSON_ID,
          :NEW.CONSENT,
          SYSTIMESTAMP,
          :NEW.AGENT_PERSON_ID,
          'U',
          'APPLICATION_ID',
          TO_CHAR(:OLD.APPLICATION_ID),
          TO_CHAR(:NEW.APPLICATION_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.PERSON_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.PERSON_ID), 'NULL') THEN
      INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
          ID,
          APPLICATION_ID,
          PERSON_ID,
          CONSENT,
          MODIFIED_ON,
          AGENT_PERSON_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.APPLICATION_ID,
          :NEW.PERSON_ID,
          :NEW.CONSENT,
          SYSTIMESTAMP,
          :NEW.AGENT_PERSON_ID,
          'U',
          'PERSON_ID',
          TO_CHAR(:OLD.PERSON_ID),
          TO_CHAR(:NEW.PERSON_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.CONSENT), 'NULL') <> COALESCE(TO_CHAR(:NEW.CONSENT), 'NULL') THEN
      INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
          ID,
          APPLICATION_ID,
          PERSON_ID,
          CONSENT,
          MODIFIED_ON,
          AGENT_PERSON_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.APPLICATION_ID,
          :NEW.PERSON_ID,
          :NEW.CONSENT,
          SYSTIMESTAMP,
          :NEW.AGENT_PERSON_ID,
          'U',
          'CONSENT',
          TO_CHAR(:OLD.CONSENT),
          TO_CHAR(:NEW.CONSENT)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.AGENT_PERSON_ID), 'NULL') <> COALESCE(TO_CHAR(:NEW.AGENT_PERSON_ID), 'NULL') THEN
      INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
          ID,
          APPLICATION_ID,
          PERSON_ID,
          CONSENT,
          MODIFIED_ON,
          AGENT_PERSON_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.APPLICATION_ID,
          :NEW.PERSON_ID,
          :NEW.CONSENT,
          SYSTIMESTAMP,
          :NEW.AGENT_PERSON_ID,
          'U',
          'AGENT_PERSON_ID',
          TO_CHAR(:OLD.AGENT_PERSON_ID),
          TO_CHAR(:NEW.AGENT_PERSON_ID)
      );
    END IF;

  END IF;
 
  IF DELETING THEN
    INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
      ID,
      APPLICATION_ID,
      PERSON_ID,
      CONSENT,
      MODIFIED_ON,
      AGENT_PERSON_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.APPLICATION_ID,
      :OLD.PERSON_ID,
      :OLD.CONSENT,
      SYSTIMESTAMP,
      :OLD.AGENT_PERSON_ID,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;
 
  IF INSERTING THEN
    INSERT INTO GLANCE_PHOTO_HIST.ALLOW_PHOTO (
      ID,
      APPLICATION_ID,
      PERSON_ID,
      CONSENT,
      MODIFIED_ON,
      AGENT_PERSON_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.APPLICATION_ID,
      :NEW.PERSON_ID,
      :NEW.CONSENT,
      SYSTIMESTAMP,
      :NEW.AGENT_PERSON_ID,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

--rollback DROP TRIGGER GLANCE_PHOTO_SERVICE.HTR_ALLOW_PHOTO
--rollback /