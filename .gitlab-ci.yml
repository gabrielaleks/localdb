stages:
  - build
  - test
  - deploy

variables:
  PHP_IMAGE: gitlab-registry.cern.ch/fence/atlas/php-7.3.27
  PHP_LIQUIBASE_IMAGE: gitlab-registry.cern.ch/gabrielaleks/localdb/php-liquibase
  ORACLE_IMAGE: gitlab-registry.cern.ch/gabrielaleks/localdb/oracle
  ORACLE_DB_PASSWORD: ephemeral-db-password

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - membership/api/vendor/

build:
  stage: build
  image: $PHP_IMAGE
  script:
    - composer config -g http-basic.gitlab.cern.ch $GIT_USERNAME $GIT_PASSWORD
    - cd membership/api
    - composer install  --no-interaction --prefer-dist

unit-test:
  stage: test
  image: $PHP_IMAGE
  script:
    - cd membership/api
    - composer test-unit

integration-test:
  stage: test
  image: $PHP_LIQUIBASE_IMAGE
  variables:
    # Use the variable below to debug the oracle-db service
    # CI_DEBUG_SERVICES: "true"
    ORACLE_URL: jdbc:oracle:thin:@oracle-db:1521:FREE
    ORACLE_DB_SYSTEM_USER: SYSTEM
    ORACLE_DB_ATLAS_AUTHDB_USER: ATLAS_AUTHDB
  services:
    - name: $ORACLE_IMAGE
      alias: oracle-db
      variables:
        ORACLE_PWD: "$ORACLE_DB_PASSWORD"
        ORACLE_CHARACTERSET: WE8ISO8859P9
  script:
    - sleep 10 # alternative ?
    # Create users
    - liquibase --changeLogFile=./migrations/system_changelog.xml --url=${ORACLE_URL} --username=${ORACLE_DB_SYSTEM_USER} --password=${ORACLE_DB_PASSWORD} updateToTag --tag=version_001
    # Create baseline for schemas
    - liquibase --changeLogFile=./migrations/atlas_authdb_changelog.xml --url=${ORACLE_URL} --username=${ORACLE_DB_ATLAS_AUTHDB_USER} --password=${ORACLE_DB_PASSWORD} updateToTag --tag=version_001_create_baseline
    # Grant references
    - liquibase --changeLogFile=./migrations/system_changelog.xml --url=${ORACLE_URL} --username=${ORACLE_DB_SYSTEM_USER} --password=${ORACLE_DB_PASSWORD} updateToTag --tag=version_002
    # Create constraints
    - liquibase --changeLogFile=./migrations/atlas_authdb_changelog.xml --url=${ORACLE_URL} --username=${ORACLE_DB_ATLAS_AUTHDB_USER} --password=${ORACLE_DB_PASSWORD} updateToTag --tag=version_002_create_constraints
    # Insert data
    - liquibase --changeLogFile=./migrations/atlas_authdb_changelog.xml --url=${ORACLE_URL} --username=${ORACLE_DB_ATLAS_AUTHDB_USER} --password=${ORACLE_DB_PASSWORD} updateToTag --tag=version_003_insert_base_member_data
    # Test
    - cd membership/api
    - composer test-integration

use-case-test:
  stage: test
  image: $PHP_IMAGE
  script:
    - cd membership/api
    - composer test-use-case

deploy-int:
  stage: deploy
  image: liquibase/liquibase:4.17
  only:
    - stage
  script:
    - echo 'Deploying to integration environment!'

deploy-prod:
  stage: deploy
  image: liquibase/liquibase:4.17
  only:
    - master
  script:
    - echo 'Deploying to production environment!'